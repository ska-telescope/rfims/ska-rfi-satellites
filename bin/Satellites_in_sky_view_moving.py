# -*- coding: utf-8 -*-
"""
Script based on original by Marta Bautista and Benjamin Winkel

Calculates satellites positions continuously and updates the sky plot in a 
defined observer location. 

It also includes a tooltip feature to select a satellite and see its name and
more parameters.

Satellites TLEs are downloaded from CELESTRAK at the begining of the script
and then propagated using cysgp4.

@author: M. Bautista, B. Winkel, F. Di Vruno


"""

import requests
import datetime
import astropy.time as time
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import animation, rc
from matplotlib import colors

import cysgp4


#%% Get the TLEs from Celestrak
#Active satellites from celestrak
#celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/active.txt')

#OneWeb satellites from celestrak
celestrak_oneweb = requests.get('https://celestrak.com/NORAD/elements/oneweb.txt')

#Starlink satellites from celestrak
celestrak_starlink = requests.get('https://celestrak.com/NORAD/elements/starlink.txt')


#all the active satellites in the sky
celestrak_active = requests.get('https://celestrak.com/NORAD/elements/active.txt')

#Geostationary satellites from celestrack
celestrak_geo = requests.get('https://celestrak.com/NORAD/elements/geo.txt')


T = time.Time.now().iso
np.savez('TLEs/' + T[0:10]+'-%s'%T[11:13]+'-%s'%T[14:16]+'-%s'%T[17:19] ,
         starlink = celestrak_starlink,
         oneweb = celestrak_oneweb,
         geo = celestrak_geo,
         active = celestrak_active)


starlink_tles = np.array(cysgp4.tles_from_text(celestrak_starlink.text))
oneweb_tles = np.array(cysgp4.tles_from_text(celestrak_oneweb.text))
geo_tles = np.array(cysgp4.tles_from_text(celestrak_geo.text))
active_tles = np.array(cysgp4.tles_from_text(celestrak_active.text))



##remove starlink and oneweb from the list of active satellites
satname = 'STARLINK'
aux = []
for k in range(len(active_tles)):
    if satname in active_tles[k].__repr__():
        aux.append(k)
active_tles = np.delete(active_tles, aux)

satname = 'ONEWEB'
aux = []
for k in range(len(active_tles)):
    if satname in active_tles[k].__repr__():
        aux.append(k)
active_tles = np.delete(active_tles, aux)

     
     
    
my_time = cysgp4.PyDateTime(datetime.datetime.utcnow())
mjd_epoch = my_time.mjd  # get current time

# sat_tles = oneweb_tles
sat_tles = starlink_tles


#%% define the observer location:

#YEBES:
# observer = cysgp4.PyObserver(-3.08853, 40.523475, 0.915)
# obs_name = 'Yebes 13.2m'

#JBO:
# observer = cysgp4.PyObserver(-2.306790, 53.235035 , 0.1) 
# obs_name = 'Jodrell Bank'

# #Tenerife:
# observer = cysgp4.PyObserver( -16 , 28 , 0.1) 
# obs_name = 'GroundBird'


#SKA-MID:
observer = cysgp4.PyObserver(21.4,-30.7,1) 
obs_name = 'MeerKAT'

# #Effelsberg:
# observer = cysgp4.PyObserver(5,56,1) 
# obs_name = 'Effelsberg'

# ONSALA:
# obs_name = 'Onsala'
# observer = cysgp4.PyObserver( 11.917778,57.393056, 0.02) # long,lat,alt, [deg, deg, km]



# pointing direction of the observer antenna
obs_el = 30
obs_az = 93
obs_diam = 1 #m this is the observer antenna diameter
obs_freq = 11e9 #Hz
obs_BW = 3e8/obs_freq/obs_diam*180/np.pi/0.8 #half power BW
obs_pointing = np.array([obs_az, obs_el]) #az, el in deg

#annotate the bw direction with a circle of the size of the beam
l = np.linspace(0,np.pi*2)
x_b = obs_BW/2*np.cos(l)
y_b = obs_BW/2*np.sin(l)
x_p = x_b + obs_pointing[0]
y_p = y_b + obs_pointing[1]


#%% Generate the figure to plot the satellites

vmin, vmax = np.log10(100), np.log10(100000)

# fig, axs = plt.subplots(1,2,figsize=(16,12),
#                         gridspec_kw={'width_ratios': [1, 0.1]},
#                         sharey=False)
# ax = axs[0]
# cax = axs[1]


fig = plt.figure(figsize=(15, 7), dpi=100)
ax = fig.add_axes((0.1, 0.1, 0.8, 0.8)) #main axes
cax = fig.add_axes((0.91, 0.3, 0.05, 0.4)) #colorbar axes
ax.set_xlabel('Azimuth [deg]')
ax.set_ylabel('Elevation [deg]')
ax.set_xlim((-180, 180))
ax.set_ylim((0, 90))
ax.set_xticks(range(-150, 180, 30))
ax.set_yticks(range(0, 91, 15))
ax.plot(x_p,y_p,'r',linewidth=2)
# ax.set_aspect('equal')
ax.set_aspect(1)
ax.grid()
ax.text(-170, 75, obs_name, fontsize=16)


annot = ax.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                    bbox=dict(boxstyle="round", fc="w"),
                    arrowprops=dict(arrowstyle="->"))
annot.set_visible(False)

#%% Plot the GEO orbit

def plot_static_sats(tles,
                     time=[],
                     fig=[],
                     vmin = np.log10(100),
                     vmax = np.log10(100000)):
    '''
    

    Parameters
    ----------
    tles : TYPE
        has to be a PyTle object from sgp4
    time : TYPE, optional
        has to be a cysgp4.PyDateTime() object
        
    fig : TYPE, optional
        DESCRIPTION. The default is [].

    Returns
    -------
    fig : TYPE
        DESCRIPTION.

    '''
    
    if fig ==[]:

        
        
        fig = plt.figure(figsize=(15, 7), dpi=100)
        ax = fig.add_axes((0.1, 0.1, 0.8, 0.8)) #main axes
        cax = fig.add_axes((0.91, 0.2, 0.02, 0.5)) #colorbar axes
        ax.set_xlabel('Azimuth [deg]')
        ax.set_ylabel('Elevation [deg]')
        ax.set_xlim((-180, 180))
        ax.set_ylim((0, 90))
        ax.set_xticks(range(-150, 180, 30))
        ax.set_yticks(range(0, 91, 30))
        ax.plot(x_p,y_p,'r',linewidth=2)
        ax.set_aspect('equal')
        ax.grid()
        ax.text(-170, 75, obs_name, fontsize=16)
        
        
        annot = ax.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                            bbox=dict(boxstyle="round", fc="w"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)
        
    else:
        ax = fig.axes[0]
        cax = fig.axes[1]
    if time == []:
        my_time = cysgp4.PyDateTime(datetime.datetime.utcnow())
    else :
        my_time = time
    start_mjd = my_time.mjd  # get current time
    trama = 1 #number of moments
    time_sats = 1 #number of seconds 
    td = np.arange(0, trama, time_sats) / 86400.  # 2000 s in steps of 1s
    mjds = start_mjd - td
    
    result = cysgp4.propagate_many(
        mjds[:,np.newaxis],
        tles[np.newaxis,:],
        observer,
        do_sat_azel=False,
        on_error = 'coerce_to_nan', #dismiss error due to too old TLEs
        )
    
    topo_pos = result['topo']
    
    # print(topo_pos.shape)
    topo_pos_az, topo_pos_el, topo_pos_dist, _ = (topo_pos[..., j] for j in range(4))
    topo_pos_az = (topo_pos_az + 180.) % 360. - 180.    
    
    vis_mask = topo_pos_el>0
    
    topo_pos_az = topo_pos_az[vis_mask]
    topo_pos_el = topo_pos_el[vis_mask]
    topo_pos_dist = topo_pos_dist[vis_mask]
    
    points = ax.scatter(
        topo_pos_az[:], topo_pos_el[:],
        # c='green',#np.log10(topo_pos_dist[:]),
        vmin=vmin, vmax = vmax,
        s=2
        )
    return fig

#plot GEO satellites
plot_static_sats(geo_tles, fig = fig)

#plot all the satellites
# plot_static_sats(active_tles, fig = fig)

#%% Calculate the starlink positions for now and 30min later

plot_tracks = 1
if plot_tracks:
    start_mjd = mjd_epoch
    time_sats = 5 #number of seconds 
    trama = 60*3 #number of moments
    td = np.arange(0, trama, time_sats) / 86400.  # 2000 s in steps of 1s
    mjds = start_mjd + td
    
    result = cysgp4.propagate_many(
        mjds[:,np.newaxis],
        sat_tles[np.newaxis,:],
        observer,
        do_sat_azel=False,
        on_error = 'coerce_to_nan',
        )
    
    topo_pos = result['topo']
    
    # print(topo_pos.shape)
    topo_pos_az, topo_pos_el, topo_pos_dist, _ = (topo_pos[..., j] for j in range(4))
    topo_pos_az = (topo_pos_az + 180.) % 360. - 180.    
    
    
    
    for i in range(topo_pos.shape[1]):
        vis_mask = topo_pos[:,i,1]>0 #mask for elevation >0
        if sum(vis_mask)>0:
            topo_pos_az1 = topo_pos_az[vis_mask,i]
            topo_pos_el1 = topo_pos_el[vis_mask,i]
            topo_pos_dist1 = topo_pos_dist[vis_mask,i]
        
            lines = ax.plot(
                topo_pos_az1, topo_pos_el1,'.',
                color='black',
                markersize =1)
            

    
    
#%% plot the SL satellites as they move

pos = None

sat_indx = None

# sat_tles = starlink_tles
# sat_tles = oneweb_tles 
# sat_tles = active_tles 
 
def animate(i):
    global pos
    global sat_indx
    my_time = cysgp4.PyDateTime(datetime.datetime.utcnow())
    start_mjd = my_time.mjd  # get current time
    trama = 2 #number of moments
    time_sats = 2 #number of seconds 
    td = np.arange(0, trama, time_sats) / 86400.  # 2000 s in steps of 1s
    mjds = start_mjd - td

    result = cysgp4.propagate_many(
        mjds[:,np.newaxis],
        sat_tles[np.newaxis,:],
        observer,
        do_sat_azel=False,
        on_error = 'coerce_to_nan',
        )
    
    topo_pos = result['topo']
    
    
    topo_pos_az, topo_pos_el, topo_pos_dist, _ = (topo_pos[..., j] for j in range(4))
    topo_pos_az = (topo_pos_az + 180.) % 360. - 180.
    
    topo_pos_az2 = np.reshape(topo_pos_az, (topo_pos_az.shape[0]*topo_pos_az.shape[1]))
    topo_pos_el2 = np.reshape(topo_pos_el, (topo_pos_el.shape[0]*topo_pos_el.shape[1]))
    topo_pos_dist2 = np.reshape(topo_pos_dist, (topo_pos_dist.shape[0]*topo_pos_dist.shape[1]))
    
    N = topo_pos_az.shape[1]
    
    cm = plt.cm.get_cmap('viridis')
    my_cmap = cm(plt.Normalize(np.log10(topo_pos_dist2[:]).min(), np.log10(topo_pos_dist2[:]).max())(np.log10(topo_pos_dist2[:])))
    alphas = []
    auxi = np.linspace(0.8, 0.001, int(trama/time_sats))
    for ii in range(len(auxi)):
        alphas = np.append(alphas,np.tile(auxi[ii],N))
    my_cmap[:, -1] = alphas  
        
    points = ax.scatter(
        topo_pos_az2[:], topo_pos_el2[:],
        c = my_cmap,
        s = 20,
        vmin=vmin, vmax = vmax, #
        )

    
    

    
    # print(pos)
    if (pos is not None):
        # print ("Hello")
        annot.xy = (topo_pos_az2[sat_indx[0]], topo_pos_el2[sat_indx[0]])

        
        
#        text2 = "SAT:{}\n AZ:{}\n EL:{}\n DIST:{} km ".format(" ".join([sat_tles[sat_indx[0]].__repr__()[8:21]]),
#                               " ".([str(topo_pos_az2[sat_indx[0]])[0:5]]),
#                               " ".join([str(topo_pos_el2[sat_indx[0]])[0:5]]),
#                               " ".join([str(topo_pos_dist2[sat_indx[0]])[0:5] ]))
        text2 = "SAT:{}\n AZ:{:.4f}\n EL:{:.4f}\n DIST:{:.4f} km ".format(sat_tles[sat_indx[0]].__repr__()[8:21], topo_pos_az2[sat_indx[0]], topo_pos_el2[sat_indx[0]], topo_pos_dist2[sat_indx[0]])

        #print(text2)
        annot.set_text(text2)
        # annot.get_bbox_patch().set_facecolor(cmap(norm(c[ind["ind"][0]])))
        annot.get_bbox_patch().set_alpha(0.4)

    def update_annot(ind):
        N = topo_pos_az.shape[1]
        global sat_indx
        sat_indx = np.unique(ind["ind"] % N)
        
        global pos
        pos = points.get_offsets()[sat_indx[0]]
        
        
        
        
        
        

    def onclick(event):
        vis = annot.get_visible()
        if event.inaxes == ax:
            cont, ind = points.contains(event)
            
            if cont:
                update_annot(ind)
                annot.set_visible(True)
                fig.canvas.draw_idle()
            else:
                if vis:
                    annot.set_visible(True)
                    fig.canvas.draw_idle()
                  


    

     
    

    points.set_offsets(np.column_stack([topo_pos_az2, topo_pos_el2]))
    #points.set_array(np.reshape(np.log10(topo_pos_dist2),topo_pos_dist.shape[0]*topo_pos_dist.shape[1]))
    
    

    
    title = ax.text(
        174, 75, '{:%y/%m/%d %H:%M:%S}'.format(my_time.datetime),
        fontsize=15, ha='right'
        )

    title.set_text('{:%y/%m/%d %H:%M:%S}'.format(my_time.datetime))
    fig.canvas.mpl_connect('button_press_event',onclick)
    fig.canvas.draw()
    

    return points, title


anim2 = animation.FuncAnimation(
    fig, animate, frames = 200,interval=1000, blit=True, cache_frame_data=False
    )


plt.show()
