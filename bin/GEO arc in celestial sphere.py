# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 23:51:27 2022

Calculate the projection of the GEO orbit onto the Celestial sphere
similar calculation to the one used in the ITU-R RA.517
 
@author: f.divruno
"""
import numpy as np
import matplotlib.pyplot as plt
import astropy.units as u
import astropy
from astropy.coordinates import SkyCoord, FK4, AltAz,EarthLocation, ICRS


Re = 6371*u.km
Rs = 6.6*Re 

#%% in 3d
# obsserver location:
lon_o = 0*u.deg 
# lat_o = np.linspace(-60,60,6) *u.deg
lat_o = np.array((-32,-22,18,34,50,57)) *u.deg
names = ['Parkes','Itapetinga','Arecibo','VLA','Effelsberg','Onsala']
h_o = Re

Xo = h_o*np.cos(lat_o)*np.cos(lon_o)
Yo = h_o*np.cos(lat_o)*np.sin(lon_o)
Zo = h_o*np.sin(lat_o)
Ro = np.array([Xo,Yo,Zo])

#Satellites location in the GEO arc:
lon_s = np.linspace(-180,180,360)*u.deg
lat_s = np.zeros(360)*u.deg
h_s = Rs

Xs = h_s*np.cos(lat_s)*np.cos(lon_s)
Ys = h_s*np.cos(lat_s)*np.sin(lon_s)
Zs = h_s*np.sin(lat_s)
R1 = np.array([Xs,Ys,Zs])


#vector obs_position to satellite position
V = R1[...,None] - Ro[:,None,:] 

#proyection of V into the XY plane, pointing direction towards the celestial equator
Vp = np.copy(V)
Vp[2] = 0

#Angle between V and Vp
alpha = np.arccos(np.einsum('i...,i...',V,Vp)\
                /np.linalg.norm(V,axis=0)\
                /np.linalg.norm(Vp,axis=0))*u.rad    

sig = -np.sign(lat_o)
alpha *= sig

#plot
plt.figure(figsize=[20,15])
plt.plot(lon_s/15,alpha.to(u.deg))
plt.grid()
plt.xlim([-6,6])
plt.ylim([-9,6])
plt.legend(names)
plt.xlabel('hour angle (h)')
plt.ylabel('Declination angle (deg)')
plt.savefig('figs/ITU-R RA.517 GEO sats on celestial sphere.png')


#%% delta of X degrees on each projection
# See the effect of an avoidance angle to the satellites in the GEO arc. The value of avoidance depends on the emission level of the satellites and the Gain of the RAS antenna. According to the model SA.509 the 0dBi gein is within 19deg from boresight. At 5 deg from boresight the gain in the model SA.509 is about 15dBi so this is the extra attenuation for GEO satellites defined in RA.769.


# 19 degrees avoidance
avoidance = 19 *u.deg
d = (np.random.rand(1000)-0.5)*2*avoidance
alpha1 = alpha[...,None] + d[None,None]

c = ['r','b','g','c','y','k']
# fig = plt.figure(figsize=[20,15])
fig,axes = plt.subplots(1,6,sharey=True,figsize=[20,15])
plt.suptitle('Projection of the GEO arc in the celestial sphere with %s avoidance'%avoidance)

for i in range(6):
    axes[i].plot(lon_s/15,alpha[:,i].to(u.deg),'--',color=c[i],label=names[i])
    axes[i].plot(lon_s/15,alpha1[:,i,:].to(u.deg),'.',alpha=0.02,color=c[i])
    axes[i].set_xlim([-2,2])
    # axes[i].set_ylim([-14,14])
    axes[i].set_ylim([-30,30])
    axes[i].grid()
    axes[i].set_xlabel(names[i])
plt.savefig('figs/GEO sats on celestial sphere avoid 19 deg.png')


# 5 degrees avoidance
avoidance = 5*u.deg
d = (np.random.rand(1000)-0.5)*2*avoidance
alpha1 = alpha[...,None] + d[None,None]

c = ['r','b','g','c','y','k']
# fig = plt.figure(figsize=[20,15])
fig,axes = plt.subplots(1,6,sharey=True,figsize=[20,15])
plt.suptitle('Projection of the GEO arc in the celestial sphere with %s avoidance'%avoidance)

for i in range(6):
    axes[i].plot(lon_s/15,alpha[:,i].to(u.deg),'--',color=c[i],label=names[i])
    axes[i].plot(lon_s/15,alpha1[:,i,:].to(u.deg),'.',alpha=0.02,color=c[i])
    axes[i].set_xlim([-2,2])
    # axes[i].set_ylim([-14,14])
    axes[i].set_ylim([-30,30])
    axes[i].grid()
    axes[i].set_xlabel(names[i])
plt.savefig('figs/GEO sats on celestial sphere avoid 5 deg.png')


