# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 02:10:11 2020


@author: f.divruno
@author: Braam Otto

Last updated: 29 March 2021
"""

import astropy.units as u
import astropy.constants as const
from astropy import time as timeA
import numpy as np
import time
import matplotlib.pyplot as plt
import sys
import pycraf.protection as protection
import pycraf

sys.path += ['../src']

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'
if os.name == 'nt':
    os.system('cls')
else:
    os.system('clear')

import ska
from ska import  constellation, telescope

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 12}
import matplotlib 
matplotlib.rc('font', **font)


#%% Simulation parameters

#observer
observer_name = 'SKA-MID'
observer_geo = np.array((21.443800, -30.712919, 1.052)) # deg, deg, km
observer_diam = 15*u.m


#observer
# observer_name = 'Tenerife'
# observer_geo = np.array( (-16.51 , 28.3 , 0.1)) # deg, deg, km
# observer_diam = 2.25*u.m

#JBO:
# observer_name = 'Home'
# observer_geo = np.array((-2.205, 53.15 , 0.1))
# observer_diam = 0.3*u.m


# Simulation frequency
freq = 11.7 * u.GHz #use the centre freque as approximation

# number of times the constellation and tesselation is calculated
Niters = 1

# number of time steps
Ntime = 100

#length of a time step
time_step = 1 #seconds

# channel bandwidth
BW = 250 * u.MHz

# avoidance angle 
# optional parameter only used for steerable beams constellations
avoid_angle = 0 * u.deg

# Masking:
# masking in the signal chain: a time vector could be defined when a satellite
# has a boresight angle less than a defined value
# In real life this requires good knowledge of the ephemerides of satellites and
# propagation of them.
masking = False
boresight_limit = 1 * u.deg

# Power flagging:
# flag the received power higher than this limit
# of received power in a 4 kHz BW
power_flag = True
T = 20 * u.K # Kelvin
B = 1 *u.Hz#4e3 * u.Hz  # Hz (4 kHz)
k_b = 1.3806E-23 
Plim = (T*k_b * u.W * u.s / u.K).to(u.W/u.Hz)

print("Tsys = ", T)
print("BW = ", B)
print("Plim/Hz = kT = ", 10.*np.log10(Plim.value/1E-3), "dBm/Hz")


RA769_lim = protection.ra769_calculate_entry(freq,1*u.Hz,1e-8*u.K,T)[3]

print("Protection thresgolds as defined in RA.769: %.2f"%RA769_lim.value)

#Antenna receiver model
high_SLL = 0 # Increase the sidelobe level to account for uncertainties
SLL = 4 # dBi Value of sidelobes


# Constellations based on their theretical parameters
const_name = 'Starlink ph1'
# const_name = 'Starlink ph2'
# const_name = 'OneWeb ph1'
# const_name = 'OneWeb ph2'
# const_name = 'GW'
# const_name = 'GNSS'     #Genrated orbits with parameters
# const_name = 'Geo'

# constellations based on TLEs from Celestrak
# Actual satellites in orbit
# const_name = 'TLE-Starlink'
# const_name = 'TLE-Geo'
# const_name = 'TLE-OneWeb'
# const_name = 'TLE-Active'
# const_name = 'TLE-Orbcomm'
# const_name = 'TLE-Iridium'
# const_name = 'TLE-GNSS'

# constellations based on TLEs from Celestrak from a saved file
# Actual satellites in orbit
# const_name = 'TLE-file-Starlink'
# const_name = 'TLE-file-Geo'
# const_name = 'TLE-file-OneWeb'
# const_name = 'TLE-file-Active'
TLE_file = 'TLEs/2022-05-21-16-52-22.npz' 

# using a fixed time
T = timeA.Time('2022-05-21 16:52:22',format = 'iso')
# T = timestamp[0] # THis comes from the KU band measurement script  
epoch_fix = T.mjd
epoch_random = True


# beam_type
# beam_type = 'constant'    
beam_type = 'statistical'   #uses an average pfd obtained from the script "steerable_beams_probability.py"
# beam_type = 'isotropic'   #-146 dBW/m2 is the nominal PFD of these Ku constellations
                            # in 4kHz BW
                            


Nchannels = 1
if 'OneWeb' in const_name:
    Nchannels = 8
    beam_type = 'fixed' #for Oneweb satellites, generates 8 beams.
    


#string to use in saving figures
sim_flags = '-high_SLL %d-avd_ang %s-\
masking %d-pow_flag %d-Niters %d-Ntime %d-t_step %.2f'\
    %(high_SLL, avoid_angle, masking, power_flag, Niters, Ntime,time_step)

print("\nSimulation Flags : ", sim_flags)
#%% 
load_file = False
if load_file:
    try:
        # to get the values back from saved values
        # aux = np.load('out/'+Constellation_name+'/out '+sim_flags[-5] + '.npz')
        aux = np.load('out/'+const_name+'/out '+sim_flags + '.npz')
        aux.allow_pickle = True
        Prx_iters = aux['Prx_iters'] # in u.W
        Niters = aux['Niters']
        Ntime = aux['Ntime']
        time_step = aux['time_step']
        BW = aux['BW']
        masking = aux['masking']
        boresight_limit = aux['boresight_limit'] 
        power_flag = aux['power_flag'] 
        T = aux['T']
        B = aux['B']
        Plim = aux['Plim'] # in *u.W
        high_SLL = aux['high_SLL']
        SLL = aux['SLL']
        point_el = aux['point_el']
        point_az = aux['point_az']
        sim_flags = str(aux['sim_flags'])
        observer_name = str(aux['observer_name'])
        observer_diam = aux['observer_diam']
        const_name = str(aux['const_name'])
        observer_geo = aux['observer_geo']
        Ncells = aux['Ncells']
        Nchannels = aux['Nchannels']
        cell_lon = aux['cell_lon']
        cell_lat_low = aux['cell_lat_low']
        cell_lat_high = aux['cell_let_high']
        cell_lon_low = aux['cell_lon_low']
        cell_lon_high = aux['cell_lon_high']
        
        A_eff_max = (np.pi * (observer_diam/2)**2 * 1)  #in * u.m**2 # m2
        ESPFD_iters = Prx_iters / A_eff_max / (4e3) # W/m2/Hz
        p_az = point_az[0,:,None]
        p_el = point_el[0,:,None]
        
        observer = ska.Telescope(observer_name, observer_geo)
        Const = ska.Constellation(const_name,
                                   observer_name,
                                   observer_geo)
    
        print('success loading: ')
        print('out/'+const_name+'/out '+sim_flags + '.npz')
    except:
        print('Error loading file')
        
        
else:
    # POINTING OPTIONS:
    # 1- tesselate the semiphere (horizontal coordinates)
    point_az, point_el,grid_info =\
        ska.general.sky_cells_m1583(niters=Niters, step_size=2 * u.deg,
                                    lat_range=(10 * u.deg, 90 * u.deg),
                                    test = 0
                                   )
        
    # 2- fixed pointing
    # Best fit to the pointing from home is az=101, el=33.2. Can be verified with OneWeb satellites
    # in the day capture file = r'\record2022-05-21' at 4488 seconds. 
    # point_az = np.repeat(np.atleast_1d([97.8, 105])[None,:],Niters,axis=0)
    # point_el = np.repeat(np.atleast_1d([27.8, 26])[None,:],Niters,axis=0)
      
    
    Ncells = point_az.shape[1]
    print("Number of Cells = ", Ncells)
    
    # output vectors
    ESPFD_iters = np.zeros([Niters, Ncells, Ntime,Nchannels]) #*u.Jy
    Prx_iters = np.zeros([Niters, Ncells, Ntime,Nchannels]) #*u.W
    
    # define the constellation
    Const = constellation.Constellation(
        const_name = const_name,\
        observer_name = observer_name,\
        observer_geo = observer_geo,\
        beam_type = beam_type,
        TLE_file = TLE_file)
    
    
    #initialize spfd function
    Const.spfd_init(avd_angle = avoid_angle)
    
    print("Constellation defined...done")
        
    
    #%% define the telescope receiver
    Observer = telescope.Telescope(observer_name,
                                   observer_geo,
                                   diam = observer_diam)
    print("Observer defined...done")
    
    #plot observer antenna
    theta = np.logspace(-3,np.log10(180),1000)*u.deg
    RAS_G = Observer.gain(theta, observer_diam, freq, 100*u.percent,
                     high_SLL=high_SLL, SLL = SLL,
                     do_bessel=False) #in lin gain
    
    fig = plt.figure('Observer antenna model', figsize=(9, 6))
    plt.semilogx(theta,10*np.log10(RAS_G), '-k')
    plt.title(Observer.name + ' Antenna Model')
    plt.xlabel('Boresight Angle [$^o$]')
    plt.ylabel('Gain [dBi]')
    plt.grid(True, 'both')
    plt.tight_layout()
    fig.savefig('figs/'+const_name+'/'+Observer.name+' antenna gain - ' + sim_flags + '.jpg')
    
    print("Number of Simulation Iterations =", Niters)
    #%% Propagation and received power
    for m in range(Niters):
        print('Iter: %d'%m)
        niter = m
    
        # pointings of the RAS antenna
        p_az = point_az[niter, :, None]
        p_el = point_el[niter, :, None]
    
    
        # positions of the satellites in the horizontal frame
        if epoch_random:
            epoch = float(51544.5 + np.random.rand(1)*10)
        else:
            epoch = epoch_fix
        
        Const.propagate(epoch = epoch,
                         iters = 1,
                         time_steps = Ntime,
                         time_step = time_step,
                         verbose = False
                         )
    
        vis_mask = Const.vis_mask
        sat_az = Const.topo_pos[vis_mask, 0][None]
        sat_el = Const.topo_pos[vis_mask, 1][None]
    
        Ntime = Const.topo_pos.shape[1]
        Nsats = Const.topo_pos.shape[2]
    
        # SPECTRAL POWER FLUX DENSITY
        # ---------------------------
        # spfd generated by each satellite visible
        # the statistical function only has the elevation
        # of the satellite as a parameter
        spfd_sat = Const.spfd()  # (u.W/u.m**2) 
    
        Nsegments = 20 # cells divided in N steps to accomodate the amount of memory required
        segment = Ncells//(Nsegments-1)
        if segment == 0:
            Nsegments = 1
            segment = Ncells
        
            
        PSD_rx_sparse = np.zeros([segment, 1, Ntime, Nsats,Nchannels])
    
        for n in range(Nsegments):
            t_start = time.time()
            # print('slice %d'%i)
            idx = slice(n*segment,
                        (n+1)*segment,
                        1)
            if idx.stop > p_az.shape[0]:
                idx = slice(n*segment,
                            p_az.shape[0],
                            1)
    
            p_az1 = p_az[idx]
            p_el1 = p_el[idx]
            # print('start',p_az1[0],p_az1[-1])
            Ncells1 = p_az1.shape[0]
    
    
            #Angular distance
            theta = pycraf.geometry.true_angular_distance(p_az1*u.deg,p_el1*u.deg,sat_az*u.deg,sat_el*u.deg)
    
            #Gain of the RAS antenna towards the satellites
            RAS_G = Observer.gain(theta, observer_diam, freq, 90*u.percent,
                             high_SLL=high_SLL, SLL = SLL,
                             do_bessel=False) #in lin gain
    
            # Telescope masking 
            if masking:
                RAS_G[theta < boresight_limit] = np.nan
                # when the summation is don in the satellite axis,
                # a flagged satellite in the main beam flags all the time step
                # as the telescope receives the power from all satellites
    
            #effective area        
            A_eff = RAS_G * (((const.c/freq).to_value(u.m))**2/4/np.pi)  * u.m**2
    
            #Received power
            PSD_rx_lin = spfd_sat[None] * A_eff[...,None] #in u.W/u.Hz [pointings,time_satellite,channels]
            # Prx_lin = cp.asnumpy(cp.power(10,cp.array(Prx)/10)) #*u.W/u.m**2
    
            t_start1 = time.time()
            #convert from dense to sparse matrix [Niters, Ncells1, Ntime, Nsats]
            if Ncells1==PSD_rx_sparse.shape[0]:
                PSD_rx_sparse *= 0
            else:
                PSD_rx_sparse = np.zeros([Ncells1,
                                          1,
                                          Ntime,
                                          Nsats,Nchannels])
    
            PSD_rx_sparse[:, vis_mask] = PSD_rx_lin
    
            PSD_rx_tot = (np.nansum(PSD_rx_sparse, axis=-2))  # sum in satellite dimension [Ncells1,Ntime,Nchannels]
            t_stop1 = time.time()
            print('Elapsed time Sparsing matrix:'+ str(t_stop1-t_start1))
    
            #Equivalent PFD
            Prx_tot = PSD_rx_tot * u.W / u.Hz # to W in 1 hz
    
            #save to output vectors
            Prx_iters[m, idx, :] = Prx_tot[:, 0, :] # [Niters,Ncells,Ntime]  in W/Hz
    
            t_stop = time.time()
            print(str(n) + ' - Elapsed time slice:'+ str(t_stop-t_start))
    
    A_eff_max = (np.pi * (observer_diam/2)**2 * 1) * u.m**2 # m2
    ESPFD_iters = Prx_iters / A_eff_max.value # [Niters,Ncells,Ntime]  in W/m2/Hz

    cell_lon = grid_info['cell_lat'],
    cell_lat_low = grid_info['cell_lat_low'],
    cell_lat_high = grid_info['cell_lat_high'],
    cell_lon_low = grid_info['cell_lon_low'],
    cell_lon_high = grid_info['cell_lon_high'], 


    #----------------------- Save the calculated variables
    np.savez('out/'+Const.name+'/out '+sim_flags,
             Prx_iters=Prx_iters,
             Niters = Niters,
             Ntime = Ntime,
             time_step = time_step,
             BW = BW,
             masking = masking,
             boresight_limit = boresight_limit, 
             power_flag = power_flag, 
             T = T,
             B = B,
             Plim = Plim,
             high_SLL = high_SLL ,
             SLL = SLL,
             point_el = point_el,
             point_az = point_az,
             sim_flags= sim_flags,
             observer_name = observer_name,
             observer_diam = observer_diam,
             const_name = const_name,
             observer_geo = observer_geo,
             Ncells = Ncells,
             Nchannels= Nchannels,
             cell_lon = cell_lon,
             cell_lat_low = cell_lat_low,
             cell_lat_high = cell_lat_high,
             cell_lon_low = cell_lon_low,
             cell_lon_high = cell_lon_high
             )


#%% plot satellites above the horizon
plt.figure()
# plt.plot(Const.topo_pos[Const.vis_mask,0],Const.topo_pos[Const.vis_mask,1],'.')
plt.plot(Const.topo_pos[0,0,:,0],Const.topo_pos[0,0,:,1],'.')
plt.plot(Const.topo_pos[0,0:1000,:,0],Const.topo_pos[0,0:1000,:,1],alpha = 1)

plt.ylim([0,90])
plt.plot(point_az, point_el, '.', color='blue')
    
#%% correct approximations that result in negative values:
# Prx_iters[Prx_iters<0] = np.nanmin(Prx_iters[Prx_iters>0]) / 100
# ESPFD_iters[ESPFD_iters<0] = np.nanmin(ESPFD_iters[ESPFD_iters>0]) / 100

#%% print the amount of data masked
print('Total flagged samples for satellites closer than %s :'%boresight_limit)
print('%.2f %%'%(np.sum(np.isnan(ESPFD_iters))/len(ESPFD_iters.flatten())*100))

#%% flatten time domain plot 
RFI_BW = 2e9/8*u.Hz # Bandwidth of the RFI, in this case from 10.7-12.7 GHz
RX_BW = 7e9*u.Hz # Bandwidth of the Receiver, for Band5b is 8.3 to 15.4 GHz

# this is useful to see the received power levels
fig = plt.figure(figsize=(9, 6))
# fig = plt.figure('Time Domain Power', figsize=(9, 6))
time_vector= np.repeat(np.linspace(0,Ntime*time_step,Ntime)[None], Ncells, axis=0)
P_time = ((Prx_iters[0,...,0]*u.W/u.Hz*RFI_BW).to(u.dB(u.mW)))
plt.plot(time_vector.T,P_time.T)
         # label='Integrated satellite RFI in %.2f GHz BW'%(RFI_BW/1e9))
plt.grid(True, 'both')
plt.xlabel('seconds')
plt.ylabel(P_time.unit)
plt.title('Time Domain Rx Power - '+ Const.name)
# plt.ylim([-190,-120])
plt.axhline(y=(Plim*RX_BW).to_value(u.dB(u.mW)),
            label = 'Integrated noise power in %.2f GHz BW'%(RX_BW.to_value(u.GHz)),
            color='red')
plt.legend()
# fig.savefig('figs/'+const_name+'/'+observer_name+\
#             'time domain Prx - ' + sim_flags)

#%% Histogram of instantaneous received power :
    
fig = plt.figure('Histogram of Time Domain Power', figsize=(9, 6))

for i  in range(Niters):
    P_hist = (Prx_iters[i]*u.W/u.Hz*RFI_BW).flatten().to(u.dB(u.mW)).value
   
    a,b = np.histogram(P_hist,200)#, histtype = 'step', density='True')
    plt.semilogy((b[1:]+b[0:-1])/2,a/np.sum(a)*100,c='r',alpha=0.2)#,label = 'Integrated satellite RFI in %d GHz BW'%(RFI_BW/1e9))

#plot mean
P_hist_mean = np.mean((Prx_iters[i]*u.W/u.Hz*RFI_BW),axis=0).flatten().to(u.dB(u.mW)).value
a,b = np.histogram(P_hist,200)#, histtype = 'step', density='True')
plt.semilogy((b[1:]+b[0:-1])/2,a/np.sum(a)*100,c='b',alpha=1,label = 'Mean integrated power in %d GHz BW'%(RFI_BW.to_value(u.GHz)))

plt.grid(True, 'both')
plt.xlabel('Received power per channel [dBmW]')
plt.ylabel('% of occurences')
plt.title('Histogram of Time Domain Rx Power - '+ str(Const.name))
plt.axvline(x=(Plim*RX_BW).to_value(u.dB(u.mW)),
            label = 'Integrated noise power in %d GHz BW'%(RX_BW.to_value(u.GHz)),
            color='red')
# plt.legend()
# fig.savefig('figs/'+str(const_name)+'/'+str(observer_name)+\
#             'histogram time domain Prx - ' + str(sim_flags))

    
# CDF
a = a/np.sum(a)*100
fig = plt.figure('Cumulative distribution of Time Domain Power', figsize=(9, 6))
cdf = np.zeros(len(a)) 
for i in range(len(a)):
    cdf[i] = np.sum(a[0:i])
    
plt.plot((b[1:]+b[0:-1])/2, cdf)
plt.grid(True, 'both')
plt.xlabel('Received power per channel [dBmW]')
plt.ylabel('CDF')
plt.title('CDF of Time Domain Rx Power '+ str(Const.name))
# fig.savefig('figs/'+str(const_name)+'/'+str(observer_name)+\
#             'CDF time domain Prx - ' + str(sim_flags))



#%% Function to plot the sky map    
def plot_sky(value,
             grid_info,
             title = '',
             xlabel = '',
             ylabel = '',
             zunit = '',
             plot_type = 'triangles',
             contours = False,
             savefig = False,
             filename = ''
             ):
    '''
    *************
    * plot_sky  *
    *************
    Parameters
    ----------
    value : TYPE
        DESCRIPTION.
    grid_info : TYPE
        DESCRIPTION.
    title : TYPE, optional
        DESCRIPTION. The default is ''.
    xlabel : TYPE, optional
        DESCRIPTION. The default is ''.
    ylabel : TYPE, optional
        DESCRIPTION. The default is ''.
    zunit : TYPE, optional
        DESCRIPTION. The default is ''.
    plot_type : TYPE, optional
        DESCRIPTION. The default is 'triangles'.
    contours : TYPE, optional
        DESCRIPTION. The default is False.
    savefig : TYPE, optional
        DESCRIPTION. The default is False.
    filename : TYPE, optional
        DESCRIPTION. The default is ''.

    Returns
    -------
    fig : TYPE
        DESCRIPTION.

    '''

    # fig = plt.figure(figsize=(9, 6))
    val = value
    ind_plot = np.isfinite(val)

    # color=plt.cm.viridis(val_norm),

    vmin, vmax = np.nanmin(val[ind_plot]), np.nanmax(val[ind_plot])
    val_norm = (val[ind_plot] - vmin) / (vmax - vmin)
    # if plot_type == 'bars':
    #     plt.bar(
    #         grid_info['cell_lon_low'][ind_plot,0] ,
    #         height=grid_info['cell_lat_high'][ind_plot,0] - grid_info['cell_lat_low'][ind_plot,0],
    #         width=grid_info['cell_lon_high'][ind_plot,0] - grid_info['cell_lon_low'][ind_plot,0],
    #         bottom=grid_info['cell_lat_low'][ind_plot,0],
    #         color=plt.cm.viridis(val_norm),
    #         align='edge',
    #         linewidth = 0,
    #         )



    # if plot_type == 'triangles':
    #     plt.tricontourf((p_az[ind_plot,0]),p_el[ind_plot,0],val[ind_plot],levels=100,cm='jet')
    

    # sm = plt.cm.ScalarMappable(cmap=plt.cm.viridis, norm=plt.Normalize(vmin=vmin, vmax=vmax))
    # cbar = plt.colorbar(sm)
    # cbar.set_label(zunit)
    # plt.xlim((0, 360))
    # plt.ylim((0, 90))
    # plt.xlabel(xlabel)
    # plt.ylabel(ylabel)

    # #plt.plot(sat_sph[:,0],sat_sph[:,1],'xr')
    # if contours:
    #     plt.tricontour((p_az[ind_plot,0]),p_el[ind_plot,0],val[ind_plot],
    #                    levels=[val.max()*0.1,val.max()*0.4,val.max()*0.75],
    #                    colors=['green','black','red'])
    #     plt.plot([],color='green',label='%.2f %s'%(val.max()*0.1,zunit))
    #     plt.plot([],color='black',label='%.2f %s'%(val.max()*0.4,zunit))
    #     plt.plot([],color='red',label='%.2f  %s'%(val.max()*0.75,zunit))
    #     plt.legend()
    # plt.title(title)
    # plt.tight_layout()
    # if savefig:
    #     fig.savefig(filename)


    fig2 = plt.figure(figsize=[20,15])
    ax = fig2.add_axes([0.1, 0.1, 0.8, 0.8],
                       polar=True)
    
    ax.set_theta_zero_location("N")
    colors = plt.cm.viridis(val_norm)
    th = grid_info['cell_lon'][ind_plot,0]*np.pi/180
    rad = 90 - grid_info['cell_lat_high'][ind_plot,0]
    w = (grid_info['cell_lon_high'][ind_plot,0] - grid_info['cell_lon_low'][ind_plot,0])*np.pi/180
    b = 90-grid_info['cell_lat_low'][ind_plot,0] - rad
    
    ax.bar(th,rad,
           width= w,
           bottom= b,
           color=colors, edgecolor = colors)
    
    sm = plt.cm.ScalarMappable(cmap=plt.cm.viridis, norm=plt.Normalize(vmin=vmin, vmax=vmax))
    cbar = plt.colorbar(sm)
    cbar.set_label(zunit)

    # ax.set_ylim([0,70])
    return fig


#%% plot the sky map before flagging

# ESPFD_m = np.nanmean(np.nanmean(ESPFD_iters,axis=0),axis=-1).value
ESPFD_m = np.mean(np.nanmean(ESPFD_iters,axis=0),axis=-2)/(10**-26)

# ESPFD_m = np.sum(ESPFD_m,axis=-1)
ESPFD_m = ESPFD_m[:,0]
ESPFD_m_dB = 10*np.log10(ESPFD_m) # W/m2/Hz to dBJy

plot_sky(ESPFD_m_dB,
         grid_info,
         plot_type = 'bars',
         title = 'Equivalent Power Flux Density: Before Flagging - ' + str(Const.name),
         xlabel = 'Azimuth [$^o$]',
         ylabel = 'Elevation [$^o$]',
         zunit = 'dBJy',
         savefig = True,
         contours = True,
         filename = 'figs/'+str(Const.name)+'/EPFD_dBJy_before_flag - ' + str(sim_flags),
        )

#%% Power Flagging
ESPFD_flag = np.copy(ESPFD_iters)
Prx_flag = np.copy(Prx_iters) 
                                       
if power_flag:
    flag = np.where(Prx_iters*RFI_BW > Plim.value*RX_BW)

    ESPFD_flag[flag] = np.NaN
    Prx_flag[flag] = np.NaN

    print('Total Power flagging, Plim = %.2f dBW'%(10*np.log10(Plim.value)))
    print('%.2f %%'%(len(flag[0])/len(Prx_iters.flatten())*100))

    # fig = plt.figure('Time Domain Power', figsize=(9, 6))
    # plt.plot((Prx_flag[0]*u.W).flatten().to(u.dB(u.W)))
    # plt.grid(True, 'both')

#%% Total lost samples
print('Total lost samples (masking + power flagging):')
print('%.2f %%'%(np.sum(np.isnan(ESPFD_flag))/len(ESPFD_flag.flatten())*100))

#%% plot the sky map

ESPFD_m = np.nanmean(np.nanmean(ESPFD_flag,axis=0),axis=-2)[...,0]
ESPFD_m[np.isnan(ESPFD_m)] = 0
val = ESPFD_m/(10**-26) # W/m2/Hz to Jy

plot_sky(val,
         grid_info,
         plot_type = 'bars',
         title = 'Equivalent Power Flux Density: after Flagging - ' + str(Const.name),
         xlabel = 'Azimuth [$^o$]',
         ylabel = 'Elevation [$^o$]',
         zunit = 'Jy',
         savefig = True,
         contours = True,
         filename = 'figs/'+str(Const.name)+'/EPFD_dBJy_after_flag - ' + str(sim_flags),
         )

#%% plot the flagging sky
flags = np.isnan(ESPFD_flag)
#calculate percentage of flagged samples per direction in the sky
val = np.sum(np.sum(flags,axis=0),axis=1)/flags.shape[0]/flags.shape[2]*100

plot_sky(val,
         grid_info,
         title = 'Flagged Percentage - ' + str(Const.name),
         xlabel = 'Azimuth [$^o$]',
         ylabel = 'Elevation [$^o$]',
         zunit = 'Percentage [%]',
         savefig = True,
         contours = True,
         plot_type = 'triangles',
         filename = 'figs/'+str(Const.name)+'/Flagged_percentage - ' + str(sim_flags),
         )

plt.show()


#%% plot the histogram of the received power 
Prx_ave = np.nanmean(Prx_iters,axis=-1)
plt.figure(figsize=[15,7])
[plt.hist(Prx_ave[i], bins = 200, cumulative=1) for i  in range(10)]
