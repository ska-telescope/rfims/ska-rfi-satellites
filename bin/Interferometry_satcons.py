# -*- coding: utf-8 -*-
"""
Created on Wed Jul  6 23:22:04 2022

@author: f.divruno
"""
import numpy as np
import matplotlib.pyplot as plt
import astropy.time as time
import astropy.units as u
import sys

sys.path += [r'../src']

from ska import Constellation 

import pycraf

# radio telescope 1 cartesian location
o_name = 'Wetzell'
o_geo = np.array((12.87889957, 49.14419937, 0.6)) # long,lat,alt, [deg, deg, km]


o_name1 = 'Onsala'
# o_geo1 = np.array((12.875, 49.14419937, 0.6)) # long,lat,alt, [deg, deg, km]
o_geo1 = np.array(( 11.917778,57.393056, 0.02)) # long,lat,alt, [deg, deg, km]


phi = pycraf.geometry.true_angular_distance(o_geo[0]*u.deg, o_geo[1]*u.deg, o_geo1[0]*u.deg, o_geo1[1]*u.deg)
print('Telescopes distance = ', 6371*phi.to_value(u.rad)*u.km)


#%% define constellation
#sim the full constellation

beam_type = 'statistical'
# beam_type = 'statistical_constant'
# beam_type = 'isotropic'
# beam_type = 'fixed'

#create constellation
# const_name = 'OneWeb ph1'
const_name = 'Starlink ph1'
# const_name = 'TLE-Geo'
Const = Constellation(
    const_name,
    o_name,
    o_geo,
    beam_type = beam_type)

Const1 = Constellation(
    const_name,
    o_name1,
    o_geo1,
    beam_type = beam_type)



# #propagate for a certain 
Niters = 10
Nt = 10
dt = 1
dt2 = 0.1
N2 = int(Nt*dt/dt2)
epoch = time.Time.now().mjd #+ np.linspace(0,Nt*dt,Nt)/24/60/60
epoch2 = time.Time.now().mjd + np.linspace(0,Nt*dt,N2)/24/60/60
Const.propagate(iters=Niters,
                 epoch = epoch,
                 time_steps = Nt,
                 time_step = dt,
                 verbose=True,
                 rand_seed=10)

Const1.propagate(iters=Niters,
                 epoch = epoch,
                 time_steps = Nt,
                 time_step = dt,
                 verbose=True,
                 rand_seed=10)


#%% plot satellite vie in topo
#all visible satellites
plt.figure()
plt.plot(Const.topo_pos[Const.vis_mask,0],Const.topo_pos[Const.vis_mask,1],'.')
plt.plot(Const1.topo_pos[Const1.vis_mask,0],Const1.topo_pos[Const1.vis_mask,1],'o')

#only some iterations
plt.figure()
plt.plot(Const.topo_pos[0,Const.vis_mask[0],0],Const.topo_pos[0,Const.vis_mask[0],1],'.')
plt.plot(Const.topo_pos[1,Const.vis_mask[1],0],Const.topo_pos[1,Const.vis_mask[1],1],'o')


#%% Distance satellite - Radio telescope
mask_t = (Const.vis_mask) * (Const1.vis_mask)
d = Const.topo_pos[...,2] *1e3
d1 = Const1.topo_pos[...,2] *1e3

# mask not visible satellites
d[~Const.vis_mask] = np.nan
d1[~Const1.vis_mask] = np.nan

#plot distance changes
# plt.figure(figsize=[20,15])
# plt.plot(d[0,:],'.')
# plt.plot(d1[0,:],'x')

#interpolate  distance
dint = np.zeros([Niters,N2,mask_t.shape[2]])
dint1 = np.zeros([Niters,N2,mask_t.shape[2]])

epoch = epoch + np.linspace(0,Nt*dt,Nt)/24/60/60
for j in range(mask_t.shape[0]):
    for i in range(mask_t.shape[2]):
        dint[j,:,i] = np.interp(epoch2,epoch,d[j,:,i])
        dint1[j,:,i] = np.interp(epoch2,epoch,d1[j,:,i])
        print(j,i)


#phase of arrival to each antenna
f = 11e9
theta = 2*np.pi*f/3e8*dint
theta1 = 2*np.pi*f/3e8*dint1


#%% Instantaneous Voltage sums with attenuation and phase

#random phase per satellite
sat_phase = 0#np.random.rand(mask_t.shape[2])[None,None]

#instantaneous voltage received per antenna from all satellites
V_1 = abs(np.nansum(1/(4*np.pi*dint**2)**0.5*np.cos(theta),axis=2))*(4*np.pi*np.nanmin(dint)**2)**0.5
V1_1 = abs(np.nansum(1/(4*np.pi*dint1**2)**0.5*np.cos(theta1),axis=2))*(4*np.pi*np.nanmin(dint)**2)**0.5


V_2 = abs(np.nansum(1/(4*np.pi*dint**2)**0.5*np.exp(1j*theta+sat_phase),axis=2))*(4*np.pi*np.nanmin(dint)**2)**0.5
V1_2 = abs(np.nansum(1/(4*np.pi*dint**2)**0.5*np.exp(1j*theta1+sat_phase),axis=2))*(4*np.pi*np.nanmin(dint)**2)**0.5


P = np.nansum(1/(4*np.pi*dint**2),axis=2)*(4*np.pi*np.nanmin(dint)**2)
P1 = np.nansum(1/(4*np.pi*dint1**2),axis=2)*(4*np.pi*np.nanmin(dint)**2)
# correlation product, average in time
# Vc = abs(np.mean(V*V1,axis=1)/np.nanmax(V**2))

plt.figure(figsize=[20,15])
a= plt.hist(20*np.log10(V_1.flatten()),50,histtype='step')
a= plt.hist(20*np.log10(V1_1.flatten()),50,histtype='step')
a= plt.hist(20*np.log10(V_2.flatten()),50,histtype='step')
a= plt.hist(20*np.log10(V1_2.flatten()),50,histtype='step')
a= plt.hist(10*np.log10(P.flatten()),50,histtype='step')
a= plt.hist(10*np.log10(P1.flatten()),50,histtype='step')

# print(np.std(Vc))

# #%% Multiplication of individual satellites
# V = 1/(4*np.pi*dint**2)*np.cos(theta+sat_phase)
# V1 = 1/(4*np.pi*dint1**2)*np.cos(theta1+sat_phase)

# Vc1 = V*V1/np.nanmax(V**2)
# # sum in satellite axis, mean in time axis
# Vc = abs(np.mean(np.nansum(Vc1,axis=2),axis=1))

# a= plt.hist(10*np.log10(Vc),50)
# print(np.std(Vc))

#%%
plt.figure(figsize=[20,15])
plt.plot((V*V1)[0,:,0:1000])
