# -*- coding: utf-8 -*-
"""
Created on Mon Dec 12 22:00:26 2022

Convert ephemeris of satellites from different sources into format that can be 
used by astronomical observatories.


last modified: 17-03-2023

@author: f.divruno

License: 
    
"""


from astropy.coordinates import SkyCoord, EarthLocation, AltAz
import astropy.units as u
from astropy.time import Time
import numpy as np
import matplotlib.pyplot as plt


font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 22}
import matplotlib
matplotlib.rc('font', **font)


    
def read_privateer_oems(fname):
    # load the ephemeris data from Privateer space.
    # these follow the OEM format described in the CCSDS standard
    # https://public.ccsds.org/Pubs/502x0b2c1e2.pdf
    
    # read all lines in the text file
    with open(fname, "r") as fp:
        lines = fp.readlines()

    #get metadata in the first 13 lines
    META = lines[0:13]
               
    # Select position lines, using a hardcoded year, can be done better.
    epoch_lines = [line[:23] for line in lines[4:] if line[0:4] == "2023"]
    ephem_lines = [line[24:] for line in lines[4:] if line[0:4] == "2023"]
    
    # Read data from string into floats
    d = np.loadtxt(ephem_lines)
    x, y, z, vx, vy, vz = d.T
    
    # Create arrays with 3 components
    pos = np.array((x,y,z)).T
    vel = np.array((vx,vy,vz)).T

    # Find the start of the covariance section
    # get the position and velocity covariances
    cov_start = np.where(np.array(lines)=='COVARIANCE_START\n')[0][0]
    cov_epoch_lines = lines[cov_start+1::8]
    cov_epoch = [line[8:31] for line in cov_epoch_lines]
    cov_1 = lines[cov_start+2::8]
    cov_2 = lines[cov_start+3::8]
    cov_3 = lines[cov_start+4::8]
    cov_4 = lines[cov_start+5::8]
    cov_5 = lines[cov_start+6::8]
    cov_6 = lines[cov_start+7::8]
    
    #position covars
    cov_xx = np.loadtxt(cov_1)
    cov_yy = np.loadtxt(cov_2)[:,1]
    cov_zz = np.loadtxt(cov_3)[:,2]
    cov_pos = np.array((cov_xx, cov_yy, cov_zz)).T

    #velocity covars
    cov_vxx = np.loadtxt(cov_4)[:,3]
    cov_vyy = np.loadtxt(cov_5)[:,4]
    cov_vzz = np.loadtxt(cov_6)[:,5]
    cov_vel = np.array((cov_vxx, cov_vyy, cov_vzz)).T
    
    
    # Extract times
    pos_t = Time(epoch_lines, format="isot", scale="utc")
    cov_t = Time(cov_epoch, format="isot", scale="utc")

    return META, pos_t, pos, vel,\
            cov_t, cov_pos, cov_vel


def read_spacetrak_memes(fname):
    # see description here: https://www.space-track.org/documents/Spaceflight_Safety_Handbook_for_Operators.pdf
    # data is provided in Mean Equator, Mean Equinox (J2000)
    # Precessed geocentric frame
    
    with open(fname, "r") as fp:
        lines = fp.readlines()

    META = lines[0:4]

    # Read state vector
    ephem_lines = lines[4::4]
    d = np.loadtxt(ephem_lines)
    x, y, z, vx, vy, vz = d[:, 1:].T

    pos = np.array((x,y,z)).T
    vel = np.array((vx,vy,vz)).T

    #read cov data
    cov_1 = lines[5::4]
    cov_2 = lines[6::4]
    cov_3 = lines[7::4]

    d1 = np.loadtxt(cov_1)
    d2 = np.loadtxt(cov_2)
    d3 = np.loadtxt(cov_3)
    
    cov_pos = np.array((d1[:,0], d1[:,2], d1[:,5])).T
    cov_vel = np.array((d2[:,2], d3[:,0], d3[:,6])).T    
    
    
    # extract epochs
    tstr = [f"{l[0:4]}:{l[4:7]}:{l[7:9]}:{l[9:11]}:{l[11:17]}" for l in ephem_lines]
    pos_t = Time(tstr, format="yday", scale="utc")
    
    cov_t = pos_t
    
    return META, pos_t, pos, vel,\
            cov_t, cov_pos, cov_vel



#%% 1 - get the ephemeris:        
# look at satellite Starlink 1601 (NORADID 46120) in 12/03/2023
privateer_oem = '46120_2023_03_12.oem' #get from Privateer Space webpage
SL_spacetrak = 'SL_1601_spacetrak.txt'

# privateer OEM files
meta_priv, pos_t, pos, vel,\
cov_t, cov_pos, cov_vel = read_privateer_oems(privateer_oem)

#interpolate covariances
cov_x = (np.interp(pos_t.mjd,cov_t.mjd,cov_pos[:,0]))
cov_y = (np.interp(pos_t.mjd,cov_t.mjd,cov_pos[:,1]))
cov_z = (np.interp(pos_t.mjd,cov_t.mjd,cov_pos[:,2]))
cov_pos = np.array((cov_x, cov_y, cov_z)).T


#Starlinks MEME files from spacetrak
meta_spacetrak, pos_t1, pos1, vel1,\
cov_t1, cov_pos1, cov_vel1 = read_spacetrak_memes(SL_spacetrak)

#print the meta_priv and meta_spacetrak to show differences


#%% 2- interpolate state vectors in one time vector to allow comparisons

#look for min and maximum common
mjd_min = max(pos_t1.mjd[0],pos_t.mjd[0])
mjd_max = min(pos_t1.mjd[-1],pos_t.mjd[-1])

#create a new time vector using the spacetrak one
pos_t2= pos_t1[(pos_t1.mjd>=mjd_min)&(pos_t1.mjd<=mjd_max)]

#interpolate privateer ephems
posx = (np.interp(pos_t2.mjd,pos_t.mjd,pos[:,0]))
posy = (np.interp(pos_t2.mjd,pos_t.mjd,pos[:,1]))
posz = (np.interp(pos_t2.mjd,pos_t.mjd,pos[:,2]))
pos_interp = np.array((posx, posy, posz)).T

covx = (np.interp(pos_t2.mjd,pos_t.mjd,cov_pos[:,0]))
covy = (np.interp(pos_t2.mjd,pos_t.mjd,cov_pos[:,1]))
covz = (np.interp(pos_t2.mjd,pos_t.mjd,cov_pos[:,2]))
cov_interp = np.array((covx, covy, covz)).T

# slice spacetrak ephems in the new time vector
pos2 = pos1[(pos_t1.mjd>=mjd_min)&(pos_t1.mjd<=mjd_max)]
cov_pos2 = cov_pos1[(pos_t1.mjd>=mjd_min)&(pos_t1.mjd<=mjd_max)]

print('Sliced time vector done')
print('Original spacetrak time vector: %d samples'%len(pos_t1))
print('Original privateer time vector: %d samples'%len(pos_t))
print('final time vector: %d samples'%len(pos_t2))


#%% 3- plot in 3D

# create 3d Axes
fig = plt.figure(figsize=[15,15])
ax = fig.add_subplot(111,  projection='3d')

#wireframe sphere:
u1, v1 = np.mgrid[0:2*np.pi:0.1, 0:np.pi:0.1]
x = np.cos(u1)*np.sin(v1)*6371
y = np.sin(u1)*np.sin(v1)*6371
z = np.cos(v1)*6371
ax.plot_wireframe(x, y, z, color='blue', alpha=0.2)

#plot satellite positions
ax.plot(pos_interp[:,0],pos_interp[:,1],pos_interp[:,2],'.',markersize=1)
ax.plot(pos2[:,0],pos2[:,1],pos2[:,2],'.',markersize=1)

# plot observer position 
# observer location
Obs = EarthLocation.from_geodetic(lat = -30.7*u.deg, lon=21.4*u.deg,height=1000*u.m)
ax.scatter(Obs.geocentric[0].to(u.km),Obs.geocentric[1].to(u.km),Obs.geocentric[2].to(u.km),s=50,color='red')

ax.set_xlabel('x')
ax.set_ylabel('y')
ax.set_zlabel('z')
 

#%% 4- transform ephemeris to topocentric coordinates using astropy

# define observer location
Obs = EarthLocation.from_geodetic(lat = -30.7*u.deg, lon=21.4*u.deg,height=1000*u.m)
# define the topocentric reference frame on Earth surface
Earth_frame = AltAz(obstime = pos_t2, location = Obs)


# 4a - Create a skycoord object from Privateer state vectors
P = SkyCoord(pos_interp[:,0],pos_interp[:,1], pos_interp[:,2],\
             unit='km', representation_type='cartesian', frame='gcrs', obstime=pos_t2)
# use covariances to produce extreme positions (probably better to do a montecarlo approach)
Pmax = SkyCoord(pos_interp[:,0]+cov_interp[:,0]**0.5,pos_interp[:,1]+cov_interp[:,1]**0.5,\
            pos_interp[:,2]+cov_interp[:,2]**0.5, unit='km',\
            representation_type='cartesian', frame='gcrs', obstime=pos_t2)
Pmin = SkyCoord(pos_interp[:,0]-cov_interp[:,0]**0.5,pos_interp[:,1]-cov_interp[:,1]**0.5,\
            pos_interp[:,2]-cov_interp[:,2]**0.5, unit='km',\
            representation_type='cartesian', frame='gcrs', obstime=pos_t2)

# Transform coordinates to Earth ref frame
Pazel = P.transform_to(Earth_frame)
Pazelmax = Pmax.transform_to(Earth_frame)
Pazelmin = Pmin.transform_to(Earth_frame)


# 4b - Create skycoord object from Starlink state vectors from spacetrak 
# option 1 using Geocentric Celestial Reference System 
P11 = SkyCoord(pos2[:,0],pos2[:,1], pos2[:,2], unit='km',\
              representation_type='cartesian', frame='gcrs', obstime=pos_t2)
# use covariances to produce extreme positions (probably better to do a montecarlo approach)
P11max = SkyCoord(pos2[:,0]+cov_pos2[:,0]**0.5,pos2[:,1]+cov_pos2[:,1]**0.5,\
                  pos2[:,2]+cov_pos2[:,2]**0.5, unit='km',\
                      representation_type='cartesian', frame='gcrs', obstime=pos_t2)
P11min = SkyCoord(pos2[:,0]-cov_pos2[:,0]**0.5,pos2[:,1]-cov_pos2[:,1]**0.5,\
                  pos2[:,2]-cov_pos2[:,2]**0.5, unit='km',\
                      representation_type='cartesian', frame='gcrs', obstime=pos_t2)
    
# option 2 using the precessed geocentric ref system (Mean Equatorial Mean Equinox)
P12 = SkyCoord(pos2[:,0],pos2[:,1], pos2[:,2], unit='km',\
            representation_type='cartesian', frame='precessedgeocentric', obstime=pos_t2)

P12max = SkyCoord(pos2[:,0]+cov_pos2[:,0]**0.5,pos2[:,1]+cov_pos2[:,1]**0.5,\
            pos2[:,2]+cov_pos2[:,2]**0.5, unit='km',\
            representation_type='cartesian', frame='precessedgeocentric', obstime=pos_t2)
    
P12min = SkyCoord(pos2[:,0]-cov_pos2[:,0]**0.5,pos2[:,1]-cov_pos2[:,1]**0.5,\
            pos2[:,2]-cov_pos2[:,2]**0.5, unit='km',\
            representation_type='cartesian', frame='precessedgeocentric', obstime=pos_t2)

    # Transform to Earth ref frame
Pazel11 = P11.transform_to(Earth_frame)
Pazel11max = P11max.transform_to(Earth_frame)
Pazel11min = P11min.transform_to(Earth_frame)

Pazel12 = P12.transform_to(Earth_frame)
Pazel12max = P12max.transform_to(Earth_frame)
Pazel12min = P12min.transform_to(Earth_frame)

print('Done transformation')

#%% 5- Plot satellite coordinates in AZel

plt.figure(figsize=[20,16])
plt.plot(Pazel.az,Pazel.alt,'.', color='red', label='Privateer')
plt.plot(Pazelmax.az,Pazelmax.alt, 'x', color='red', alpha = 1)
plt.plot(Pazelmin.az,Pazelmin.alt, 'x', color='red', alpha = 1)

plt.plot(Pazel12.az,Pazel12.alt,'s', color='orange', label = 'SpaceTrak Precessed Geocentric')
plt.plot(Pazel12max.az,Pazel12max.alt, 'x', color='orange', alpha = 1)
plt.plot(Pazel12min.az,Pazel12min.alt, 'x', color='orange', alpha = 1)

plt.plot(Pazel11.az,Pazel11.alt,'o', color= 'green', label = 'SpaceTrak GCRS')
plt.plot(Pazel11max.az,Pazel11max.alt, 'x', color='green', alpha = 1)
plt.plot(Pazel11min.az,Pazel11min.alt, 'x', color='green', alpha = 1)

plt.xlabel('Az')
plt.ylabel('Alt')
plt.grid()
plt.ylim([0,90])
plt.legend()
plt.title('Az Alt coordinates of satellites')



#%% 6- plot uncertainties when converted to AltAz
# This can be done in a much better way, but its just to get an idea...

# errors from privateer ephems
error_min = (((Pazel.az - Pazelmin.az)**2 + (Pazel.alt - Pazelmin.alt)**2)**0.5).to(u.arcsec)
error_max = (((Pazel.az - Pazelmax.az)**2 + (Pazel.alt - Pazelmax.alt)**2)**0.5).to(u.arcsec)

error1_min = (((Pazel11.az - Pazel11min.az)**2 + (Pazel11.alt - Pazel11min.alt)**2)**0.5).to(u.arcsec)
error1_max = (((Pazel11.az - Pazel11max.az)**2 + (Pazel11.alt - Pazel11max.alt)**2)**0.5).to(u.arcsec)

plt.figure(figsize=[20,16])
plt.plot(pos_t2.mjd,error_min,'.',label='Privateer')
plt.plot(pos_t2.mjd,error_max,'.',label='Privateer')
plt.plot(pos_t2.mjd,error1_min,'o',label='Spacetrak')
plt.plot(pos_t2.mjd,error1_max,'o',label='Spacetrak')

# privateer_creation = Time(meta_priv[1].split(' ')[2].split('\n')[0].split('+')[0],format='isot')
# plt.axvline(privateer_creation.mjd)
# plt.axvline(pos_t2[0].mjd)

plt.title('RMS uncertainty when coverted to AltAz')
plt.xlabel('sample')
plt.ylabel('arcsec')
plt.grid()
plt.ylim([0,300])
plt.legend()



plt.figure(figsize=[20,16])
plt.hist(error_min.to_value(u.arcsec),100,histtype='step',label='priv')
plt.hist(error_max.to_value(u.arcsec),100,histtype='step',label='priv')
plt.hist(error1_min[error1_min<1000*u.arcsec].to_value(u.arcsec),100,histtype='step',label='spacetrak')
plt.hist(error1_max[error1_max<1000*u.arcsec].to_value(u.arcsec),100,histtype='step',label='spacetrak')
plt.grid()
plt.title('Histograms of RMS uncertainty in AzAlt derived from OEMs')
plt.xlabel('arcsec')
plt.legend()


#%% 7- Calculate with TLEs:
import requests
import cysgp4

read_new_TLEs = 0 #use this flag to load the TLEs only once and save them
if read_new_TLEs:
    # Import the TLEs from Celestrak
    # Starlink TLEs from celestrak
    celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/starlink.txt')
    with open('celestrak_latest_20230313.txt', 'w', encoding='utf-8') as f:
        f.write(celestrak_latest.text)
    
    # Supplemental Starlink TLEs from celestrak
    celestrak_supplemental = requests.get('http://celestrak.org/NORAD/elements/supplemental/sup-gp.php?FILE=starlink&FORMAT=tle')
    with open('celestrak_sup_20230313.txt', 'w') as f:
        f.write(celestrak_supplemental.text)
    
    
with open('celestrak_sup_20230313.txt', 'r') as f:
    celestrak_sup_text = f.read()
    celestrak_sup_text = str.replace(celestrak_sup_text, '\n\n', '\r\n') #replace control characters to allow cysgp4 to read them
    
with open('celestrak_latest_20230313.txt', 'r') as f:
    celestrak_latest_text = f.read()
    celestrak_latest_text = str.replace(celestrak_latest_text, '\n\n', '\r\n')


#%% 7a - Using cysgp4 from Benjamin Winkel to propagate TLEs

# generate the TLEs objects with cysgp4
tles = np.array(cysgp4.tles_from_text(celestrak_latest_text))
tles_sup = np.array(cysgp4.tles_from_text(celestrak_sup_text))

tles = tles_sup # use the supplemental TLEs, comment to use GP TLEs 

#SKA-MID as observer in south africa:
observer = cysgp4.PyObserver(21.4,-30.7,1)  #lon deg, lat deg, elev in km
obs_name = 'SKA-MID'

# get time vector in mjd
mjds = pos_t2.mjd

# propagate satellite in the time vector
# NOTE: satellite number 1601 is index 461
result = cysgp4.propagate_many(
    mjds,
    tles[461],
    observer,
    do_sat_azel=False,
    on_error = 'coerce_to_nan', #dismiss error due to too old TLEs
    )

#get the coordinates in topocentric (AltAz)
topo_pos = result['topo']
topo_pos_az, topo_pos_el, topo_pos_dist, _ = (topo_pos[..., j] for j in range(4))


plt.figure(figsize=[20,16])

plt.plot(Pazel.az,Pazel.alt,'.', color='red', label='Privateer')
plt.plot(Pazelmax.az,Pazelmax.alt, 'x', color='red', alpha = 1)
plt.plot(Pazelmin.az,Pazelmin.alt, 'x', color='red', alpha = 1)

plt.plot(Pazel12.az,Pazel12.alt,'s', color='orange', label = 'SpaceTrak Precessed Geocentric')
plt.plot(Pazel12max.az,Pazel12max.alt, 'x', color='orange', alpha = 1)
plt.plot(Pazel12min.az,Pazel12min.alt, 'x', color='orange', alpha = 1)

plt.plot(Pazel11.az,Pazel11.alt,'o', color= 'green', label = 'SpaceTrak GCRS')
plt.plot(Pazel11max.az,Pazel11max.alt, 'x', color='green', alpha = 1)
plt.plot(Pazel11min.az,Pazel11min.alt, 'x', color='green', alpha = 1)

plt.plot(topo_pos_az,topo_pos_el,'o',label='From Celestrak sup TLEs with cysgp4')

plt.legend()
plt.xlabel('Az')
plt.ylabel('Alt')
plt.ylim([0,90])
plt.grid()
# plt.title(oem_file + '\nAzel position from \n'+ str(Obs.geodetic ))


#%% 7b - Using skyfield to propagate TLEs
from skyfield.api import Topos, load

# Load the TLE data from a file
TLES_skyf = load.tle_file('celestrak_sup_20230313.txt')

# load timescale
ts = load.timescale()
t_skyf = ts.from_astropy(pos_t2) #load the time vector we were using before

# Define the location of the observer
obs = Topos('30.7 S', '21.4 E',elevation_m=1000)

# Select the satellite to track
satellite = TLES_skyf[461]

# Get the satellites positions
position = satellite.at(t_skyf).position.km

# create the difference and get the altaz coordinates
# here it could be another coordinate system
alt_skyf, az_skyf, dif = (satellite - obs).at(t_skyf).altaz()

# plot all calculated AltAz in a same plot
plt.figure(figsize=[20,16])

plt.plot(Pazel.az,Pazel.alt,'.', color='red', label='Privateer')
plt.plot(Pazelmax.az,Pazelmax.alt, 'x', color='red', alpha = 1)
plt.plot(Pazelmin.az,Pazelmin.alt, 'x', color='red', alpha = 1)

plt.plot(Pazel12.az,Pazel12.alt,'s', color='orange', label = 'SpaceTrak Precessed Geocentric')
plt.plot(Pazel12max.az,Pazel12max.alt, 'x', color='orange', alpha = 1)
plt.plot(Pazel12min.az,Pazel12min.alt, 'x', color='orange', alpha = 1)

plt.plot(Pazel11.az,Pazel11.alt,'o', color= 'green', label = 'SpaceTrak GCRS')
plt.plot(Pazel11max.az,Pazel11max.alt, 'x', color='green', alpha = 1)
plt.plot(Pazel11min.az,Pazel11min.alt, 'x', color='green', alpha = 1)

plt.plot(topo_pos_az,topo_pos_el,'o',label='From Celestrak TLEs cysgp4')

plt.plot(az_skyf.degrees,alt_skyf.degrees,'o',label='From Celestrak TLEs skyfield')

plt.legend()
plt.xlabel('Az')
plt.ylabel('Alt')
plt.ylim([0,90])
plt.grid()


#%% 8- plot differences in position in az and el
mask = topo_pos_el>=0

error_priv_spacetrak = ((Pazel.az - Pazel11.az)**2 + (Pazel.alt - Pazel11.alt)**2)**0.5
error_tle_spacetrak = ((topo_pos_az*u.deg - Pazel11.az)**2 + (topo_pos_el*u.deg - Pazel11.alt)**2)**0.5
error_tleskyf_spacetrak = ((az_skyf.degrees*u.deg - Pazel11.az)**2 + (alt_skyf.degrees*u.deg - Pazel11.alt)**2)**0.5

plt.figure(figsize=[20,16])
plt.plot(error_priv_spacetrak[mask]*60,'-', label='privateer vs spacetrak')
plt.plot(error_tle_spacetrak[mask]*60,'-', label='sup TLEs (cysgp4) vs spacetrak')
plt.plot(error_tleskyf_spacetrak[mask]*60,'-', label='sup TLEs (skyfield) vs spacetrak')
plt.plot(error1_min[mask].to(u.arcmin),'-',label='Spacetrak uncertainty min')
plt.plot(error1_max[mask].to(u.arcmin),'-',label='Spacetrak uncertainty max')

plt.grid()
plt.xlabel('samples')
plt.ylabel('arrcmin')
plt.title('RMS Difference TLEs and privateer OEMs to spacetrak ephemeris')
plt.ylim([0,3*60])
plt.legend()


#%% histograms of errors
plt.figure()
plt.plot(error_priv_spacetrak[mask].value)
plt.plot(error_tle_spacetrak[mask].value)
plt.plot(error_tleskyf_spacetrak[mask].value)
plt.grid()
plt.xlabel('samples')
plt.ylabel('RMS error in deg')
