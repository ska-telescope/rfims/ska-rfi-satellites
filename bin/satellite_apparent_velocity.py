# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 02:10:11 2020


@author: f.divruno
@author: Braam Otto

Last updated: 29 March 2021
"""

import astropy.units as u
import astropy.constants as const
import numpy as np
import time
import matplotlib.pyplot as plt
import sys
import pycraf.protection as protection

sys.path += ['../src']

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'
if os.name == 'nt':
    os.system('cls')
else:
    os.system('clear')

import ska
from ska import  constellation, telescope


#%% Simulation parameters

# Simulation frequency
freq = 11.7 * u.GHz #use the centre freque as approximation

# number of times the constellation and tesselation is calculated
Niters = 1

# number of time steps
Ntime = 200

#length of a time step
time_step = 1

# channel bandwidth
BW = 200 * u.MHz

# avoidance angle 
avoid_angle = 0 * u.deg

#masking in the signal chain: a time vector could be defined when a satellite
# has a boresight angle less than a defined value
masking = False
boresight_limit = 1 * u.deg

#Power flagging
# flag the received power higher than this limit
# in a received power in a 4 kHz BW
# This value needs to be checked to understand if possible in the
# receiver.
power_flag = True
T = 20 * u.K # Kelvin
B = 1 *u.Hz#4e3 * u.Hz  # Hz (4 kHz)
# Boltzmann's Constant = 1.3806E-23 
Plim = (T*B*1.38e-23 * u.W * u.s / u.K).to(u.W)/u.Hz
print("Tsys = ", T)
print("BW = ", B)
print("Plim/Hz = kT = ", 10.*np.log10(Plim.value/1E-3), "dBm")

RA769_lim = protection.ra769_calculate_entry(freq,1*u.Hz,1e-8*u.K,T)[3]

print("Protection thresgolds as defined in RA.769: %.2f"%RA769_lim.value)

#Antenna receiver model
high_SLL = 1 # flag
SLL = 4 # dBi

#beam_type
# beam_type = 'constant'
beam_type = 'statistical'

#string to use in saving figures
sim_flags = 'apparent velocity'

print("Simulation Flags = ", sim_flags)
#%% 

# define the constellation
#%% define the telescope receiver
obs_name = 'SKA-Mid'
obs_geo = np.array([ 21.443800, -30.712919, 1.052]) # lon,lat,alt # deg, deg, km

TEL = telescope.Telescope(obs_name,
                          obs_geo)

# Constellation name:
const_name = 'Starlink ph1'
ConstX = constellation.Constellation(
    const_name,\
    TEL.name,\
    TEL.coord_geo,\
    beam_type = beam_type)


print("Constellation defined...done")




#%% positions of the satellites in the horizontal frame
ConstX.propagate(epoch = float(51544.5 + np.random.rand(1)*10),
                 iters = Niters,
                 time_steps = Ntime,
                 time_step = time_step,
                 verbose = False
                 )

#%% Calculate velocity in horizontal frame (azel)

Nsats = ConstX.topo_pos.shape[2]
horiz_vel_az = np.zeros([Niters,Ntime,Nsats])
horiz_vel_el = np.zeros([Niters,Ntime,Nsats])
horiz_vel_az[:,0:Ntime-1,:] = (ConstX.topo_pos[:,1::,:,0] - ConstX.topo_pos[:,0:-1,:,0])/time_step
horiz_vel_el[:,0:Ntime-1,:] = (ConstX.topo_pos[:,1::,:,1] - ConstX.topo_pos[:,0:-1,:,1])/time_step
horiz_vel_mod = np.sqrt(horiz_vel_az**2 + horiz_vel_el**2)
horiz_vel_el[abs(horiz_vel_el)>100] = 0  
horiz_vel_az[abs(horiz_vel_az)>100] = 0  
horiz_vel_mod[abs(horiz_vel_mod)>100] = 0  


vis_mask = ConstX.vis_mask


plt.figure()
plt.title('Azimuth and elevation speed wrt satellite elevation in horizontal frame')
plt.plot(ConstX.topo_pos[vis_mask,1],horiz_vel_az[vis_mask],'.',label = 'az vel')
plt.plot(ConstX.topo_pos[vis_mask,1],horiz_vel_el[vis_mask],'.',label = 'el vel')
plt.xlabel('sat elevation')
plt.ylabel('deg/sec')
plt.legend()
plt.grid()
plt.ylim([-4,4])


plt.figure()
plt.plot(ConstX.topo_pos[vis_mask,1],horiz_vel_mod[vis_mask],'.',label = 'mod vel')
plt.xlabel('sat elevation')
plt.ylabel('deg/sec')
plt.legend()
plt.grid()

# plt.figure()
# plt.tricontourf(ConstX.topo_pos[vis_mask,0],
#                 ConstX.topo_pos[vis_mask,1],
#                 horiz_vel_mod[vis_mask],levels=100,colormap='jet')

    
