# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 02:10:11 2020


@author: f.divruno


Last updated: 16/07/2022
"""

import astropy.units as u
import astropy.constants as const
import numpy as np
import time
import matplotlib.pyplot as plt
import sys
import pycraf.protection as protection
import pycraf

sys.path += ['../src']

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'
if os.name == 'nt':
    os.system('cls')
else:
    os.system('clear')

import ska
from ska import  constellation, telescope
import cysgp4
from datetime import datetime
import astropy
from astropy.coordinates import FK5, AltAz, FK4, ICRS, SkyCoord

font = {'family' : 'normal',
    'weight' : 'bold',
    'size'   : 22}

import matplotlib
matplotlib.rc('font', **font)


#%% load pointing file for VGOS observation
aux = open('vo2027.txt')
lines = aux.readlines()

Scan=[]
Line =[]
Source = []
Az =[]
El = []
Cable =[]
Start =[]
Stop = []
Dur = []
date = '2022Jan27'
for line in lines:
    if line[0:4] == 'date':
        yyyy = int(line[7:11])
        mm = 1
        dd = int(line[14:16])
        
    if line[0:2] == ' 0':
        Scan.append(line[1:10])
        Line.append(line[12:15])
        Source.append(line[16:24])
        Az.append(float(line[25:28]))
        El.append(float(line[29:31]))
        Cable.append(line[32:37])
        
        Start.append(cysgp4.PyDateTime(datetime(yyyy,mm,dd,int(line[39:41]),int(line[42:44]),int(line[45:47]))))
        Stop.append(cysgp4.PyDateTime(datetime(yyyy,mm,dd,int(line[49:51]),int(line[52:54]),int(line[55:57]))))
        Dur.append(float(line[63:65]))

Az = np.array(Az)
El = np.array(El)

Az[Az>=180] -= 360

fig = plt.figure('VGOS observing plan', figsize=(9, 6))
plt.plot(Az,El,'.')
plt.xlabel('Azimuth [deg]')
plt.ylabel('Elevation [deg]')
plt.xlim([-180,180])
# plt.xticks(np.linspace(0,360,10))
plt.xticks(np.linspace(-180,180,10))
plt.yticks(np.linspace(0,90,10))
plt.grid(True, 'both', linestyle='--')
plt.title('VGOS observation plan from Wetzell of %s'%Start[0])
# plt.tight_layout()
fig.savefig('figs/VGOS/ - observation_plan_azel.png')


# group the results per source
Sources_u = np.unique(Source)

source_index = []
for name in Sources_u:
    i_aux = []
    for i in range(len(Source)):
        if name in Source[i]:
            i_aux.append(i)
    source_index.append(i_aux)

#Calculate the ITRF coordinates
#observer
observer_name = 'Wetzell'
observer_geo = np.array((12.87889957, 49.14419937, 0.6)) # long,lat,alt, [deg, deg, km]
observer_diam = 13*u.m

#define observer:
O = astropy.coordinates.EarthLocation(observer_geo[0],observer_geo[1],observer_geo[2]*1e3)

Source_radec=np.zeros([len(source_index),2])*u.deg
Source_name=[]
for i,indx in enumerate(source_index):
    indx = indx[0]
    #define astropy time:
    epocha = astropy.time.Time(Start[indx].mjd,format='mjd')
    altaz = AltAz(location = O,
                  obstime = epocha)
    S = SkyCoord(Az[indx]*u.deg,
                 El[indx]*u.deg,
                 frame=altaz,
                 representation_type='spherical')

    ra = S.fk4.ra
    dec = S.fk4.dec
    
    
    if(ra>180*u.deg):
        ra -= 360*u.deg
    

    Source_radec[i] = (ra,dec)
    Source_name.append(Source[indx])

##
#make figure
fig = plt.figure(figsize=[14,13])
ax = fig.add_subplot(111, projection='aitoff' ) # hammer or mollweide, aitoff, or lambert. [example](http://matplotlib.org/examples/pylab_examples/geo_demo.html) 
ax.scatter((Source_radec[:,0]).to(u.rad),(Source_radec[:,1]).to(u.rad), marker='o', color='b')


ax.grid(True)
# ax.set_xticklabels(['14h','16h','18h','20h','22h','0h','2h','4h','6h','8h','10h']) #use if you want to change to a better version of RA. 
plt.xlabel('Ra')
plt.ylabel('Dec')
plt.show()
plt.title('Targets in VGOS schedule of %s'%Start[0])
fig.savefig('figs/VGOS/ -allsky_radec_aitoff.jpg')


#%% Simulation parameters


# Simulation frequency
freq = 11.7 * u.GHz #use the centre freque as approximation

# number of times the constellation and tesselation is calculated
Niters = 100

# number of time steps
Ntime = 100 #100

#length of a time step
time_step = 1 # for single antenna simulation
# time_step = 0.1 # for correlation simulation

# channel bandwidth
BW = 250 * u.MHz
RFI_BW = 2/8*u.GHz # Bandwidth of the RFI, in this case from 10.7-12.7 GHz
RX_analog_BW = 13*u.GHz# Bandwidth of the Receiver, for Band5b is 8.3 to 15.4 GHz
RX_digital_BW = 1*u.GHz


# avoidance angle 
# optional parameter only used for steerable beams constellations
avoid_angle = 0 * u.deg

# Masking:
# masking in the signal chain: a time vector could be defined when a satellite
# has a boresight angle less than a defined value
# In real life this requires good knowledge of the ephemerides of satellites and
# propagation of them.
masking = False
boresight_limit = 1 * u.deg

# Power flagging:
# flag the received power higher than this limit
# of received power in a 4 kHz BW
power_flag = True
T = 20 * u.K # Kelvin
B = 1 *u.Hz#4e3 * u.Hz  # Hz (4 kHz)
k_b = 1.3806E-23 
Plim = (T*k_b * u.W * u.s / u.K).to(u.W/u.Hz)

print("Tsys = ", T)
print("BW = ", B)
print("Plim/Hz = kT = ", Plim.to(u.dB(u.W/u.Hz)))


RA769_lim = protection.ra769_calculate_entry(freq,1*u.Hz,1e-8*u.K,T)[3]

print("Protection thresgolds as defined in RA.769: %.2f"%RA769_lim.value)

#Antenna receiver model
high_SLL = 0 # Increase the sidelobe level to account for uncertainties
SLL = 4 # dBi Value of sidelobes


#Options for constellations:
const_name = 'Starlink ph1'
# const_name = 'Starlink ph2'
# const_name = 'OneWeb ph1'
# const_name = 'OneWeb ph2'
# const_name = 'GW'
# const_name = 'GNSS'     #Genrated orbits with parameters
# const_name = 'Geo'


# constellations based on TLEs from Celestrak
# Actual satellites in orbit
# const_name = 'TLE-Starlink'
# const_name = 'TLE-Geo'
# const_name = 'TLE-Orbcomm'
# const_name = 'TLE-Iridium'
# const_name = 'TLE-GNSS'
# const_name = 'TLE-file'

# beam_type
# beam_type = 'constant'    
beam_type = 'statistical'   #uses an average pfd obtained from the script "steerable_beams_probability.py"
# beam_type = 'isotropic'   #-146 dBW/m2 is the nominal PFD of these Ku constellations
                            # in 4kHz BW

Nchannels = 1
if 'OneWeb ph1' in const_name:
    Nchannels = 8
    beam_type = 'fixed'


# Correlation control:
# do_correlation = False # Only calculates the received power by the Observer 0 and 1
do_correlation = True # Calculates the received power by the Observer 0 and 1 and the correlation


#Do the epfd calculation or load a saved file?
calculate_epfd = True 
method = 'Voltage'

#%% Define cells in the sky, observers and constellation
#Option1:  tesselate the sky (horizontal coordinates)
point_ra1, point_dec1,grid_info =\
    ska.general.sky_cells_m1583(niters=1, step_size=10 * u.deg,
                                lat_range=(-20 * u.deg, 90 * u.deg),
                                test = 0
                                )
point_ra1[point_ra1>=180] -= 360
point_ra0 = np.repeat(point_ra1[0,:,None],Ntime,axis=-1)*u.deg
point_dec0 = np.repeat(point_dec1[0,:,None],Ntime,axis=-1)*u.deg

#Option2: Calculate the Azel based on the celesial sources observed
point_ra1 = np.repeat(Source_radec[:,0,None],Ntime,axis=-1)
point_dec1 = np.repeat(Source_radec[:,1,None],Ntime,axis=-1)

#pointing selection:
point_ra = point_ra0 
point_dec = point_dec0

Ncells = point_dec.shape[0]
print("Number of Cells = ", Ncells)

#string to use in saving figures
sim_flags = '%d-%d-%d-%d-%d-%.4f'\
    %(do_correlation,high_SLL, Ncells, Niters, Ntime,time_step)

print("\nSave string : ", sim_flags)


fig = plt.figure(figsize=[20,15])
ax = fig.add_subplot(111, projection='aitoff' ) # hammer or mollweide, aitoff, or lambert. [example](http://matplotlib.org/examples/pylab_examples/geo_demo.html) 
ax.scatter((point_ra0[:,0]).to(u.rad),(point_dec0[:,0]).to(u.rad),
           marker='o', color='b', s=180,
           label = 'Pointings for simulation')
ax.scatter((point_ra1[:,0]).to(u.rad),(point_dec1[:,0]).to(u.rad),
           marker='s', color='r',s=180,
           label = 'VGOS schedule')

# ax.scatter((point_ra[:,0]).to(u.rad),(point_dec[:,0]).to(u.rad),
#            marker='x', color='g',s=300,
#            label = 'Pointings for simulation')

ax.grid(True)
# ax.set_xticklabels(['14h','16h','18h','20h','22h','0h','2h','4h','6h','8h','10h']) #use if you want to change to a better version of RA. 
plt.xlabel('Ra')
plt.ylabel('Dec')
plt.show()
plt.title('Source coordinates')
plt.legend(loc='upper right')
fig.savefig('figs/VGOS/ Sky coordinates.jpg')




#Observer:
# observer_name1 = 'Onsala OTT1'
# observer_geo1 = np.array(( 11.91975,57.393575, 0.02)) # long,lat,alt, [deg, deg, km]
# observer_diam1 = 13*u.m

# observer_name = 'Onsala OTT2'
# observer_geo = np.array(( 11.918958,57.393083, 0.02)) # long,lat,alt, [deg, deg, km]
# observer_diam = 13*u.m


# Observers:
observer_name = 'Wetzell'
observer_geo = np.array((12.87889957, 49.14419937, 0.6)) # long,lat,alt, [deg, deg, km]
observer_diam = 13*u.m

observer_name1 = 'Onsala'
observer_geo1 = np.array(( 11.917778,57.393056, 0.02)) # long,lat,alt, [deg, deg, km]
observer_diam1 = 13*u.m


#Define the constellation in Observer1:
Const = constellation.Constellation(
    const_name = const_name,\
    observer_name = observer_name,\
    observer_geo = observer_geo,\
    beam_type = beam_type,
    TLE_file = r'C:\Users\f.divruno\Dropbox (SKAO)\Python_codes\ska-rfi-satellites\bin\space-track-2022-01-26-27.txt')


#Define the constellation in observer2:
Const1 = constellation.Constellation(
    const_name = const_name,\
    observer_name = observer_name1,\
    observer_geo = observer_geo1,\
    beam_type = beam_type,
    TLE_file = r'C:\Users\f.divruno\Dropbox (SKAO)\Python_codes\ska-rfi-satellites\bin\space-track-2022-01-26-27.txt')
    
    
#initialize spfd function
Const.spfd_init(avd_angle = avoid_angle)
Const1.spfd_init(avd_angle = avoid_angle)

print("Constellation defined...done")

#%% plot spfd function:
if Const.name == 'OneWeb ph1':
    el_angle = np.linspace(-90,90,1000) 
    az_angle = np.zeros(1000)
    dist = np.ones(1000)*1200e3
    spfd_data = Const.spfd(el_angle,
                           az_angle,
                           dist).value*0.9 #in W/m2/Hz
else:
    el_angle = np.logspace(-1,np.log10(90),1000) 
    spfd_data = Const.spfd_fit(el_angle) #in W/m2/Hz
    # spfd_data = Const.spfd(el_angle).value #in W/m2/Hz #for isotropic
    
fig = plt.figure(figsize=(20,15))

plt.plot(el_angle,10*np.log10(spfd_data), '-b')
plt.title(Const.name+ ' spfd function',)
plt.xlabel('angle across track [deg]')
plt.ylabel('spfd [dB(W/m2/Hz)]')
plt.grid(True, 'both')
plt.tight_layout()

plt.axhline(y = -146 - 10*np.log10(4e3),color='red',label='nominal spfd = %.2f dBW/m2/Hz'%(-146 - 10*np.log10(4e3)))
plt.legend()
plt.ylim([-225,-175])
plt.xlim([-90,90])
fig.savefig('figs/VGOS/'+Const.name+'/'+Const.observer_name+' - spfd vs el function  - ' + sim_flags+'.jpg')
   


#%% define the telescope receiver
Observer = telescope.Telescope(observer_name,
                               observer_geo,
                               diam = observer_diam)

Observer1 = telescope.Telescope(observer_name1,
                               observer_geo1,
                               diam = observer_diam1)

print("Observer defined...done")

#plot observer antenna
theta = np.logspace(-3,np.log10(180),1000)*u.deg
RAS_G = Observer.gain(theta, observer_diam, freq, 100*u.percent,
                 high_SLL=high_SLL, SLL = SLL,
                 do_bessel=False) #in lin gain
RAS_G1 = Observer1.gain(theta, observer_diam1, freq, 100*u.percent,
                 high_SLL=high_SLL, SLL = SLL,
                 do_bessel=False) #in lin gain


fig = plt.figure('Observer antenna model', figsize=(9, 6))
plt.semilogx(theta,10*np.log10(RAS_G), '-k',label=observer_name)
plt.semilogx(theta,10*np.log10(RAS_G1), '-b',label=observer_name1)
plt.title(' Antenna Model')
plt.xlabel('Boresight Angle [$^o$]')
plt.ylabel('Gain [dBi]')
plt.grid(True, 'both')
plt.tight_layout()
plt.legend()
fig.savefig('figs/VGOS/'+Const.name+'/'+Const.observer_name+' -antenna gain - ' + sim_flags+'.jpg')

print("Plotted observer antenna pattern")


#%% Select calculation method 1: Adding voltages


if method == 'Voltage':
    print('Calculating EPFD as Voltage addition')
    input('Press enter...')
    
    #Output vectors for the calculation
    Vtot_iters = np.zeros([Niters, Ncells, Ntime, Nchannels])* (np.nan+1j*np.nan) #* u.W / u.Hz 
    Vtot_iters1 = np.copy(Vtot_iters)
    Vtot_corr_iters= np.copy(Vtot_iters)
    
    
    #define observers for astropy to change coordinate system:
    O = astropy.coordinates.EarthLocation(observer_geo[0],observer_geo[1],observer_geo[2]*1e3)
    O1 = astropy.coordinates.EarthLocation(observer_geo1[0],observer_geo1[1],observer_geo1[2]*1e3)            
    
    plt.figure('fig_pointing',figsize=(10, 8))
    plt.title(Const.observer_name + ' - Azel pointing of telescopes')    
    plt.ylim([0,90])
    plt.figure('fig_pointing1',figsize=(10, 8))
    plt.title(Const1.observer_name + ' - Azel pointing of telescopes')
    plt.ylim([0,90])
    
    rand_seed = 5 #Using a random seed for repeatability   
    
    
    if calculate_epfd == False:
        # to get the values back from saved values
        # aux = np.load('out/'+Constellation_name+'/out '+sim_flags[-5] + '.npz')
        print('Loading the EPFD calculation:')
        filename = 'out/VGOS/'+const_name+'/EPFD '+\
                      Const.name + '-'+Const.observer_name+'-'+Const1.observer_name+'-'+sim_flags + '.npz'
        aux = np.load(filename)
        PSD_iters = np.array(aux['PSD_iters_W_Hz']) * (u.W/u.Hz)
        PSD_iters1 = aux['PSD_iters1_W_Hz'] * (u.W/u.Hz)
        PSD_corr_iters =aux['PSD_corr_iters_W_Hz'] * (u.W/u.Hz)
    
        print('loaded:') 
        print('PSD_iters',PSD_iters.shape)
        print('PSD_iters1',PSD_iters1.shape)
        print('PSD_corr_iters',PSD_corr_iters.shape)
        
    else:
        print('Starting EPFD calculation:')
    
        for k in range(Niters):
            print('iteration %d of %d'%(k,Niters))
            #start epoch:
            np.random.seed(rand_seed+k)    
            epoch = Start[0].mjd + np.random.rand(1)[0]*2
            
            print("Calculating positions of constellation...")
            # positions of the satellites in the duration of the simulation
            Const.propagate(epoch = epoch,
                              iters = 1,
                              time_steps = Ntime,
                              time_step = time_step,
                              verbose = False,
                              rand_seed = rand_seed+k
                              )
            Const1.propagate(epoch = epoch,
                              iters = 1,
                              time_steps = Ntime,
                              time_step = time_step,
                              verbose = False,
                              rand_seed = rand_seed+k
                              )
            print("Done")
            
            #distance difference from each satellite to Observer and to Observer1
            
    
            # SPECTRAL POWER FLUX DENSITY
            # ---------------------------
            # spfd generated by each satellite visible
            # the statistical function only has the elevation
            # of the satellite as a parameter
            
            #random phase for each satellite
            np.random.seed(rand_seed+k)
            ph_sat = np.random.rand(Const.topo_pos.shape[2])*np.pi*2
                
            #with phasors
            # V_sat = np.sqrt(Const.spfd(all_flag=True))*\
            #     np.exp(1j*(2*np.pi*u.rad/(const.c/freq)*Const.topo_pos[...,2,None]*1e3*u.m).value+
            #            ph_sat[None,None,:,None])
    
            # V_sat1 = np.sqrt(Const1.spfd(all_flag=True))*\
            #     np.exp(1j*(2*np.pi*u.rad/(const.c/freq)*Const1.topo_pos[...,2,None]*1e3*u.m).value + 
            #            ph_sat[None,None,:,None])
            
            ph = (2*np.pi*u.rad/(const.c/freq)*Const.topo_pos[...,2,None]*1e3*u.m).value
            ph1 = (2*np.pi*u.rad/(const.c/freq)*Const1.topo_pos[...,2,None]*1e3*u.m).value
            V_sat = np.sqrt(Const.spfd(all_flag=True))*np.exp(1j*ph)
            V_sat1 = np.sqrt(Const1.spfd(all_flag=True))*np.exp(1j*ph1)

    
                
    
            mask = Const.vis_mask + Const1.vis_mask
            
            V_sat = V_sat[mask]
            V_sat1 = V_sat1[mask]
            
            #pointing direction of the radio telescope
            # Calculating the azel from the telescope location for each timestep:
                
            #define astropy time: considers that all observations happen at the same
            # time, this is maybe not realistic but saves a lot of time of simulation
            
            # astrotime = astropy.time.Time(epoch+np.linspace(0,Ntime*time_step,Ntime)/60/60/24,
            #                               format='mjd')
            
            astrotime = astropy.time.Time(
                np.repeat((epoch+np.linspace(0,Ntime*time_step,Ntime)/60/60/24)[None],
                          Ncells,axis=0),format='mjd')          
            
            #define altaz frame at this epoch
            print("Calculating altaz...")
            altaz = astropy.coordinates.AltAz( location = O,
                                                obstime = astrotime)
    
    
            altaz1 = astropy.coordinates.AltAz( location = O1,
                                                obstime = astrotime)
    
            
            # Define the SkyCoord element with the FK4 coordinates
            S = astropy.coordinates.SkyCoord(
                point_ra,
                point_dec,
                frame=FK4
                )
            
            
            
            # calculate the Az el:
            S1 = S.transform_to(altaz)
            p_el = S1.alt.value
            p_az = S1.az.value
            
    
            S2 = S.transform_to(altaz1)
            p_el1 = S2.alt.value
            p_az1 = S2.az.value
    
            print("Done")
    
            
            plt.figure('fig_pointing')
            plt.plot(p_az.flatten(),p_el.flatten(),'.',label=observer_name1) 
            
    
            plt.figure('fig_pointing1')
            plt.plot(p_az1.flatten(),p_el1.flatten(),'.',label=observer_name1)              
            
                
            Ncells = p_el.shape[0]
    
            #azel of satellites  #works well, can using mask array be faster?
            sat_az = Const.topo_pos[mask,0]
            sat_el = Const.topo_pos[mask,1]
            
    
            sat_az1 = Const1.topo_pos[mask,0]
            sat_el1 = Const1.topo_pos[mask,1]
    
    
            Nsats = Const.topo_pos.shape[2]
                        
            #power spectral density in all the pointing direction
            # PSD_rx_sparse = np.zeros([Ncells,1, Ntime, Nsats])* u.W / u.Hz 
                
                
            # angular distance from the RAS pointing to the satellite position
            print("Calculating boreshigt angle in the RAS pattern...")            
       
    
            #expand the pointing vectors to be able to use the satellite mask, this will linearize number of satellites and times to use only
            # satellites that are visible.
            p_az_1 = np.repeat(p_az[...,None],Nsats,axis=-1)
            p_az_1 = p_az_1[:,mask[0,...]]
         
            p_el_1 = np.repeat(p_el[...,None],Nsats,axis=-1)
            p_el_1 = p_el_1[:,mask[0,...]]
    
            p_az1_1 = np.repeat(p_az1[...,None],Nsats,axis=-1)
            p_az1_1 = p_az1_1[:,mask[0,...]]        
            p_el1_1 = np.repeat(p_el1[...,None],Nsats,axis=-1)
            p_el1_1 = p_el1_1[:,mask[0,...]]        
            
    
    
    
            
    
            theta = pycraf.geometry.true_angular_distance(p_az_1*u.deg,
                                          p_el_1*u.deg,
                                          sat_az[None]*u.deg,
                                          sat_el[None]*u.deg)
            
            theta1 = pycraf.geometry.true_angular_distance(p_az1_1*u.deg,
                                          p_el1_1*u.deg,
                                          sat_az1[None]*u.deg,
                                          sat_el1[None]*u.deg)
                
         
    
            print("Done")
    
    
    
            
            #Gain of the RAS antenna towards the satellites
            print("Calculating Antenna Gain...")            
            RAS_G = Observer.gain(theta, observer_diam, freq, 100*u.percent,
                              high_SLL=high_SLL, SLL = SLL,
                              do_bessel=False) #in lin gain
    
    
            RAS_G1 = Observer.gain(theta1, observer_diam1, freq, 100*u.percent,
                              high_SLL=high_SLL, SLL = SLL,
                              do_bessel=False) #in lin gain
            print("Done")
    
            
            # Telescope masking 
            if masking:
                RAS_G[theta < boresight_limit] = np.nan
                RAS_G1[theta1 < boresight_limit] = np.nan
                # when the summation is done in the satellite axis,
                # a flagged satellite in the main beam flags all the time step
                # as the telescope receives the power from all satellites
            
            #effective area according to the calculated gain towards the satellite        
            A_eff = RAS_G * (((const.c/freq).to_value(u.m))**2/4/np.pi)  #* u.m**2
            A_eff1 = RAS_G1 * (((const.c/freq).to_value(u.m))**2/4/np.pi)  #* u.m**2
            
    
            #received Voltage 
            Vrx = V_sat[None] * np.sqrt(A_eff[...,None])
            Vrx1 = V_sat1[None] * np.sqrt(A_eff1[...,None])
      
            #Expand into sparse matrices to be able to sum in satellite dimension
            Vrx_expanded = np.ones((Ncells,Ntime,Nsats,Nchannels))*(np.nan+1j*np.nan)
            Vrx1_expanded = np.ones((Ncells,Ntime,Nsats,Nchannels))*(np.nan+1j*np.nan)
            Vrx_expanded[:,mask[0],:] = Vrx
            Vrx1_expanded[:,mask[0],:] = Vrx1
    

 
        
            # sum in satellite dimension [Niters,Ncells,Ntime]
            # still power spectral density        
            aux = (np.nansum(Vrx_expanded, axis=-2)) # add in satellite axis
            aux[aux==0] = np.nan+1j*np.nan
            Vtot_iters[k] = aux #* np.sqrt(u.W/u.Hz)
    
            aux1 = (np.nansum(Vrx1_expanded, axis=-2)) # add in satellite axis
            aux1[aux1==0] = np.nan+1j*np.nan
            Vtot_iters1[k] = aux #* np.sqrt(u.W/u.Hz) 
    
    #   DEBUG:   
            debug=0
            if debug==1:
                ind = np.where(~np.isnan(Vrx_expanded))
                i = 100
                plt.figure(figsize=[20,15])
                plt.plot(np.angle(aux[i])*180/np.pi,'-.')
                plt.plot(np.angle(aux1[i])*180/np.pi,'-.')
                plt.title('angle sum voltage')
                
                plt.figure(figsize=[20,15])
                plt.plot(np.real(aux[i]),'-.')
                plt.plot(np.real(aux1[i]),'-.')
                plt.title('Real Voltage sum voltage')
        
                plt.figure(figsize=[20,15])
                plt.plot(np.abs(aux[i])**2,'-.')
                plt.plot(np.abs(aux1[i])**2,'-.')
                plt.plot(np.real(aux[i]*aux1[i]),'-.')
                plt.plot(np.imag(aux[i]*aux1[i]),'-.')
                plt.title('Power sum voltage')
    
    
            if do_correlation:
                aux = (aux*aux1)
                aux[aux==0] = np.nan+1j*np.nan            
                Vtot_corr_iters[k] = aux #* u.W/u.Hz
    
        PSD_iters = abs(Vtot_iters)**2 *u.W/u.Hz
        PSD_iters1 = abs(Vtot_iters1)**2 *u.W/u.Hz
        PSD_corr_iters = np.real(Vtot_corr_iters)*u.W/u.Hz
    
#%% Select calculation method 2: Adding powers

if method == 'Power':
    print('Calculating EPFD as power addition')
    input('Press enter...')
    #Output vectors for the calculation
    PSD_iters = np.zeros([Niters, Ncells, Ntime, Nchannels]) *np.nan * u.W / u.Hz 
    PSD_iters1 = np.copy(PSD_iters)
    PSD_corr_iters= np.copy(PSD_iters)
    
    #define observers for astropy to change coordinate system:
    O = astropy.coordinates.EarthLocation(observer_geo[0],observer_geo[1],observer_geo[2]*1e3)
    O1 = astropy.coordinates.EarthLocation(observer_geo1[0],observer_geo1[1],observer_geo1[2]*1e3)            
    
    plt.figure('fig_pointing',figsize=(10, 8))
    plt.title(Const.observer_name + ' - Azel pointing of telescopes')    
    plt.ylim([0,90])
    plt.figure('fig_pointing1',figsize=(10, 8))
    plt.title(Const1.observer_name + ' - Azel pointing of telescopes')
    plt.ylim([0,90])
    
    rand_seed = 5 #Using a random seed for repeatability   
    
    
    if calculate_epfd == False:
        # to get the values back from saved values
        # aux = np.load('out/'+Constellation_name+'/out '+sim_flags[-5] + '.npz')
        print('Loading the EPFD calculation:')
        filename = 'out/VGOS/'+const_name+'/EPFD '+\
                      Const.name + '-'+Const.observer_name+'-'+Const1.observer_name+'-'+sim_flags + '.npz'
        aux = np.load(filename)
        PSD_iters = np.array(aux['PSD_iters_W_Hz']) * (u.W/u.Hz)
        PSD_iters1 = np.array(aux['PSD_iters1_W_Hz']) * (u.W/u.Hz)
        PSD_corr_iters = np.array(aux['PSD_corr_iters_W_Hz']) * (u.W/u.Hz)
    
        print('loaded:') 
        print('PSD_iters',PSD_iters.shape)
        print('PSD_iters1',PSD_iters1.shape)
        print('PSD_corr_iters',PSD_corr_iters.shape)
        
    else:
        print('Starting EPFD calculation:')
    
        for k in range(Niters):
            print('iteration %d of %d'%(k,Niters))
            #start epoch:
            np.random.seed(rand_seed+k)    
            epoch = Start[0].mjd + np.random.rand(1)[0]*2
            
            print("Calculating positions of constellation...")
            # positions of the satellites in the duration of the simulation
            Const.propagate(epoch = epoch,
                              iters = 1,
                              time_steps = Ntime,
                              time_step = time_step,
                              verbose = False,
                              rand_seed = rand_seed+k
                              )
            Const1.propagate(epoch = epoch,
                              iters = 1,
                              time_steps = Ntime,
                              time_step = time_step,
                              verbose = False,
                              rand_seed = rand_seed+k
                              )
            print("Done")
            
            #distance difference from each satellite to Observer and to Observer1
            
    
            # SPECTRAL POWER FLUX DENSITY
            # ---------------------------
            # spfd generated by each satellite visible
            # the statistical function only has the elevation
            # of the satellite as a parameter
            
            #random phase for each satellite
            # np.random.seed(rand_seed+k)
            # ph_sat = np.random.rand(Const.topo_pos.shape[2])*np.pi*2
                
            #with phasors
            spfd_sat = Const.spfd(all_flag=True)
            spfd_sat1 = Const1.spfd(all_flag=True)
                
                            
    
            mask = Const.vis_mask + Const1.vis_mask
            
            spfd_sat = spfd_sat[mask]
            spfd_sat1 = spfd_sat1[mask]
            
            #pointing direction of the radio telescope
            # Calculating the azel from the telescope location for each timestep:
                
            #define astropy time: considers that all observations happen at the same
            # time, this is maybe not realistic but saves a lot of time of simulation
            
            # astrotime = astropy.time.Time(epoch+np.linspace(0,Ntime*time_step,Ntime)/60/60/24,
            #                               format='mjd')
            
            astrotime = astropy.time.Time(
                np.repeat((epoch+np.linspace(0,Ntime*time_step,Ntime)/60/60/24)[None],
                          Ncells,axis=0),format='mjd')          
            
            #define altaz frame at this epoch
            print("Calculating altaz...")
            altaz = astropy.coordinates.AltAz( location = O,
                                                obstime = astrotime)
    
    
            altaz1 = astropy.coordinates.AltAz( location = O1,
                                                obstime = astrotime)
    
            
            # Define the SkyCoord element with the FK4 coordinates
            S = astropy.coordinates.SkyCoord(
                point_ra,
                point_dec,
                frame=FK4
                )
            
            
            
            # calculate the Az el:
            S1 = S.transform_to(altaz)
            p_el = S1.alt.value
            p_az = S1.az.value
            
    
            S2 = S.transform_to(altaz1)
            p_el1 = S2.alt.value
            p_az1 = S2.az.value
    
            print("Done")
    
            
            plt.figure('fig_pointing')
            plt.plot(p_az.flatten(),p_el.flatten(),'.',label=observer_name1) 
            
    
            plt.figure('fig_pointing1')
            plt.plot(p_az1.flatten(),p_el1.flatten(),'.',label=observer_name1)              
            
                
            Ncells = p_el.shape[0]
    
            #azel of satellites  #works well, can using mask array be faster?
            sat_az = Const.topo_pos[mask,0]
            sat_el = Const.topo_pos[mask,1]
            
    
            sat_az1 = Const1.topo_pos[mask,0]
            sat_el1 = Const1.topo_pos[mask,1]
    
    
            Nsats = Const.topo_pos.shape[2]
                        
            #power spectral density in all the pointing direction
            # PSD_rx_sparse = np.zeros([Ncells,1, Ntime, Nsats])* u.W / u.Hz 
                
                
            # angular distance from the RAS pointing to the satellite position
            print("Calculating boreshigt angle in the RAS pattern...")            
       
    
            #expand the pointing vectors to be able to use the satellite mask, this will linearize number of satellites and times to use only
            # satellites that are visible.
            p_az_1 = np.repeat(p_az[...,None],Nsats,axis=-1)
            p_az_1 = p_az_1[:,mask[0,...]]
         
            p_el_1 = np.repeat(p_el[...,None],Nsats,axis=-1)
            p_el_1 = p_el_1[:,mask[0,...]]
    
            p_az1_1 = np.repeat(p_az1[...,None],Nsats,axis=-1)
            p_az1_1 = p_az1_1[:,mask[0,...]]        
            p_el1_1 = np.repeat(p_el1[...,None],Nsats,axis=-1)
            p_el1_1 = p_el1_1[:,mask[0,...]]        
            
    
    
    
            
    
            theta = pycraf.geometry.true_angular_distance(p_az_1*u.deg,
                                          p_el_1*u.deg,
                                          sat_az[None]*u.deg,
                                          sat_el[None]*u.deg)
            
            theta1 = pycraf.geometry.true_angular_distance(p_az1_1*u.deg,
                                          p_el1_1*u.deg,
                                          sat_az1[None]*u.deg,
                                          sat_el1[None]*u.deg)
                
         
    
            print("Done")
    
    
    
            
            #Gain of the RAS antenna towards the satellites
            print("Calculating Antenna Gain...")            
            RAS_G = Observer.gain(theta, observer_diam, freq, 100*u.percent,
                              high_SLL=high_SLL, SLL = SLL,
                              do_bessel=False) #in lin gain
    
    
            RAS_G1 = Observer.gain(theta1, observer_diam1, freq, 100*u.percent,
                              high_SLL=high_SLL, SLL = SLL,
                              do_bessel=False) #in lin gain
            print("Done")
    
            
            # Telescope masking 
            if masking:
                RAS_G[theta < boresight_limit] = np.nan
                RAS_G1[theta1 < boresight_limit] = np.nan
                # when the summation is done in the satellite axis,
                # a flagged satellite in the main beam flags all the time step
                # as the telescope receives the power from all satellites
            
            #effective area according to the calculated gain towards the satellite        
            A_eff = RAS_G * (((const.c/freq).to_value(u.m))**2/4/np.pi)  * u.m**2
            A_eff1 = RAS_G1 * (((const.c/freq).to_value(u.m))**2/4/np.pi)  * u.m**2
            
    
            #received Voltage 
            Prx = spfd_sat[None] * (A_eff[...,None])
            Prx1 = spfd_sat1[None] * (A_eff1[...,None])
      
            #Expand into sparse matrices to be able to sum in satellite dimension
            Prx_expanded = np.ones((Ncells,Ntime,Nsats,Nchannels)) *np.nan
            Prx1_expanded = np.ones((Ncells,Ntime,Nsats,Nchannels))*np.nan
            Prx_expanded[:,mask[0],:] = Prx
            Prx1_expanded[:,mask[0],:] = Prx1
    
 

            # sum in satellite dimension [Niters,Ncells,Ntime]
            # still power spectral density        
            aux = (np.nansum(Prx_expanded, axis=-2)) # add in satellite axis
            aux[aux==0] = np.nan
            PSD_iters[k] = aux * u.W/u.Hz
    
            aux1 = (np.nansum(Prx1_expanded, axis=-2)) # add in satellite axis
            aux1[aux1==0] = np.nan
            PSD_iters1[k] = aux1 * u.W/u.Hz

    #   DEBUG:   
            debug=0
            if debug==1:
    
                ind = np.where(~np.isnan(Prx_expanded))
                i = 100
                plt.figure(figsize=[20,15])
                plt.plot(np.angle(aux[i])*180/np.pi,'-.')
                plt.plot(np.angle(aux1[i])*180/np.pi,'-.')
                plt.title('angle sum power')
                
                plt.figure(figsize=[20,15])
                plt.plot(np.real(aux[i]),'-.')
                plt.plot(np.real(aux1[i]),'-.')
                plt.title('Real Power sum power')
        
                plt.figure(figsize=[20,15])
                plt.plot(np.abs(aux[i]),'-.')
                plt.plot(np.abs(aux1[i]),'-.')
                plt.plot(np.abs(np.sqrt(aux[i])*np.sqrt(aux1[i])),'-.')
                plt.title('Power sum power')
    
    
            if do_correlation:
                aux = (np.sqrt(aux)*np.sqrt(aux1))
                aux[aux==0] = np.nan            
                PSD_corr_iters[k] = np.abs(aux) * u.W/u.Hz       
                

#%% Plot pointings and satellites in localc sky
    plt.figure('fig_pointing')
    plt.grid()
    plt.savefig('figs/VGOS/'+Const.name+'/'+Const.observer_name+' - pointing in Azel- ' + sim_flags+'.jpg')
    
    
    plt.figure('fig_pointing1')
    plt.grid()
    plt.savefig('figs/VGOS/'+Const1.name+'/'+Const1.observer_name+' - pointing in Azel- ' + sim_flags+'.jpg')
    
        
    #% Plot satellites in azel and sources in azel:
    
    #fig_pointing contains the azel pointings of the antenna so far
    Const.plot_topo_view(figname='fig_pointing')
    plt.savefig('figs/VGOS/'+Const.name+'/'+Const.observer_name+' - pointing in Azel with sats- ' + sim_flags+'.jpg')
    
    
    Const1.plot_topo_view(figname='fig_pointing1')
    plt.savefig('figs/VGOS/'+Const1.name+'/'+Const1.observer_name+' - pointing in Azel with sats- ' + sim_flags+'.jpg')

#%% Saving the epfd calculation 
    print('Saving the EPFD calculation:')
    filename = 'out/VGOS/'+const_name+'/EPFD Voltage'+\
                  Const.name + '-'+Const.observer_name+'-'+Const1.observer_name+'-'+sim_flags + '.npz'
    if method == 'Voltage':
        aux = np.savez(filename,
                    Vtot_iters = Vtot_iters,
                    Vtot_iters1 = Vtot_iters1,
                    Vtot_corr_iters = Vtot_corr_iters,
                    point_dec = point_dec)

    if method == 'Power':
        aux = np.savez(filename,
                    PSD_iters_W_Hz = PSD_iters.value,
                    PSD_iters1_W_Hz = PSD_iters1.value,
                    PSD_corr_iters_W_Hz = PSD_corr_iters.value,
                    point_dec = point_dec)

    print('Saved EPFD calculation in:')
    print(filename)

#%% 1- Calculate Full Band Data Loss (FBDL)

if 'OneWeb' in Const.name:
    Nch = 1
else:
    Nch = 8
    
P_FBDL = np.transpose((np.nansum(PSD_iters,axis=-1)*Nch*250*u.MHz).to(u.dB(u.mW)),[1,0,2])
P_FBDL1 = np.transpose((np.nansum(PSD_iters1,axis=-1)*Nch*250*u.MHz).to(u.dB(u.mW)),[1,0,2])

P_FBDL = np.reshape(P_FBDL,[277,P_FBDL.shape[1]*P_FBDL.shape[2]])
P_FBDL1 = np.reshape(P_FBDL1,[277,P_FBDL1.shape[1]*P_FBDL1.shape[2]])

flag_FB = P_FBDL >= -50*u.dB(u.mW)
flag_FB1 = P_FBDL1 >= -50*u.dB(u.mW) 

plt.figure(figsize=(20, 15))
ax = plt.subplot(211)

ax.plot(point_dec[:,0],np.nanmax(P_FBDL,axis=1).value,'.',
        markersize=20,
        label = 'max',
        color= 'r',alpha=1)

ax.plot(point_dec[:,0],np.nanmean(P_FBDL.value,axis=1),'.',
        label= 'mean',
        linewidth=1,
        markersize=20,color='b',alpha=1)

ax.plot(point_dec[:,0],np.nanmin(P_FBDL,axis=1).value,'.',
        markersize=20,
        label = 'min',
        color= 'g',alpha=1)


ax1 = plt.subplot(212)

ax1.plot(point_dec[:,0],np.nanmax(P_FBDL1,axis=1).value,'.',
        markersize=20,
        label = 'max',
        color= 'r',alpha=1)

ax1.plot(point_dec[:,0],np.nanmean(P_FBDL1.value,axis=1),'.',
        label= 'mean',
        linewidth=1,
        markersize=20,color='b',alpha=1)

ax1.plot(point_dec[:,0],np.nanmin(P_FBDL1,axis=1).value,'.',
        markersize=20,
        label = 'min',
        color= 'g',alpha=1)


ax.set_title(Const.observer_name)
ax1.set_title(Const1.observer_name)
ax.axhline(y=-30, color = 'red', label = 'Saturation limit = -30 dBm')
ax.axhline(y=-50, color = 'green', label = 'Linearity limit = -50 dBm')
ax.set_ylabel('Total power [dBm]')

ax.grid()

ax1.axhline(y=-30, color = 'red', label = 'Saturation limit = -30 dBm')
ax1.axhline(y=-50, color = 'green', label = 'Linearity limit = -50 dBm')
ax1.set_ylabel('Total power [dBm]')
ax1.set_xlabel('Declination [deg]')
ax1.grid()

ax.legend()
ax1.legend()

plt.savefig('figs/VGOS/'+Const.name+'/Maximum instantaneous power received by one antenna '+ sim_flags + '.png')

#%% 2- Calculate Digitizer data loss (DDL)

# PSD > than 10% of noise power spectral density Plim

flags = np.sum(np.sum(PSD_iters> Plim,axis=0),axis=1)/(Ntime*Niters)*100
flags1 = np.sum(np.sum(PSD_iters1> Plim,axis=0),axis=1)/(Ntime*Niters)*100


plt.figure(figsize=(20, 16))
ax = plt.subplot(211)
ax.plot(point_dec[:,0],flags,'.',
        label= Const.observer_name,
        linewidth=1,
        markersize=20)#,'r',alpha=0.03)

ax2 = plt.subplot(212)
ax2.plot(point_dec[:,0],flags1,'.',
        label= Const1.observer_name,
        linewidth=1,
        markersize=20)#,'r',alpha=0.03)
ax.grid()
ax2.grid()
ax.set_ylim([0,10])
ax2.set_ylim([0,10])
ax.set_ylabel('Flagged %')
ax2.set_ylabel('Flagged %')
ax2.set_xlabel('Declination [deg]')
ax.legend()
ax2.legend()
ax.set_title(Const.name+' - Flagged percentage - PSD > PSD_noise')
plt.savefig('figs/VGOS/'+Const.name+'/flagged percentage '+ sim_flags + '.png')



#%% 3- Calculate power spectral flux density in Jy

flag = PSD_iters> Plim*1
flag1 = PSD_iters1> Plim*1

# flag = PSD_iters> Plim*0.1
# flag1 = PSD_iters1> Plim*0.1


flag_c = flag + flag1
PSD_corr_flagged = np.copy(PSD_corr_iters)
PSD_flagged = np.copy(PSD_iters)
PSD_flagged1 = np.copy(PSD_iters1)

flag_perc = np.sum(np.sum(flag_c,axis=0),axis=1)/flag_c.shape[0]/flag_c.shape[2]*100
PSD_corr_flagged[flag_c] = np.nan
PSD_flagged[flag_c] = np.nan
PSD_flagged1[flag_c] = np.nan


A_eff = 0.8*np.pi*observer_diam**2/4

Jy = np.abs(np.mean(np.nanmean(PSD_flagged,axis=0),axis=1)) / A_eff
Jy1 = np.abs(np.mean(np.nanmean(PSD_flagged1,axis=0),axis=1)) / A_eff
Jy_corr = np.abs(np.mean(np.nanmean(PSD_corr_flagged,axis=0),axis=1))/ A_eff

plt.figure(figsize=(20, 16))
ax = plt.subplot(311)
ax.plot(point_dec[:,0], Jy.to_value((u.Jy)),'.',
        #label=Const.observer_name,
        linewidth=1,
        markersize=20)

ax2 = plt.subplot(312)
ax2.plot(point_dec[:,0], Jy1.to_value((u.Jy)),'.',
        #label=Const1.observer_name,
        linewidth=1,
        markersize=20)

ax3 = plt.subplot(313)
ax3.plot(point_dec[:,0],flag_perc,'.',
        #label= '% of flags',
        linewidth=1,
        markersize=20)


ax.grid()
ax2.grid()
ax3.grid()

ax.set_ylim([0,300])
ax2.set_ylim([0,300])
ax3.set_ylim([0,20])

ax.axhline(y=10**(-19.3)/19.6e3/1e-26,color='red', label='RA.769 VLBI thresholds')
ax2.axhline(y=10**(-19.3)/19.6e3/1e-26,color='red', label='RA.769 VLBI thresholds')

ax.set_ylabel('spfd [Jy]')
ax2.set_ylabel('spfd [Jy]')
ax3.set_ylabel('flags [%]')
ax3.set_xlabel('Declination [deg]')
ax.set_title(Const.observer_name)
ax2.set_title(Const1.observer_name)
ax3.set_title('Flagged %')
plt.suptitle(Const.name+' - equivalent spfd in Jy ')
# plt.savefig('figs/VGOS/'+Const.name+'/'+Const.observer_name+' flagged percentage '+ sim_flags + '.png')

#%% 4- Histogram and CDF 
# calculate the histogram of the received total power per sample
    

fig = plt.figure('histogram', figsize=(20, 15))
fig = plt.figure('CDF', figsize=(20, 15))
Nhist = 50

aux = PSD_iters1[...,0] #first channel
obs_name = Const1.observer_name

# calculate the histogram of each iteration:
for i  in range(Niters):
    
    ind = ~((aux[i] == 0*u.W) + np.isnan(aux[i])) #find all valid indexes
    
    P_hist = (aux[i,ind]).to(u.dB(u.W/u.Hz)).value

    #histogram
    a,b = np.histogram(P_hist,Nhist)#, histtype = 'step', density='True')
    plt.figure('histogram')
    plt.bar((b[1:]+b[0:-1])/2,a/np.sum(a)*100,width=(b[1:]-b[0:-1]),
            color='r',alpha=0.1)#,label = 'Integrated satellite RFI in %d GHz BW'%(RFI_BW/1e9))

    #CDF
    a = a/np.sum(a)*100
    cdf = np.zeros(len(a)) 
    for i in range(len(a)):
        cdf[i] = np.sum(a[0:i])
    plt.figure('CDF')
    plt.plot((b[1:]+b[0:-1])/2, cdf,alpha = 0.05,color='r')


#Calculate the histogram of all the iterations together:
aux = aux.flatten().to_value(u.dB(u.W/u.Hz)) # np.mean(Prx_iters,axis=0).to_value(u.dB(u.mW))
ind = ~((aux == 0*u.W) + ~np.isreal(aux) + ~np.isfinite(aux))

#Histogram:
a,b = np.histogram(aux[ind],Nhist)#, histtype = 'step', density='True')


plt.figure('histogram')
plt.plot((b[1:]+b[0:-1])/2,a/np.sum(a)*100,
        '-.',
        color='b',
        label = 'All iterations')


plt.grid(True, 'both')
plt.xlabel('Received psd in one satellite channel [dB(W/Hz)]')
plt.ylabel('% of occurences')
plt.title('Histogram of Rx psd - '+ str(Const.name) + '-' + obs_name)
plt.axvline(x=(Plim*0.1).to_value(u.dB(u.W/u.Hz)),
            label = 'Noise power',
            color='green')
plt.legend()
plt.savefig('figs/VGOS/'+Const.name+'/'+obs_name+\
            'histogram time domain psd - ' + str(sim_flags)+'.png')


#CDF
a = a/np.sum(a)*100
cdf = np.zeros(len(a)) 
for i in range(len(a)):
    cdf[i] = np.sum(a[0:i])
    
    
plt.figure('CDF')
plt.plot((b[1:]+b[0:-1])/2, cdf, color = 'b')

plt.axvline(x=(Plim*0.1).to_value(u.dB(u.W/u.Hz)),
            label = 'Noise power',
            color='red')

ind_plim = np.where((b[1:]+b[0:-1])/2>(Plim*0.1).to_value(u.dB(u.W/u.Hz)))[0][0]
plt.axhline(y = cdf[ind_plim],
            color = 'black')


plt.annotate('%.2f %% of values less than noise psd'%cdf[ind_plim],
             [(Plim*0.1).to_value(u.dB(u.W/u.Hz))+5,cdf[ind_plim]-10])

plt.grid(True, 'both')
plt.xlabel('Received psd  [dB(W/Hz)]')
plt.ylabel('CDF')
plt.title('CDF of received psd '+ str(Const.name) + '-' + obs_name)

plt.savefig('figs/VGOS/'+Const.name+'/'+obs_name+\
            'CDF time domain Psd - ' + str(sim_flags)+'.png')


    


#%% 5- power spectral density per sample per channel (time domain samples)


Plim_analog = Plim * RX_analog_BW
Plim_digital = Plim * RX_digital_BW


plt.figure(figsize=(10, 8))
[plt.plot(PSD_iters[...,i].flatten().to(u.dB(u.mW/u.Hz)), label=Const.observer_name+'Channel'+ str(i)) for i in range(Nchannels)]
if do_correlation:
    [plt.plot(np.abs(PSD_corr_iters[...,i].flatten()).to(u.dB(u.mW/u.Hz)), label="Correlation"+'Channel'+ str(i)) for i in range(Nchannels)]

plt.plot((Plim*np.ones(Ncells*Ntime*Niters)).to(u.dB(u.mW/u.Hz)),'--r',label="RX noise in 20 K receiver")
plt.legend(loc ='upper right')
plt.grid()
plt.xlabel("Samples")
plt.ylabel("dBm/Hz")
# plt.ylim([-240,-160])
plt.title(Const.observer_name+'-Received PSD value per time sample per channel')
plt.savefig('figs/VGOS/'+Const.name+'/'+Const.observer_name+' received PSD per sample per channel '+ sim_flags + '.png')

plt.figure(figsize=(10, 8))
[plt.plot(PSD_iters1[...,i].flatten().to(u.dB(u.mW/u.Hz)), label=Const1.observer_name+'Channel'+ str(i)) for i in range(Nchannels)]
if do_correlation:
    [plt.plot(np.abs(PSD_corr_iters[...,i].flatten()).to(u.dB(u.mW/u.Hz)), label="Correlation"+'Channel'+ str(i)) for i in range(Nchannels)]
plt.plot((Plim*np.ones(Ncells*Ntime*Niters)).to(u.dB(u.mW/u.Hz)),'--r',label="RX noise in 20 K receiver")
plt.legend(loc ='upper right')
plt.grid()
plt.xlabel("Samples")
plt.ylabel("dBm/Hz")
# plt.ylim([-240,-160])
plt.title(Const1.observer_name+'-Received PSD value per time sample per channel')
plt.savefig('figs/VGOS/'+Const1.name+'/'+Const1.observer_name+' received PSD per sample per channel '+ sim_flags + '.png')



#%% 6- aitoff proyection of received spfd in Jy
# Received power when pointing at each source for all timesteps and iterations
Aux = PSD_iters/A_eff
Prx_source = np.transpose(np.copy(Aux),[1,0,2,3])
Prx_source = np.reshape(Prx_source,[Ncells,int(Niters*Ntime),Nchannels]) 

Prx_source_mean = np.nanmean(Prx_source,axis=-2)
Prx_source95 = np.nanpercentile(Prx_source,95,axis=-2)
Prx_source_max = np.nanmax(Prx_source,axis=-2)
Prx_source_min = np.nanmin(Prx_source,axis=-2)

#make figure in the celestial sphere:
# per channel
for i in range(Nchannels):        
    fig = plt.figure(figsize=(20, 15))
    ax = fig.add_subplot(111, projection='aitoff' ) # hammer or mollweide, aitoff, or lambert. [example](http://matplotlib.org/examples/pylab_examples/geo_demo.html) 
    m = ax.scatter(point_ra[:,0].to(u.rad),point_dec[:,1].to(u.rad),
               marker='o',
               s= 300,
               c= Prx_source_mean.to(u.dB(u.Jy))[:,i])
    
    ax.grid(True)
    ax.set_xticklabels(['14h','16h','18h','20h','22h','0h','2h','4h','6h','8h','10h']) #use if you want to change to a better version of RA. 
    plt.xlabel('Ra')
    plt.ylabel('Dec')
    plt.show()
    plt.suptitle(Const.observer_name+' Channel %d - RaDec coordinates of targets in VGOS schedule of %s'%(i,Start[0]))
    plt.colorbar(m,label='Mean spfd [dBJy]')
    
    fig.savefig('figs/VGOS/'+Const.name+'/'+Const.observer_name+' CH %d'%i +' - allsky_radec_color mean power '+ sim_flags +'.png')

#add all channels:
fig = plt.figure(figsize=(20, 15))
ax = fig.add_subplot(111, projection='aitoff' ) # hammer or mollweide, aitoff, or lambert. [example](http://matplotlib.org/examples/pylab_examples/geo_demo.html) 
m = ax.scatter(point_ra[:,0].to(u.rad),point_dec[:,1].to(u.rad),
           marker='o',
           s= 300,
           c= np.sum(Prx_source_mean,axis=-1).to(u.dB(u.Jy)))

ax.grid(True)
ax.set_xticklabels(['14h','16h','18h','20h','22h','0h','2h','4h','6h','8h','10h']) #use if you want to change to a better version of RA. 
plt.xlabel('Ra')
plt.ylabel('Dec')
plt.show()
plt.suptitle(Const.observer_name+' Aggregated channels - RaDec coordinates of targets in VGOS schedule of %s'%(Start[0]))
plt.colorbar(m,label='Mean power [dBmW]')
fig.savefig('figs/VGOS/'+Const.name+'/'+Const.observer_name+' Aggregated CHs' +' - allsky_radec_color mean power '+ sim_flags +'.png')


if do_correlation:
    Aux = PSD_iters1
    Prx_source = np.transpose(np.copy(Aux)*RFI_BW,[1,0,2,3])
    Prx_source = np.reshape(Prx_source,[Ncells,int(Niters*Ntime),Nchannels]) 
    
    Prx_source_mean = np.nanmean(Prx_source,axis=-2)
    Prx_source95 = np.nanpercentile(Prx_source,95,axis=-2)
    Prx_source_max = np.nanmax(Prx_source,axis=-2)
    Prx_source_min = np.nanmin(Prx_source,axis=-2)
    
    #make figure in the celestial sphere:
    # per channel
    for i in range(Nchannels):        
        fig = plt.figure(figsize=(20, 15))
        ax = fig.add_subplot(111, projection='aitoff' ) # hammer or mollweide, aitoff, or lambert. [example](http://matplotlib.org/examples/pylab_examples/geo_demo.html) 
        m = ax.scatter(point_ra[:,0].to(u.rad),point_dec[:,1].to(u.rad),
                   marker='o',
                   s= 300,
                   c= Prx_source_mean.to(u.dB(u.mW))[:,i])
        
        ax.grid(True)
        ax.set_xticklabels(['14h','16h','18h','20h','22h','0h','2h','4h','6h','8h','10h']) #use if you want to change to a better version of RA. 
        plt.xlabel('Ra')
        plt.ylabel('Dec')
        plt.show()
        plt.suptitle(Const1.observer_name+' Channel %d - RaDec coordinates of targets in VGOS schedule of %s'%(i,Start[0]))
        plt.colorbar(m,label='Mean power [dBmW]')
        
        fig.savefig('figs/VGOS/'+Const1.name+'/'+Const1.observer_name+' CH %d'%i +' - allsky_radec_color mean power '+ sim_flags +'.png')
    
    #add all channels:
    fig = plt.figure(figsize=(20, 15))
    ax = fig.add_subplot(111, projection='aitoff' ) # hammer or mollweide, aitoff, or lambert. [example](http://matplotlib.org/examples/pylab_examples/geo_demo.html) 
    m = ax.scatter(point_ra[:,0].to(u.rad),point_dec[:,1].to(u.rad),
               marker='o',
               s= 300,
               c= np.sum(Prx_source_mean,axis=-1).to(u.dB(u.mW)))
    
    ax.grid(True)
    ax.set_xticklabels(['14h','16h','18h','20h','22h','0h','2h','4h','6h','8h','10h']) #use if you want to change to a better version of RA. 
    plt.xlabel('Ra')
    plt.ylabel('Dec')
    plt.show()
    plt.suptitle(Const1.observer_name+' Aggregated channels - RaDec coordinates of targets in VGOS schedule of %s'%(Start[0]))
    plt.colorbar(m,label='Mean power [dBmW]')
    fig.savefig('figs/VGOS/'+Const1.name+'/'+Const1.observer_name+' Aggregated CHs' +' - allsky_radec_color mean power '+ sim_flags +'.png')
    


   




#%% Mean flux in Jy as a function of declination for each channel
# this has to be calculated per channel of the satellites
Aeff = np.pi*(Observer.diam/2)**2 * 0.7 # 0.7 efficiency
Aeff1 = np.pi*(Observer1.diam/2)**2 * 0.7 # 0.7 efficiency

#observer0
plt.figure(figsize=(10, 8))
ax = plt.axes()
aux = np.mean(np.nanmean(PSD_iters,axis=-2),axis=0)/Aeff
ax.semilogy(point_dec[:,0],aux.to((u.Jy)),'o',
        linewidth=1,
        markersize=4)#,'r',alpha=0.03)
if do_correlation:
    aux = abs(np.mean(np.nanmean(PSD_corr_iters,axis=-2),axis=0)/Aeff)
    ax.plot(point_dec[:,0],aux.to((u.Jy)),'x',
            label='Correlation',
            linewidth=1,
            markersize=5)#,'r',alpha=0.03)

ax.legend(['Ch %d'%i for i in range (Nchannels)],loc ='upper right')
ax.grid()
# ax.set_xlim([0,Ncells])
# ax.set_xticks(np.arange(Ncells))
# ax.set_xticklabels(Source_name, rotation="vertical")
ax.set_ylabel("Jansky")
ax.set_xlabel('declination [deg]')
ax.set_title(Const.name+'-'+Const.observer_name+'- Received equiv flux averaged (per channel) in %.2f seconds'%(Ntime*time_step)    )
plt.savefig('figs/VGOS/'+Const.name+'/'+Const.observer_name+' received equiv flux averaged '+ sim_flags + '.png')

#observer1
plt.figure(figsize=(10, 8))
ax = plt.axes()
aux = np.mean(np.nanmean(PSD_iters1,axis=-2),axis=0)/Aeff
ax.semilogy(point_dec[:,0],aux.to((u.Jy)),'o',
        linewidth=1,
        markersize=4)#,'r',alpha=0.03)
if do_correlation:
    aux = abs(np.mean(np.nanmean(PSD_corr_iters,axis=-2),axis=0)/Aeff)
    ax.plot(point_dec[:,0],aux.to((u.Jy)),'x',
            label='Correlation',
            linewidth=1,
            markersize=5)#,'r',alpha=0.03)

ax.legend(['Ch %d'%i for i in range (Nchannels)],loc ='upper right')
ax.grid()
# ax.set_xlim([0,Ncells])
# ax.set_xticks(np.arange(Ncells))
# ax.set_xticklabels(Source_name, rotation="vertical")
ax.set_ylabel("Jansky")
ax.set_xlabel('declination [deg]')
ax.set_title(Const1.name+'-'+Const1.observer_name+'- Received equiv flux averaged (per channel) in %.2f seconds'%(Ntime*time_step)    )
plt.savefig('figs/VGOS/'+Const1.name+'/'+Const1.observer_name+' received equiv flux averaged '+ sim_flags + '.png')





    
