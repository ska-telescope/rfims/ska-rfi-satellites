# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 02:10:11 2020


@author: f.divruno
"""

import astropy.units as u
import astropy.constants as const
import numpy as np
import time
import matplotlib.pyplot as plt
import sys
from pycraf import protection
sys.path += ['../src']

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'
if os.name == 'nt':
    os.system('cls')
else:
    os.system('clear')
    
import ska

#%% Simulation parameters

# Simulation frequency
freq = 11.7*u.GHz #use the centre freque as approximation

# number of times the constellation and tesselation is calculated
Niters = 1

# number of time steps
Ntime = 100

#length of a time step
time_step = 0.002 #sec

print("Total simulation time %.2f s"%(time_step*Ntime))

# channel bandwidth
BW = 250*u.MHz 

#beam_type
# beam_type = 'statistical_constant'
beam_type = 'statistical'

# avoidance angle 
avoid_angle = 0*u.deg

#masking in the signal chain: a time vector could be defined when a satellite
# has a boresight angle less than a defined value
masking = False
boresight_limit = 1 * u.deg

#Power flagging
# flag the received power higher than this limit
# in a received power in a 4 kHz BW
# This value needs to be checked to understand if possible in the
# receiver.
power_flag = True
T = 20 * u.K # Kelvin
B = 1*u.Hz
Plim = (T*B*1.38e-23 * u.W * u.s / u.K).to(u.W)
print("Tsys = ", T)
print("BW = ", B)
print("Plim/Hz = kT = ", 10.*np.log10(Plim.value/1E-3), "dBm")

RA769_lim = protection.ra769_calculate_entry(freq,1*u.Hz,1e-8*u.K,T)[3]

print("Protection thresgolds as defined in RA.769: {:.2f}".format(RA769_lim))

#Antenna receiver model
high_SLL = 0 # flag
SLL = 4 # dBi


#string to use in saving figures
sim_flags = ' - high_SLL %d - avd_ang %s -\
                masking %d - pow_flag %d - Niters %d - Ntime %d .png'\
                %(high_SLL, avoid_angle, masking, power_flag, Niters, Ntime)



#%% define the telescope receiver
site_name = 'SKA-MID'
SKA1 = ska.Telescope('SKA-MID073', np.array([21.443222,-30.713174,1])) #core antenna
SKA2 = ska.Telescope('SKA-MID068', np.array([21.441913,-30.712950,1])) #core antenna
SKA3 = ska.Telescope('SKA-MID057', np.array([21.442969,-30.712450,1])) #core antenna
SKA4 = ska.Telescope('SKA-MID113', np.array([21.499014,-31.519592,1])) #south spiral arm
SKA5 = ska.Telescope('SKA-MID004', np.array([22.220797,-30.269023,1])) #north spiral arm
SKA6 = ska.Telescope('SKA-MID008', np.array([20.597376,-30.308659,1])) #north spiral arm

#%%

# tesselate the semiphere (horizontal coordinates)
point_az, point_el,grid_info = ska.general.sky_cells_m1583(
                        niters=Niters, step_size=5 * u.deg,
                        lat_range=(15 * u.deg, 90 * u.deg),
                        test = 0
                        )
Ncells = point_az.shape[1]



# or define just some points in the azel plane
point_el = np.arange(0,90,1)
az = 0
point_az = np.ones(point_el.shape)*az
Ncells = point_az.shape[0]
point_el = np.repeat(point_el[None],Niters,axis=0)
point_az = np.repeat(point_az[None],Niters,axis=0)



#%% define the constellation
const_name = 'Starlink ph1'
Const1 = ska.Constellation(const_name, SKA1.name, SKA1.coord_geo, beam_type = beam_type )
Const2 = ska.Constellation(const_name, SKA2.name, SKA2.coord_geo, beam_type = beam_type)
Const3 = ska.Constellation(const_name, SKA3.name, SKA3.coord_geo, beam_type = beam_type)
Const4 = ska.Constellation(const_name, SKA4.name, SKA4.coord_geo, beam_type = beam_type)
Const5 = ska.Constellation(const_name, SKA5.name, SKA5.coord_geo, beam_type = beam_type)
Const6 = ska.Constellation(const_name, SKA6.name, SKA6.coord_geo, beam_type = beam_type)


#initialize spfd function
Const1.spfd_init(avd_angle = avoid_angle)
Const2.spfd_init(avd_angle = avoid_angle)
Const3.spfd_init(avd_angle = avoid_angle)
Const4.spfd_init(avd_angle = avoid_angle)
Const5.spfd_init(avd_angle = avoid_angle)
Const6.spfd_init(avd_angle = avoid_angle)

#%%
#plot SKA antenna gain
theta = np.logspace(-3,np.log10(180),1000)*u.deg
RAS_G = SKA1.gain(theta, 15*u.m, freq, 100*u.percent,
                 high_SLL=high_SLL, SLL = SLL, 
                 do_bessel=False) #in lin gain

fig = plt.figure('SKA antenna model', figsize=[15,8])
plt.semilogx(theta,10*np.log10(RAS_G))
plt.title('SKA antenna model')
plt.xlabel('boresight angle [deg]')
plt.ylabel('gain [dBi]')
plt.grid()

fig.savefig('figs/'+Const1.name+'/'+site_name+'/SKA antenna gain - ' + sim_flags)

#%% define output vectors

# ESPFD_iters = np.zeros([Niters,Ncells,Ntime]) #*u.Jy
Prx_iters1 = np.zeros([Niters,Ncells,Ntime]) #*u.W
Prx_iters2 = np.zeros([Niters,Ncells,Ntime]) #*u.W
Prx_iters3 = np.zeros([Niters,Ncells,Ntime]) #*u.W
Prx_iters4 = np.zeros([Niters,Ncells,Ntime]) #*u.W
Prx_iters5 = np.zeros([Niters,Ncells,Ntime]) #*u.W
Prx_iters6 = np.zeros([Niters,Ncells,Ntime]) #*u.W

Prx12_iters = np.zeros([Niters,Ncells,Ntime]) #*u.W
Prx13_iters = np.zeros([Niters,Ncells,Ntime]) #*u.W
Prx23_iters = np.zeros([Niters,Ncells,Ntime]) #*u.W
Prx45_iters = np.zeros([Niters,Ncells,Ntime]) #*u.W
Prx46_iters = np.zeros([Niters,Ncells,Ntime]) #*u.W
Prx56_iters = np.zeros([Niters,Ncells,Ntime]) #*u.W


#%%
for j  in range(Niters):
    print('Iter: %d'%j)
    niter = j

    # pointings of the RAS antenna
    p_az = point_az[niter,:,None]
    p_el = point_el[niter,:,None]
    
    
    # positions of the satellites in the horizontal 
    propagate_yes = int(input('Propagate constellations? If already in memory saves time 1=y 0=no'))
    if propagate_yes:
        print('Propagating satellites')
        np.random.seed(j)
        epoch = float(51544.5 + np.random.rand(1)*10)
        Const1.propagate(epoch=epoch,
                           iters = 1,
                           time_steps = Ntime,
                           time_step = time_step,
                           verbose = False,
                           rand_seed=j,
                           )
        print(SKA1.name)
        Const2.propagate(epoch = epoch,
                           iters = 1,
                           time_steps = Ntime,
                           time_step = time_step,
                           verbose = False,
                           rand_seed=j,
                           )
        print(SKA2.name)
        Const3.propagate(epoch=epoch,
                           iters = 1,
                           time_steps = Ntime,
                           time_step = time_step,
                           verbose = False,
                           rand_seed=j,
                           )
        print(SKA3.name)
        Const4.propagate(epoch = epoch,
                           iters = 1,
                           time_steps = Ntime,
                           time_step = time_step,
                           verbose = False,
                           rand_seed=j,
                           ) 
        print(SKA4.name)
        Const5.propagate(epoch=epoch,
                           iters = 1,
                           time_steps = Ntime,
                           time_step = time_step,
                           verbose = False,
                           rand_seed=j,
                           )
        print(SKA5.name)
        Const6.propagate(epoch = epoch,
                           iters = 1,
                           time_steps = Ntime,
                           time_step = time_step,
                           verbose = False,
                           rand_seed=j,
                           )   
        
    else:
        print('already propagated orbits in memory')
    
    vis_mask = Const1.vis_mask * Const2.vis_mask *\
                Const3.vis_mask * Const4.vis_mask *\
                Const5.vis_mask * Const6.vis_mask 
                
    #match the visible mask for satellites that are present at both telescopes.
    Const1.vis_mask = vis_mask
    Const2.vis_mask = vis_mask
    Const3.vis_mask = vis_mask
    Const4.vis_mask = vis_mask
    Const5.vis_mask = vis_mask
    Const6.vis_mask = vis_mask

    sat_az1 = Const1.topo_pos[vis_mask,0][None]
    sat_el1 = Const1.topo_pos[vis_mask,1][None]

    sat_az2 = Const2.topo_pos[vis_mask,0][None]
    sat_el2 = Const2.topo_pos[vis_mask,1][None]

    sat_az3 = Const3.topo_pos[vis_mask,0][None]
    sat_el3 = Const3.topo_pos[vis_mask,1][None]

    sat_az4 = Const4.topo_pos[vis_mask,0][None]
    sat_el4 = Const4.topo_pos[vis_mask,1][None]

    sat_az5 = Const5.topo_pos[vis_mask,0][None]
    sat_el5 = Const5.topo_pos[vis_mask,1][None]

    sat_az6 = Const6.topo_pos[vis_mask,0][None]
    sat_el6 = Const6.topo_pos[vis_mask,1][None]


    delta_d12 = abs(Const1.topo_pos[vis_mask,2][None] - Const2.topo_pos[vis_mask,2][None])
    delta_d13 = abs(Const1.topo_pos[vis_mask,2][None] - Const3.topo_pos[vis_mask,2][None])
    delta_d23 = abs(Const2.topo_pos[vis_mask,2][None] - Const3.topo_pos[vis_mask,2][None])
    delta_d45 = abs(Const4.topo_pos[vis_mask,2][None] - Const5.topo_pos[vis_mask,2][None])
    delta_d46 = abs(Const4.topo_pos[vis_mask,2][None] - Const6.topo_pos[vis_mask,2][None])
    delta_d56 = abs(Const5.topo_pos[vis_mask,2][None] - Const6.topo_pos[vis_mask,2][None])
    
    Ntime = Const1.topo_pos.shape[1]
    Nsats = Const1.topo_pos.shape[2]
    

    # spfd generated by each satellite visible
    # the statistical function only has the elevation of the satellite as a 
    # parameter
    spfd_sat1 = Const1.spfd() #(u.W/u.m**2) 
    spfd_sat2 = Const2.spfd() #(u.W/u.m**2) 
    spfd_sat3 = Const3.spfd() #(u.W/u.m**2) 
    spfd_sat4 = Const4.spfd() #(u.W/u.m**2) 
    spfd_sat5 = Const5.spfd() #(u.W/u.m**2) 
    spfd_sat6 = Const6.spfd() #(u.W/u.m**2) 
    
    Nsegments = 2 # cells divided in N stepsto accomodate the amount of memory needed
    segment = Ncells//(Nsegments-1)
    
    Psdrx_sparse = np.zeros([segment,1,Ntime,Nsats])
    
    for i in range(Nsegments):
        t_start = time.time()
#        print('slice %d'%i)
        idx = slice(i*segment,(i+1)*segment,1)
        if idx.stop > p_az.shape[0]: 
            idx = slice(i*segment,p_az.shape[0],1)
        p_az1 = p_az[idx]
        p_el1 = p_el[idx]
#        print('start',p_az1[0],p_az1[-1])
        Ncells1 = p_az1.shape[0]
    
        # angular distance from the RAS pointing to the satellite position
        theta1 = ska.general.angle(p_az1,p_el1,sat_az1,sat_el1) * u.deg
        theta2 = ska.general.angle(p_az1,p_el1,sat_az2,sat_el2) * u.deg
        theta3 = ska.general.angle(p_az1,p_el1,sat_az3,sat_el3) * u.deg
        theta4 = ska.general.angle(p_az1,p_el1,sat_az4,sat_el4) * u.deg
        theta5 = ska.general.angle(p_az1,p_el1,sat_az5,sat_el5) * u.deg
        theta6 = ska.general.angle(p_az1,p_el1,sat_az6,sat_el6) * u.deg
        
       
        
        #Gain of the RAS antenna towards the satellites
        RAS_G1 = SKA1.gain(theta1, 15*u.m, freq, 100*u.percent,
                         high_SLL=high_SLL, SLL = SLL, 
                         do_bessel=False) #in lin gain

        RAS_G2 = SKA2.gain(theta2, 15*u.m, freq, 100*u.percent,
                         high_SLL=high_SLL, SLL = SLL, 
                         do_bessel=False) #in lin gain

        RAS_G3 = SKA3.gain(theta3, 15*u.m, freq, 100*u.percent,
                         high_SLL=high_SLL, SLL = SLL, 
                         do_bessel=False) #in lin gain

        RAS_G4 = SKA4.gain(theta4, 15*u.m, freq, 100*u.percent,
                         high_SLL=high_SLL, SLL = SLL, 
                         do_bessel=False) #in lin gain

        RAS_G5 = SKA5.gain(theta5, 15*u.m, freq, 100*u.percent,
                         high_SLL=high_SLL, SLL = SLL, 
                         do_bessel=False) #in lin gain

        RAS_G6 = SKA6.gain(theta6, 15*u.m, freq, 100*u.percent,
                         high_SLL=high_SLL, SLL = SLL, 
                         do_bessel=False) #in lin gain


        # Telescope masking 
        if masking:
            RAS_G1[theta1 < boresight_limit] = np.nan 
            RAS_G2[theta2 < boresight_limit] = np.nan 
            # when the summation is don in the satellite axis,
            # a flagged satellite in the main beam flags all the time step
            # as the telescope receives the power from all satellites
            
        #effective area        
        A_eff1 = RAS_G1 * (((const.c/freq).to_value(u.m))**2/4/np.pi)  * u.m**2
        A_eff2 = RAS_G2 * (((const.c/freq).to_value(u.m))**2/4/np.pi)  * u.m**2
        A_eff3 = RAS_G3 * (((const.c/freq).to_value(u.m))**2/4/np.pi)  * u.m**2
        A_eff4 = RAS_G4 * (((const.c/freq).to_value(u.m))**2/4/np.pi)  * u.m**2
        A_eff5 = RAS_G5 * (((const.c/freq).to_value(u.m))**2/4/np.pi)  * u.m**2
        A_eff6 = RAS_G6 * (((const.c/freq).to_value(u.m))**2/4/np.pi)  * u.m**2
    

        #Received power on each antenna
        Psdrx_lin1 = spfd_sat1 * A_eff1 #in u.W/u.Hz
        Psdrx_lin2 = spfd_sat2 * A_eff2 #in u.W/u.Hz
        Psdrx_lin3 = spfd_sat3 * A_eff3 #in u.W/u.Hz
        Psdrx_lin4 = spfd_sat4 * A_eff4 #in u.W/u.Hz
        Psdrx_lin5 = spfd_sat5 * A_eff5 #in u.W/u.Hz
        Psdrx_lin6 = spfd_sat6 * A_eff6 #in u.W/u.Hz

        #Convert to correlated power (no delay tracking)
        Pcorr12 = Psdrx_lin1**0.5 * Psdrx_lin2**0.5 * \
                      (np.cos(2*np.pi*freq.to_value(u.Hz)*delta_d12/3e8))
        Pcorr13 = Psdrx_lin1**0.5 * Psdrx_lin3**0.5 * \
                      (np.cos(2*np.pi*freq.to_value(u.Hz)*delta_d13/3e8))
        Pcorr23 = Psdrx_lin2**0.5 * Psdrx_lin3**0.5 * \
                      (np.cos(2*np.pi*freq.to_value(u.Hz)*delta_d23/3e8))
        Pcorr45 = Psdrx_lin4**0.5 * Psdrx_lin5**0.5 * \
                      (np.cos(2*np.pi*freq.to_value(u.Hz)*delta_d45/3e8))
        Pcorr46 = Psdrx_lin4**0.5 * Psdrx_lin6**0.5 * \
                      (np.cos(2*np.pi*freq.to_value(u.Hz)*delta_d46/3e8))
        Pcorr56 = Psdrx_lin5**0.5 * Psdrx_lin6**0.5 * \
                      (np.cos(2*np.pi*freq.to_value(u.Hz)*delta_d56/3e8))
        
        
        #test
        # plt.figure()
        # plt.plot((Psdrx_lin1.flatten()).to(u.dB(u.W/u.Hz)))
        # plt.plot((Psdrx_lin2.flatten()).to(u.dB(u.W/u.Hz)))
        # plt.plot((Pcorrelated.flatten()).to(u.dB(u.W/u.Hz)))
        #test
        
        t_start1 = time.time()
        #convert from dense to sparse matrix [Niters, Ncells1, Ntime, Nsats]
        if Ncells1==Psdrx_sparse.shape[0]:
            Psdrx_sparse *= 0
        else:
            Psdrx_sparse = np.zeros([Ncells1,1,Ntime,Nsats])
        
        Psdrx_sparse[:,vis_mask] = Psdrx_lin1
        Psdrx_tot1 = (np.sum(Psdrx_sparse,axis=-1)) # sum in satellite dimension [Ncells1,Ntime]

        Psdrx_sparse[:,vis_mask] = Psdrx_lin2
        Psdrx_tot2 = (np.sum(Psdrx_sparse,axis=-1)) # sum in satellite dimension [Ncells1,Ntime]

        Psdrx_sparse[:,vis_mask] = Psdrx_lin3
        Psdrx_tot3 = (np.sum(Psdrx_sparse,axis=-1)) # sum in satellite dimension [Ncells1,Ntime]

        Psdrx_sparse[:,vis_mask] = Psdrx_lin4
        Psdrx_tot4 = (np.sum(Psdrx_sparse,axis=-1)) # sum in satellite dimension [Ncells1,Ntime]

        Psdrx_sparse[:,vis_mask] = Psdrx_lin5
        Psdrx_tot5 = (np.sum(Psdrx_sparse,axis=-1)) # sum in satellite dimension [Ncells1,Ntime]

        Psdrx_sparse[:,vis_mask] = Psdrx_lin6
        Psdrx_tot6 = (np.sum(Psdrx_sparse,axis=-1)) # sum in satellite dimension [Ncells1,Ntime]


        Psdrx_sparse[:,vis_mask] = Pcorr12
        Psdcorr12_tot = (np.sum(Psdrx_sparse,axis=-1)) # sum in satellite dimension [Ncells1,Ntime]

        Psdrx_sparse[:,vis_mask] = Pcorr13
        Psdcorr13_tot = (np.sum(Psdrx_sparse,axis=-1)) # sum in satellite dimension [Ncells1,Ntime]

        Psdrx_sparse[:,vis_mask] = Pcorr23
        Psdcorr23_tot = (np.sum(Psdrx_sparse,axis=-1)) # sum in satellite dimension [Ncells1,Ntime]

        Psdrx_sparse[:,vis_mask] = Pcorr45
        Psdcorr45_tot = (np.sum(Psdrx_sparse,axis=-1)) # sum in satellite dimension [Ncells1,Ntime]

        Psdrx_sparse[:,vis_mask] = Pcorr46
        Psdcorr46_tot = (np.sum(Psdrx_sparse,axis=-1)) # sum in satellite dimension [Ncells1,Ntime]

        Psdrx_sparse[:,vis_mask] = Pcorr56
        Psdcorr56_tot = (np.sum(Psdrx_sparse,axis=-1)) # sum in satellite dimension [Ncells1,Ntime]

        
        t_stop1 = time.time()
        print('Elapsed time Sparsing matrix:'+ str(t_stop1-t_start1))

    #histogram
#    plt.figure('Prx')
#    plt.hist((Prx.flatten()).value,100,histtype='step')
#    plt.title('Received power from one satellite, %s in 4kHz'%(Prx.unit))
#    plt.axvline(x = -117,color='r')
#    
#    plt.figure('Prx_total')
#    plt.hist((Prx_tot.flatten()).value,100,histtype='step')
#    plt.title('Received power from all satellites, %s in 4kHz'%(Prx_tot.unit))
#    plt.axvline(x = -117,color='r')
#    
#    plt.figure('Psd_tot')
#    plt.hist((Psd_tot.flatten()).value,100,histtype='step')
#    plt.title('Received power spectral density from all visible satellites, %s '%(Psd_tot.unit))
    #    plt.axvline(x = np.mean(Psd_tot.flatten()).value,color='r')
    
    
#    plt.figure('EPFD')
#    plt.hist((EPFD.flatten()).value,100,histtype='step')
#    plt.title('Equivalent PFD from all visible satellites, %s '%(EPFD.unit))
#    #    plt.axvline(x = np.mean(EPFD.flatten()).value,color='r')
#    
#    plt.figure('EPFD_Jy')
#    plt.hist((EPFD.flatten()).to_value(u.dB(u.Jy)),100,histtype='step')
#    plt.title('Equivalent PFD from all visible satellites, %s '%(u.dB(u.Jy)))
#    plt.axvline(x = np.mean((EPFD.flatten()).to_value(u.dB(u.Jy))),color='r')
        
        #save to output vectors
        Prx_iters1[j,idx,:] = Psdrx_tot1[:,0,:] * 4e3 * u.Hz # [Niters,Ncells,Ntime]  in W/Hz
        Prx_iters2[j,idx,:] = Psdrx_tot2[:,0,:] * 4e3 * u.Hz # [Niters,Ncells,Ntime]  in W/Hz
        Prx_iters3[j,idx,:] = Psdrx_tot3[:,0,:] * 4e3 * u.Hz # [Niters,Ncells,Ntime]  in W/Hz
        Prx_iters4[j,idx,:] = Psdrx_tot4[:,0,:] * 4e3 * u.Hz # [Niters,Ncells,Ntime]  in W/Hz
        Prx_iters5[j,idx,:] = Psdrx_tot5[:,0,:] * 4e3 * u.Hz # [Niters,Ncells,Ntime]  in W/Hz
        Prx_iters6[j,idx,:] = Psdrx_tot6[:,0,:] * 4e3 * u.Hz # [Niters,Ncells,Ntime]  in W/Hz

        Prx12_iters[j,idx,:] = Psdcorr12_tot[:,0,:] * 4e3 * u.Hz  # [Niters,Ncells,Ntime]  in W/Hz
        Prx13_iters[j,idx,:] = Psdcorr13_tot[:,0,:] * 4e3 * u.Hz  # [Niters,Ncells,Ntime]  in W/Hz
        Prx23_iters[j,idx,:] = Psdcorr23_tot[:,0,:] * 4e3 * u.Hz  # [Niters,Ncells,Ntime]  in W/Hz
        Prx45_iters[j,idx,:] = Psdcorr45_tot[:,0,:] * 4e3 * u.Hz  # [Niters,Ncells,Ntime]  in W/Hz
        Prx46_iters[j,idx,:] = Psdcorr46_tot[:,0,:] * 4e3 * u.Hz  # [Niters,Ncells,Ntime]  in W/Hz
        Prx56_iters[j,idx,:] = Psdcorr56_tot[:,0,:] * 4e3 * u.Hz  # [Niters,Ncells,Ntime]  in W/Hz

        
        t_stop = time.time()
        print(str(i) + ' - Elapsed time slice:'+ str(t_stop-t_start))
        
        
# Equivalent PFD

A_eff_max = (np.pi * (7.5)**2 * 1) * u.m**2 # m2
ESPFD_iters1 = Prx_iters1 / A_eff_max / (4e3*u.Hz)  # [Niters,Ncells,Ntime]  in W/m2/Hz
ESPFD_iters2 = Prx_iters1 / A_eff_max / (4e3*u.Hz)  # [Niters,Ncells,Ntime]  in W/m2/Hz
ESPFD_iters3 = Prx_iters1 / A_eff_max / (4e3*u.Hz)  # [Niters,Ncells,Ntime]  in W/m2/Hz
ESPFD_iters4 = Prx_iters1 / A_eff_max / (4e3*u.Hz)  # [Niters,Ncells,Ntime]  in W/m2/Hz
ESPFD_iters5 = Prx_iters1 / A_eff_max / (4e3*u.Hz)  # [Niters,Ncells,Ntime]  in W/m2/Hz
ESPFD_iters6 = Prx_iters1 / A_eff_max / (4e3*u.Hz)  # [Niters,Ncells,Ntime]  in W/m2/Hz

ESPFD_iters12 = Prx12_iters / A_eff_max / (4e3*u.Hz)  # [Niters,Ncells,Ntime]  in W/m2/Hz
ESPFD_iters13 = Prx13_iters / A_eff_max / (4e3*u.Hz)  # [Niters,Ncells,Ntime]  in W/m2/Hz
ESPFD_iters23 = Prx23_iters / A_eff_max / (4e3*u.Hz)  # [Niters,Ncells,Ntime]  in W/m2/Hz
ESPFD_iters45 = Prx45_iters / A_eff_max / (4e3*u.Hz)  # [Niters,Ncells,Ntime]  in W/m2/Hz
ESPFD_iters46 = Prx46_iters / A_eff_max / (4e3*u.Hz)  # [Niters,Ncells,Ntime]  in W/m2/Hz
ESPFD_iters56 = Prx56_iters / A_eff_max / (4e3*u.Hz)  # [Niters,Ncells,Ntime]  in W/m2/Hz


# #%% print the amount of data masked
# print('Total flagged samples for satellites closer than %s :'%boresight_limit)
# print('%.2f %%'%(np.sum(np.isnan(ESPFD_iters))/len(ESPFD_iters.flatten())*100))
#%% plot the satellites in horizontal frame
# Const1.plot_geodetic_view()
# Const2.plot_geodetic_view()
# Const3.plot_geodetic_view()

fig = Const1.plot_topo_view('1')
fig.savefig('figs/'+Const1.name+'/'+site_name+'/topo_view -' + Const1.observer_name)

fig = Const2.plot_topo_view('2')
fig.savefig('figs/'+Const1.name+'/'+site_name+'/topo_view -' + Const2.observer_name)

fig = Const3.plot_topo_view('3')
fig.savefig('figs/'+Const1.name+'/'+site_name+'/topo_view -' + Const3.observer_name)

fig = Const4.plot_topo_view('4')
fig.savefig('figs/'+Const1.name+'/'+site_name+'/topo_view -' + Const4.observer_name)

fig = Const5.plot_topo_view('5')
fig.savefig('figs/'+Const1.name+'/'+site_name+'/topo_view -' + Const5.observer_name)

fig = Const6.plot_topo_view('6')
fig.savefig('figs/'+Const1.name+'/'+site_name+'/topo_view -' + Const6.observer_name)

#%% flatten time domain plot 
# this is useful to see the received power levels
fig = plt.figure('time domain power', figsize=[15,8])
plt.plot((Prx_iters1*u.W).flatten(),label=	SKA1.name + '- 1')
plt.plot((Prx_iters2*u.W).flatten(),label=	SKA2.name + '- 2')
plt.plot((Prx_iters3*u.W).flatten(),label=	SKA3.name + '- 3')
plt.plot((Prx_iters4*u.W).flatten(),label=	SKA4.name + '- 4')
plt.plot((Prx_iters5*u.W).flatten(),label=	SKA5.name + '- 5')
plt.plot((Prx_iters6*u.W).flatten(),label=	SKA6.name + '- 6')

plt.grid()
plt.xlabel('Samples')
plt.ylabel('dBW/4kHz')
plt.title('time domain rx power - '+Const1.name)
plt.legend()

fig.savefig('figs/'+Const1.name+'/'+site_name+'/time domain Prx - ' + sim_flags)

###
fig = plt.figure('time domain correlated power', figsize=[15,8])

plt.plot((Prx12_iters*u.W).flatten(),label='12')
plt.plot((Prx13_iters*u.W).flatten(),label='13')
plt.plot((Prx23_iters*u.W).flatten(),label='23')
plt.plot((Prx45_iters*u.W).flatten(),label='45')
plt.plot((Prx46_iters*u.W).flatten(),label='46')
plt.plot((Prx56_iters*u.W).flatten(),label='56')
plt.grid()
plt.xlabel('Samples')
plt.ylabel('dBW/4kHz')
plt.title('time domain correlated power -  ' +Const1.name)
plt.legend()
fig.savefig('figs/'+Const1.name+'/'+site_name+'/time domain correlated Prx - ' + sim_flags)


fig = plt.figure('time domain power [dB]', figsize=[15,8])
plt.plot((Prx_iters1*u.W).to(u.dB(u.W)).flatten(),label=	SKA1.name + '- 1')
plt.plot((Prx_iters2*u.W).to(u.dB(u.W)).flatten(),label=	SKA2.name + '- 2')
plt.plot((Prx_iters3*u.W).to(u.dB(u.W)).flatten(),label=	SKA3.name + '- 3')
plt.plot((Prx_iters4*u.W).to(u.dB(u.W)).flatten(),label=	SKA4.name + '- 4')
plt.plot((Prx_iters5*u.W).to(u.dB(u.W)).flatten(),label=	SKA5.name + '- 5')
plt.plot((Prx_iters6*u.W).to(u.dB(u.W)).flatten(),label=	SKA6.name + '- 6')

plt.grid()
plt.xlabel('Samples')
plt.ylabel('dBW/4kHz')
plt.title('time domain rx power [dB]-  ' +Const1.name)
plt.legend()

fig.savefig('figs/'+Const1.name+'/'+site_name+'/time domain Prx [dB]- ' + sim_flags)


fig = plt.figure('time domain correlated power [dB]', figsize=[15,8])

plt.plot(abs(Prx12_iters*u.W).to(u.dB(u.W)).flatten(),label='12')
plt.plot(abs(Prx13_iters*u.W).to(u.dB(u.W)).flatten(),label='13')
plt.plot(abs(Prx23_iters*u.W).to(u.dB(u.W)).flatten(),label='23')
plt.plot(abs(Prx45_iters*u.W).to(u.dB(u.W)).flatten(),label='45')
plt.plot(abs(Prx46_iters*u.W).to(u.dB(u.W)).flatten(),label='46')
plt.plot(abs(Prx56_iters*u.W).to(u.dB(u.W)).flatten(),label='56')
plt.grid()
plt.xlabel('Samples')
plt.ylabel('dBW/4kHz')
plt.title('time domain correlated power [dB]-  ' +Const1.name)
plt.legend()
fig.savefig('figs/'+Const1.name+'/'+site_name+'/time domain correlated Prx [dB]- ' + sim_flags)



#%% Mean power

Pm1 = np.nanmean(np.nanmean(Prx_iters1,axis=-1),axis=0)
Pm2 = np.nanmean(np.nanmean(Prx_iters2,axis=-1),axis=0)
Pm3 = np.nanmean(np.nanmean(Prx_iters3,axis=-1),axis=0)
Pm4 = np.nanmean(np.nanmean(Prx_iters4,axis=-1),axis=0)
Pm5 = np.nanmean(np.nanmean(Prx_iters5,axis=-1),axis=0)
Pm6 = np.nanmean(np.nanmean(Prx_iters6,axis=-1),axis=0)

Pmmax = np.max((Pm1.max(),
                Pm2.max(),
                Pm3.max(),
                Pm4.max(),
                Pm5.max(),
                Pm6.max()))

Pm12 = abs(np.nanmean(np.nanmean(Prx12_iters,axis=-1),axis=0))
Pm13 = abs(np.nanmean(np.nanmean(Prx13_iters,axis=-1),axis=0))
Pm23 = abs(np.nanmean(np.nanmean(Prx23_iters,axis=-1),axis=0))
Pm45 = abs(np.nanmean(np.nanmean(Prx45_iters,axis=-1),axis=0))
Pm46 = abs(np.nanmean(np.nanmean(Prx46_iters,axis=-1),axis=0))
Pm56 = abs(np.nanmean(np.nanmean(Prx56_iters,axis=-1),axis=0))

Pmmin = np.min((Pm12.min(),
                Pm13.min(),
                Pm23.min(),
                Pm45.min(),
                Pm46.min(),
                Pm56.min()))


fig = plt.figure('average power for each pointing direction - single dish')
plt.plot((Pm1*u.W).to(u.dB(u.W)),label = SKA1.name + '- 1')
plt.plot((Pm2*u.W).to(u.dB(u.W)),label = SKA2.name + '- 2')
plt.plot((Pm3*u.W).to(u.dB(u.W)),label = SKA3.name + '- 3')
plt.plot((Pm4*u.W).to(u.dB(u.W)),label = SKA4.name + '- 4')
plt.plot((Pm5*u.W).to(u.dB(u.W)),label = SKA5.name + '- 5')
plt.plot((Pm6*u.W).to(u.dB(u.W)),label = SKA6.name + '- 6')
plt.grid()
plt.xlabel('pointings')
plt.ylabel('dBW/4kHz')
# plt.ylim([(Pmmin*u.W).to(u.dB(u.W)).value,
#           (Pmmax*u.W).to(u.dB(u.W)).value])
plt.title('P average in pointings -  ' +Const1.name)
plt.legend()
fig.savefig('figs/'+Const1.name+'/'+site_name+'/averaged pow - single dish' + sim_flags)


fig = plt.figure('average power for each pointing direction - correlated')
plt.plot((Pm12*u.W).to(u.dB(u.W)),label = "12")
plt.plot((Pm13*u.W).to(u.dB(u.W)),label = "13")
plt.plot((Pm23*u.W).to(u.dB(u.W)),label = "23")
plt.plot((Pm45*u.W).to(u.dB(u.W)),label = "45")
plt.plot((Pm46*u.W).to(u.dB(u.W)),label = "46")
plt.plot((Pm56*u.W).to(u.dB(u.W)),label = "56")
plt.grid()
plt.xlabel('pointings')
plt.ylabel('dBW/4kHz')
# plt.ylim([(Pmmin*u.W).to(u.dB(u.W)).value,
#           (Pmmax*u.W).to(u.dB(u.W)).value])

plt.title('P average in pointings -  ' +Const1.name)
plt.legend()
fig.savefig('figs/'+Const1.name+'/'+site_name+'/averaged pow - correlated ' + sim_flags)



fig = plt.figure('average power for each pointing direction')
plt.plot((Pm1*u.W).to(u.dB(u.W)),label = SKA1.name + '- 1')
plt.plot((Pm2*u.W).to(u.dB(u.W)),label = SKA2.name + '- 2')
plt.plot((Pm3*u.W).to(u.dB(u.W)),label = SKA3.name + '- 3')
plt.plot((Pm4*u.W).to(u.dB(u.W)),label = SKA4.name + '- 4')
plt.plot((Pm5*u.W).to(u.dB(u.W)),label = SKA5.name + '- 5')
plt.plot((Pm6*u.W).to(u.dB(u.W)),label = SKA6.name + '- 6')

plt.plot((Pm12*u.W).to(u.dB(u.W)),label = "12")
plt.plot((Pm13*u.W).to(u.dB(u.W)),label = "13")
plt.plot((Pm23*u.W).to(u.dB(u.W)),label = "23")
plt.plot((Pm45*u.W).to(u.dB(u.W)),label = "45")
plt.plot((Pm46*u.W).to(u.dB(u.W)),label = "46")
plt.plot((Pm56*u.W).to(u.dB(u.W)),label = "56")
plt.grid()
plt.xlabel('pointings')
plt.ylabel('dBW/4kHz')

plt.title('P average in pointings -  ' +Const1.name)
plt.legend()
fig.savefig('figs/'+Const1.name+'/'+site_name+'/averaged pow in pointings' + sim_flags)


