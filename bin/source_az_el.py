# -*- coding: utf-8 -*-
"""
Created on Thu Aug 19 12:05:16 2021

Calculates the azimuth and elevation of an astronomical source at a certain RA/DEC

@author: f.divruno
"""
from astropy.coordinates import SkyCoord, EarthLocation
from astropy.coordinates import AltAz, FK5, ICRS, Galactic
from astropy.time import Time
from astropy import units as u
from ska.general import eq_to_azel
import numpy as np
import matplotlib.pyplot as plt
import pymap3d as pymap3d

#%% Simulating the Az El of the source



# define observer coordinates
# EFFBERG_LOC = EarthLocation.from_geodetic(
#     lat=50.52483 * u.deg, lon=6.88361 * u.deg, height=416.7 * u.m
#     )

obs_lon = 21.4
obs_lat = -30.7
OBS_LOC = EarthLocation.from_geodetic(
    lat= obs_lat * u.deg, lon= obs_lon * u.deg, height=1000 * u.m
    )

# define time; can also be done via UTC, just see help
mjd_num = 51544.5 + np.linspace(0,23.9344696/24,120) # one daay of obs
# mjd_num = 51544.5 + np.linspace(0,23.9344696/24 - 1/24,120) # one daay of obs
mjd = Time(mjd_num, format='mjd')

#astronomical source
ra_s, dec_s = 45 * u.deg, -45.7 * u.deg
# ra_s, dec_s = 79.75 * u.deg, 50 * u.deg


for deci in np.arange(40,-100,-10):
    dec = deci * u.deg
    ra = ra_s

    # or
    # ra, dec = '05h19m49.721s', '-45d46m43.78s'
          
        
    az2, el2 = eq_to_azel(mjd_num, obs_lon=obs_lon , obs_lat=obs_lat ,
               ra=ra.value, dec=dec.value)

   
    
    plt.figure('azel source', figsize=[15,7])
    plt.title('Horizontal coordinates of ra=%.2f deg dec=%.2f deg in \n \
              obs_lon= %.2f deg obs_lat=%.2f deg \n \
              start = %s stop= %s'\
              %(ra_s.value,dec_s.value, obs_lon, obs_lat,mjd[0].iso,mjd[-1].iso))
    plt.plot(az2,el2,'s',color='black',label = 'Approx')
    # plt.plot(az[0],el[0],'o',color='red')
    # plt.legend()
    plt.grid()
    plt.xlabel('azimuth [deg]')
    plt.ylabel('elevation [deg]')
    plt.ylim([0,90])
    plt.xlim([0, 360])
    

sc = SkyCoord(
    ra_s, dec_s,
    frame = FK5,  # or ICRS, if you want work with the more modern ICRS system
    obstime = mjd,
    location = OBS_LOC
    )


az3, el3 = pymap3d.radec2azel(ra_s.value, dec_s.value, obs_lat, obs_lon, mjd.to_datetime())
    
az, el = sc.altaz.az, sc.altaz.alt
plt.plot(az,el,'d',color='blue',label = 'Source-astropy')
plt.plot(az3,el3,'x',color='red',label = 'Source-pymap3d')
plt.grid()