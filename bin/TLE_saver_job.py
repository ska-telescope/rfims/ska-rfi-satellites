# -*- coding: utf-8 -*-
"""
Created on Sun Mar 13 00:49:55 2022

@author: f.divruno
"""
import requests
import numpy as np
import astropy.time as time

#%% Get the TLEs from Celestrak
#Active satellites from celestrak
#celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/active.txt')

#OneWeb satellites from celestrak

def save_TLEs():
    celestrak_oneweb = requests.get('https://celestrak.com/NORAD/elements/oneweb.txt')
    
    #Starlink satellites from celestrak
    celestrak_starlink = requests.get('https://celestrak.com/NORAD/elements/starlink.txt')
    
    
    #all the active satellites in the sky
    celestrak_active = requests.get('https://celestrak.com/NORAD/elements/active.txt')
    
    #Geostationary satellites from celestrack
    celestrak_geo = requests.get('https://celestrak.com/NORAD/elements/geo.txt')
    
    
    T = time.Time.now().iso
    np.savez('TLEs/' + T[0:4]+T[5:7]+T[8:10]+'_'+T[11:13]+T[14:16]+T[17:19],
             starlink = celestrak_starlink,
             oneweb = celestrak_oneweb,
             geo = celestrak_geo,
             active = celestrak_active)
    
    return
    
if __name__=='__main__':
    save_TLEs()
    