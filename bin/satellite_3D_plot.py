# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 02:10:11 2020


@author: f.divruno
@author: Braam Otto

Last updated: 29 March 2021
"""

import astropy.units as u
import astropy.constants as const
import numpy as np
import time
import matplotlib.pyplot as plt
import sys
import pycraf.protection as protection

sys.path += ['../src']

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'
if os.name == 'nt':
    os.system('cls')
else:
    os.system('clear')

import ska
from ska import  constellation, telescope
import cysgp4
from datetime import datetime

#%% load pointing file for VGOS observation
aux = open('vo2027.txt')
lines = aux.readlines()

Scan=[]
Line =[]
Source = []
Az =[]
El = []
Cable =[]
Start =[]
Stop = []
Dur = []
date = '2022Jan27'
for line in lines:
    if line[0:4] == 'date':
        yyyy = int(line[7:11])
        mm = 1
        dd = int(line[14:16])
        
    if line[0:2] == ' 0':
        Scan.append(line[1:10])
        Line.append(line[12:15])
        Source.append(line[16:24])
        Az.append(float(line[25:28]))
        El.append(float(line[29:31]))
        Cable.append(line[32:37])
        Start.append(cysgp4.PyDateTime(datetime(yyyy,mm,dd,int(line[39:41]),int(line[42:44]),int(line[45:47]))))
        Stop.append(cysgp4.PyDateTime(datetime(yyyy,mm,dd,int(line[49:51]),int(line[52:54]),int(line[55:57]))))
        Dur.append(float(line[63:65]))

Az = np.array(Az)
El = np.array(El)

fig = plt.figure('VGOS observing plan', figsize=(9, 6))
plt.plot(Az,El,'.')
plt.title('')
plt.xlabel('Azimuth [deg]')
plt.ylabel('Elevation [deg]')
plt.xlim([0,360])
plt.xticks(np.linspace(0,360,10))
plt.yticks(np.linspace(0,90,10))
plt.grid(True, 'both', linestyle='--')
# plt.tight_layout()
# fig.savefig('figs/'+const_name+'/'+Observer.name+' antenna gain - ' + sim_flags)
        

#%% Simulation parameters

#observer
observer_name = 'Wetzell'
observer_geo = np.array((12.87889957, 49.14419937, 0.6)) # long,lat,alt, [deg, deg, km]
observer_diam = 20*u.m

# Simulation frequency
freq = 11.7 * u.GHz #use the centre freque as approximation

# number of times the constellation and tesselation is calculated
Niters = 1

# number of time steps
Ntime = 2

#length of a time step
time_step = 1 #seconds

# channel bandwidth
BW = 250 * u.MHz

# avoidance angle 
# optional parameter only used for steerable beams constellations
avoid_angle = 0 * u.deg

# Masking:
# masking in the signal chain: a time vector could be defined when a satellite
# has a boresight angle less than a defined value
# In real life this requires good knowledge of the ephemerides of satellites and
# propagation of them.
masking = False
boresight_limit = 1 * u.deg

# Power flagging:
# flag the received power higher than this limit
# of received power in a 4 kHz BW
power_flag = True
T = 20 * u.K # Kelvin
B = 1 *u.Hz#4e3 * u.Hz  # Hz (4 kHz)
k_b = 1.3806E-23 
Plim = (T*k_b * u.W * u.s / u.K).to(u.W/u.Hz)

print("Tsys = ", T)
print("BW = ", B)
print("Plim/Hz = kT = ", 10.*np.log10(Plim.value/1E-3), "dBm/Hz")


RA769_lim = protection.ra769_calculate_entry(freq,1*u.Hz,1e-8*u.K,T)[3]

print("Protection thresgolds as defined in RA.769: %.2f"%RA769_lim.value)

#Antenna receiver model
high_SLL = 0 # Increase the sidelobe level to account for uncertainties
SLL = 4 # dBi Value of sidelobes


#Options for constellations:
const_name = 'Starlink ph1'
# const_name = 'Starlink ph2'
# const_name = 'OneWeb ph1'
# const_name = 'OneWeb ph2'
# const_name = 'GW'
# const_name = 'GNSS'     #Genrated orbits with parameters
# const_name = 'Geo'

# constellations based on TLEs from Celestrak
# Actual satellites in orbit
# const_name = 'TLE-Starlink'
# const_name = 'TLE-Geo'
# const_name = 'TLE-Orbcomm'
# const_name = 'TLE-Iridium'
# const_name = 'TLE-GNSS'
# const_name = 'TLE-Active'

# beam_type
# beam_type = 'constant'    
beam_type = 'statistical'   #uses an average pfd obtained from the script "steerable_beams_probability.py"
# beam_type = 'isotropic'   #-146 dBW/m2 is the nominal PFD of these Ku constellations
                            # in 4kHz BW

#string to use in saving figures
sim_flags = '-high_SLL %d-avd_ang %s-\
masking %d-pow_flag %d-Niters %d-Ntime %d-t_step %.2f'\
    %(high_SLL, avoid_angle, masking, power_flag, Niters, Ntime,time_step)

print("\nSimulation Flags : ", sim_flags)


point_az = Az
point_el = El

Ncells = point_az.shape[0]
print("Number of Cells = ", Ncells)

# output vectors
ESPFD_iters = np.zeros([Niters, Ncells, Ntime]) #*u.Jy
Prx_iters = np.zeros([Niters, Ncells, Ntime]) #*u.W

# define the constellation
Const = constellation.Constellation(
    const_name = const_name,\
    observer_name = observer_name,\
    observer_geo = observer_geo,\
    beam_type = beam_type)


#initialize spfd function
Const.spfd_init(avd_angle = avoid_angle)

print("Constellation defined...done")

#%% define the telescope receiver
Observer = telescope.Telescope(observer_name,
                               observer_geo,
                               diam = observer_diam)
print("Observer defined...done")

#plot observer antenna
theta = np.logspace(-3,np.log10(180),1000)*u.deg
RAS_G = Observer.gain(theta, observer_diam, freq, 100*u.percent,
                 high_SLL=high_SLL, SLL = SLL,
                 do_bessel=False) #in lin gain

fig = plt.figure('Observer antenna model', figsize=(9, 6))
plt.semilogx(theta,10*np.log10(RAS_G), '-k')
plt.title(Observer.name + ' Antenna Model')
plt.xlabel('Boresight Angle [$^o$]')
plt.ylabel('Gain [dBi]')
plt.grid(True, 'both')
plt.tight_layout()
fig.savefig('figs/'+const_name+'/'+Observer.name+' antenna gain - ' + sim_flags+'.jpg')

print("Number of Simulation Iterations =", Niters)


#%% 
iteration = 0

epoch = Start[0].mjd
duration = 30*Dur[0]
time_step = 1
time_steps = int(duration/time_step)

epoch = Start[0].mjd

# positions of the satellites in the 30 seconds of pointing
Const.propagate(epoch = epoch,
                  iters = 1,
                  time_steps = 1,
                  time_step = time_step,
                  verbose = False
                  )  

#%%
from mpl_toolkits.mplot3d import Axes3D

# create 3d Axes
fig = plt.figure(figsize=[15,15])
ax = fig.add_subplot(111,  projection='3d')


# mask satellites per altitude
if 'TLE' in Const.name:
    alt = np.histogram(np.floor(Const.geo_pos[...,2]),Const.altitudes)[1]    
else:

    alt = np.histogram(np.floor(Const.geo_pos[...,2]),20)[1]

mask_alt = (Const.geo_pos[...,2] < alt[0])
for i in range(len(alt)-1):

    x = np.squeeze(Const.eci_pos[mask_alt,0])
    y = np.squeeze(Const.eci_pos[mask_alt,1])
    z = np.squeeze(Const.eci_pos[mask_alt,2])
    ax.plot(x,y,z,'.',markersize=1)
    mask_alt = ((Const.geo_pos[...,2]>alt[i]) & (Const.geo_pos[...,2]<=alt[i+1]))

mask_alt = (Const.geo_pos[...,2] > alt[i+1])
x = np.squeeze(Const.eci_pos[mask_alt,0])
y = np.squeeze(Const.eci_pos[mask_alt,1])
z = np.squeeze(Const.eci_pos[mask_alt,2])
ax.plot(x,y,z,'.',markersize=1)


ax.set_title('Orbital view\n%s\nNr satellites:%d'%(Const.name,Const.topo_pos.shape[2]),fontsize=30)
plt.savefig('figs/Orbital view - %s'%Const.name)


#%% plot polar plot of the visible sky:
    
plt.figure(figsize=[20,20])
ax = plt.subplot(projection='polar')
ax.plot(np.pi/2,10,'o')
el = (90-Const.topo_pos[Const.vis_mask,1].flatten())
az = Const.topo_pos[Const.vis_mask,0].flatten()*np.pi/180
ax.plot(az,el,'o')

#%%
plt.figure(figsize=[20,20])
ax = plt.subplot(projection='rectangular')
ax.plot(np.pi/2,10,'o')
el = 90 - Const.topo_pos[...,1].flatten()
az = Const.topo_pos[...,0].flatten()*np.pi/180
ax.plot(az,el,'o')


