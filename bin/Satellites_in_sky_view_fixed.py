# -*- coding: utf-8 -*-
"""

Script based on original by Marta Bautista and Benjamin Winkel

Calculates satellites positions for a defined amount of time and plots them in 
an animation.

Satellites can be downloaded from CELESTRAK or can be based on orbital params
from satellite constellations filings.

@author: M. Bautista, B. Winkel, F. Di Vruno

"""

import requests
import datetime
import astropy.time as time
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import animation, rc
from matplotlib import colors

import cysgp4
import sys

sys.path += ['../src']

import ska.constellation as constellation

font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 22}
import matplotlib
matplotlib.rc('font', **font)


#%% define the observer location:

#YEBES:
# observer = cysgp4.PyObserver(-3.08853, 40.523475, 0.915)
# obs_name = 'Yebes 13.2m'

#JBO:
observer = cysgp4.PyObserver(-2.306790, 53.235035 , 0.1) 
obs_name = 'Jodrell Bank'

# #Tenerife:
# observer = cysgp4.PyObserver( -16 , 28 , 0.1) 
# obs_name = 'GroundBird'


#SKA-MID:
observer = cysgp4.PyObserver(21.4,-30.7,1) 
obs_name = 'SKA-MID'

# #Effelsberg:
# observer = cysgp4.PyObserver(5,56,1) 
# obs_name = 'Effelsberg'

# pointing direction of the observer antenna
obs_el = 26
obs_az = -108
obs_diam = 1 #m this is the observer antenna diameter
obs_freq = 11e9 #Hz
obs_BW = 3e8/obs_freq/obs_diam*180/np.pi/0.8 #half power BW
obs_pointing = np.array([obs_az, obs_el]) #az, el in deg

#annotate the bw direction with a circle of the size of the beam
l = np.linspace(0,np.pi*2)
x_b = obs_BW/2*np.cos(l)
y_b = obs_BW/2*np.sin(l)
x_p = x_b + obs_pointing[0]
y_p = y_b + obs_pointing[1]

#Epoch:
    
my_time = cysgp4.PyDateTime(datetime.datetime.utcnow())
mjd_epoch = my_time.mjd  # get current time

#%% Get the TLEs from Celestrak
#Active satellites from celestrak
#celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/active.txt')

#OneWeb satellites from celestrak
celestrak_oneweb = requests.get('https://celestrak.com/NORAD/elements/oneweb.txt')

#Starlink satellites from celestrak
celestrak_starlink = requests.get('https://celestrak.com/NORAD/elements/starlink.txt')


#all the active satellites in the sky
celestrak_active = requests.get('https://celestrak.com/NORAD/elements/active.txt')

#Geostationary satellites from celestrack
celestrak_geo = requests.get('https://celestrak.com/NORAD/elements/geo.txt')


T = time.Time.now().iso
np.savez('TLEs/' + T[0:4]+T[5:7]+T[8:10]+'_'+T[11:13]+T[14:16]+T[17:19],
         starlink = celestrak_starlink,
         oneweb = celestrak_oneweb,
         geo = celestrak_geo,
         active = celestrak_active)


starlink_tles = np.array(cysgp4.tles_from_text(celestrak_starlink.text))
oneweb_tles = np.array(cysgp4.tles_from_text(celestrak_oneweb.text))
geo_tles = np.array(cysgp4.tles_from_text(celestrak_geo.text))
active_tles = np.array(cysgp4.tles_from_text(celestrak_active.text))


# Load from saved txt:
load_from_text = 0
if load_from_text:
    filename =r'C:\Users\f.divruno\Dropbox (SKAO)\Python_codes\ska-rfi-satellites\bin\TLEs\20220124_084507_catalog.txt' 
    with open(filename) as file:
        aux = file.read()
        aux = aux.replace('\n', '\r\n')
        active_tles = np.array(cysgp4.tles_from_text(aux))
    
    

##remove starlink and oneweb from the list of active satellites
satname = 'STARLINK'
aux = []
for k in range(len(active_tles)):
    if satname in active_tles[k].__repr__():
        aux.append(k)
active_tles = np.delete(active_tles, aux)

satname = 'ONEWEB'
aux = []
for k in range(len(active_tles)):
    if satname in active_tles[k].__repr__():
        aux.append(k)
active_tles = np.delete(active_tles, aux)

#remove geo from active list:

for j in range(len(geo_tles)):
    satname = geo_tles[j].__repr__()
    # print(satname)
    aux = []
    for k in range(len(active_tles)):
        if satname == active_tles[k].__repr__():
            aux.append(k)
    active_tles = np.delete(active_tles, aux)
    

     

#%% from a description of the orbits (theoretical):

#Options for constellations:
# const_name = 'Starlink ph1'
# const_name = 'Starlink ph2'
# const_name = 'OneWeb ph1'
# const_name = 'OneWeb ph2'
# const_name = 'GW'
# const_name = 'GNSS'     #Genrated orbits with parameters
# const_name = 'Geo'

# constellations based on TLEs from Celestrak
# Actual satellites in orbit
# const_name = 'TLE-Starlink'
# const_name = 'TLE-Geo'
# const_name = 'TLE-Orbcomm'
# const_name = 'TLE-Iridium'
# const_name = 'TLE-GNSS'

obs_geo = np.array((observer.loc.lon, observer.loc.lat, observer.loc.alt))


SL1 = constellation.Constellation('Starlink ph1',
                            obs_name,
                            obs_geo)

SL2 = constellation.Constellation('Starlink ph2',
                            obs_name,
                            obs_geo)

OW1 = constellation.Constellation('OneWeb ph1',
                            obs_name,
                            obs_geo)
OW2 = constellation.Constellation('OneWeb ph2',
                            obs_name,
                            obs_geo)

GW = constellation.Constellation('GW',
                            obs_name,
                            obs_geo)



def create_constellation(mjd_epoch,
                          altitudes,
                          inclinations,
                          nplanes,
                          sats_per_plane,
                          rand_seed=0):
    my_sat_tles = []
    sat_nr = 1
    seed_cont = 1
    for alt, inc, n0, s0 in zip(
        altitudes, inclinations, nplanes, sats_per_plane):

        # Satellite Mean Anomoly
        mas0 = np.linspace(0.0, 360.0, s0, endpoint=False)  # distribution of mean anomalies around the plane
        #         mas0 += np.random.uniform(0, 360, 1) # check this, replaced by (1)
        # Satellite Right Ascension
        raans0 = np.linspace(0.0, 360.0, n0, endpoint=False)  # right ascensions distribution
        mas, raans = np.meshgrid(mas0, raans0)

        if rand_seed >= 0 :
            np.random.seed()
            np.random.seed(rand_seed*seed_cont)
            seed_cont += 1
        else:
            np.random.seed()
        # Satellite Mean Anomoly Randomness
        mas += np.random.uniform(0.0, 360.0, n0)[:, np.newaxis]  # (1) # random phase of the satellites
        # mas += np.arange(0,n0*p0,p0)[:,np.newaxis] #(2) # phase of the satellites in the planes
        mas, raans = mas.flatten(), raans.flatten()

        # Satellite Mean Motion
        mm = cysgp4.satellite_mean_motion(alt)
        for ma, raan in zip(mas, raans):
            my_sat_tles.append(
                cysgp4.tle_linestrings_from_orbital_parameters(
                    'TEST {:d}'.format(sat_nr), sat_nr, mjd_epoch,
                    inc, raan, 0.001, 0., ma, mm
                ))

            sat_nr += 1

        with open('TEST_TLEs.txt', 'w') as myfile:
            for line in my_sat_tles:
                myfile.write(str(line[0]) + "\n" +
                              str(line[1]) + "\n" +
                              str(line[2]) + "\n")
        myfile.close()
        try:
            sat_tles = np.array([
                cysgp4.PyTle(*tle)
                for tle in my_sat_tles
            ])
        except:
            print('error in ')
            print(my_sat_tles)
    return sat_tles



SL1_tles = create_constellation(mjd_epoch,
                                SL1.altitudes, 
                                SL1.inclinations, 
                                SL1.nplanes, 
                                SL1.sats_per_plane)

SL2_tles = create_constellation(mjd_epoch,
                                SL2.altitudes, 
                                SL2.inclinations, 
                                SL2.nplanes, 
                                SL2.sats_per_plane)

OW1_tles = create_constellation(mjd_epoch,
                                OW1.altitudes, 
                                OW1.inclinations, 
                                OW1.nplanes, 
                                OW1.sats_per_plane)

OW2_tles = create_constellation(mjd_epoch,
                                OW2.altitudes, 
                                OW2.inclinations, 
                                OW2.nplanes, 
                                OW2.sats_per_plane)

GW_tles = create_constellation(mjd_epoch,
                                GW.altitudes, 
                                GW.inclinations, 
                                GW.nplanes, 
                                GW.sats_per_plane)



#%% Generate the figure to plot the satellites

vmin, vmax = np.log10(100), np.log10(100000)

# fig, axs = plt.subplots(1,2,figsize=(16,11),
#                         gridspec_kw={'width_ratios': [1, 0.1]},
#                         sharey=True)

fig, axs = plt.subplots(1,1,figsize=(16,11))


# fig = plt.figure(figsize=(15, 7), dpi=100)
# ax = fig.add_axes((0.1, 0.1, 0.8, 0.8)) #main axes
# cax = fig.add_axes((0.91, 0.2, 0.02, 0.5)) #colorbar axes
ax = axs
ax.set_xlabel('Azimuth [deg]')
ax.set_ylabel('Elevation [deg]')
ax.set_xlim((-180, 180))
ax.set_ylim((0, 90))
ax.set_xticks(range(-150, 180, 30))
ax.set_yticks(range(0, 91, 15))
# ax.plot(x_p,y_p,'r',linewidth=2)
# ax.set_aspect('equal')
# ax.set_aspect(1)
ax.grid()
# ax.text(-170, 85, obs_name, fontsize=16)


annot = ax.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                    bbox=dict(boxstyle="round", fc="w"),
                    arrowprops=dict(arrowstyle="->"))
annot.set_visible(False)

plt.plot([],'blue')
plt.plot([],'red')
# plt.plot([],'green')
# plt.plot([],'brown')
# plt.plot([],'purple')
# plt.plot([],'magenta')

# plt.legend(['GEO','Starlink ph1','Starlink ph2','OneWeb ph1','OneWeb ph2','GW'],
           # loc='upper right')

ax.set_title('Current situation qithout Starlink nor OneWeb')

plt.legend(['active','GEO'],
            loc='upper right')


#%% Plot the GEO orbit

def plot_static_sats(tles,
                     time=[],
                     fig=[],
                     vmin = np.log10(100),
                     vmax = np.log10(100000)):
    '''
    

    Parameters
    ----------
    tles : TYPE
        has to be a PyTle object from sgp4
    time : TYPE, optional
        has to be a cysgp4.PyDateTime() object
        
    fig : TYPE, optional
        DESCRIPTION. The default is [].

    Returns
    -------
    fig : TYPE
        DESCRIPTION.

    '''
    
    if fig ==[]:

        
        
        fig = plt.figure(figsize=(15, 7), dpi=100)
        ax = fig.add_axes((0.1, 0.1, 0.8, 0.8)) #main axes
        cax = fig.add_axes((0.91, 0.2, 0.02, 0.5)) #colorbar axes
        ax.set_xlabel('Azimuth [deg]')
        ax.set_ylabel('Elevation [deg]')
        ax.set_xlim((-180, 180))
        ax.set_ylim((0, 90))
        ax.set_xticks(range(-150, 180, 30))
        ax.set_yticks(range(0, 91, 30))
        ax.plot(x_p,y_p,'r',linewidth=2)
        ax.set_aspect('equal')
        ax.grid()
        ax.text(-170, 75, obs_name, fontsize=16)
        
        
        annot = ax.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                            bbox=dict(boxstyle="round", fc="w"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)
        
    else:
        ax = fig.axes[0]
    if time == []:
        my_time = cysgp4.PyDateTime(datetime.datetime.utcnow())
    else :
        my_time = time
    start_mjd = my_time.mjd  # get current time
    trama = 1 #number of moments
    time_sats = 1 #number of seconds 
    td = np.arange(0, trama, time_sats) / 86400.  # 2000 s in steps of 1s
    mjds = start_mjd - td
    
    result = cysgp4.propagate_many(
        mjds[:,np.newaxis],
        tles[np.newaxis,:],
        observer,
        do_sat_azel=False,
        on_error = 'coerce_to_nan', #dismiss error due to too old TLEs
        )
    
    topo_pos = result['topo']
    
    # print(topo_pos.shape)
    topo_pos_az, topo_pos_el, topo_pos_dist, _ = (topo_pos[..., j] for j in range(4))
    topo_pos_az = (topo_pos_az + 180.) % 360. - 180.    
    
    vis_mask = topo_pos_el>0
    
    topo_pos_az = topo_pos_az[vis_mask]
    topo_pos_el = topo_pos_el[vis_mask]
    topo_pos_dist = topo_pos_dist[vis_mask]
    
    points = ax.scatter(
        topo_pos_az[:], topo_pos_el[:],
        c='blue',#np.log10(topo_pos_dist[:]),
        vmin=vmin, vmax = vmax,
        s=5
        )
    return fig

#plot GEO satellites
plot_static_sats(geo_tles, fig = fig)


#%% Calculate position now and x mins later

do_tracks = 0
if do_tracks:
    start_mjd = mjd_epoch
    time_sats = 5 #number of seconds 
    trama = 60*2 #number of moments
    td = np.arange(0, trama, time_sats) / 86400.  # 2000 s in steps of 1s
    mjds = start_mjd + td
    
    result = cysgp4.propagate_many(
        mjds[:,np.newaxis],
        active_tles[np.newaxis,:],
        observer,
        do_sat_azel=False,
        on_error = 'coerce_to_nan',
        )
    
    topo_pos = result['topo']
    
    # print(topo_pos.shape)
    topo_pos_az, topo_pos_el, topo_pos_dist, _ = (topo_pos[..., j] for j in range(4))
    topo_pos_az = (topo_pos_az + 180.) % 360. - 180.    
    
    
    
    for i in range(topo_pos.shape[1]):
        vis_mask = topo_pos[:,i,1]>0 #mask for elevation >0
        if sum(vis_mask)>0:
            topo_pos_az1 = topo_pos_az[vis_mask,i]
            topo_pos_el1 = topo_pos_el[vis_mask,i]
            topo_pos_dist1 = topo_pos_dist[vis_mask,i]
        
            lines = ax.plot(
                topo_pos_az1, topo_pos_el1,'.',
                color='black',
                markersize =1)


#%% propagate and plot function
def propagate(mjds_i,sat_tles,observer):
   result = cysgp4.propagate_many(
       mjds_i[:,np.newaxis],
       sat_tles[np.newaxis,:],
       observer,
       do_sat_azel=False,
       on_error = 'coerce_to_nan',
       )
   
   
   topo_pos = result['topo']
   
   # print(topo_pos.shape)
   topo_pos_az, topo_pos_el, topo_pos_dist, _ = (topo_pos[..., j] for j in range(4))
   topo_pos_az = (topo_pos_az + 180.) % 360. - 180.
   # print(topo_pos_az.shape[0])
   # topo_pos_az2 = np.reshape(topo_pos_az, (topo_pos_az.shape[0]*topo_pos_az.shape[1]))
   # topo_pos_el2 = np.reshape(topo_pos_el, (topo_pos_el.shape[0]*topo_pos_el.shape[1]))
   # topo_pos_dist2 = np.reshape(topo_pos_dist, (topo_pos_dist.shape[0]*topo_pos_dist.shape[1]))
   
   # N = topo_pos_az.shape[1]
   
   
   
   Names = np.array([sat_tles[i].tle_strings()[0] for i in range(len(sat_tles))])

   
   return topo_pos_az, topo_pos_el, topo_pos_dist, Names



def plot_sats(ax,sat_az,sat_el,Names,i,color):
   points = ax.scatter(
       sat_az[i,:], sat_el[i,:],
       c=color,
       s = 10,
       vmin=vmin, vmax = vmax, #
       )
    

    # for j,name in enumerate(Names[vis_mask[0]]):
    #     ax.text(
    #         topo_pos_az1[j], topo_pos_el1[j], '%s'%Names[vis_mask[0]][j],
    #         fontsize=13, ha='left'
    #         )

    
   return points




#%% Propagate all constellations 

step = 1
N_steps = 100 
mjds_i = mjd_epoch + np.linspace(0,N_steps*step,N_steps)/(60*60*24)


propagate_tles = 1

if propagate_tles:
    GEO_tle_az, GEO_tle_el, GEO_tle_dist, GEO_tle_names = propagate(mjds_i,
                                            geo_tles,
                                            observer)
    
    SL_tle_az, SL_tle_el, SL_tle_dist, SL_tle_names = propagate(mjds_i,
                                            starlink_tles,
                                            observer)

    OW_tle_az, OW_tle_el, OW_tle_dist,OW_tle_names = propagate(mjds_i,
                                            starlink_tles,
                                            observer)


    active_tle_az, active_tle_el, active_tle_dist, active_tle_names = propagate(mjds_i,
                                            active_tles,
                                            observer)

    el_min = 0
    N_NGSO = np.sum(SL_tle_el>el_min,axis=1) + np.sum(OW_tle_el>el_min,axis=1)
    N_active = np.sum(active_tle_el>el_min,axis=1)
    
    
else:
    SL1_az, SL1_el, SL1_dist = propagate(mjds_i,
                                            SL1_tles,
                                            observer)
    
    SL2_az, SL2_el, SL2_dist = propagate(mjds_i,
                                            SL2_tles,
                                            observer)
    
    OW1_az, OW1_el, OW1_dist = propagate(mjds_i,
                                            OW1_tles,
                                            observer)
    
    OW2_az, OW2_el, OW2_dist = propagate(mjds_i,
                                            OW2_tles,
                                            observer)
    
    GW_az, GW_el, GW_dist = propagate(mjds_i,
                                            GW_tles,
                                            observer)
    
    el_min = 0
    N_NGSO = np.sum(SL1_el>el_min,axis=1) + np.sum(SL2_el>el_min,axis=1)+\
              np.sum(OW1_el>el_min,axis=1) + np.sum(OW2_el>el_min,axis=1)+\
              np.sum(GW_el>el_min,axis=1)


#%% Calculate some statistics on the propagated satellites

mask = (SL_tle_el>5)[:SL_tle_el.shape[0]-1] # mask for satellites above 5 degrees
d_az = np.diff(SL_tle_az,axis=0)[mask]
d_el = np.diff(SL_tle_el,axis=0)[mask]

d_angle = np.sqrt(d_az**2+d_el**2)*60/step # convert in arcmin/second

d_angle[d_angle>1000] = np.nan

fig2, axs = plt.subplots(1,1,figsize=(16,11))


ax2 = axs
ax2.set_xlabel('Satellite elevation [deg]')
ax2.set_ylabel('Angular speed [arcmin/s]')
# ax2.set_xlim((0, 90))
ax2.set_ylim((0, 100))
# ax.set_xticks(range(-150, 180, 30))
# ax.set_yticks(range(0, 91, 15))
# ax.plot(x_p,y_p,'r',linewidth=2)
# ax.set_aspect('equal')
# ax.set_aspect(1)
ax2.grid()
ax2.set_title('Active Starlinks over %s'%obs_name, fontsize=25)

ax2.scatter(SL_tle_el[:SL_tle_el.shape[0]-1][mask],d_angle)


# el_hist, el_bins = np.histogram(SL_tle_el[SL_tle_el>5],100)

# ax2.plot(el_bins[1::],el_hist,'g')

#%% plot all satellites at all calculated times
fig3, ax3 = plt.subplots(1,1,figsize=(16,11))

ax3.set_xlabel('Azimuth [deg]')
ax3.set_ylabel('Elevation [deg]]')
ax3.set_xlim((-180, 180))
ax3.set_ylim((0, 90))
ax3.set_xticks(range(-150, 180, 30))
ax3.set_yticks(range(0, 91, 15))
# ax.plot(x_p,y_p,'r',linewidth=2)
ax3.set_aspect('equal')
# ax.set_aspect(1)
ax3.grid()
ax3.set_title('Active Starlinks in the %s sky at '%(obs_name) +
              '{:%d/%m/%y}'.format(my_time.datetime) +
              ' for %d seconds'%N_steps, fontsize=25)

ax3.scatter(SL_tle_az[SL_tle_el>5],SL_tle_el[SL_tle_el>5])




#%% plot the SL satellites as they move
fig_title = 'Active satellites and GEO satellites - No Starlink/OneWeb' 

pos = None
sat_indx = None
def animate(i):
   
    global pos
    global sat_indx

    my_time = cysgp4.PyDateTime.from_mjd(mjds_i[i]) 

    points0 = plot_sats(ax,active_tle_az,active_tle_el, active_tle_names,i,'blue') #propagate
    points1 = plot_sats(ax,GEO_tle_az,GEO_tle_el, GEO_tle_names,i,'red') #propagate
    # points2 = plot_sats(ax,SL_tle_az,SL_tle_el,SL_tle_names,i,'red') #propagate
    # points2 = plot_sats(ax,SL2_az,SL2_el,i,'green') #propagate
    # points3 = plot_sats(ax,OW_tle_az,OW_tle_el,OW_tle_names, i,'brown') #propagate
    # points4 = plot_sats(ax,OW2_az,OW2_el,i,'purple') #propagate
    # points5 = plot_sats(ax,GW_az,GW_el,i,'magenta') #propagate
    
 
    
    text_N = ax.text(
        -175, 85, 'Visible sats = %d'%N_active[i],
        fontsize=22, ha='left'
        )
    
    title = ax.set_title(fig_title + '{:%d/%m/%y %H:%M:%S}'.format(my_time.datetime))
    
    fig.canvas.draw()
    
    return points0, points1, title,text_N
    # return points0,points1,points2, points3, points4, points5,title,text_N


anim2 = animation.FuncAnimation(
    fig, animate, frames = N_steps, interval=100, blit=True, cache_frame_data=False
    )

plt.show()
