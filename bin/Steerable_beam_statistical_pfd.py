# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 09:37:57 2020

Satellite power flux density on the site.

The aim is to run a monte carlo simulation with the distribution of the ground terminals
and the position of the satellites. It can be done with one ground terminal (ie one beam)
or many.

Procedure:

1- Define the position of the observer (telescope site)
2- Define the position of the satellites by orbit propagation  [Nsats]
3- Define mesh on the ground of ground terminals [Nmesh]
4- Calculate vectors from satellites to ground terminals and satellites to observer position
5- Calculate steering angle from satellites and boresight angle to the observer
6- Calculate EIRP using steering angle and boresight angle
7- Calculate PFD with EIRP and spreading loss for each beam  [Nmesh, Nsats]

@author: f.divruno
@author: Braam Otto

Last Updated: 18 March 2021
"""

import numpy as np
import matplotlib.pyplot as plt
import pycraf
import astropy.units as u
import astropy.constants as const
from scipy.stats import gaussian_kde
import sys

sys.path += ['../src']

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'
if os.name == 'nt':
    os.system('cls')
else:
    os.system('clear')

import ska as ska
from ska import constellation, ground_terminals

#iterations to propagate the constellation
iters = 5
#number of time steps
time_steps = 10
#duration of each time step in seconds
time_step = 1

#max and min avoid angle and steps
avd_min = 0
avd_max = 25
avd_steps = 5

#beams agreggation trys
Ntrys = 3
#nr of beams aggregated per satellite
Nbeams = 15

print("Sim Parameters:")
print("iters = ", iters)
print("time_steps = ", time_steps)
print("dt = ", time_step)
print("Beam Aggregation Trys = ", Ntrys)
print("Number of Beams = ", Nbeams)

#%% ----------------------
#1 Coordinates of the observer:
obs_lon, obs_lat, obs_alt = 21.443800, -30.712919, 1.052 # deg, deg, km
obs_x,obs_y,obs_z = pycraf.geometry.sphere_to_cart(const.R_earth+obs_alt*u.km,
                                                   obs_lon*u.deg,
                                                   obs_lat*u.deg)

obs_cart = np.array((obs_x.value,obs_y.value,obs_z.value)).T*u.m

print('Observer Coordinates: \nlon: %.2f, \nlat: %.2f, \nalt: %.2f'%(obs_lon, obs_lat, obs_alt))

#%% ----------------------
#2 Satellite constellation

Const = constellation.Constellation(
    const_name = 'Starlink ph1',
    observer_name = 'SKA-Mid',
    observer_geo = np.array((obs_lon, obs_lat, obs_alt)),
    beam_type = 'steerable'
    )

Const.propagate(iters=iters,\
                 time_steps=time_steps,
                 time_step=time_step,
                 verbose=True,
                 )

# fig = plt.figure('Sat_Tracks')
# fig.savefig('figs/Const constellation in geo view')

#%% ----------------------
#3 Mesh on the ground
# there are several options to generate the ground terminals.
# option_mesh = 7 is an arrange of concentric circles around the observatory site.

filepath = ''
mesh = ground_terminals.Ground_terminals(obs_geo=np.array((obs_lon, obs_lat, obs_alt)),
                        filepath=filepath,
                        option_mesh=7,
                        plot=True)

# plt.xlim([11,37])
# plt.ylim([-38,-20])
# fig.savefig('figs/Const ground terminals')

#%%
for avd_angle in np.arange(avd_min, avd_max, avd_steps)*u.deg:
    #% ----------------------
    #4 define satellite characteristics and calculate the PFD on the site due to
    # pointing to each direction in the mesh (per beam)


    #using spfd within constellation class 
    Const.spfd_init(mesh = mesh,
                    avd_angle = avd_angle)

    #only considers single beams to every mesh positoin (for testing purposes only)
    if avd_angle == 0*u.deg:
        spfd_beam = Const.spfd(mesh = mesh,
                    avd_angle=avd_angle,
                    aggregate=False,
                    Ntrys=1,
                    Nbeams=1,
                    )

        pfd_beam = spfd_beam * 4e3*u.Hz

        # fig = plt.figure('pfd beam')
        # plt.hist(10*np.log10(pfd_beam.flatten().value),400,
        #          histtype='step', label = 'avoid angle = %s'%avd_angle)
        # plt.axvline(x=-146,color = 'r',linewidth=3)#, label = 'link budget value')
        # plt.xlabel(str(u.dB(spfd_beam.unit)) + r'/ 4 kHz')
        # plt.legend()
        fig = plt.figure('pfd beam', figsize=(9,6))
        ax1 = fig.add_subplot(111)

        x = 10*np.log10(pfd_beam[~np.isnan(pfd_beam)].flatten().value)
        n_bins = 400
        density = gaussian_kde(x)
        xs = np.linspace(np.min(x), np.max(x), n_bins)
        quantiles, counts = np.unique(x, return_counts=True)
        cumprob = np.cumsum(counts).astype(np.double) / x.size

        ax1.hist(x, n_bins, color='b',
                 histtype='step', density=True,
                 label = 'Avoidance Angle = %s' % avd_angle)

        ax1.plot(xs, density(xs), '--r', linewidth=1, label="Histogram Density")
        ax1.axvline(x=-146,color = 'r',linewidth=2)#, label = 'link budget value'

        ax2 = ax1.twinx()
        # plot the cumulative histogram
        ax2.hist(x, n_bins,
                 density=True, histtype='step',
                 cumulative=True, color='orange', label='CDF',
                 linewidth=1.5)
        ax2.plot(quantiles, cumprob,
                 '--k', linewidth=1,
                 label='Theoretical CDF')
        ax1.set_xlabel('pfd [' + str(u.dB(pfd_beam.unit)) + r' @4 kHz]')
        ax1.set_ylabel('PDF')
        ax2.set_ylabel('CDF')
        ax1.set_title('Power Flux Density Distribution')
        ax1.grid(True, 'both')

        lines_1, labels_1 = ax1.get_legend_handles_labels()
        lines_2, labels_2 = ax2.get_legend_handles_labels()
        lines = lines_1 + lines_2
        labels = labels_1 + labels_2
        ax2.legend(lines, labels, loc=3).set_zorder(2)
        fig.savefig('figs/'+Const.name+'/Single beam pfd avd angle 0 deg')

    #% aggregation of Nbeams in one satellite          
    spfd_agg = Const.spfd(mesh=mesh,
                           avd_angle=avd_angle,
                           aggregate=True,
                           Ntrys=Ntrys,
                           Nbeams=Nbeams)

    pfd_agg = spfd_agg[~np.isnan(spfd_agg)] * 4e3*u.Hz  # W/4kHz

    #calculate statistics of aggregated PFD
    pfd_agg_50 = np.percentile(pfd_agg.flatten(),50,axis=0)
    pfd_agg_90 = np.percentile(pfd_agg.flatten(),90,axis=0)
    pfd_agg_99 = np.percentile(pfd_agg.flatten(),99,axis=0)
    pfd_agg_100 = np.percentile(pfd_agg.flatten(),100,axis=0)

    x = 10*np.log10(pfd_agg[~np.isnan(pfd_agg)].flatten().value)
    n_bins = 400
    density = gaussian_kde(x)
    xs = np.linspace(np.min(x), np.max(x), n_bins)
    quantiles, counts = np.unique(x, return_counts=True)
    cumprob = np.cumsum(counts).astype(np.double) / x.size

    fig = plt.figure('pfd agg', figsize=(9, 6))
    ax1 = fig.add_subplot(111)
    # plt.hist(10*np.log10(pfd_agg.flatten().value),400,
    ax1.hist(x, n_bins,
             histtype='step', density=True,
             label = 'Avoidance Angle = %s' %avd_angle)
    ax1.plot(xs, density(xs), '--r',
             linewidth=1,
             label="Histogram Density")
    ax1.axvline(x=-146,
                color='r',
                linewidth=2)
    ax1.axvline(x= 10*np.log10((pfd_agg_50).value),
                linewidth=2, color='g',
                label='50 perc. Avoidance Angle = %s' % avd_angle)

    # plt.axvline(x= 10*np.log10((pfd_agg_90).value),
    #             linewidth=3,label = '90 percentile',
    #             color = 'blue')
    # plt.axvline(x= 10*np.log10((pfd_agg_99).value),
    #             linewidth=3,label = '99 percentile',
    #             color = 'red')
    ax1.set_xlim([-200,-140])
    ax1.set_xlabel('pfd [' + str(spfd_agg.unit*u.Hz) + r' @4 kHz]')
    ax1.set_ylabel('PDF')

    ax2 = ax1.twinx()
    # plot the cumulative histogram
    ax2.hist(x, n_bins,
             density=True, histtype='step',
             cumulative=True, color='orange', label='CDF',
             linewidth=1.5)
    ax2.plot(quantiles, cumprob,
             '--k', linewidth=1,
             label='Theoretical CDF')
    ax2.set_ylabel('CDF')
    lines_1, labels_1 = ax1.get_legend_handles_labels()
    lines_2, labels_2 = ax2.get_legend_handles_labels()
    lines = lines_1 + lines_2
    labels = labels_1 + labels_2
    ax2.legend(lines, labels, loc=3).set_zorder(2)
    ax1.grid(True, 'both')
    ax1.set_title('Aggregate Power Flux Density Distribution')
    #%

    #statistics in iteration dimension
    spfd_agg_50 = np.percentile(spfd_agg,50, axis=0)

    spfd_agg_90 = np.percentile(spfd_agg,90, axis=0)
    spfd_agg_99 = np.percentile(spfd_agg,99, axis=0)
    spfd_agg_100 = np.percentile(spfd_agg,100, axis=0)

    #% plot pfd in samples
    # plt.figure()
    # plt.plot(10*np.log10(spfd_agg.value*4e3))

    #%6 plot pfd vs elevation of the satellites
    spfd_agg_stats = spfd_agg_50
    idx = ~np.isnan(spfd_agg_stats)
    #plt.figure()
    #cf = plt.tricontourf(sat_geo[idx,0],sat_geo[idx,1],pfd_agg_100[idx].value)
    #fig = plot_geodetic_view(geo_pos[0:1], topo_pos[0:1])

    fig = ska.general.plot_geodetic_view(Const.geo_pos,
                                         Const.topo_pos)
    cs = plt.tricontour(Const.geo_pos[Const.vis_mask][idx,0],
                        Const.geo_pos[Const.vis_mask][idx,1],
                        10*np.log10(spfd_agg_stats[idx].value) \
                            + 10*np.log10(4e3),
                        levels=5)
    plt.clabel(cs,fontsize=10,inline=1,colors='white')
    # plt.xlim([-20,55])
    # plt.ylim([-50,-15])
    plt.title('pfd [dBW/m2/4kHz] from one satellite - Const')

    plt.figure('spfd_vs_el' + str(avd_angle))
    plt.plot(Const.topo_pos[Const.vis_mask][idx,1],
             10*np.log10(spfd_agg_100[idx].value),
             '.',label='100%tile')
    plt.plot(Const.topo_pos[Const.vis_mask][idx,1],
             10*np.log10(spfd_agg_99[idx].value),
             '.',label='99%tile')
    plt.plot(Const.topo_pos[Const.vis_mask][idx,1],
             10*np.log10(spfd_agg_90[idx].value),
             '.',label='90%tile')
    plt.plot(Const.topo_pos[Const.vis_mask][idx,1],
             10*np.log10(spfd_agg_50[idx].value),
             '.',label='50%tile')
    plt.axhline(y=-146-10*np.log10(4e3),color = 'b',linewidth=3)
    plt.title('Spectral power flux density vs elevation for '+ str(avd_angle)+' deg')
    plt.xlabel('elevation [deg]')
    plt.ylabel('spfd [dB %s]'%spfd_agg.unit)
    plt.grid()
    plt.legend()

    #%nterpolation: this function of spfd as a function of elevation can be
    # used in an espfd calculation
    elevation = Const.topo_pos[Const.vis_mask][idx,1]

    #50%tile
    spfd_vs_el = spfd_agg_50[idx].value
    z = np.polyfit(elevation,10*np.log10(spfd_vs_el),5)
    spfd_dB_fit = np.poly1d(z)

    el1 = np.arange(0,90,1)
    s = 10**(spfd_dB_fit(el1)/10)
    s[s<=0] = s[s>0].min()

    plt.figure('spfd_vs_el'+str(avd_angle))
    plt.plot(el1,10*np.log10(s),linewidth=3, label = 'avoid angle = %s'%avd_angle)
    # plt.axhline(y=-146-10*np.log10(4e3),color = 'b',linewidth=3)
    plt.title('Spectral power flux density vs elevation (fit) for '+ str(avd_angle)+' deg')
    plt.xlabel('elevation [deg]')
    plt.ylabel('spfd [dB %s]'%spfd_agg.unit)
    plt.legend()

    #save the fit to use in the constellation
    np.savez('statistical_spfd_dB_avd'+str(avd_angle)+'.npz',el=el1,spfd = spfd_dB_fit(el1))

    plt.figure('fit PFD in 4kHz'+ str(avd_angle))
    plt.plot(el1,10*np.log10(s) + 10*np.log10(4e3))
    plt.xlabel('elevation [deg]')
