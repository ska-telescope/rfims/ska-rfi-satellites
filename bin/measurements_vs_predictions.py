# -*- coding: utf-8 -*-
"""
Created on Tue May 24 09:36:23 2022

Processing satellite measurements and comparing to simulation of satellite positions


@author: f.divruno
"""

import numpy as np
import matplotlib.pyplot as plt
import astropy.time as time
import os
import datetime
import matplotlib 

import astropy.units as u
import astropy.constants as const
import sys
import pycraf

sys.path += ['../src']

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'
if os.name == 'nt':
    os.system('cls')
else:
    os.system('clear')

import ska
from ska import  constellation, telescope


font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 12}
matplotlib.rc('font', **font)


#%% binner function
def binner(arr, t, t_interval, ax = 0, bin_method = 'max'):
    '''
    bin a 2 dimensional matrix in one dimmension 
    in the time_inteval 
    

    Parameters
    ----------
    arr : TYPE
        DESCRIPTION.
    t : TYPE
        DESCRIPTION.
    t_interval : TYPE
        DESCRIPTION.
    bin_method : string, 'max' or 'avg'
        DESCRIPTION. The default is 'max'.

    Returns
    -------
    None.

    '''
    if ax==0:
        if (arr.shape[0] != len(t)):
            print('dimmensions 0 are not equal!')
            return -1,-1
    if ax==1:
        if (arr.shape[1] != len(t)):
            print('dimmensions 1 are not equal!')
            return -1,-1
    
    print('Starting binning...')
    ind0 = 0 #original array index
    to = t[0]
    arr_bin = [] #np.array([t//t_interval,arr.shape[1]]) #new binned array
    t_new = []
    
    if bin_method == 'max':
        method = np.max
    if bin_method == 'min':        
        method = np.min
    if bin_method == 'mean':
        method = np.mean
    
    if ax == 0:
        print('Axis 0')
        for i in range(t.shape[0]):
            if (t[i] >= to+t_interval):
                to = t[i]
                t_new.append(t[i]-t_interval/2)
                arr_bin.append(method(arr[ind0:i,:],axis=0))
                ind0 = i #update the starting index for the original array
                
                
        arr_bin = np.array(arr_bin)
        
    if ax == 1:
        print('Axis 1')
        for i in range(t.shape[0]):
            if (t[i] >= to+t_interval):
                to = t[i]
                t_new.append(t[i]-t_interval/2)
                arr_bin.append(method(arr[:,ind0:i],axis=1).T)
                ind0 = i #update the starting index for the original array
        arr_bin = np.array(arr_bin).T
    
    t_new = np.squeeze(np.array(t_new))
    
    return arr_bin, t_new

            
#%% Slicer function
def slicer(arr, t, vmin, vmax, ax = 0 ):
    '''
    Slice the 2D matrix arr between Vmin and vmax in the ax axis

    Parameters
    ----------
    arr : numpy array [2,x]
        array with frequency and time dimmensions
    t : numpy array
        time array
    vmin : TYPE
        minium value to start the slicing
    vmax : TYPE
        maximum value for the slice
    ax : TYPE, optional
        Axis in ehere the slice is performed. The default is 0.

    Returns
    -------
    arr_new : TYPE
        DESCRIPTION.
    t_new : TYPE
        DESCRIPTION.

    '''
    print('Slicing data...')
    idx = (vmin<t)*(t<vmax)
    idx = np.where(idx)
    t_new = t[idx]
    if ax==0:
        arr_new = arr[idx[0],:]
    else:
        arr_new = arr[:,idx[0]]
    print('Sliced data from %.2f to %.2f'%(vmin,vmax))
    print('Original size: ' + str(arr.shape))
    print('New size: ' + str(arr_new.shape))
    
    return arr_new, t_new

#%% receive power function

def calc_rx_power(Const, point_az, point_el, Nsegments=20):
    '''
    

    Parameters
    ----------
    Const : TYPE
        DESCRIPTION.
    point_az : TYPE
        DESCRIPTION.
    point_el : TYPE
        DESCRIPTION.
    Nsegments : TYPE, optional
        DESCRIPTION. The default is 20.

    Returns
    -------
    None.

    '''
    Ntime = Const.topo_pos.shape[1]
    Nsats = Const.topo_pos.shape[2]
    spfd_sat = Const.spfd(all_flag=False)  # (u.W/u.m**2) 
    
    Nchannels = spfd_sat.shape[-1]    
    # output vectors
    ESPFD_iters = np.zeros([Niters, Ncells, Ntime,Nchannels]) #*u.Jy
    Prx_iters = np.zeros([Niters, Ncells, Ntime,Nchannels]) #*u.W


    vis_mask = Const.vis_mask
    sat_az = Const.topo_pos[vis_mask, 0][None]
    sat_el = Const.topo_pos[vis_mask, 1][None]
    
    
    
    # Nsegments = 20 # cells divided in N steps to accomodate the amount of memory required
    segment = Ncells//(Nsegments-1)
    if segment == 0:
        Nsegments = 1
        segment = Ncells
    
        
    PSD_rx_sparse = np.zeros([segment, 1, Ntime, Nsats,Nchannels])
    
    
    # pointings of the RAS antenna
    p_az = point_az[0, :, None]
    p_el = point_el[0, :, None]
    
    
    for n in range(Nsegments):
        # t_start = time.time()
        # print('slice %d'%i)
        idx = slice(n*segment,
                    (n+1)*segment,
                    1)
        if idx.stop > p_az.shape[0]:
            idx = slice(n*segment,
                        p_az.shape[0],
                        1)
        
        p_az1 = p_az[idx]
        p_el1 = p_el[idx]
        # print('start',p_az1[0],p_az1[-1])
        Ncells1 = p_az1.shape[0]
    
        
        #Angular distance
        theta = pycraf.geometry.true_angular_distance(p_az1*u.deg,p_el1*u.deg,sat_az*u.deg,sat_el*u.deg)
        
        #Gain of the RAS antenna towards the satellites
        RAS_G = Observer.gain(theta, observer_diam, freq, 90*u.percent,
                          high_SLL=high_SLL, SLL = SLL,
                          do_bessel=False) #in lin gain
        
        # Telescope masking 
        if masking:
            RAS_G[theta < boresight_limit] = np.nan
            # when the summation is don in the satellite axis,
            # a flagged satellite in the main beam flags all the time step
            # as the telescope receives the power from all satellites
        
        #effective area        
        A_eff = RAS_G * (((const.c/freq).to_value(u.m))**2/4/np.pi)  * u.m**2
    
        #Received power
        PSD_rx_lin = spfd_sat[None] * A_eff[...,None] #in u.W/u.Hz [pointings,time_satellite,channels]
    
        
        # t_start1 = time.time()
        #convert from dense to sparse matrix [Niters, Ncells1, Ntime, Nsats]
        if Ncells1==PSD_rx_sparse.shape[0]:
            PSD_rx_sparse *= 0
        else:
            PSD_rx_sparse = np.zeros([Ncells1,
                                      1,
                                      Ntime,
                                      Nsats])
        
        PSD_rx_sparse[:, vis_mask] = PSD_rx_lin
        
        PSD_rx_tot = (np.nansum(PSD_rx_sparse, axis=-2))  # sum in satellite dimension [Ncells1,Ntime,Nchannels]
        # t_stop1 = time.time()
        # print('Elapsed time Sparsing matrix:'+ str(t_stop1-t_start1))
        
        #Equivalent PFD
        Prx_tot = PSD_rx_tot * u.W / u.Hz # to W in 1 hz
        
        #save to output vectors
        Prx_iters[0, idx, :] = Prx_tot[:, 0, :] # [Niters,Ncells,Ntime]  in W/Hz
        
        # t_stop = time.time()
        # print(str(n) + ' - Elapsed time slice:'+ str(t_stop-t_start))   
        
    A_eff_max = (np.pi * (observer_diam/2)**2 * 1) * u.m**2 # m2
    ESPFD_iters = Prx_iters / A_eff_max.value # [Niters,Ncells,Ntime]  in W/m2/Hz
        
    return Prx_iters, ESPFD_iters


#%% Load measurement file 
correction_factor = -20 #has to be added to the measured power level to get dBm

folder = r'C:\Users\f.divruno\Dropbox (SKAO)\31- Medidas Electronicas II\Modulos\VMA Simple Spectrum Analyser - 2021-01-09\saved spectrums'
# file = 'recordJBO 2021-07-09'
# file = r'\recordJBO 2021-07-13'
# file = r'\record2021-06-25'
# file = r'\record2021-06-29'
# file = r'\record2022-02-08'
# file = r'\record2022-02-09'
# file = r'\record-higher-2022-02-11'
# file = r'\record2022-03-09'
# file = r'\record2022-03-23'
# file = r'\record2022-03-26'
# file = r'\record2022-05-21' # pointing azel [90,36]
# file = r'\record2022-05-25-newpointing2'
file = r'\record2022-09-06' # pointing azel [90,36]
day = '2022-09-06'

figfolder = 'figs/'

try: #try to open a saved npz file
    print('Loading npz file')
    aux = np.load(folder+file+'.npz')
    aux.allow_pickle= True
    P = aux['P']
    tstamp = aux['tstamp']
    t = aux['t']
    f = aux['freq']
    
    timestamp = [time.Time(i,format='isot') for i in tstamp]
    
    timestamp_utc = [timestamp[i] - 60*60*u.s for i in range(len(timestamp))]
    
    t_mjd = np.array([timestamp_utc[i].mjd for i  in range(len(timestamp))])
    
    # test time vector
    plt.figure()
    plt.title('Test time vector')
    plt.plot((t_mjd-t_mjd[0])*60*60*24, label='time vector  of measurement')
    plt.plot(np.linspace(0,len(t_mjd),len(t_mjd)),label = 'linear time vector')
    
except: #open the original .sal file

    print('Failed loading saved file! \nLoading sal file')    
    with open(folder+file+'.sal','r') as reader:
        # Further file processing goes here
   
        data = reader.readlines()

    #%
    fstart = float(data[0])
    fstop = float(data[1])
    Npoints = int(data[2])
    print('Fstart: %.2f'%fstart)
    print('Fstop: %.2f'%fstop)
    print('Npoints: %.2f'%Npoints)
    f = np.linspace(fstart,fstop,Npoints-1) + 9750

    Header_points = 3
    Frame_points = Npoints*4
    Nframes = (len(data)-Header_points)/Frame_points
    
    P = []
    timestamp = []
    
    for i in range(int(Nframes)):
        print('frame %d of %d'%(i,Nframes))
        offset = Npoints*4*i
        D1 = data[3+offset:3+(Npoints-1)*4+offset]
        
        gps1 = data[4+(Npoints-1)*4+offset]
        gps12 = data[5+(Npoints-1)*4+offset]
        timestamp.append(data[6+(Npoints-1)*4+offset])
        
        Byte1 = D1[0::4]
        Byte2 = D1[1::4]
        
        P.append( ((np.array(Byte1).astype('float32') + np.array(Byte2).astype('float32') * 256) * 0.191187) - 87.139634)
    
    P = np.array(P) + correction_factor
    
    
    #% Time array
   
    start = time.Time(day+'T00:00:00',format='isot')
    print('file loaded: ' + file)
    print('start time: ' + str(start.iso))
    
    t = []
    s0 = 0
    for aux in timestamp:
        hh = float(aux.split(':')[0])
        mm = float(aux.split(':')[1])
        ss = float(aux.split(':')[2][0:2])
        seconds = hh*60*60+mm*60+ss 
        t.append(seconds)
    
    t = np.array(t).astype('float')
    
    # correct for day jumps
    for i  in range(len(t)-1):
        if t[i+1]==t[i]:
            t[i+1] += 0.25
        if (t[i]-t[i+1])>12*60*60:
            t[i+1::] += 24*60*60
            
    seconds = time.TimeDelta(t, format='sec')
    
    tstamp = start + seconds

# Save the processed file   
    timestamp = [time.Time(i,format='isot') for i in tstamp]
    
    np.savez(folder+file, 
             P= P, 
             tstamp= timestamp, 
             t= t, 
             freq= f)
    
    
    
    t_mjd = np.array([timestamp[i].mjd for i  in range(len(timestamp))])
    timestamp_utc = [timestamp[i] - 60*60*u.s for i in range(len(timestamp))]
    
#%% slice the data in bands
fo = 10950
i=0
P0,f0 = slicer(P,f, vmin=fo+250*(i-1), vmax=fo+250*i, ax=1)
i=1
P1,f1 = slicer(P,f, vmin=fo+250*(i-1), vmax=fo+250*i, ax=1)
i=2
P2,f2 = slicer(P,f, vmin=fo+250*(i-1), vmax=fo+250*i, ax=1)
i=3
P3,f3 = slicer(P,f, vmin=fo+250*(i-1), vmax=fo+250*i, ax=1)
i=4
P4,f4 = slicer(P,f, vmin=fo+250*(i-1), vmax=fo+250*i, ax=1)
i=5
P5,f5 = slicer(P,f, vmin=fo+250*(i-1), vmax=fo+250*i-60, ax=1)



#%% SIMULATION - Define constellation parameters 

#Obsever location:
observer_name = 'Home'
observer_geo = np.array((-2.205, 53.15 , 0.1))
observer_diam = 0.3*u.m


# Simulation frequency
freq = 11.7 * u.GHz #use the centre freque as approximation

# number of times the constellation and tesselation is calculated
Niters = 1

# number of time steps
Ntime = 10000 # irrelevant if using the time vector from the measurement

#length of a time step
time_step = 1 #seconds # same as previous comment

# channel bandwidth
BW = 250 * u.MHz

# avoidance angle 
# optional parameter only used for steerable beams constellations
avoid_angle = 0 * u.deg

# Masking:
# masking in the signal chain: a time vector could be defined when a satellite
# has a boresight angle less than a defined value
# In real life this requires good knowledge of the ephemerides of satellites and
# propagation of them.
masking = False
boresight_limit = 1 * u.deg

# Power flagging:
# flag the received power higher than this limit
# of received power in a 4 kHz BW
power_flag = True
T = 20 * u.K # Kelvin
B = 1 *u.Hz#4e3 * u.Hz  # Hz (4 kHz)
k_b = 1.3806E-23 
Plim = (T*k_b * u.W * u.s / u.K).to(u.W/u.Hz)

# print("Tsys = ", T)
# print("BW = ", B)
# print("Plim/Hz = kT = ", 10.*np.log10(Plim.value/1E-3), "dBm/Hz")


RA769_lim = pycraf.protection.ra769_calculate_entry(freq,1*u.Hz,1e-8*u.K,T)[3]

# print("Protection thresgolds as defined in RA.769: %.2f"%RA769_lim.value)

#Antenna receiver model
high_SLL = 0 # Increase the sidelobe level to account for uncertainties
SLL = 4 # dBi Value of sidelobes


# Constellations based on their theretical parameters
# const_name = 'Starlink ph1'
# const_name = 'Starlink ph2'
# const_name = 'OneWeb ph1'
# const_name = 'OneWeb ph2'
# const_name = 'GW'
# const_name = 'GNSS'     #Genrated orbits with parameters
# const_name = 'Geo'

# constellations based on TLEs from Celestrak
# Actual satellites in orbit
# const_name = 'TLE-Starlink'
# const_name = 'TLE-Geo'
# const_name = 'TLE-OneWeb'
# const_name = 'TLE-Active'
# const_name = 'TLE-Orbcomm'
# const_name = 'TLE-Iridium'
# const_name = 'TLE-GNSS'

# constellations based on TLEs from Celestrak from a saved file
# Actual satellites in orbit
const_name = 'TLE-file-Starlink'
# const_name2 = 'TLE-file-Geo'
# const_name2 = 'TLE-file-OneWeb'
# const_name = 'TLE-file-Active'
# TLE_file = 'TLEs/2022-05-21-16-52-22.npz' 
# TLE_file = 'TLEs/2022-05-25-07-49-24.npz'
TLE_file = 'TLEs/20220906_084455.npz'

# using a fixed time
# T = timeA.Time('2022-05-21 16:52:22',format = 'iso')
T = timestamp_utc[0] # THis comes from the KU band measurement script  
epoch_vector = t_mjd
epoch_random = False


# beam_type
# beam_type = 'constant'    
beam_type = 'statistical'   #uses an average pfd obtained from the script "steerable_beams_probability.py"
# beam_type = 'isotropic'   #-146 dBW/m2 is the nominal PFD of these Ku constellations
                            # in 4kHz BW
                            
Nchannels = 1
if 'OneWeb' in const_name:
    Nchannels = 8
    beam_type = 'fixed' #for Oneweb satellites, generates 8 beams.

print('Simulation parameters:\n\
      Constellation: %s \n\
      TLE_file: %s \n\
      Start Time: %s \n\
      Observer: %s \n\
      Observer Location: %s'%(const_name, TLE_file,
                              str(T),observer_name, str(observer_geo)) )
      
    
#%% Pointing direction 
    
# 2- fixed pointing
az,el = [90,36.5] #FDV antenna pointing to East
# az,el = [110, 36.5]

point_az = np.repeat(np.atleast_1d([az])[None,:],Niters,axis=0)
point_el = np.repeat(np.atleast_1d([el])[None,:],Niters,axis=0)

Ncells = point_az.shape[1]
print("Number of Cells = ", Ncells)
print('Ponting direction: az: %s, el: %s '%(str(point_az),str(point_el)))
    

#%%  output vectors and constellation definition

# define the constellation
Const = constellation.Constellation(
    const_name = 'TLE-file-Starlink',\
    observer_name = observer_name,\
    observer_geo = observer_geo,\
    beam_type = beam_type,
    TLE_file = TLE_file,
    nchannels = 1)

Const2 = constellation.Constellation(
    const_name = 'TLE-file-OneWeb',\
    observer_name = observer_name,\
    observer_geo = observer_geo,\
    beam_type = 'fixed',
    TLE_file = TLE_file,
    nchannels = 8)
    

#initialize spfd function
Const.spfd_init(avd_angle = avoid_angle)
Const2.spfd_init(avd_angle = avoid_angle)

print("Constellation defined...done")
    
#%% Definition of telescope observer

Observer = telescope.Telescope(observer_name,
                               observer_geo,
                               diam = observer_diam)
print("Observer defined...done")

#plot observer antenna
theta = np.logspace(-3,np.log10(180),1000)*u.deg
RAS_G = Observer.gain(theta, observer_diam, freq, 100*u.percent,
                 high_SLL=high_SLL, SLL = SLL,
                 do_bessel=False) #in lin gain

fig = plt.figure('Observer antenna model', figsize=(9, 6))
plt.semilogx(theta,10*np.log10(RAS_G), '-k')
plt.title(Observer.name + ' Antenna Model')
plt.xlabel('Boresight Angle [$^o$]')
plt.ylabel('Gain [dBi]')
plt.grid(True, 'both')
plt.tight_layout()
fig.savefig('figs/Ku-band measurements/'+str(T.mjd)
            +'-'+ observer_name + '-' + ' antenna model.jpg')

print("Number of Simulation Iterations =", Niters)



#%% Propagation of satellite positions

Const.propagate(epoch = t_mjd,
                 iters = 1,
                 time_steps = Ntime,
                 time_step = time_step,
                 verbose = False
                 )

vis_mask = Const.vis_mask
sat_az = Const.topo_pos[vis_mask, 0][None]
sat_el = Const.topo_pos[vis_mask, 1][None]

Ntime = Const.topo_pos.shape[1]
Nsats = Const.topo_pos.shape[2]



Const2.propagate(epoch = t_mjd,
                 iters = 1,
                 time_steps = Ntime,
                 time_step = time_step,
                 verbose = False
                 )

vis_mask2 = Const2.vis_mask
sat_az2 = Const2.topo_pos[vis_mask2, 0][None]
sat_el2 = Const2.topo_pos[vis_mask2, 1][None]

Ntime2 = Const2.topo_pos.shape[1]
Nsats2 = Const2.topo_pos.shape[2]




#%% Search beam crossings:

theta = pycraf.geometry.true_angular_distance(
                            Const.topo_pos[0,:,:,0]*u.deg,
                            Const.topo_pos[0,:,:,1]*u.deg,
                            point_az*u.deg,
                            point_el*u.deg)

aux = np.where(abs(theta)<2*u.deg)

#unique satellite
ind_cross_t = []
ind_cross_s = []
for satname in np.unique(aux[1]):
    i = np.where(aux[1]==satname)[0][0]
    ind_cross_t.append(aux[0][i])
    ind_cross_s.append(aux[1][i])
    
ind_cross = [np.array(ind_cross_t), np.array(ind_cross_s)]
ind_cross = aux #every step a satellite is in the main beam


theta2 = pycraf.geometry.true_angular_distance(
                            Const2.topo_pos[0,:,:,0]*u.deg,
                            Const2.topo_pos[0,:,:,1]*u.deg,
                            point_az*u.deg,
                            point_el*u.deg)

aux = np.where(abs(theta2)<2*u.deg)

#unique satellite
ind_cross_t2 = []
ind_cross_s2 = []
for satname2 in np.unique(aux[1]):
    i = np.where(aux[1]==satname2)[0][0]
    ind_cross_t2.append(aux[0][i])
    ind_cross_s2.append(aux[1][i])
    
ind_cross2 = [np.array(ind_cross_t2).astype(int), np.array(ind_cross_s2).astype(int)]
ind_cross2 = [np.array(aux[0]).astype(int), np.array(aux[1]).astype(int)]

#%% Save names
aux = np.load(TLE_file)
aux.allow_pickle = True
# celestrak_geo = np.atleast_1d(aux['geo'])[0]
celestrak_oneweb = np.atleast_1d(aux['oneweb'])[0]
celestrak_starlink = np.atleast_1d(aux['starlink'])[0]
# celestrak_active = np.atleast_1d(aux['active'])[0]

aux = str.split(celestrak_starlink.text,'\r\n')
aux2 = str.split(celestrak_oneweb.text,'\r\n')
satname = [aux[i*3] for i in range(len(aux)//3)]
satnum = [aux[i*3+1][3:7] for i in range(len(aux)//3)]

satname2 = [aux2[i*3] for i in range(len(aux2)//3)]
satnum2 = [aux2[i*3+1][3:7] for i in range(len(aux2)//3)]

# print(satname,satnum)

print('i, timestamp, theta, satnum, satname')
for i in range(len(ind_cross[0])):
    print(i,timestamp[ind_cross[0][i]].iso,theta,satnum[ind_cross[1][i]],satname[ind_cross[1][i]])


#%% Plot satellites above the horizon
plt.figure()
# plt.plot(Const.topo_pos[Const.vis_mask,0],Const.topo_pos[Const.vis_mask,1],'.')
# plt.plot(Const.topo_pos[0,0,:,0],Const.topo_pos[0,0,:,1],'.')
# plt.plot(Const.topo_pos[0,0:1000,:,0],Const.topo_pos[0,0:1000,:,1],alpha = 1)

plt.plot(Const2.topo_pos[0,0,:,0],Const2.topo_pos[0,0,:,1],'.')
plt.plot(Const2.topo_pos[0,0:1000,:,0],Const2.topo_pos[0,0:1000,:,1],alpha = 1)


# plt.ylim([0,90])
plt.plot(point_az, point_el, 'o', color='r')
    
#%% Calculate received power
    
Prx_iters, ESPFD_iters = calc_rx_power(Const, point_az, point_el)
Prx_iters2, ESPFD_iters2 = calc_rx_power(Const2, point_az, point_el)


#%% plot Band 0
t = (t_mjd - t_mjd[0])*60*60*24
limit = -60
data = 10*np.log10(np.sum(10**(P0/10),axis=1))
flag1 = data>limit
fig, ax = plt.subplots(2,1,figsize=(15, 10),
                       gridspec_kw={'height_ratios': [1, 0.25]},
                       sharex=True)

# ax[0].plot(t,10*np.log10(Prx_iters[0,0,:,:])+150,label=[str(i) for i in range(Nchannels)])
ax[0].plot(t, data,color='k',label='Measurement')
flag1 = np.convolve(flag1,np.ones(10),mode='same')>0
# ax[0].plot(t[~flag1]-t[0],data[~flag1])
ax[0].axhline(y= limit,color='red')
ax[0].set_title(' Band 0 flagged: ' + str( np.sum(flag1)) + ' - percentage: ' +str(np.sum(flag1)/len(flag1)*100) )
ax[1].stem(t,flag1)
ax[1].grid()
ax[0].grid()



# ax[0].plot(t,10*np.log10(Prx_iters[0,2,:,5]).T+160)
ax[0].legend()

#%% plot Band 1
t = (t_mjd - t_mjd[0])*60*60*24
limit = -60
data = 10*np.log10(np.sum(10**(P1/10),axis=1))
flag1 = data>limit
fig, ax = plt.subplots(2,1,figsize=(15, 10),
                       gridspec_kw={'height_ratios': [1, 0.25]},
                       sharex=True)


# ax[0].plot(t,10*np.log10(Prx_iters[0,0,:,:])+150,label=[str(i) for i in range(8)])
ax[0].plot(t, data,color='k',label='Measurement')

flag1 = np.convolve(flag1,np.ones(10),mode='same')>0
# ax[0].plot(t[~flag1]-t[0],data[~flag1])
ax[0].axhline(y= limit,color='red')
ax[0].set_title(' Band 1 flagged: ' + str( np.sum(flag1)) + ' - percentage: ' +str(np.sum(flag1)/len(flag1)*100) )
ax[1].stem(t,flag1)
ax[1].grid()
ax[0].grid()
# ax[0].plot(t,10*np.log10(Prx_iters[0,2,:,5]).T+160)
ax[0].legend()

#%% plot Band 2

limit = -62
data = 10*np.log10(np.sum(10**(P2/10),axis=1))
flag2 = data>limit
fig, ax = plt.subplots(2,1,figsize=(15, 10),
                       gridspec_kw={'height_ratios': [1, 0.25]},
                       sharex=True)

# ax[0].plot(t,10*np.log10(Prx_iters[0,0,:,:])+150,label=[str(i) for i in range(8)])
ax[0].plot(t, data,color='k',label='Measurement')

flag2 = np.convolve(flag2,np.ones(10),mode='same')>0
ax[0].plot(t[~flag2]-t[0],data[~flag2])

ax[0].axhline(y= limit,color='red')
ax[0].set_title(' Band 2 flagged: ' + str( np.sum(flag2)) + ' - percentage: ' +str(np.sum(flag2)/len(flag2)*100) )
ax[1].stem(t,flag2)
ax[1].grid()
ax[0].grid()

ax[0].legend()

#%% plot Band 3

limit = -65
data = 10*np.log10(np.sum(10**(P3/10),axis=1))
flag3 = data>limit
fig, ax = plt.subplots(2,1,figsize=(15, 10),
                       gridspec_kw={'height_ratios': [1, 0.25]},
                       sharex=True)

# ax[0].plot(t,10*np.log10(Prx_iters[0,0,:,:])+150,label=[str(i) for i in range(8)])
ax[0].plot(t, data,color='k',label='Measurement')

ax[0].axhline(y= limit,color='red')
ax[0].set_title(' Band 3 flagged: ' + str( np.sum(flag1)) + ' - percentage: ' +str(np.sum(flag1)/len(flag1)*100) )
ax[1].stem(t,flag3)
ax[1].grid()
ax[0].grid()

ax[0].legend()

#%% plot Band 4

limit = -68
data = 10*np.log10(np.sum(10**(P4/10),axis=1))
flag4 = data>limit
fig, ax = plt.subplots(2,1,figsize=(15, 10),
                       gridspec_kw={'height_ratios': [1, 0.25]},
                       sharex=True)

# ax[0].plot(t,10*np.log10(Prx_iters[0,0,:,:])+150,label=[str(i) for i in range(8)])
ax[0].plot(t, data,color='k',label='Measurement')

ax[0].axhline(y= limit,color='red')
ax[0].set_title(' Band 4 flagged: ' + str( np.sum(flag4)) + ' - percentage: ' +str(np.sum(flag4)/len(flag4)*100) )
ax[1].stem(t-t[0],flag4)
ax[1].grid()
ax[0].grid()
ax[0].legend()

#%% plot Band 5
limit = -74
data = 10*np.log10(np.sum(10**(P5/10),axis=1))
flag5 = data>limit
fig, ax = plt.subplots(2,1,figsize=(15, 10),
                       gridspec_kw={'height_ratios': [1, 0.25]},
                       sharex=True)

# ax[0].plot(t,10*np.log10(Prx_iters[0,0,:,:])+150,label=[str(i) for i in range(8)])
ax[0].plot(t, data,color='k',label='Measurement')

ax[0].axhline(y= limit,color='red')
ax[0].set_title(' Band 5 flagged: ' + str( np.sum(flag5)) + ' - percentage: ' +str(np.sum(flag5)/len(flag5)*100) )
ax[1].stem(t-t[0],flag5)
ax[1].grid()
ax[0].grid()

ax[0].legend()


#%% plot All bands together (total power)
data = 10*np.log10(np.sum(10**(P/10),axis=1))
flag_tot = (flag1 + flag2 + flag3 + flag4 + flag5)>0
fig, ax = plt.subplots(2,1,figsize=(15, 10),
                       gridspec_kw={'height_ratios': [1, 0.25]},
                       sharex=True)

ax[0].plot(t, data,color='k',label='Measurement')

# ax[0].axhline(y= limit,color='red')
ax[0].set_title(' Total power flagged: ' + str( np.sum(flag_tot)) + ' - percentage: ' +str(np.sum(flag_tot)/len(flag_tot)*100) )
ax[1].stem(flag_tot)
ax[1].grid()
ax[0].grid()

ax[0].legend()

#%% plot All bands, not summed


data1 = 10*np.log10(np.sum(10**(P1/10),axis=1))
data2 = 10*np.log10(np.sum(10**(P2/10),axis=1))
data3 = 10*np.log10(np.sum(10**(P3/10),axis=1))
data4 = 10*np.log10(np.sum(10**(P4/10),axis=1))
data5 = 10*np.log10(np.sum(10**(P5/10),axis=1))

fig, ax = plt.subplots(2,1,figsize=(15, 10),
                       gridspec_kw={'height_ratios': [1, 0.25]},
                       sharex=True)

ax[0].plot(t,10*np.log10(Prx_iters[0,0,:,:])+160,label=[str(i) for i in range(8)],alpha=0.5)

ax[0].plot(t,data1, label ='Band1')
ax[0].plot(t,data2+3, label ='Band2')
ax[0].plot(t,data3+6, label ='Band3')
ax[0].plot(t,data4+10.5, label ='Band4')
ax[0].plot(t,data5+14, label ='Band5')
ax[1].set_xlabel('Time [s]')

# ax[0].axhline(y= limit,color='red')
ax[0].set_title('All bands together') 
# ax[1].stem(t-t[0],flag_tot)
ax[1].grid()
ax[0].grid()

ax[0].legend()

#%% plot all bands in frequency
P_max = np.max(P,axis=0)
P_99p = np.percentile(P,99,axis=0)
P_90p = np.percentile(P,90,axis=0)
P_baseline = np.percentile(P,80,axis=0)


plt.figure(figsize=[18,10])
plt.plot(f, P_max - P_baseline, label= 'maximum')
plt.plot(f, P_99p - P_baseline, label= '99 percentile')
plt.plot(f, P_90p - P_baseline, label= '90 percentile')
plt.title('Spectrum measurements')
plt.xlabel('Frequency [MHz]')
plt.ylabel('Amplitude [dB] (uncalibrated)')
plt.grid()
plt.legend()
# plt.plot(f, P[0:1000].T - P_baseline[...,None])
plt.savefig('figs/Ku-band measurements/Spectrum' +str(day) +'.jpg')


#%% Waterfall of flagged data
data1 = P - P_baseline[None]
bins = 5
data = np.max(np.reshape(data1,
                  [int(data1.shape[0]//bins),
                   bins,
                   data1.shape[1]]),axis=1)

vmin = -10
vmax = 20


fig, ax = plt.subplots(2,1,figsize=(15, 10),
                       gridspec_kw={'height_ratios': [1, 0.25]},
                       sharex=True)
    
img = ax[0].imshow(data.T, aspect='auto', vmin=vmin, vmax=vmax, origin='lower',  #in mjds
                    extent=[timestamp[0].mjd, timestamp[-1].mjd, f[0], f[-1]])

# img = ax[0].imshow(data.T, aspect='auto', vmin=vmin, vmax=vmax, origin='lower',  #in seconds
#                     extent=[0, t[-1]-t[0], f[0], f[-1]])


ax[0].set_xlabel("Time (s)")
ax[0].set_ylabel("Frequency (MHz)")
cbar = fig.colorbar(img, ax=ax)
cbar.set_label("Power (dB)", rotation=270)

data_integ = 10*np.log10( np.sum(10**(data/10),axis=1))


mjds = np.array([timestamp[i].mjd for i in np.arange(0,len(timestamp),bins)])
# ax[1].plot(t-t[0], data_integ)
ax[1].plot(mjds, data_integ)
ax[1].grid()


#%% Waterfall of flagged data with flags and satellite crossings
data = P - P_baseline[None]
vmin = -10
vmax = 20


fig, ax = plt.subplots(2,1,figsize=(15, 10),
                       gridspec_kw={'height_ratios': [1, 0.25]},
                       sharex=True)
    
img = ax[0].imshow(data.T, aspect='auto', vmin=vmin, vmax=vmax, origin='lower',  #in mjds
                    extent=[timestamp[0].mjd, timestamp[-1].mjd, f[0], f[-1]])

# img = ax[0].imshow(data.T, aspect='auto', vmin=vmin, vmax=vmax, origin='lower',  #in seconds
#                     extent=[0, t[-1]-t[0], f[0], f[-1]])


ax[0].set_xlabel("Time (s)")
ax[0].set_ylabel("Frequency (MHz)")
cbar = fig.colorbar(img, ax=ax)
cbar.set_label("Power (dB)", rotation=270)

data_integ = 10*np.log10( np.sum(10**(data/10),axis=1))


mjds = np.array([timestamp[i].mjd for i in range(len(timestamp))])
# ax[1].plot(t-t[0], data_integ)
# ax[1].plot(mjds, data_integ)
ax[1].plot(mjds,flag_tot,'.')
[ax[1].plot(mjds[ind_cross[0][i]],1,'x',color='green') for i in range(len(ind_cross[0]))]
[ax[1].plot(mjds[ind_cross2[0][i]],1,'x',color='red') for i in range(len(ind_cross2[0]))]
ax[1].grid()




#%% save only the valid data


P_valid = np.zeros((np.sum(~masked_data.mask[:,0]),masked_data.shape[1]))
t_valid = np.zeros((np.sum(~masked_data.mask[:,0]),masked_data.shape[1]))
for i in range(P_valid.shape[1]): #for each frequency range
    P_valid[:,i] = masked_data[:,i].compressed()
    t_valid[:,i] = t[~masked_data.mask[:,i]]

P_valid_sum = 10*np.log10(np.sum(10**(P_valid/10),axis=1))
P_interp = np.interp(t,t_valid[:,0],P_valid_sum)

# find new mean value
N=100
P_s = 10*np.log10((np.convolve(10**(P_interp/10),np.ones(N),mode='same')/N))



#%% All bands together (total power) + interpolation without flagged data
data = 10*np.log10(np.sum(10**(P/10),axis=1))
fig, ax = plt.subplots(2,1,figsize=(15, 10),
                       gridspec_kw={'height_ratios': [1, 0.25]},
                       sharex=True)

# ax[0].plot(data)
# ax[0].plot(P_interp)
# ax[0].plot(P_s)
ax[0].plot(10*np.log10(np.abs(10**(data/10) - 10**(P_s/10))))
# ax[0].axhline(y= limit,color='red')
ax[0].set_title(' Total power flagged: ' + str( np.sum(flag_tot)) + ' - percentage: ' +str(np.sum(flag_tot)/len(flag_tot)*100) )
ax[1].stem(flag_tot)
ax[1].grid()
ax[0].grid()