# -*- coding: utf-8 -*-
"""
Created on Thu Jul 22 13:15:18 2021

Estimate the average power flux density in dBW/m2 in 4kHz bw  of a satellite as 
a function:
    elevation
    maximum steering angle
    number of beams per frequency
    
This estimation is only valid for one channel, it could be assumed that a 
satellite with N channels will produce the same average pfd on a telescope
site in each frequency.

@author: f.divruno
"""

import numpy as np
import matplotlib.pyplot as plt


"""
Created on Fri Aug 28 16:11:03 2020

@author: f.divruno
@author: Braam Otto

Calculation of the average pfd of a satellite constellation with steerable
beam as a function of its parameters.

Copyright (C) 2020+  Federico Di Vruno (F.DiVruno@skatelescope.org)
"""
import matplotlib
import astropy.units as u
import sys
sys.path += ['../src']
from ska import constellation


font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 22}
matplotlib.rc('font', **font)


#%% Parameters
D = 25 * u.km # diameter of a cell on the ground
h = 550 * u.km # constellation altitude
Re = 6371 * u.km #Earth radii
Rs = Re + h
max_alpha = 56 * u.deg # Maximum steerable antgle from satellite
avd_angle = 0 * u.deg


el_min = np.arcsin((Re+h)/Re*np.sin(max_alpha)).to(u.deg) # minimum elevation from ground terminal

# maxangle between satellite and ground terminal from centered on the Earth
theta_p_max  = el_min - max_alpha.to(u.rad)

#maximum distance the satellite can direct its beam to
DT1 = Re*theta_p_max.to_value(u.rad)

# Area illuminated by the satellite
Area = 2*np.pi*(1-np.cos(theta_p_max))*Re**2

# Number of cells in the area:
# In theory how many cells fit in the area illuminated by the satellite
Ncells = Area/ (np.pi*(D/2)**2)

#approximation with a flath Earth
DT = h*np.tan(max_alpha) 
NT = int(DT/D)+1 #number of rings

iters = 2000
N = 18 #number of simultaneous beams to consider
N_el = 90 #number of elevation angles of the observer to consider

plot_figs = 1



#%% generate random set of pointings to the cells on the ground
phi = np.random.rand(N,iters,N_el)*360*u.deg
r = DT1*np.sqrt(np.random.rand(N,iters,N_el)) # distance on the surface of the (spherical) Earth


#bin the random pointings to cells
r[r<=(D/2)] = 0 #centre cell
phi[r<=(D/2)] = 0 #centre cell
for i in range(1,NT+1):
    # find the pointings in the ring 'i'
    r_o = i*D
    ind_R = (r>(r_o-D/2))*(r<=(r_o+D/2))
    r[ind_R] = r_o
    # find the angles in the number of cells within the ring
    phi_o = np.linspace(0,360,int(i*6+1))*u.deg
    for k in range(len(phi_o)-1):
        phi_o_mean = (phi_o[k] + phi_o[k+1])/2
        # print(r_o,theta_o_mean)
        # get index of the angles and radius belonging to the cell
        ind = (phi>phi_o[k])*(phi<=phi_o[k+1])*ind_R
        # print(theta_o[k],theta_o[k+1])
        phi[ind] = phi_o_mean #bin angle


# plot some of the trials with N beams distributed

if plot_figs:        
    fig, axs = plt.subplots(3,3,subplot_kw={'projection': 'polar'})
        
    for i in range(9):
        axs.flatten()[i].plot(phi[:,i,0].to(u.rad),r[:,i,0],'.')
        axs.flatten()[i].set_rmax(DT1.value*1.1)


#nadir angle from the centre of the Earth:
theta_p_pointing = r/Re * u.rad

#steering angle towards the illuminated cells
alpha_pointing = np.arctan2(Re*np.sin(theta_p_pointing),
                            Rs - Re*np.cos(theta_p_pointing))


# Cartesian coordinates of cells illuminated in the satellite ref frame
# since r is the distance on the surface of the Earth, to get the actual r in the XYZ frame of the satellite
# the Z coordinate has to be corrected.
r_p = Re * np.sin(theta_p_pointing)
X_p = r_p*np.cos(phi)
Y_p = r_p*np.sin(phi)

Z_p = - (Rs - Re*np.cos(theta_p_pointing))
Z_p1 = -np.ones([N,iters,N_el])*h # no correction for Earth curvature

if plot_figs:
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    ax.scatter(X_p[0,0],Y_p[0,0],Z_p[0,0],'.')
    plt.title('randomly illuminated cells')
    ax.set_box_aspect([1,1,0.5])
    ax.set_xlabel('km')
    ax.set_ylabel('km')
    ax.set_zlabel('km')

#%% define position of the observer
obs_phi = np.zeros(N_el)*u.deg #phi is not relevant, so considered 0 deg

# as a function of elevation, TODO: account for spherical Earth
# obs_el = np.linspace(0,1,N_el)*89*u.deg
obs_el = np.logspace(-1,0,N_el)*89*u.deg

#satellite-observer distance as a function of elevation
theta = obs_el + np.arcsin( Re / Rs *np.cos(obs_el)) # angle between observer plane and satellite position
theta_p = np.pi/2 * u.rad - theta # angle between observer position and satellite position Earth centered
A = Rs * np.sin(theta_p)
B = Rs * np.cos(theta_p)
h_p = B - Re 
obs_sat_d =  np.sqrt(A**2 + h_p**2)

    
# Cartesian coordinates of positions of the observer
X_obs = Re * np.sin(theta_p) #obs_r*np.cos(obs_theta)
Y_obs = 0*np.ones(theta_p.shape)*u.km #obs_r*np.sin(obs_theta)
Z_obs = - (Rs - Re * np.cos(theta_p))

#distance from the nadir direction of the satellite to observer (cell location)
obs_r = Re * theta_p.to_value(u.rad)

if plot_figs:
    fig, axs = plt.subplots(1,1,figsize=[15,14],subplot_kw={'projection': 'polar'})
    i = 0   
    for j in range(1,NT+1):
        # find the pointings in the ring 'i'
        r_o = j*D
        phi_o = np.linspace(0,360,int(j*6+1))*u.deg
        for k in range(len(phi_o)-1):
            phi_o_mean = (phi_o[k] + phi_o[k+1])/2
            axs.plot(phi_o_mean.to(u.rad),r_o,'.',color='green',alpha=0.2)
    axs.plot(phi[:,i,0].to(u.rad),r[:,i,0],'.',color = "red")
    axs.set_rmax(DT1.value*1.1)
    axs.plot(obs_phi,obs_r,'.',color='blue')   
    axs.plot([],color='green',label='cells')
    axs.plot([],color='red',label='illuminated cells')
    axs.plot([],color='blue',label='observer positions')
    plt.legend()


if plot_figs:
    fig = plt.figure()
    ax = plt.axes(projection='3d')
    ax.scatter(X_p[0,0],Y_p[0,0],Z_p[0,0],'o',color='red')
    ax.scatter(X_obs,Y_obs,Z_obs,'.',color='blue')
    plt.title('randomly illuminated cells + observer location')
    ax.set_box_aspect([1,1,0.5])
    ax.set_xlabel('km')
    ax.set_ylabel('km')
    ax.set_zlabel('km')

#%% calculate angles in the satellite reference frame
# to do this as an approximation the 



#create numpy arrays
R_p = np.transpose(np.array((X_p.to_value(u.m),Y_p.to_value(u.m),Z_p.to_value(u.m))),axes=(1,2,3,0))
R_obs = np.array((X_obs.to_value(u.m),Y_obs.to_value(u.m),Z_obs.to_value(u.m)))[None,None]
R_obs = np.transpose(np.repeat(np.repeat(R_obs,N,axis=0),iters,axis=1),axes=(0,1,3,2))

#angular difference using the dot product with einsum:
phi_cell_obs = np.arccos(np.einsum('...i,...i',R_obs,R_p)\
                /np.linalg.norm(R_obs,axis=-1)\
                /np.linalg.norm(R_p,axis=-1))*u.rad    
 
#avoidance angle:
# if an avoidance angle is selected, it makes nan any pointing direction
# where the phi is less than the avoidance angle
phi_cell_obs[abs(phi_cell_obs)<avd_angle] = np.nan

if plot_figs:
    plt.figure()
    plt.plot((phi_cell_obs.to(u.deg)).flatten()[0:1000],label='angle cell-observer')
    plt.plot((alpha_pointing.to(u.deg)).flatten()[0:1000],label='steering angle')
    plt.legend()
    plt.title('steering angles and boresight angles')

    plt.figure()
    plt.hist(alpha_pointing.to_value(u.deg).flatten(),100)
    plt.title('Histogram of steering angles')
    
    
#%% Calculate EIRP
if plot_figs:
    
    #% using sinc model for EIRP
    theta_test = np.linspace(0,60,100)
    phi_test = np.logspace(-0,np.log10(180),1000)
    theta_test, phi_test = np.meshgrid(theta_test, phi_test)*u.deg
    eirp_test = constellation.single_beam_eirp(theta_test.to(u.deg), phi_test.to(u.deg),plot=plot_figs).value
    plt.figure()
    plt.semilogx(phi_test,10*np.log10(eirp_test/eirp_test.max() ))
    plt.ylim([-55,0])
    plt.grid()

    plt.figure()
    plt.title('Beam pattern used for Starlink satellites')
    angle = np.logspace(-2,np.log10(180),1000)*u.deg
    eirp = constellation.single_beam_csv(0*u.deg, angle)
    plt.plot(angle,eirp.to(u.dB(u.W)))
    plt.xlabel('Angle [deg]')
    plt.ylabel('EIRP')

# calc EIRP
# eirp = constellation.single_beam_eirp(alpha_pointing.to(u.deg), phi_cell_obs.to(u.deg))
eirp = constellation.single_beam_csv(alpha_pointing.to(u.deg), phi_cell_obs.to(u.deg))



print('Mean eirp in 4kHz')
print(np.nanmean(eirp).to(u.dB(u.W)))


#%% Caculate distance and spreading loss
# d = np.linalg.norm(R_obs,axis=-1)*u.m #not considering pherical earth
d = obs_sat_d # Spherical earth
spread_loss = 1/(np.pi*4*d**2)

pfd = eirp*spread_loss

if plot_figs:
    plt.figure()
    plt.hist(pfd.to_value(u.dB(u.W/u.m**2)).flatten(),100, density=True)
    plt.title('PFD histogram - all beams')
    plt.xlabel('PFD dB(W/m2) in 4kHz')
    plt.axvline(x=-146,color ='red',label='Link budget')
    
    


#%% aggregate the pfd from all the beams in the first axis (beams)

pfd_agg = np.nansum(pfd,axis=0)


if plot_figs:
    
    plt.figure()
    plt.hist(pfd_agg.to_value(u.dB(u.W/u.m**2)).flatten(),100, density=True)
    plt.title('Histogram of PFD (aggregated %d beams) histogram '%N)
    plt.xlabel('PFD dB(W/m2) in 4kHz')
    plt.axvline(x=-146,color ='red',label='Link budget')

    plt.figure()
    plt.hist(pfd_agg.to_value(u.dB(u.W/u.m**2)),
             bins = 100,cumulative=1,histtype='step' )
    plt.title('CDF of PFD (aggregated %d beams) histogram '%N)
    plt.axvline(x=-146,color ='red',label='Link budget')
    plt.xlabel('PFD dB(W/m2) in 4kHz')
    
print('Mean pfd in 4kHz')
print(np.nanmean(pfd).to(u.dB(u.W/u.m**2)))

print('Mean Aggregated pfd')
print(np.mean(pfd_agg).to(u.dB(u.W/u.m**2)))


#%%  Calculate mean pfd in all iterations
pfd_mean = np.mean(pfd_agg,axis=0).to(u.dB(u.W/u.m**2))

plt.figure(figsize=[20,15])
plt.plot(obs_el,pfd_mean)
plt.grid()
plt.title('Mean aggregated pfd in 4kHz in one channel of %d beams,\
avd_angle = %.1f deg'%(N,avd_angle.value))
plt.axhline(y = -146,color='red',label='nominal pfd=-146 dBW/m2 in 4 kHz')
plt.legend()
plt.ylabel('mean pfd [dBW/m2 in 4kHz]')
plt.xlabel('satellite elevation in local coordinates [deg]')
plt.ylim([-180,-140])

plt.figure(figsize=[20,15])
plt.plot(obs_el,pfd_mean-(4*u.kHz).to(u.dB(u.Hz)), '-b')
plt.grid()
plt.title('Mean aggregated spfd in one channel of %d beams,\
avd_angle = %.1f deg'%(N,avd_angle.value))
plt.title('Starlink spfd function')

plt.axhline(y = -146 - 10*np.log10(4e3),color='red',label='nominal spfd = %.2f dBW/m2/Hz'%(-146 - 10*np.log10(4e3)))
plt.legend()
plt.ylabel('mean spfd [dBW/m2/Hz]')
plt.xlabel('satellite elevation in local coordinates [deg]')
plt.ylim([-225,-175])
plt.tight_layout()

#%%Fit to use in simulations 


pfd_mean[~np.isfinite(pfd_mean)] = np.nan
spfd_vs_el = pfd_mean.value - 10*np.log10(4e3) #spfd in W/m**2/Hz
# z = np.polyfit((obs_el.value)[1::],spfd_vs_el[1::],4)
# spfd_dB_fit = np.poly1d(z)

# el1 = np.arange(0,90,1)
# s = spfd_dB_fit(el1)
# plt.plot(el1, s,color = 'green')
# np.savez('steerable_spfd_avd'+str(avd_angle)+'.npz',spfd = 10**(spfd_dB_fit(el1)/10), el=el1)


from scipy import interpolate
x = obs_el
y = 10**(spfd_vs_el/10)
f = interpolate.interp1d(x, y,fill_value='extrapolate')

el1 = np.arange(10,90,3)
plt.plot(el1, 10*np.log10(f(el1)),color = 'green')
np.savez('../src/ska/starlink_spfd_vs_el_%dbeams_%ddegavoidance.npz'%(N,avd_angle.value),f=f)
    
