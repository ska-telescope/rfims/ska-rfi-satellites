# -*- coding: utf-8 -*-
"""
Script to find and plot all geosynchronous satellites listed in CELESTRAK

Download the TLEs of GEO satellites from celestrak

Propagate the position of the satellites and calculate the az/el at a defined
observer location

Plots the positions of the satellites in az/el (local sky) including the name
of the satellites. This is very useful to identify visible satellites and their
position in the local sky.


The script also includes a simplified raster plan to sweep angles close to a 
GEO satellite with the purpose of antenna beam calibration.


"""

import requests
import datetime
import astropy.time as time
import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib import animation, rc
from matplotlib import colors

import cysgp4
import sys

sys.path += ['../src']

import ska.constellation as constellation

font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 22}
import matplotlib
matplotlib.rc('font', **font)


#%% define the observer location:

#YEBES:
# observer = cysgp4.PyObserver(-3.08853, 40.523475, 0.915)
# obs_name = 'Yebes 13.2m'


#Fede home:
observer = cysgp4.PyObserver( -2.204997,  53.161276 , 0.1) 
obs_name = 'Fede Home'


#JBO:
# observer = cysgp4.PyObserver(-2.306790, 53.235035 , 0.1) 
# obs_name = 'Jodrell Bank'

# #Tenerife:
# observer = cysgp4.PyObserver( -16 , 28 , 0.1) 
# obs_name = 'GroundBird'


#SKA-MID:
# observer = cysgp4.PyObserver(21.4,-30.7,1) 
# obs_name = 'SKA-MID'

# Effelsberg:
observer = cysgp4.PyObserver(6.8828,50.5247,0.3) 
obs_name = 'Effelsberg'

# pointing direction of the observer antenna
obs_el = 31.54290
obs_az = 166.96711
obs_diam = 100 #m this is the observer antenna diameter
obs_freq = 11e9 #Hz
obs_BW = 3e8/obs_freq/obs_diam*180/np.pi/0.8 #half power BW
obs_pointing = np.array([obs_az, obs_el]) #az, el in deg

#annotate the bw direction with a circle of the size of the beam
l = np.linspace(0,np.pi*2)
x_b = obs_BW/2*np.cos(l)
y_b = obs_BW/2*np.sin(l)
x_p = x_b + obs_pointing[0]
y_p = y_b + obs_pointing[1]


#%% Get the TLEs from Celestrak
#Active satellites from celestrak
#celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/active.txt')

#OneWeb satellites from celestrak
# celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/oneweb.txt')

#Starlink satellites from celestrak
celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/starlink.txt')


# celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/geo.txt')

#all the active satellites in the sky
celestrak_active = requests.get('https://celestrak.com/NORAD/elements/active.txt')

#Geostationary satellites from celestrack
celestrak_geo = requests.get('https://celestrak.com/NORAD/elements/geo.txt')



# np.savez('TLEs/' + time.Time.now().iso[0:10] ,
#          starlink = celestrak_latest,
#          geo = celestrak_geo)


sat_tles = np.array(cysgp4.tles_from_text(celestrak_latest.text))
geo_tles = np.array(cysgp4.tles_from_text(celestrak_geo.text))
active_tles = np.array(cysgp4.tles_from_text(celestrak_active.text))



##Is necesary to remove the falcon 9 deb
satname = 'FALCON 9 DEB'
aux = []
for k in range(len(sat_tles)):
    if satname in sat_tles[k].__repr__():
        aux.append(k)
sat_tles = np.delete(sat_tles, aux)


     

#%% from a description of the orbits (theoretical):

#Options for constellations:
# const_name = 'Starlink ph1'
const_name = 'Starlink ph2'
# const_name = 'OneWeb ph1'
# const_name = 'OneWeb ph2'
# const_name = 'GW'
# const_name = 'GNSS'     #Genrated orbits with parameters
# const_name = 'Geo'

# constellations based on TLEs from Celestrak
# Actual satellites in orbit
# const_name = 'TLE-Starlink'
# const_name = 'TLE-Geo'
# const_name = 'TLE-Orbcomm'
# const_name = 'TLE-Iridium'
# const_name = 'TLE-GNSS'

obs_geo = np.array((observer.loc.lon, observer.loc.lat, observer.loc.alt))


SL1 = constellation.Constellation('Starlink ph1',
                           obs_name,
                           obs_geo)

SL2 = constellation.Constellation('Starlink ph2',
                           obs_name,
                           obs_geo)

OW1 = constellation.Constellation('OneWeb ph1',
                           obs_name,
                           obs_geo)
OW2 = constellation.Constellation('OneWeb ph2',
                           obs_name,
                           obs_geo)

GW = constellation.Constellation('GW',
                           obs_name,
                           obs_geo)

Total = constellation.Constellation('Total',
                           obs_name,
                           obs_geo)




my_time = cysgp4.PyDateTime(datetime.datetime.utcnow())
mjd_epoch = my_time.mjd  # get current time


def create_constellation(mjd_epoch,
                         altitudes,
                         inclinations,
                         nplanes,
                         sats_per_plane,
                         rand_seed=0):
    my_sat_tles = []
    sat_nr = 1
    seed_cont = 1
    for alt, inc, n0, s0 in zip(
        altitudes, inclinations, nplanes, sats_per_plane):

        # Satellite Mean Anomoly
        mas0 = np.linspace(0.0, 360.0, s0, endpoint=False)  # distribution of mean anomalies around the plane
        #         mas0 += np.random.uniform(0, 360, 1) # check this, replaced by (1)
        # Satellite Right Ascension
        raans0 = np.linspace(0.0, 360.0, n0, endpoint=False)  # right ascensions distribution
        mas, raans = np.meshgrid(mas0, raans0)

        if rand_seed >= 0 :
            np.random.seed()
            np.random.seed(rand_seed*seed_cont)
            seed_cont += 1
        else:
            np.random.seed()
        # Satellite Mean Anomoly Randomness
        mas += np.random.uniform(0.0, 360.0, n0)[:, np.newaxis]  # (1) # random phase of the satellites
        # mas += np.arange(0,n0*p0,p0)[:,np.newaxis] #(2) # phase of the satellites in the planes
        mas, raans = mas.flatten(), raans.flatten()

        # Satellite Mean Motion
        mm = cysgp4.satellite_mean_motion(alt)
        for ma, raan in zip(mas, raans):
            my_sat_tles.append(
                cysgp4.tle_linestrings_from_orbital_parameters(
                    'TEST {:d}'.format(sat_nr), sat_nr, mjd_epoch,
                    inc, raan, 0.001, 0., ma, mm
                ))

            sat_nr += 1

        with open('TEST_TLEs.txt', 'w') as myfile:
            for line in my_sat_tles:
                myfile.write(str(line[0]) + "\n" +
                             str(line[1]) + "\n" +
                             str(line[2]) + "\n")
        myfile.close()
        try:
            sat_tles = np.array([
                cysgp4.PyTle(*tle)
                for tle in my_sat_tles
            ])
        except:
            print('error in ')
            print(my_sat_tles)
    return sat_tles



# SL1_tles = create_constellation(mjd_epoch,
#                                 SL1.altitudes, 
#                                 SL1.inclinations, 
#                                 SL1.nplanes, 
#                                 SL1.sats_per_plane)

# SL2_tles = create_constellation(mjd_epoch,
#                                 SL2.altitudes, 
#                                 SL2.inclinations, 
#                                 SL2.nplanes, 
#                                 SL2.sats_per_plane)

# OW1_tles = create_constellation(mjd_epoch,
#                                 OW1.altitudes, 
#                                 OW1.inclinations, 
#                                 OW1.nplanes, 
#                                 OW1.sats_per_plane)

# OW2_tles = create_constellation(mjd_epoch,
#                                 OW2.altitudes, 
#                                 OW2.inclinations, 
#                                 OW2.nplanes, 
#                                 OW2.sats_per_plane)

# GW_tles = create_constellation(mjd_epoch,
#                                 GW.altitudes, 
#                                 GW.inclinations, 
#                                 GW.nplanes, 
#                                 GW.sats_per_plane)



#%% Generate the figure to plot the satellites

vmin, vmax = np.log10(100), np.log10(100000)

# fig, axs = plt.subplots(1,2,figsize=(16,11),
#                         gridspec_kw={'width_ratios': [1, 0.1]},
#                         sharey=True)

fig, axs = plt.subplots(1,1,figsize=(16,11))


# fig = plt.figure(figsize=(15, 7), dpi=100)
# ax = fig.add_axes((0.1, 0.1, 0.8, 0.8)) #main axes
# cax = fig.add_axes((0.91, 0.2, 0.02, 0.5)) #colorbar axes
ax = axs
ax.set_xlabel('Azimuth [deg]')
ax.set_ylabel('Elevation [deg]')
ax.set_xlim((-180, 180))
ax.set_ylim((0, 90))
# ax.set_xticks(range(-150, 180, 30))
# ax.set_yticks(range(0, 91, 15))
# ax.plot(x_p,y_p,'r',linewidth=2)
# ax.set_aspect('equal')
# ax.set_aspect(1)
ax.grid()
# ax.text(-170, 85, obs_name, fontsize=16)


annot = ax.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                    bbox=dict(boxstyle="round", fc="w"),
                    arrowprops=dict(arrowstyle="->"))
annot.set_visible(False)

plt.plot([],'blue')
# plt.plot([],'red')
# plt.plot([],'green')
# plt.plot([],'brown')
# plt.plot([],'purple')
# plt.plot([],'magenta')

plt.legend(['GEO','Starlink ph1','Starlink ph2','OneWeb ph1','OneWeb ph2','GW'],
           loc='upper right')



#%% Plot the GEO orbit

def plot_GEO_sats(tles,
                     time=[],
                     fig=[],
                     vmin = np.log10(100),
                     vmax = np.log10(100000)):
    '''
    

    Parameters
    ----------
    tles : TYPE
        has to be a PyTle object from sgp4
    time : TYPE, optional
        has to be a cysgp4.PyDateTime() object
        
    fig : TYPE, optional
        DESCRIPTION. The default is [].

    Returns
    -------
    fig : TYPE
        DESCRIPTION.

    '''
    
    if fig ==[]:

        
        
        fig = plt.figure(figsize=(15, 7), dpi=100)
        ax = fig.add_axes((0.1, 0.1, 0.8, 0.8)) #main axes
        cax = fig.add_axes((0.91, 0.2, 0.02, 0.5)) #colorbar axes
        ax.set_xlabel('Azimuth [deg]')
        ax.set_ylabel('Elevation [deg]')
        ax.set_xlim((-180, 180))
        ax.set_ylim((0, 90))
        ax.set_xticks(range(-150, 180, 30))
        ax.set_yticks(range(0, 91, 30))
        ax.plot(x_p,y_p,'r',linewidth=2)
        ax.set_aspect('equal')
        ax.grid()
        ax.text(-170, 75, obs_name, fontsize=16)
        
        
        annot = ax.annotate("", xy=(0,0), xytext=(20,20),textcoords="offset points",
                            bbox=dict(boxstyle="round", fc="w"),
                            arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)
        
    else:
        ax = fig.axes[0]
    if time == []:
        my_time = cysgp4.PyDateTime(datetime.datetime.utcnow())
    else :
        my_time = time
    start_mjd = my_time.mjd  # get current time
    trama = 1 #number of moments
    time_sats = 1 #number of seconds 
    td = np.arange(0, trama, time_sats) / 86400.  # 2000 s in steps of 1s
    mjds = start_mjd - td
    
    result = cysgp4.propagate_many(
        mjds[:,np.newaxis],
        tles[np.newaxis,:],
        observer,
        do_sat_azel=False,
        on_error = 'coerce_to_nan', #dismiss error due to too old TLEs
        )
    
    topo_pos = result['topo']
    
    # print(topo_pos.shape)
    topo_pos_az, topo_pos_el, topo_pos_dist, _ = (topo_pos[..., j] for j in range(4))
    topo_pos_az = (topo_pos_az + 180.) % 360. - 180.    
    
    vis_mask = topo_pos_el>0
    
    topo_pos_az1 = topo_pos_az[vis_mask]
    topo_pos_el1 = topo_pos_el[vis_mask]
    topo_pos_dist1 = topo_pos_dist[vis_mask]


    Names = np.array([tles[i].tle_strings()[0] for i in range(len(tles))])


    points = ax.scatter(
        topo_pos_az1[:], topo_pos_el1[:],
        c='blue',#np.log10(topo_pos_dist[:]),
        vmin=vmin, vmax = vmax,
        s=5
        )
    
    for j,name in enumerate(Names[vis_mask[0]]):
        ax.text(
            topo_pos_az1[j], topo_pos_el1[j], '%s'%Names[vis_mask[0]][j],
            fontsize=13, ha='left'
            )

    return Names,topo_pos_az, topo_pos_el, topo_pos_dist



#plot GEO satellites
GEO_names, GEO_az, GEO_el, GEO_dist = plot_GEO_sats(geo_tles, fig = fig)

mask = np.where(GEO_el>0)
print_all_visible=1
if print_all_visible:
    k = np.argsort(GEO_names[mask[1]])
    print('All visible GEO satellites in ',obs_name)
    for i in k:
        print(GEO_names[mask[1][i]],':', 'az: %.5f'%GEO_az[0][mask[1][i]], ' - el: %.5f'%GEO_el[0][mask[1][i]])


#%% planned raster observation with Mark II

# find a satellite per name:
sat_name1 = 'EUTELSAT 16A'
k_1 = []
for i  in range(len(GEO_names)):
    if sat_name1 in GEO_names[i]:
        k_1.append(i)

sat_name2 = 'AMOS-17'
k_2 = []
for i  in range(len(GEO_names)):
    if sat_name2 in GEO_names[i]:
        k_2.append(i)


print('\nPosition of in AZ EL from the site: ',obs_name)
print(sat_name1,'-', 'az: %.2f'%GEO_az[0][k_1[0]], ' - el: %.2f'%GEO_el[0][k_1[0]])
print(sat_name2,'-', 'az: %.2f'%GEO_az[0][k_2[0]], ' - el: %.2f'%GEO_el[0][k_2[0]])
print('\n')

ax.scatter(GEO_az[0][k_1[0]], GEO_el[0][k_1[0]],s=20,c='r')
ax.scatter(GEO_az[0][k_2[0]], GEO_el[0][k_2[0]],s=20,c='r')
                                      
# Raster on Satellite 1
az0 = GEO_az[0][k_1[0]]
el0 = GEO_el[0][k_1[0]]

eli = np.linspace(-2.5/60,2.5/60,200)
azi = np.linspace(-2.5/60,2.5/60,200)
azi,eli = np.meshgrid(azi,eli)

ax.scatter(az0+azi,el0+eli,c='g',s=40,alpha = 0.1)


# Raster on Satellite 2
az0 = GEO_az[0][k_2[0]]
el0 = GEO_el[0][k_2[0]]

eli = np.linspace(-2.5/60,2.5/60,200)
azi = np.linspace(-2.5/60,2.5/60,200)
azi,eli = np.meshgrid(azi,eli)

ax.scatter(az0+azi,el0+eli,c='b',s=40,alpha = 0.1)




