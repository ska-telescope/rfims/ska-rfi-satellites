# -*- coding: utf-8 -*-
"""
Created on Fri Jun 10 16:08:02 2022

Vusialization of the geometry and density of different constellations


@author: f.divruno
"""

import numpy as np
# import pycraf
import astropy.units as u
import astropy.constants as const
import astropy.time as Time
# from pycraf import geometry as geometry
import matplotlib.pyplot as plt
import sys

sys.path += [r'../src']

# import ska

# from ska.ground_terminals import Ground_terminals
from ska.constellation import Constellation

   
font = {'family' : 'normal',
    'weight' : 'bold',
    'size'   : 22}

import matplotlib
matplotlib.rc('font', **font)

FIGPATH = r'figs/'

#%% Observers

#SKA-Mid
# obs_geo = np.array((21.7,-30,1)) # SKA_mid
# obs_name = 'SKA-Mid South Africa'

#JBO
# obs_geo = np.array((-2.306790, 53.235035 , 0.1)) #JBO
# obs_name = 'Jodrell Bank Observatory'

#MWA
# obs_geo = np.array((117, -26 , 0.1)) #JBO
# obs_name = 'MWA Australia'

#Onsala
obs_name = 'Onsala'
obs_geo = np.array(( 11.917778,57.393056, 0.02)) # long,lat,alt, [deg, deg, km]


#Wetzell
# obs_name = 'Wetzell'
# obs_geo = np.array((12.87889957, 49.14419937, 0.6)) # long,lat,alt, [deg, deg, km]

#%% 
beam_type = 'statistical'
# beam_type = 'statistical_constant'
# beam_type = 'isotropic'
# beam_type = 'fixed'

#create constellation
# const_name = 'OneWeb ph1'
# const_name = 'Starlink ph1'
# const_name = 'TLE-Geo'
Const_ST1 = Constellation(
    'Starlink ph1',
    obs_name,
    obs_geo,
    beam_type = beam_type)

Const_OW2 = Constellation(
    'OneWeb ph2',
    obs_name,
    obs_geo,
    beam_type = beam_type)


Const_ST2 = Constellation(
    'Starlink ph2',
    obs_name,
    obs_geo,
    beam_type = beam_type)



Const_OW1 = Constellation(
    'OneWeb ph1',
    obs_name,
    obs_geo,
    beam_type = beam_type)


Const_TLEGEO = Constellation(
    'TLE_Geo',
    obs_name,
    obs_geo,
    beam_type = beam_type)

Const_TLEActive2018 = Constellation(
    'TLE_Active2018nogeo',
    obs_name,
    obs_geo,
    beam_type = beam_type)


Const_GW = Constellation(
    'GW',
    obs_name,
    obs_geo,
    beam_type = beam_type)


#propagate for a certain epoch
Niters= 1
Ntime= 100
time_step = 1

epoch = Time.Time.now().mjd

Const_ST1.propagate( epoch = epoch,
                  iters= Niters,
                  time_steps = Ntime,
                  time_step = time_step,
                  verbose=True,
                  rand_seed=10)

Const_OW1.propagate( epoch = epoch,
                  iters=Niters,
                  time_steps = Ntime,
                  time_step = time_step,
                  verbose=True,
                  rand_seed=10)

# Const_TLEGEO.propagate( epoch = epoch,
#                   iters=Niters,
#                   time_steps = Ntime,
#                   time_step = time_step,
#                   verbose=True,
#                   rand_seed=10)

# Const_ST2.propagate( epoch = epoch,
#                   iters=Niters,
#                   time_steps = Ntime,
#                   time_step = time_step,
#                   verbose=True,
#                   rand_seed=10)

# Const_OW2.propagate( epoch = epoch,
#                   iters=Niters,
#                   time_steps = Ntime,
#                   time_step = time_step,
#                   verbose=True,
#                   rand_seed=10)

# Const_GW.propagate( epoch = epoch,
#                   iters=Niters,
#                   time_steps = Ntime,
#                   time_step = time_step,
#                   verbose=True,
#                   rand_seed=10)

# Const_TLEActive2018.propagate( epoch = epoch,
#                   iters=Niters,
#                   time_steps = Ntime,
#                   time_step = time_step,
#                   verbose=True,
#                   rand_seed=10)


#%%  Polar plot
Const_l = [Const_TLEGEO,Const_TLEActive2018, Const_ST1, Const_OW1, Const_GW, Const_OW2, Const_ST2]

Const_l=[Const_ST1, Const_OW1]

# Const_l=[Const_TLEActive2018]

for i in range(len(Const_l)):
    Const = Const_l[i]
    az = Const.topo_pos[Const.vis_mask,0]*np.pi/180
    el = 1 - Const.topo_pos[Const.vis_mask,1]/90
    sat_dist = Const.topo_pos[Const.vis_mask,2]
    az0 = Const.topo_pos[0,0,Const.vis_mask[0,0,:],0]*np.pi/180
    el0 = 1 - Const.topo_pos[0,0,Const.vis_mask[0,0,:],1]/90
    sat_dist0 = Const.topo_pos[0,0,Const.vis_mask[0,0,:],2]
    
    vis_sats = np.sum(Const.vis_mask[0,0].flatten())
    
    # plt.figure('Sats polar',figsize=[15,15])
    
    if i==0: 
        plt.figure(figsize=[25,15])
        # plt.tight_layout()
    # plt.figure(figsize=[15,15])
    # plt.tight_layout()
    axs = plt.subplot(projection='polar')
    axs.set_theta_zero_location("N")
    axs.set_yticks( ticks = np.linspace(0,1,10),labels=[str(int(i)) for i in np.linspace(90,0,10)],alpha=0.6)
    axs.set_xticks( ticks = [0,np.pi/2,np.pi,3*np.pi/2],labels = ['N','W','S','E'])
    # axs.plot(90*np.pi/180,0.8,'.')
    
    # mask_h = np.where((el<(1-10/90)))
    # mask_h0 = np.where((el0<(1-10/90)))
    mask_h = np.where((el<(1-0)))
    mask_h0 = np.where((el0<(1-0)))
    if len(mask_h0[0])>0:
        axs.plot(az[mask_h],el[mask_h],'.', alpha=0.1,
                 markersize=5, 
                 label=Const.name+' - total=%d Above 10deg =%d'%(Const.topo_pos.shape[2],len(mask_h0[0])))  
    
        cm = plt.cm.get_cmap('viridis')
        vmin = np.log10(sat_dist[mask_h]).min()
        vmax = np.log10(sat_dist[mask_h]).max()
        my_cmap = cm(plt.Normalize(vmin,vmax)(np.log10(sat_dist0[mask_h0])))
        # points = axs.scatter(
        #     az0[mask_h0], el0[mask_h0],
        #     c = my_cmap,
        #     s = 30,
        #     vmin=vmin, vmax = vmax, #
        #     )
        points = axs.scatter(
            az0[mask_h0], el0[mask_h0],
            c = 'black',
            s = 30,
            vmin=vmin, vmax = vmax, #
            )
    
        # if i==0:
            # [axs.annotate(xy=[az0[mask_h0][j],el0[mask_h0][j]],
            #              text= np.array(Const_l[i].satname)[Const_l[i].vis_mask[0,0,:]][mask_h0][j],
            #              fontsize =15,
            #              alpha = 0.5) for j in range(len(mask_h0[0]))]
        
        axs.set_ylim([0,1])
        # axs.set_title(Const.name + ' satellites seen from '+obs_name)
        plt.tight_layout()
        plt.legend(loc='upper left',
                   bbox_to_anchor=(0.8, 1),
                   fontsize = 15)
        
        plt.title('Sky view from %s in %d seconds'%(Const.observer_name,Ntime))
        # plt.savefig('figs/polar_view_'+Const.observer_name+'_%d.jpg'%i)
  

#%% plot a polar plot from a side view
#useful?

x = np.cos(az[(az<np.pi)*(az>0)])*np.sin(el[(az<np.pi)*(az>0)]*np.pi/2)
y = np.cos(el[(az<np.pi)*(az>0)]*np.pi/2)
x0 = np.cos(az0[(az0<np.pi)*(az0>0)])*np.sin(el0[(az0<np.pi)*(az0>0)]*np.pi/2)
y0 = np.cos(el0[(az0<np.pi)*(az0>0)]*np.pi/2)
plt.figure('Sky view',figsize=[15,15])
axs = plt.subplot(projection='rectilinear')
axs.plot(x,y,'.', alpha=0.1,markersize=15,color='orange')
axs.plot(x0,y0,'.', alpha=1,markersize=15,color='black')


#%% ****************************** 
# plot the horizontal sky density
# as satellites/sec per beam
# 

el_min = 5
Const = Const_l[0]

# bins for histogram:
az = np.linspace(0,361,361)
el = np.linspace(0,89,89)

sat_az = Const.topo_pos[...,0]
sat_el = Const.topo_pos[...,1]

point_mask = Const.topo_pos[...,1] > el_min # mask for minimum pointing elevation

#Calculate the 2d histogram of the distribution of satellites above 0 elev
# number of satellites in sky cell
H,xedges, yedges = np.histogram2d(sat_az[point_mask].flatten(),
                                  sat_el[point_mask].flatten(),
                                  bins=[az,el])



#calculate steradians per grid point:
az_cent = (az[0:-1] + az[1:])/2
el_cent = (el[0:-1] + el[1:])/2
az_cent,el_cent = np.meshgrid(az_cent,el_cent)

az_d = (az[1:] - az[0:-1])
el_d = (el[1:] - el[0:-1])
az_d,el_d = np.meshgrid(az_d,el_d)

deg2 = el_d*az_d*np.cos(np.radians(el_cent))
sr = deg2*(np.pi/180)**2 # sr in rad**2


# Move from histogram to satellites per steradian per second
H = H.T
Hsr = H/Niters/Ntime/time_step/sr


# Define beam width to use in the calculation, can be HPBW or a defined value
beam_w = np.radians(0.5) 
# beam_w = HPBW 
beam_sr = 2*np.pi*(1-np.cos(beam_w))


Hsr = H/Niters/Ntime/sr*beam_sr

fig = plt.figure(figsize=(15, 7), dpi=100)
ax = fig.add_axes((0.1, 0.1, 0.8, 0.8)) #main axes
plt.title(Const.name+ ' - %s - %.2f deg beamwidth'%(Const.observer_name,np.degrees(beam_w*2)))

cax = fig.add_axes((0.88, 0.1, 0.02, 0.8)) #colorbar axes
ax.set_xlabel('Azimuth [deg]')
ax.set_ylabel('Elevation [deg]')
ax.set_xlim((0, 360))
ax.set_ylim((0, 90))
ax.set_xticks(range(0, 360, 30))
ax.set_yticks(range(0, 100, 10))
ax.set_aspect('equal')

# ax.axhline(y=TEL.min_el.value,color='red')


c = ax.imshow(Hsr, cmap= plt.cm.jet,
              interpolation='nearest', origin='lower',
               aspect= 2,
               extent=[xedges[0], xedges[-1], yedges[0], yedges[-1]], vmax=Hsr.max(), vmin=Hsr.min())



plt.colorbar(c,label='nr of sats/sec',cax=cax)

mean_sky_prob = np.mean(Hsr[(el_cent>el_min)].flatten())

ax.annotate(xy = (150,85),
            text = 'average sky (el>%.0f deg) = %.6f sats/sec'%(el_min,mean_sky_prob),color='white')

ax.annotate(xy = (150,80),
            text = 'max (el>%.0f deg) = %.4f sats/sec'%(el_min,Hsr[(el_cent>el_min)].max()),color='white')

# plt.savefig('figs/Densities/Density_rect_'+Const.observer_name+'_'+Const.name+'.jpg')


#plot polar
fig = plt.figure(figsize=[20,17])
axs = plt.subplot(projection='polar')
cax = fig.add_axes((0.88, 0.1, 0.02, 0.8)) #colorbar axes

theta = np.radians(az)
r = 1-el/90
theta,r = np.meshgrid(theta, r)


im = axs.pcolormesh(theta, r, (Hsr),
                    cmap='viridis', shading='auto',
                    vmax=Hsr.max(),vmin=Hsr.min())
plt.colorbar(im,label='nr of sats/sec',cax=cax)

axs.set_theta_zero_location("N")
axs.set_yticks( ticks = np.linspace(0,1,7),labels=[str(int(i)) for i in np.linspace(90,0,7)])
axs.set_xticks( ticks = [0,np.pi/2,np.pi,3*np.pi/2],labels = ['N','W','S','E'])

mean_sky_prob = np.mean(Hsr[(el_cent>el_min)].flatten())

axs.annotate(xy = (np.pi/4,0.8),
            text = 'average sky (el>%.0f deg) = %.6f sats/sec'%(el_min,mean_sky_prob),color='white')

axs.annotate(xy = (np.pi/4,0.9),
            text = 'max (el>%.0f deg) = %.4f sats/sec'%(el_min,Hsr[(el_cent>el_min)].max()),color='white')
axs.set_title(Const.name+ ' - %s - %.2f deg beamwidth'%(Const.observer_name,np.degrees(beam_w*2)))

# plt.savefig('figs/Densities/Density_polar_'+Const.observer_name+'_'+Const.name+'.jpg')

#Look for maximum pointing directions:
idx_max = np.where(Hsr>=Hsr.max()*0.95)
print('Maximum pointing direction:')
print('Az: '+str(az[idx_max[1]]))
print('El: '+str(el[idx_max[0]]))
#%% plot histogram by altitude
# TODO: needs some tweaking to work. 

Nconst = len(Const_l)

plt.figure(figsize=[15,13])
plt.loglog()
b = np.logspace(2,4,100)
# b = np.linspace(0,50e3,100)
line = []
for i in range(Nconst):
    geo_pth = Const_l[i].geo_pos
    a,b = np.histogram(geo_pth[0,:,:,2].flatten(),bins=b) # histogram as a function of latitude
    plt.bar((b[1:]+b[0:-1])/2,a,width=b[1:]-b[0:-1],alpha=1, label = Const_l[i].name)

plt.legend(loc='upper right')



#%% plot satelite density as a function of Latitude
deg2_sr = 3282.8 

N = 90
b = np.linspace(0,90,N+1)*u.deg
a_1 = 2*np.pi*(1-np.sin(b[1:]))  #upper spherical cap
a_0 = 2*np.pi*(1-np.sin(b[0:-1])) #lower spherical cap
area = (a_0 - a_1)*deg2_sr  # area in square deg
b = np.linspace(-90,90,2*N+1)
area = np.array([area[-1::-1],area]).flatten()

plt.figure(figsize=[14,10])
for i in range(len(Const_l)):
    geo_pth = Const_l[i].geo_pos
    a,b = np.histogram(geo_pth[0,:,:,1].flatten(),bins=b) # histogram as a function of latitude
    plt.plot(a/area,(b[1:]+b[0:-1])/2, label = Const_l[i].name)#,label = 'Integrated satellite RFI in %d GHz BW'%(RFI_BW/1e9))
    
plt.title('Satellite density in latitude')
plt.xlabel('Sats/deg^2')
plt.ylabel('Lat [deg]')
plt.legend(loc='upper right')
plt.xlim([0,40])
plt.grid()
# plt.savefig(fig_folder+'Densities all.png')

#%%
# each constellation in a figure
# get the satellite density without any megaconstellation
geo_pth = Const[0][-1].geo_pos
a1,b = np.histogram(geo_pth[0,:,:,1].flatten(),bins=b) # histogram as a function of latitude

for i in range(len(Const_l)):
    plt.figure(figsize=[14,10])
    geo_pth = Const_l[i].geo_pos
    a,b = np.histogram(geo_pth[0,:,:,1].flatten(),bins=b) # histogram as a function of latitude
    plt.plot(a/area,(b[1:]+b[0:-1])/2, label = Const_l[i].name)
    plt.plot(a1/area,(b[1:]+b[0:-1])/2, label = Const_l[-1].name)
    
    # [plt.axhline(y=Const[0][i].inclinations[k],
    #              label=str(Const[0][i].inclinations[k]))
    #              for k in range(len(Const[0][i].inclinations))]
                     
    plt.title('Satellite density in latitude')
    plt.xlabel('Sats/deg^2')
    plt.ylabel('Lat [deg]')
    plt.xlim([0,40])
    plt.legend(loc='upper right')
    plt.grid()
    # plt.savefig(fig_folder+'Densities ' +Const_l[i].name +'.png')

#%% plot constellations in geodetic view but a cut
i = 0
C = Const_l[i]
time_idx = 0
plot_all = True
Re = const.R_earth

Rsat = np.linalg.norm(C.eci_pos,axis=-1) * u.m

# fig = plt.figure(num='Spherical_Sat_Tracks', figsize=(10, 10))
fig = plt.figure(figsize=(20, 20))
ax = fig.add_subplot(111, projection='3d',)

idx = np.argsort(C.altitudes)

shell_ind = 0
for alt, inc, n0, s0 in zip(
    C.altitudes, C.inclinations, C.nplanes, C.sats_per_plane):
    

    ind0 = np.sum(C.nplanes[0:shell_ind]*C.sats_per_plane[0:shell_ind])
    print('Shell',shell_ind)
    shell_ind +=1        

    phi =\
        np.deg2rad(C.geo_pos[0,0,ind0:ind0+s0*n0,
                            0]*u.deg)

    # Polar/Elevation or Latitude = THETA
    theta =\
        np.deg2rad(90*u.deg - C.geo_pos[0,0,ind0:ind0+s0*n0,
                                        1]*u.deg)

    # THETA, PHI = np.meshgrid(theta, phi)
    THETA = theta
    PHI = phi
    # Altitude
    R = Re + C.geo_pos[0,0,ind0:ind0+s0*n0,
                          2] * u.km

    # Cartesian Coordinates [ECEF]
    X = R * np.sin(THETA) * np.cos(PHI)
    Y = R * np.sin(THETA) * np.sin(PHI)
    Z = R * np.cos(THETA)
    
    ax.scatter(X, Y, Z,'.', s=10, alpha=0.5,label='Shell %d'%shell_ind)

M = 1.1
ax.set_xlim([-Re.to_value(u.m)*M,Re.to_value(u.m)*M])
ax.set_ylim([-Re.to_value(u.m)*M,Re.to_value(u.m)*M])
ax.set_zlim([-Re.to_value(u.m)*M,Re.to_value(u.m)*M])

ax.set_xlabel('km',labelpad=20)
ax.set_ylabel('km',labelpad=20)
ax.set_zlabel('km',labelpad=20)

ax.set_title(C.name+'\n 30000 satellites ')
# # ax.grid(False)
# # ax.axis('off')
# ax.set_xticks([])
# ax.set_yticks([])
# ax.set_zticks([])
# plt.legend(loc='upper right')

el_view = 20
ax.view_init(azim = 45, elev = el_view)
fig.savefig(r'figs/Orbital shells' + C.name + '-'+str(el_view)+'.png')


#%% 3D view of active satellites in 2018
i = 4
C = Const[0][i]
time_idx = 0
plot_all = True
Re = const.R_earth

Rsat = np.linalg.norm(C.eci_pos,axis=-1) * u.m

# fig = plt.figure(num='Spherical_Sat_Tracks', figsize=(10, 10))
fig = plt.figure(figsize=(20, 15))
ax = fig.add_subplot(111, projection='3d')


phi =\
    np.deg2rad(C.geo_pos[0,0,:,
                        0]*u.deg)

# Polar/Elevation or Latitude = THETA
theta =\
    np.deg2rad(90*u.deg - C.geo_pos[0,0,:,
                                    1]*u.deg)

# THETA, PHI = np.meshgrid(theta, phi)
THETA = theta
PHI = phi
# Altitude
R = Re + C.geo_pos[0,0,:,
                      2] * u.km



# Cartesian Coordinates [ECEF]
X = R * np.sin(THETA) * np.cos(PHI)
Y = R * np.sin(THETA) * np.sin(PHI)
Z = R * np.cos(THETA)

ind = R<=Re + 2000*u.km
ax.scatter(X[ind], Y[ind], Z[ind],'.', s=10, alpha=0.5,label='LEO')

ind = (R>=Re + 2000*u.km) * (R<=Re*6)
ax.scatter(X[ind], Y[ind], Z[ind],'.', s=10, alpha=0.5,label='HEO')

ind = (R>=Re*6)*(R<=Re*7)
ax.scatter(X[ind], Y[ind], Z[ind],'.', s=10, alpha=0.5,label='GEO')
M = 1.1
ax.set_xlim([-Re.to_value(u.m)*M,Re.to_value(u.m)*M])
ax.set_ylim([-Re.to_value(u.m)*M,Re.to_value(u.m)*M])
ax.set_zlim([-Re.to_value(u.m)*M,Re.to_value(u.m)*M])

ax.set_title(C.name)
ax.grid(False)
ax.axis('off')
ax.set_xticks([])
ax.set_yticks([])
ax.set_zticks([])
plt.legend(loc='upper right')

el_view = 90
ax.view_init(azim = 25, elev = el_view)
fig.savefig(fig_folder+'Orbital shells' + C.name + '-'+str(el_view)+'.png')




#%% Number of satellites above horizon as function of the latitude

N_vis = np.ones([Nlats,Nconst])

for k in range(Nconst): #constellations
    for i in range(Nlats): #observer 
        topo_pos = Const[i][k].topo_pos # az,el,d,d_rate 
        geo_pos = Const[i][k].geo_pos # az,el,d,d_rate 
        mask = (topo_pos[0,:,:,1] > 0) & (geo_pos[0,:,:,2]<4000)
        
        N_vis[i,k] = np.sum(mask)/topo_pos.shape[1] #divide by the number of timesteps



plt.figure()
plt.plot(np.array(observer_geo)[:,1], N_vis, label = [Const[0][i].name for i in range(Nconst)])

plt.legend()



fig, ax = plt.subplots(1,1,figsize=[15,8])

for i in range(Nconst):
    ax.plot(np.array(observer_geo)[:,1], N_vis[:,i], label = Const[0][i].name,
            linestyle='-', marker= i+4, markersize =15)

ax.plot(np.array(observer_geo)[:,1], np.sum(N_vis,axis=1), label = 'Sum',
        linestyle='-', marker= i+5, markersize =15)

ax.grid(linestyle='--')
ax.set_xlabel('Latitude of the observer')
ax.set_ylabel('N')
ax.set_title('Nr of satellites above the horizon')

# Customize the tick marks and turn the grid on
ax.minorticks_on()
ax.tick_params(which='major', length=10, width=2, direction='in')
ax.tick_params(which='minor', length=5, width=2, direction='in')
ax.tick_params(which='both', bottom=True, top=True, left=True, right=True) 

ax.set_xlim([-60,60])
ax.set_ylim([0,np.sum(N_vis,axis=1).max()*2])

# ax.axhline(y = A769[-3].value, color = 'grey', linestyle = '--', label='RA769 protection level')
# ax.annotate('Total',
#             xy=(300, -210), xycoords= 'data', color='grey')
# ax.annotate('Layer 1',
#             xy=(400, -228), xycoords= 'data', color='r')

# ax.annotate('RA769',
#             xy=(400, -245), xycoords= 'data', color='grey')

# ax.annotate('Total AAS',
#             xy=(40, -180), xycoords= 'data', color='black', alpha=1)

plt.legend(loc='upper right')

