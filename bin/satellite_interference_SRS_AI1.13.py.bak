# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 02:10:11 2020


@author: f.divruno
@author: Braam Otto

Last updated: 29 March 2021
"""

import astropy.units as u
import astropy.constants as const
from astropy import time as timeA
import numpy as np
import time
import matplotlib.pyplot as plt
import sys
import pycraf.protection as protection
import pycraf
import pycraf.conversions as cnv
from scipy.stats import percentileofscore
import astropy.time as astrotime

sys.path += ['../src']

import os
os.environ['KMP_DUPLICATE_LIB_OK']='True'
if os.name == 'nt':
    os.system('cls')
else:
    os.system('clear')

import ska
from ska import  constellation, telescope

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
        'size'   : 12}
import matplotlib 
matplotlib.rc('font', **font)


FIGPATH = 'fig'
DATAPATH = 'out'

import os
from contextlib import suppress

with suppress(IOError):
    os.makedirs(FIGPATH)

with suppress(IOError):
    os.makedirs(DATAPATH)

pjoin = os.path.join


def timestamp():
    return str(astrotime.Time.now())


#%% Simulation parameters

#observer
# observer_name = 'Tenerife'
# observer_geo = np.array( (-16.51 , 28.3 , 0.1)) # deg, deg, km
# observer_diam = 2.25*u.m

#JBO:
# observer_name = 'Home'
# observer_geo = np.array((-2.205, 53.15 , 0.1))
# observer_diam = 0.3*u.m


#observer
observer_name = 'SKA-MID'
observer_geo = np.array((21.443800, -30.712919, 1.052)) # deg, deg, km

observer_ecef = pycraf.geometry.sphere_to_cart(6371*u.km+observer_geo[2]*u.km,
                                          observer_geo[0]*u.deg,
                                          observer_geo[1]*u.deg
                                          )
observer_diam = 15*u.m
observer_efficiency = 90*u.percent

# SRS observer
SRS_name = 'SRS Earth station'
SRS_geo = np.array((21.4, -30.712919, 1.052)) # deg, deg, km
# SRS_geo = np.array((21.443800, -30.712919, 1.052)) # deg, deg, km
SRS_ecef = pycraf.geometry.sphere_to_cart(6371*u.km+SRS_geo[2]*u.km,
                                          SRS_geo[0]*u.deg,
                                          SRS_geo[1]*u.deg
                                          )

angle_RAS_SRS = pycraf.geometry.true_angular_distance(SRS_geo[0]*u.deg,
                                              SRS_geo[1]*u.deg,
                                              observer_geo[0]*u.deg,
                                              observer_geo[1]*u.deg)



# Simulation frequency
freq = 15.37 * u.GHz #use the centre freque as approximation

# number of times the constellation and tesselation is calculated
Niters = 1000

# number of time steps
Ntime = 2000

#length of a time step
time_step = 1 #seconds

# channel bandwidth
BW = 50 * u.MHz

T = 20 * u.K # Kelvin
B = 1 *u.Hz#4e3 * u.Hz  # Hz (4 kHz)
k_b = 1.3806E-23 
Plim = (T*k_b * u.W * u.s / u.K).to(u.W/u.Hz)

print("Tsys = ", T)
print("BW = ", B)
print("Plim/Hz = kT = ", 10.*np.log10(Plim.value/1E-3), "dBm/Hz")


RA769_lim_dBW_Hz = protection.ra769_limits()[12][5]
RA769_lim_dBW = protection.ra769_limits()[12][6]



#Antenna receiver model
high_SLL = 0 # Increase the sidelobe level to account for uncertainties
SLL = 4 # dBi Value of sidelobes


# Constellations based on their theretical parameters
# const_name = 'Starlink ph1'
# const_name = 'Starlink ph2'
# const_name = 'OneWeb ph1'
# const_name = 'OneWeb ph2'
# const_name = 'GW'
# const_name = 'GNSS'     #Genrated orbits with parameters
# const_name = 'Geo'
const_name = 'SRS AI1.13 USA'
const_name = 'SRS AI1.13'

# constellations based on TLEs from Celestrak
# Actual satellites in orbit
# const_name = 'TLE-Starlink'
# const_name = 'TLE-Geo'
# const_name = 'TLE-OneWeb'
# const_name = 'TLE-Active'
# const_name = 'TLE-Orbcomm'
# const_name = 'TLE-Iridium'
# const_name = 'TLE-GNSS'

# constellations based on TLEs from Celestrak from a saved file
# Actual satellites in orbit
# const_name = 'TLE-file-Starlink'
# const_name = 'TLE-file-Geo'
# const_name = 'TLE-file-OneWeb'
# const_name = 'TLE-file-Active'
TLE_file = 'TLEs/2022-05-21-16-52-22.npz' 

# using a fixed time
T = timeA.Time('2022-05-21 16:52:22',format = 'iso')
# T = timestamp[0] # THis comes from the KU band measurement script  
epoch_fix = T.mjd
epoch_random = True

#satellite params:

# beam_type
beam_type = 'SA.509'     
# beam_type = 'constant'    
# beam_type = 'statistical'   #uses an average pfd obtained from the script "steerable_beams_probability.py"
# beam_type = 'isotropic'   #-146 dBW/m2 is the nominal PFD of these Ku constellations
                            # in 4kHz BW
                            
#antenna diam
antenna_diam = 0.38*u.m
antenna_efficiency = 0.55

#Tx power
tx_power_dBW = 5*cnv.dB_W

Nchannels = 1
if 'OneWeb' in const_name:
    Nchannels = 8
    beam_type = 'fixed' #for Oneweb satellites, generates 8 beams.
    

    


#string to use in saving figures
sim_id = 0
increment_filename = 1
if increment_filename:
    while(1):
        try: 
            basename = '{:s}_{:s}_{:d}'.format(const_name, observer_name,sim_id)
            info_file_name = pjoin(FIGPATH, basename + '_info_and_results.txt')
            open(info_file_name)
            sim_id += 1
        except:
            break
else:
    basename = basename = '{:s}_{:s}_{:d}'.format(const_name, observer_name,sim_id)
    info_file_name = pjoin(FIGPATH, basename + '_info_and_results.txt')

sim_flags = 'sim_id: {:d}'.format(sim_id)

# function to print to file
infofile = open(info_file_name, 'w')
def print_info(*args, **kwargs):
    print(*args, **kwargs)
    print(*args, **kwargs, file=infofile)
    infofile.flush()

print_info('\n\nDate time: {:s}'.format(str(astrotime.Time.now())))
print_info(info_file_name)
print_info('\nConfiguration ')
print_info('-------------------')
print_info('Frequency: {:.2f}'.format(
    freq.to(u.MHz)
    ))
print_info('Iterations: {:d}'.format(Niters))
print_info('Number of time steps: {:d}'.format(Ntime))
print_info('Time step duration: {:.4f} s'.format(time_step))

print_info('BW of RAS channel: {:.2f}'.format(
    BW.to(u.MHz)
    ))
print_info('RA769 limit in RAS channel: {:.2f} // {:.2f}'.format(
    RA769_lim_dBW.to(cnv.dB_W), RA769_lim_dBW_Hz.to(cnv.dB_W_Hz)
    ))



print_info('\nObserver: {:s}'.format(observer_name))
print_info('\tGeo location: {:.4f} deg, {:.4f} deg, {:.4f} km (lon,lat,height)'.format(
    observer_geo[0],observer_geo[1],observer_geo[2]
    ))
print_info('\tAntenna diameter: {:.2f}'.format(
    observer_diam
    ))
 
print_info('\nSRS Earth Station: {:s}'.format(observer_name))
print_info('\tGeo location: {:.4f} deg, {:.4f} deg, {:.4f} km (lon,lat,height)'.format(
    SRS_geo[0],SRS_geo[1],SRS_geo[2]
    ))

print_info('\tDistance from SRS to RAS: {:.2f}'.format(
      (angle_RAS_SRS.to_value(u.rad)*const.R_earth).to(u.km)
      ))




#%% 
load_file = False
if load_file:
    try:
        # to get the values back from saved values
        # aux = np.load('out/'+Constellation_name+'/out '+sim_flags[-5] + '.npz')
        aux = np.load('out/'+const_name+'/out '+sim_flags + '.npz')
        aux.allow_pickle = True
        Prx_iters = aux['Prx_iters'] # in u.W
        Niters = aux['Niters']
        Ntime = aux['Ntime']
        time_step = aux['time_step']
        BW = aux['BW']
        masking = aux['masking']
        boresight_limit = aux['boresight_limit'] 
        power_flag = aux['power_flag'] 
        T = aux['T']
        B = aux['B']
        Plim = aux['Plim'] # in *u.W
        high_SLL = aux['high_SLL']
        SLL = aux['SLL']
        point_el = aux['point_el']
        point_az = aux['point_az']
        sim_flags = str(aux['sim_flags'])
        observer_name = str(aux['observer_name'])
        observer_diam = aux['observer_diam']
        const_name = str(aux['const_name'])
        observer_geo = aux['observer_geo']
        Ncells = aux['Ncells']
        Nchannels = aux['Nchannels']
        cell_lon = aux['cell_lon']
        cell_lat_low = aux['cell_lat_low']
        cell_lat_high = aux['cell_let_high']
        cell_lon_low = aux['cell_lon_low']
        cell_lon_high = aux['cell_lon_high']
        
        A_eff_max = (np.pi * (observer_diam/2)**2 * 1)  #in * u.m**2 # m2
        ESPFD_iters = Prx_iters / A_eff_max / (4e3) # W/m2/Hz
        p_az = point_az[0,:,None]
        p_el = point_el[0,:,None]
        
        observer = ska.Telescope(observer_name, observer_geo)
        Const = ska.Constellation(const_name,
                                   observer_name,
                                   observer_geo)
    
        print('success loading: ')
        print('out/'+const_name+'/out '+sim_flags + '.npz')
    except:
        print('Error loading file')
        
        
else:
    # POINTING OPTIONS:
    # 1- tesselate the semiphere (horizontal coordinates)
    min_el = 10*u.deg
    max_el = 90*u.deg
    az_step = 4*u.deg
    point_az, point_el,grid_info =\
        ska.general.sky_cells_m1583(niters=Niters, step_size=az_step,
                                    lat_range=(min_el, max_el),
                                    test = 0
                                   )
        
    # 2- fixed pointing
    # Best fit to the pointing from home is az=101, el=33.2. Can be verified with OneWeb satellites
    # in the day capture file = r'\record2022-05-21' at 4488 seconds. 
    # point_az = np.repeat(np.atleast_1d([97.8, 105])[None,:],Niters,axis=0)
    # point_el = np.repeat(np.atleast_1d([27.8, 26])[None,:],Niters,axis=0)
      
    
    Ncells = point_az.shape[1]
    A_eff_max = (np.pi * (observer_diam/2)**2 *
                 observer_efficiency.to_value(cnv.dimless) )
    
    # output vectors
    ESPFD_iters = np.zeros([Niters, Ncells,Nchannels]) #*u.Jy
    Prx_iters = np.zeros([Niters, Ncells,Nchannels]) #*u.W
    
    # define the constellation
    Const = constellation.Constellation(
        const_name = const_name,\
        observer_name = observer_name,\
        observer_geo = observer_geo,\
        beam_type = beam_type,
        TLE_file = TLE_file)
    
    
    #initialize spfd function
    Const.spfd_init()
    
    print_info('\nConstellation: {:s}'.format(const_name))
    print_info('\tHeights [km]: '+ str(Const.altitudes))
    print_info('\tInclinations [deg]: '+ str(Const.inclinations))
    print_info('\tNumber of planes: '+ str(Const.nplanes))
    print_info('\tSatellites per plane: '+ str(Const.sats_per_plane))
    print_info('\tAntenna beam: {:s}'.format(beam_type))
    print_info('\tAntenna diam: {:.2f}'.format(antenna_diam))
    print_info('\tAntenna efficiency: {:.2f}'.format(antenna_efficiency))
    print_info('\tTx power: {:.2f}'.format(tx_power_dBW))
    print_info('\tAntenna pointing: follow SRS ES'.format())
    print_info('\tNumber of channels: {:.2f}'.format(Nchannels))
    
    #%% define the telescope receiver
    Observer = telescope.Telescope(observer_name,
                                   observer_geo,
                                   diam = observer_diam)
    print("Observer defined...done")
    
    #plot observer antenna
    theta = np.logspace(-3,np.log10(180),1000)*u.deg
    RAS_G = Observer.gain(theta, observer_diam, freq, observer_efficiency,
                     high_SLL=high_SLL, SLL = SLL,
                     do_bessel=False) #in lin gain
    
    fig = plt.figure('Observer antenna model', figsize=(9, 6))
    plt.semilogx(theta,10*np.log10(RAS_G), '-k')
    plt.title(Observer.name + ' Antenna Model')
    plt.xlabel('Boresight Angle [$^o$]')
    plt.ylabel('Gain [dBi]')
    plt.grid(True, 'both')
    plt.tight_layout()
    fig.savefig(pjoin(FIGPATH,basename+' RAS antenna gain.jpg'))
    
    print_info('\nRAS antenna: '.format())
    print_info('\tAntenna model: ITU-R RA.1631')
    print_info('\tDiameter: {:.2f} '.format(observer_diam))
    print_info('\tEfficiency: {:.2f} '.format(observer_efficiency))
    print_info('\tMax gain: {:.2f} '.format((RAS_G.max()*cnv.dimless).to(cnv.dB)))   
    print_info('\tMax Effective area: {:.2f} '.format(A_eff_max))
    print_info('\tMin elevation: {:.2f} '.format(min_el))
    print_info('\tMax elevation: {:.2f}'.format(max_el))
    print_info('\tAzimuth steps: {:.2f}'.format(az_step))
    print_info('\tTotal number of cells: {:.2f}'.format(Ncells))
                                                

    #%% Propagation and received power
    
    
    print_info('\nStarting simulation '+ timestamp())
    print_info('---------------------------')
    print_info('Info: Max pfd of SRS n-GSO satellite is -114 dBW/m2/MHz according to SA.2141')
    for m in range(Niters):
        print_info('Iteration: %d'%m)
        niter = m
    
        # pointings of the RAS antenna
        p_az = point_az[niter, :, None]
        p_el = point_el[niter, :, None]
    
    
        # positions of the satellites in the horizontal frame
        if epoch_random:
            epoch = float(51544.5 + np.random.rand(1)*10)
        else:
            epoch = epoch_fix
        
        print_info('Simulation epoch: {:s}'.format(
            astrotime.Time(epoch,format='mjd').to_value('iso')
            ))
        
        Const.propagate(epoch = epoch,
                         iters = 1,
                         time_steps = Ntime,
                         time_step = time_step,
                         verbose = False
                         )
    
        vis_mask = Const.vis_mask
        sat_az = Const.topo_pos[vis_mask, 0][None]
        sat_el = Const.topo_pos[vis_mask, 1][None]
        
        # print_info('Propagated constellation - '+timestamp())
        
        Ntime = Const.topo_pos.shape[1]
        Nsats = Const.topo_pos.shape[2]
 
        # test plot cartesian coordinates of satellites
        # plt.figure()
        # plt.subplot(projection='3d')
        # plt.plot(Const.ecef_pos[0,:,1,0].flatten()/1e3,
        #             Const.ecef_pos[0,:,1,1].flatten()/1e3,
        #             Const.ecef_pos[0,:,1,2].flatten()/1e3,
        #             label= 'ecef')
        # plt.plot(Const.eci_pos[0,:,1,0].flatten()/1e3,
        #             Const.eci_pos[0,:,1,1].flatten()/1e3,
        #             Const.eci_pos[0,:,1,2].flatten()/1e3,
        #             label= 'eci')

        # plt.plot(observer_ecef[0].to(u.km),
        #          observer_ecef[1].to(u.km),
        #          observer_ecef[2].to(u.km),'o')
        # plt.plot(SRS_ecef[0].to(u.km),
        #          SRS_ecef[1].to(u.km),
        #          SRS_ecef[2].to(u.km),'x')
        # plt.legend()        

    #
        # SPECTRAL POWER FLUX DENSITY
        # ---------------------------
        # spfd generated by each satellite visible
        # the statistical function only has the elevation
        # of the satellite as a parameter
        
        # angle beteen sat_SRS site and sat_RAS site:
        Rs = Const.ecef_pos
        Rsrs = np.array([SRS_ecef[i].value for i in range(3)])
        Rras = np.array([observer_ecef[i].value for i in range(3)])

        Ps_srs = Rsrs - Rs
        Ps_ras = Rras - Rs
        P_dot = np.einsum('...j,...j->...',Ps_srs,Ps_ras)
        Ps_srs_n = np.linalg.norm(Ps_srs,axis=-1)
        Ps_ras_n = np.linalg.norm(Ps_ras,axis=-1)
        angle = np.arccos(P_dot/Ps_srs_n/Ps_ras_n)*u.rad
        
        # plt.figure()
        # plt.plot(angle[Const.vis_mask]*180/np.pi)
        
        spfd_sat = Const.spfd(angle, 5*u.dB(u.W), 200*u.MHz,0.38*u.m, freq)  # (u.W/u.m**2) 
        

        # plt.figure()
        # plt.plot(10*np.log10(spfd_sat[Const.vis_mask].value))
        
        spfd_sat = spfd_sat[Const.vis_mask]
        
        #% Verification of the spfd value
        # print_info('\tMax pfd in this iteration: {:.2f}'.format(
        #     (spfd_sat.max()).to(u.dB(u.W/u.m**2/u.MHz))
        #     ))
        
        # print_info('\tStarting received power per cell'+timestamp())
        
        Nsegments = 10 # cells divided in N steps to accomodate 
                        #the amount of memory required
        segment = Ncells//(Nsegments-1)
        if segment == 0:
            Nsegments = 1
            segment = Ncells
        
            
        PSD_rx_sparse = np.zeros([segment, 1, Ntime, Nsats,Nchannels])
    
        for n in range(Nsegments):
            t_start = time.time()
            # print('slice %d'%i)
            idx = slice(n*segment,
                        (n+1)*segment,
                        1)
            if idx.stop > p_az.shape[0]:
                idx = slice(n*segment,
                            p_az.shape[0],
                            1)
    
            p_az1 = p_az[idx]
            p_el1 = p_el[idx]
            # print('start',p_az1[0],p_az1[-1])
            Ncells1 = p_az1.shape[0]
    
    
            #Angular distance
            theta = pycraf.geometry.true_angular_distance(p_az1*u.deg,
                                                          p_el1*u.deg,
                                                          sat_az*u.deg,
                                                          sat_el*u.deg)
    
            #Gain of the RAS antenna towards the satellites
            RAS_G = Observer.gain(theta, observer_diam, freq, observer_efficiency,
                             high_SLL=high_SLL, SLL = SLL,
                             do_bessel=False) #in lin gain
    
            # Telescope masking 
            # if masking:
            #     RAS_G[theta < boresight_limit] = np.nan
            #     # when the summation is don in the satellite axis,
            #     # a flagged satellite in the main beam flags all the time step
            #     # as the telescope receives the power from all satellites
    
            #effective area        
            A_eff = RAS_G * (((const.c/freq).to_value(u.m))**2/4/np.pi)  * u.m**2
    
            #Received power
            PSD_rx_lin = spfd_sat[None,:,None] * A_eff[...,None] #in u.W/u.Hz [pointings,time_satellite,channels]
            # Prx_lin = cp.asnumpy(cp.power(10,cp.array(Prx)/10)) #*u.W/u.m**2
    
            t_start1 = time.time()
            #convert from dense to sparse matrix [Niters, Ncells1, Ntime, Nsats]
            if Ncells1==PSD_rx_sparse.shape[0]:
                PSD_rx_sparse *= 0
            else:
                PSD_rx_sparse = np.zeros([Ncells1,
                                          1,
                                          Ntime,
                                          Nsats,Nchannels])
    
            PSD_rx_sparse[:, vis_mask] = PSD_rx_lin
    
            PSD_rx_tot = (np.nansum(PSD_rx_sparse, axis=-2))  # sum in satellite dimension [Ncells1,Ntime,Nchannels] in W/Hz
            t_stop1 = time.time()
            # print('Elapsed time Sparsing matrix:'+ str(t_stop1-t_start1))
    
            #Equivalent PFD
            Prx_tot = PSD_rx_tot * u.W / u.Hz # to W in 1 hz
    
            #save to output vectors
            Prx_iters[m, idx] = np.sum(Prx_tot[:, 0],axis=1) # [Niters,Ncells,Ntime]  in W/Hz
    
            t_stop = time.time()
            print(str(n) + ' - Elapsed time slice:'+ str(t_stop-t_start))
            
        print_info('\tFinished received power per cell'+timestamp())
        
    print_info('Finished iterations'+timestamp())
    print_info('Averaged power in 2000 s saved in Prx_iters \
               in [niters,ncells,nchannels] in W/Hz (linear scale)')
               
    #%%
    if 0 in Prx_iters :
        Prx_iters += 1e-30

    
    ESPFD_iters = Prx_iters / A_eff_max.value # [Niters,Ncells,Ntime]  in W/m2/Hz

    cell_lon = grid_info['cell_lat'],
    cell_lat_low = grid_info['cell_lat_low'],
    cell_lat_high = grid_info['cell_lat_high'],
    cell_lon_low = grid_info['cell_lon_low'],
    cell_lon_high = grid_info['cell_lon_high'], 


    #----------------------- Save the calculated variables
    datafile = pjoin(DATAPATH,basename)
    print_info('Saving data in : ' + datafile)
    np.savez(datafile,
             Prx_iters=Prx_iters,
             Niters = Niters,
             Ntime = Ntime,
             time_step = time_step,
             BW = BW,
             # masking = masking,
             # boresight_limit = boresight_limit, 
             # power_flag = power_flag, 
             T = T,
             B = B,
             Plim = Plim,
             high_SLL = high_SLL ,
             SLL = SLL,
             point_el = point_el,
             point_az = point_az,
             sim_flags= sim_flags,
             observer_name = observer_name,
             observer_diam = observer_diam,
             const_name = const_name,
             observer_geo = observer_geo,
             Ncells = Ncells,
             Nchannels= Nchannels,
             cell_lon = cell_lon,
             cell_lat_low = cell_lat_low,
             cell_lat_high = cell_lat_high,
             cell_lon_low = cell_lon_low,
             cell_lon_high = cell_lon_high
             )
    

#%% Analyse/visualize the results.

print_info('\nResults:')
print_info('-------------------')
p_lim = -202 *cnv.dB_W # power threshold from RA.769 table 1

p_lin = (Prx_iters[...,0]) * BW.to_value(u.Hz)


p_avg = (np.mean(p_lin, axis=0)*u.W).to(cnv.dB_W) #average in iteration
p_98p = (np.percentile(p_lin, 98., axis=0) * u.W ).to(cnv.dB_W)
p_max = (np.max(p_lin, axis=0) * u.W).to(cnv.dB_W)

_p_lim_W = p_lim.to_value(u.W)
data_loss_per_cell = np.array([
    100 - percentileofscore(pl, _p_lim_W, kind='strict')
    for pl in p_lin.T
    ])

data_loss = (
    100 - percentileofscore(p_lin.flatten(), _p_lim_W, kind='strict')
    ) * u.percent


data_loss_per_iteration = np.array([
    100 - percentileofscore(pl, _p_lim_W, kind='strict')
    for pl in p_lin
    ])

data_loss_mean = np.mean(data_loss_per_iteration)
data_loss_m1s, data_loss_median, data_loss_p1s = np.percentile(
    data_loss_per_iteration, [15.865, 50., 84.135]
    )

print_info('Data loss (total): {0.value:.2f} (+{1:.2f} / -{2:.2f}) {0.unit}'.format(
        data_loss,
        data_loss_p1s - data_loss.to_value(u.percent),
        data_loss.to_value(u.percent) - data_loss_m1s,
    ))

bad_cells = np.count_nonzero(p_avg > p_lim)
print_info('Number of sky cells above threshold: {:f} ({:.2f} %)'.format(
    bad_cells, 100 * bad_cells / len(grid_info)
    ))


#%% Plot CFDs
p_dist = (np.sort(p_lin) * u.W ).to(cnv.dB_W)
p_dist_all = (np.sort(p_lin.flatten()) * u.W ).to(cnv.dB_W)


p98p = np.percentile(p_lin, 98., axis=1)
p98p_all = np.percentile(p_lin, 98.)
p98p_mean = np.mean(p98p)
p98p_sigma = np.std(p98p, ddof=1)

print_info(
    '98%: p = {0.value:.1f} (+{1:.1f} / -{2:.1f}) {0.unit}'.format(
        (p98p_all * u.W ).to(cnv.dB_W),
        np.abs(((1 + p98p_sigma / p98p_mean) * cnv.dimless).to_value(cnv.dB)),
        np.abs(((1 - p98p_sigma / p98p_mean) * cnv.dimless).to_value(cnv.dB)),
    ))
 
print_info(
    'RAS margin = {:.1f} (+{:.1f} / -{:.1f}) dB'.format(
        p_lim.to_value(cnv.dB_W) - (p98p_all * u.W).to_value(cnv.dB_W),
        np.abs(((1 - p98p_sigma / p98p_mean) * cnv.dimless).to_value(cnv.dB)),
        np.abs(((1 + p98p_sigma / p98p_mean) * cnv.dimless).to_value(cnv.dB)),
    ))



fig = plt.figure(figsize=(20, 15))
plt.plot(
    p_dist.T, 100 * np.linspace(0, 1, p_dist.shape[1], endpoint=True),
    'b-', alpha=0.01,
    )
plt.plot([], [], 'b-', label='CDF (individual iters)')
plt.plot(
    p_dist_all[::20], 100 * np.linspace(0, 1, p_dist_all.size, endpoint=True)[::20],
    'k-', label='CDF (all iters)'
    )
hline = plt.axhline(98., color='r', alpha=0.5)
vline = plt.axvline(p_lim.to_value(cnv.dB_W), color='r', alpha=0.5)
plt.grid(color='0.8')
plt.title('EPFD {:s} constellation: total data loss: {:.2f}'.format(
    'SRS', data_loss
    ))
plt.xlabel('P [dB(W)]')
plt.ylabel('Cumulative probability [%]')
plt.legend(*plt.gca().get_legend_handles_labels())
xmin, xmax = plt.xlim()
ymin, ymax = plt.ylim()
plt.text(
    xmin + 2, 95.,
    r'98%: p = ${0.value:.1f}^{{+{1:.1f}}}_{{-{2:.1f}}}$ {0.unit}'.format(
        (p98p_all * u.W ).to(cnv.dB_W),
        np.abs(((1 + p98p_sigma / p98p_mean) * cnv.dimless).to_value(cnv.dB)),
        np.abs(((1 - p98p_sigma / p98p_mean) * cnv.dimless).to_value(cnv.dB)),
        ), color='red', ha='left', va='top',
    )
plt.text(
    xmin + 2, 88.,
    r'$\rightarrow$ RAS margin = ${:.1f}^{{+{:.1f}}}_{{-{:.1f}}}$ dB'.format(
        p_lim.to_value(cnv.dB_W) - (p98p_all * u.W ).to_value(cnv.dB_W),
        np.abs(((1 - p98p_sigma / p98p_mean) * cnv.dimless).to_value(cnv.dB)),
        np.abs(((1 + p98p_sigma / p98p_mean) * cnv.dimless).to_value(cnv.dB)),
        ), color='red', ha='left', va='top',
    )
plt.text(
    p_lim.to_value(cnv.dB_W) + 0.5, ymin + 20,
    r'RA.769 exceeded @ ${0.value:.2f}^{{+{1:.2f}}}_{{-{2:.2f}}}$ {0.unit}'.format(
        100 * u.percent - data_loss,
        data_loss.to_value(u.percent) - data_loss_m1s,
        data_loss_p1s - data_loss.to_value(u.percent),
        ),
    color='red', ha='left', va='bottom', rotation=90.,
    )
plt.savefig(
    pjoin(FIGPATH, '{:s}_cumulative_data_loss_horizontal.png'.format(basename)),
    bbox_inches='tight', dpi=100,
    )

#%% Function to plot the sky map    

def plot_sky(value,
             grid_info,
             title = '',
             xlabel = '',
             ylabel = '',
             zunit = '',
             plot_type = 'triangles',
             contours = False,
             savefig = False,
             filename = ''
             ):
    '''
    *************
    * plot_sky  *
    *************
    Parameters
    ----------
    value : TYPE
        DESCRIPTION.
    grid_info : TYPE
        DESCRIPTION.
    title : TYPE, optional
        DESCRIPTION. The default is ''.
    xlabel : TYPE, optional
        DESCRIPTION. The default is ''.
    ylabel : TYPE, optional
        DESCRIPTION. The default is ''.
    zunit : TYPE, optional
        DESCRIPTION. The default is ''.
    plot_type : TYPE, optional
        DESCRIPTION. The default is 'triangles'.
    contours : TYPE, optional
        DESCRIPTION. The default is False.
    savefig : TYPE, optional
        DESCRIPTION. The default is False.
    filename : TYPE, optional
        DESCRIPTION. The default is ''.

    Returns
    -------
    fig : TYPE
        DESCRIPTION.

    '''

    # fig = plt.figure(figsize=(9, 6))
    val = value
    ind_plot = np.isfinite(val)

    # color=plt.cm.viridis(val_norm),

    vmin, vmax = np.nanmin(val[ind_plot]), np.nanmax(val[ind_plot])
    val_norm = (val[ind_plot] - vmin) / (vmax - vmin)


    fig2 = plt.figure(figsize=[20,15])
    ax = fig2.add_axes([0.1, 0.1, 0.8, 0.8],
                       polar=True)
    
    ax.set_theta_zero_location("N")
    colors = plt.cm.viridis(val_norm)
    th = grid_info['cell_lon'][ind_plot,0]*np.pi/180
    rad = 90 - grid_info['cell_lat_high'][ind_plot,0]
    
    w = (grid_info['cell_lon_high'][ind_plot,0] - 
         grid_info['cell_lon_low'][ind_plot,0])*np.pi/180
    
    b = 90-grid_info['cell_lat_low'][ind_plot,0] - rad
    
    ax.bar(th,rad,
           width= w,
           bottom= b,
           color=colors, edgecolor = colors)
    
    sm = plt.cm.ScalarMappable(cmap=plt.cm.viridis,
                               norm=plt.Normalize(vmin=vmin, vmax=vmax))
    cbar = plt.colorbar(sm)
    cbar.set_label(zunit)

    # ax.set_ylim([0,70])
    return fig


#%% plot the sky map

# ESPFD_m = np.nanmean(np.nanmean(ESPFD_iters,axis=0),axis=-1).value
ESPFD_m = np.nanmean(ESPFD_iters,axis=0)/(10**-26) # in Jy


ESPFD_m = ESPFD_m[:,0]
ESPFD_m_dB = 10*np.log10(ESPFD_m) # W/m2/Hz to dBJy

plot_sky(ESPFD_m_dB,
         grid_info,
         plot_type = 'bars',
         title = 'Equivalent Power Flux Density: Before Flagging - ' + str(Const.name),
         xlabel = 'Azimuth [$^o$]',
         ylabel = 'Elevation [$^o$]',
         zunit = 'dBJy',
         savefig = True,
         contours = True,
         filename = 'figs/'+str(Const.name)+'/EPFD_dBJy_before_flag - ' + str(sim_flags),
        )

plt.savefig(
    pjoin(FIGPATH, '{:s}_sky plot.png'.format(basename)),
    bbox_inches='tight', dpi=100,
    )

