# -*- coding: utf-8 -*-
"""
Definition of ground terminals used in the simulation of satellite constellations
    Author:
    Federico Di Vruno - f.divrun@skatelescope.org
    Braam Otto - b.otto@skatelescope.org

    Date:
    24/11/2020

    Last Updated:
    01 Sep 2021
    
    Note: This module is not used now, but has some useful functions to define
    points on the sphere.
    
"""
import numpy as np
import pycraf.geometry as geometry
import astropy.constants as const
import astropy.units as u
import ska.general as general


class Ground_terminals:

    def __init__(self,
                 obs_geo: np.array,
                 option_mesh: int,
                 filepath = '',
                 sat_geo = np.array([]),
                 include_RQZ = False,
                 plot_flag = False):
        """
        Definition of ground terminals used in the simulation of satellite constellations
        mesh with cells on the ground:

        Parameters:
        -----------
            obs_geo: np.array containing:

                obs_lon: float in deg
                    observer longitude

                obs_lat: float in deg
                    observer latitude

                obs_alt: float in km
                    observer altitude abode sea level

            option_mesh: int
                options:
                    1-from KML or json files

                    2-manually generated grid

                    3-one point in the observer site

                    4-spot mesh under each satellite

                    5-ring around the observer

                    6-ring around the core

                    7-concentric rings centered on the observer location

            filepath: string
                Full path of the kml/json file to load in option 1

            sat_geo: array, optional
                geodetic positions of the satellites for option number 4.
                default is []
            
            include_RQZ: Boolean, optional
                flag to include the poligon of a radio quiet zone in the definition
                of the mesh 
            
            plot_flag: Boolean, optional
                flag to plot the mesh generated

        Returns:
        --------
            Creates the following attributes:
            mesh_cart: array
                cartesian rep ofthe mesh in ECEF coordinates

            mesh_sph: array
                spherical representation of the mesh

        """
        self.obs_lon = obs_geo[0]
        self.obs_lat = obs_geo[1]
        self.obs_alt = obs_geo[2]

        if option_mesh == 1:
            # option 1 from kml file: 
            if 'kml' in filepath[-4::]:
                hexagons, centres = self.get_hexagons(filepath) # from 
            elif 'json' in filepath[-4::]:
                hexagons, centres = self.get_hexagons_json(filepath)

            else:
                print('\nERROR: \nOption 1 needs a valid filepath to load a kml or json file')
                return 
            
            mesh_x,mesh_y,mesh_z = geometry.sphere_to_cart(
                                            const.R_earth,
                                            centres[:,0]*u.deg,
                                            centres[:,1]*u.deg)
            
            mesh_cart = np.array((mesh_x,mesh_y,mesh_z)).T
            mesh_sph = np.array(centres)

        if option_mesh ==2:
            #option 2 manually generated grid
            mesh_lon,mesh_lat,mesh_alt = self.hexa_mesh(
                lon_cent = self.obs_lon,
                lat_cent = self.obs_lat,
                poly = include_RQZ)
            mesh_x,\
            mesh_y,\
            mesh_z = geometry.sphere_to_cart(
                const.R_earth + mesh_alt * u.km,\
                mesh_lon*u.deg,\
                mesh_lat*u.deg)

            mesh_cart = np.array((mesh_x,mesh_y,mesh_z)).T * u.m
            mesh_sph = np.array((mesh_lon,mesh_lat,mesh_alt)).T

        if option_mesh ==3:
            #option 3 one point in the observer location
            mesh_lon = np.array((self.obs_lon,self.obs_lon))
            mesh_lat = np.array((self.obs_lat,self.obs_lat))
            mesh_alt = np.array((self.obs_alt,self.obs_alt))

            mesh_x,\
            mesh_y,\
            mesh_z = geometry.sphere_to_cart(
                    const.R_earth+mesh_alt*u.km,
                    mesh_lon*u.deg,
                    mesh_lat*u.deg)

            mesh_sph = np.array((mesh_lon,mesh_lat,mesh_alt)).T
            mesh_cart = (np.array((mesh_x.value,mesh_y.value,mesh_z.value)).T)

        if option_mesh ==4:
            mesh_cart, mesh_sph = self.spot_mesh(sat_geo[:,0],sat_geo[:,1],d_km=6, N=30, test=1 )

        if option_mesh ==5:
            #first ring outside of the core cells
            mesh_lon,\
            mesh_lat,\
            mesh_alt = np.array((21.67,21.2,21.1,21.22,\
                                 21.35,21.57,21.78, 21.655, 21.427 )),\
                       np.array((-30.77,-30.5,-30.6,-30.83,\
                                 -30.97,-30.955,-30.6,-30.47,-30.485)),\
                       np.array((1,1,1,1,1,1,1,1,1,))

            mesh_x,\
            mesh_y,\
            mesh_z = geometry.sphere_to_cart(
                    const.R_earth+mesh_alt*u.km,
                    mesh_lon*u.deg,
                    mesh_lat*u.deg)

            mesh_sph = np.array((mesh_lon,mesh_lat,mesh_alt)).T
            mesh_cart = (np.array((mesh_x.value,mesh_y.value,mesh_z.value)).T)

        if option_mesh ==6:
            # ring in the outside of the core
            i = 8
            d = i*25*u.km
            sep = 25*u.km
            r = d/const.R_earth*u.rad
            t = np.linspace(0,2*np.pi,int(2*np.pi*d/sep))
            lons = self.obs_lon*u.deg + r*np.cos(t)
            lats = self.obs_lat*u.deg + r*np.sin(t)

            mesh_lon,mesh_lat,mesh_alt = np.array((lons,lats,np.ones(t.shape)))

            mesh_x,\
            mesh_y,\
            mesh_z = geometry.sphere_to_cart(
                    const.R_earth+mesh_alt*u.km,
                    mesh_lon*u.deg,
                    mesh_lat*u.deg)

            mesh_sph = np.array((mesh_lon,mesh_lat,mesh_alt)).T
            mesh_cart = (np.array((mesh_x.value,mesh_y.value,mesh_z.value)).T)

        if option_mesh ==7:
            #mesh made of circles around the observer location
            i = np.arange(0,35,1)
            sep = 35*u.km
            d = i*sep
            lons = np.array([])*u.deg
            lats = np.array([])*u.deg

            for di in d:
                r = di/const.R_earth*u.rad
                t = np.linspace(0,2*np.pi,int(2*np.pi*di/sep))
                lons = np.append(lons,(self.obs_lon*u.deg + r*np.cos(t)))
                lats = np.append(lats,(self.obs_lat*u.deg + r*np.sin(t)))

            mesh_lon,\
            mesh_lat,\
            mesh_alt = np.array(lons), np.array(lats), np.ones(len(lons))

            mesh_x,\
            mesh_y,\
            mesh_z = geometry.sphere_to_cart(
                    const.R_earth+mesh_alt*u.km,
                    mesh_lon*u.deg,
                    mesh_lat*u.deg)

            mesh_sph = np.array((mesh_lon,mesh_lat,mesh_alt)).T
            mesh_cart = (np.array((mesh_x.value,mesh_y.value,mesh_z.value)).T)

        if plot_flag:
            import matplotlib.pyplot as plt
            fig = self.plot_earth_map()
            [plt.plot(mesh_sph[i,0],mesh_sph[i,1],'or',markersize=5)\
                 for i in range(mesh_sph.shape[0])]
            RQZ_lons,RQZ_lats = general.SKA_mid_site(area = 3) # Smallest zone     
            plt.plot(RQZ_lons,RQZ_lats)
            plt.plot(self.obs_lon,self.obs_lat,'db',markersize=7)

        self.mesh_cart = mesh_cart
        self.mesh_sph = mesh_sph


    def plot_earth_map(self):
        import cartopy.crs as ccrs
        import matplotlib.pyplot as plt
        from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER

        def my_stock_img(ax):
            source_proj = ccrs.PlateCarree()
            try:
                ax.imshow(plt.imread(r'figs\Earth_img.jpg'), origin='upper',
                                   transform=source_proj,
                                   extent=[-180, 180, -90, 90],
                                   alpha=0.8)
            except:
                import shutil
                import requests
                import os
                url = 'https://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74192/world.200411.3x5400x2700.jpg' #2 Mb
                response = requests.get(url, stream=True)
                if not os.path.exists('figs'):
                    os.makedirs('figs')

                with open(r'figs\Earth_img.jpg', 'wb') as out_file:
                    shutil.copyfileobj(response.raw, out_file)
                del response
                ax.imshow(plt.imread(r'figs\Earth_img.jpg'), origin='upper',
                                   transform=source_proj,
                                   extent=[-180, 180, -90, 90],
                                   alpha=0.8)

        fig = plt.figure(figsize=[15,10])
        ax = plt.axes(projection=ccrs.PlateCarree())
        # ax.stock_img()
        my_stock_img(ax)

        gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                          linewidth=2, color='gray', alpha=0.5, linestyle='--')
        gl.top_labels = False
        gl.right_labels = False
        # gl.xlocator = mticker.FixedLocator([-150,-100,-50,0,50,100,150])
        gl.xformatter = LONGITUDE_FORMATTER
        gl.yformatter = LATITUDE_FORMATTER
        # gl.xlabel_style = {'size': 15, 'color': 'gray'}
        # gl.xlabel_style = {'color': 'red', 'weight': 'bold'}

        return fig


    def spot_mesh(self,
                  lon0,
                  lat0,
                  d_km = 5,
                  N = 40,
                  test = 0 ):
        """
        Generates a grid of points within a radius d km
        around a centre point ph0, th0 in a circle.

        Parameters
        ----------
        lon0 : TYPE
            DESCRIPTION.
        lat0 : TYPE
            DESCRIPTION.
        d_angle : TYPE, optional
            DESCRIPTION. The default is 5.
        N : TYPE, optional
            DESCRIPTION. The default is 40.
        test : TYPE, optional
            DESCRIPTION. The default is 0.

        Returns
        -------
        mesh_cart:
            cartesian coordinates of the messh in hte ECEF ref frame
        mesh_sph:
            spherical coordinates of the mesh
        """
        d_angle = d_km/const.R_earth.to_value(u.km)
        #linear
        # dph = np.linspace(-d_angle,d_angle,N)
        # dth = np.linspace(-d_angle,d_angle,N)

        #random
        dph = (np.random.rand(N)-0.5)*d_angle*2
        dth = (np.random.rand(N)-0.5)*d_angle*2

        dph,dth = np.meshgrid(dph,dth)

        ph = lon0[:,None] + dph.flatten()[None]
        th = lat0[:,None] + dth.flatten()[None]

        Re = const.R_earth

        mesh_cart = np.array( geometry.sphere_to_cart(Re,ph*u.deg,th*u.deg)).T
        mesh_sph = np.array((ph,th)).T

        if test:
            import matplotlib.pyplot as plt
            fig = plt.figure()
            ax = fig.add_subplot(projection='3d')
            # ax.plot(mesh_cart[:,0,0]/1e3,mesh_cart[:,0,1]/1e3,mesh_cart[:,0,2]/1e3,'o')
            [ax.plot(mesh_cart[:,i,0]/1e3,mesh_cart[:,i,1]/1e3,mesh_cart[:,i,2]/1e3,'o') for i in range(mesh_cart.shape[1])]
            ax.set_xlim(-Re.value/1e3,Re.value/1e3)
            ax.set_ylim(-Re.value/1e3,Re.value/1e3)
            ax.set_zlim(-Re.value/1e3,Re.value/1e3)
            D = np.linalg.norm(mesh_cart[0:-1]-mesh_cart[1:],axis=-1)
            print(np.min(D)/1e3)

        return mesh_cart, mesh_sph*u.deg


    def get_hexagons_json(self,
                  filepath):
        """
        Parser for json file with coordinates for the hexagons
        The coordinates for the hexagon coordinates shall be in 
        'features' -> 'goemetry' -> 'coordinates'
        
        Parameters
        ----------
        filepath : TYPE
            DESCRIPTION.
        
        Returns
        -------
        None.

        """
        import json

        with open(filepath) as f:
          data = json.load(f)

        features = data['features']

        hexagon = []
        centres = []
        for hexa in features:
            lons = np.array([])
            lats = np.array([])
            for item in  hexa['geometry']['coordinates'][0][0]:
                lons = np.append(lons,float(item[0]))
                lats = np.append(lats,float(item[1]))
            hexagon.append([lons,lats])
            centres.append([np.mean(lons),np.mean(lats)])

        hexagon = np.array(hexagon)
        centres = np.array(centres)

        return hexagon, centres


    def hexa_mesh(self,
                  lon_cent=21.4,
                  lat_cent=-30.8,
                  dkm=37,
                  N=10,
                  poly=1,
                  plot_flag= False):
        """
        Creates the hexagonal mesh in geodetic coordinates
        lon_cent and lat_cent are the centre coordinates of the mesh
        dkm is the distance between centres of hexagons
        N is the number of hexagonal rings to create
        poly defines an inner poligon where there are no mesh cells

        by default the centre position is the SKA-MID core.
        
        Parameters
        ----------
        lon_cent : TYPE, optional
            DESCRIPTION. The default is 21.4.
        lat_cent : TYPE, optional
            DESCRIPTION. The default is -30.8.
        dkm : TYPE, optional
            DESCRIPTION. The default is 37.
        N : TYPE, optional
            DESCRIPTION. The default is 10.
        poly : TYPE, optional
            DESCRIPTION. The default is 1.
        test : TYPE, optional
            DESCRIPTION. The default is False.

        Returns
        -------
        xc : TYPE
            DESCRIPTION.
        yc : TYPE
            DESCRIPTION.
        """
        def HexagonRing(x,y,n,r):
            xc = []
            yc = []
            dc = n*np.sqrt(3) # distance between to neighbouring hexagon centers
            x0 = x
            y0 = y-r*dc # hexagon center of one before first hexagon (=last hexagon)
            dx,dy = -dc*np.sqrt(3)/2,dc/2 # direction vector to next hexagon center
            for i in range(6):
                # draw r hexagons in line
                for j in range(0,r):
                    xc.append( x0+dx*(j+1))
                    yc.append( y0+dy*(j+1))
                # rotate direction vector by 60°
                dx,dy = (np.cos(np.pi/3)*dx+np.sin(np.pi/3)*dy,
                       -np.sin(np.pi/3)*dx+np.cos(np.pi/3)*dy)
                x0 = xc[-1]
                y0 = yc[-1]

            return xc,yc

        lon,lat = np.array([0]),np.array([0])
        for i in range(N):
            xc,yc = HexagonRing(0,0,dkm/3**0.5,i+1)
            lon = np.append(lon,np.array(xc)*180/np.pi/6378)
            lat = np.append(lat,np.array(yc)*180/np.pi/6378)

        lon += lon_cent #21.45
        lat += lat_cent #-30.8
        alt = np.zeros(lon.shape)

        if poly>0:
            import matplotlib.path as mpltPath
            # modify the mesh to keep the ones that are outside the polygon
            RQZ_lons,RQZ_lats = general.SKA_mid_site(area = 3) # Smallest zone     
            poly = mpltPath.Path(np.array([RQZ_lons,RQZ_lats]).transpose())
            # print('Nr of mesh points befor poly subtraction:', len(lon))
            idx = []
            for i in range(len(lon)):
                if poly.contains_point([lon[i],lat[i]]):
                    idx.append(i)

            lon = np.delete(lon,idx)
            lat = np.delete(lat,idx)
            alt = np.delete(alt,idx)
            # print('Nr of mesh points after poly subtraction:', len(lon))

        if plot_flag:
            import matplotlib.pyplot as plt
            lon,lat,alt = self.hexa_mesh(poly=1)
            plt.figure()
            plt.plot(lon,lat,'xr')

        return lon,lat,alt


    def get_hexagons(self,
                     filepath,
                     plot = False,
                     do_poly = False):
        """
        Load the positions on the ground with hexagon kml files.

        Parameters
        ----------
            path: string
                filename directory

            filename: string
                filename of the file to load when using selection = 3

            plot : Boolean, optional
                DESCRIPTION. The default is False.

            do_poly : Boolean, optional
                Subtract the RQZ polygon from the imported mesh.\
                The default is False.

        Returns
        -------
        None.

        """
        import numpy as np
        import xml.etree.ElementTree as ET


        tree = ET.parse(filepath)
        root = tree.getroot()

        # Parsing the KLm
        hexagon = []
        centres = []
        ns = {'kml': 'http://www.opengis.net/kml/2.2'}
        A = root[0].find('kml:Folder',ns)
        for placemark in A.findall('kml:Placemark',ns):
            for polygon in placemark.findall('kml:Polygon',ns):
                A = (polygon[0][0][0]).text
                A = str.split(A,'\n\t\t\t\t\t\t\t')
                A = str.split(A[1],',0 \n\t\t\t\t\t\t')
                A = str.split(A[0],',0 ')
                lons = np.array([])
                lats = np.array([])
                for item in  A:
                    lons = np.append(lons,float(item.split(',')[0]))
                    lats = np.append(lats,float(item.split(',')[1]))
                hexagon.append([lons,lats])
                centres.append([np.mean(lons),np.mean(lats)])

        hexagon = np.array(hexagon)
        centres = np.array(centres)

        if do_poly>0:
            import matplotlib.path as mpltPath
            # modify the mesh to keep the ones that are outside the polygon
            RQZ_lons,RQZ_lats = general.SKA_mid_site(area = 3) # Smallest zone     
            poly = mpltPath.Path(np.array([RQZ_lons,RQZ_lats]).transpose())
            # print('Nr of mesh points befor poly subtraction:', len(lon))
            idx = []
            for i in range((centres.shape[0])):
                if poly.contains_point([centres[i,0],centres[i,1]]):
                    idx.append(i)

            c_lon = centres[:,0]
            c_lat = centres[:,1]
            c_lon = np.delete(c_lon,idx)
            c_lat = np.delete(c_lat,idx)
            centres = np.array((c_lon,c_lat)).T

            hexagon = np.array((np.delete(hexagon[:,:,i],idx) for i in range(19)))
            # print('Nr of mesh points after poly subtraction:', len(lon))

        if plot:
            hexagon, centres = self.get_hexagons(filepath,0,do_poly)
            import matplotlib.pyplot as plt
            plt.figure()
            [plt.plot(hexagon[i,0,:],hexagon[i,1,:],'b') for i in range(hexagon.shape[0])]
            [plt.plot(centres[i,0],centres[i,1],'xr') for i in range(centres.shape[0])]
            RQZ_lons,RQZ_lats = general.SKA_mid_site(area = 3) # Smallest zone     
            plt.plot(RQZ_lons,RQZ_lats)
            plt.show()
        return hexagon, centres


#%% Examples of use:
if __name__ == '__main__':
    obs_geo = np.array([21.6,-30.4,1]) #lon,lat,alt(km)

    
    # mesh = Ground_terminals(obs_geo, 1, plot_flag=True)
    mesh = Ground_terminals(obs_geo, 2, plot_flag=True)
    # mesh = Ground_terminals(obs_geo, 3, plot_flag=True)