# -*- coding: utf-8 -*-

"""Module init code."""


__version__ = '0.0.0'

__author__ = 'Federico Di Vruno'
__email__ = 'f.divruno@skatelescope.org'

from .constellation import Constellation
from .general import *
from .ground_terminals import Ground_terminals
from .telescope import Telescope

    

