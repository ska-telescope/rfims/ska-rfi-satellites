# -*- coding: utf-8 -*-
"""
    Telescope Class and Functions that provide..... to do

    Author: F. Di Vruno
    Author: Braam Otto

    Organisation: SKA Telescope
    Copyright: 2020 SKA Telescope
    Date: 15 Sept. 2020

    Last Updated: 6 April 2021
"""
from datetime import datetime
import numpy as np
import scipy.special as sp
import scipy.io
import pymap3d as pm
from astropy import units as u
from ska.general import sky_cells_m1583



class Telescope:
    """
        Telescope Class
    """
    # pylint: disable=too-many-instance-attributes

    def __init__(self,
                 name: str,
                 coords_geo: np.array,
                 freq: u.GHz = 11.7 * u.GHz,
                 diam: u.m = 15*u.m,
                 verbose = False
                 ):
        """
        Parameters
        ----------
        name : str
            Name of the telescope, this can be also a list of names of an
            array of antennas.

        coords_geo : np.array
            coordinates of the telescope. [long, lat, height over sea level in km]
            this can be also used to define N antennas. [Nx3]

        freq : u.GHz, optional
            DESCRIPTION. The default is 11.7 * u.GHz.

        diam : u.m, optional
            diameter of the observer antenna in m. The default is 15 * u.m.

        verbose : TYPE, optional
            DESCRIPTION. The default is False.

        Returns
        -------
        None.

        """
        
        self.name = name
        #antenna diameter
        self.diam = diam
        
        self.verbose = verbose

        # Probability Threshold
        self.threshold = 1e-7
        # Telescope Saturation
        self.saturation = -70. * u.dB(u.mW)

        # TELESCOPE COORDINATES (degrees)
        self.coord_geo = coords_geo 
        
        # TELESCOPE OBSERVER:
        self.ell_wgs84 = pm.Ellipsoid('wgs84')
        lat0, lon0, height0 =\
            self.coord_geo[...,1],\
            self.coord_geo[...,0],\
            self.coord_geo[...,2]*1e3

        # ENU Coordinates (LOCAL)
        self.coord_e0, self.coord_n0, self.coord_u0 =\
            0.0, 0.0, 0.0
        self.coord_enu =\
            np.array([self.coord_e0,
                      self.coord_n0,
                      self.coord_u0])

        # TELESCOPE ECEF Coordinates (GLOBAL)
        self.coord_ecef =\
            pm.enu2ecef(self.coord_e0,
                             self.coord_n0,
                             self.coord_u0,
                             lat0, lon0, height0,
                             ell=self.ell_wgs84,
                             deg=True)
            
        # Telescope Observer Vector Array in ECEF
        self.coord_ecef_u =\
            self.coord_ecef / np.linalg.norm(self.coord_ecef)
        
        # function to generate a random pointing in az/el
        self.random_pointing = sky_cells_m1583
        
        self.gain = self.ITU_RA1631_gain
        
    # Telescope Pointing
    # TODO: define a fixed and a random pointing
    def fixed_pointing(self, az, el):
        self.az = az
        self.el = el
            
        
    def ska_thresholds(freq,
                   unit = 'psd',
                   plot = False):
        """
        
    
        Parameters
        ----------
        freq : float or array
            frequency point or array in MHz
    
        plot : TYPE, optional
            DESCRIPTION. The default is False.
    
        Returns
        -------
        None.
    
        """    
        freq = np.atleast_1d(freq)
        
        Low = -17.2708 * np.log10(freq) - 192.0714  #dBm/Hz
        High = -249 * np.ones(freq.shape) #dBm/Hz
        
        ft = 2000
        
        cont_PSD = np.zeros(freq.shape)
        
        cont_PSD[freq <= ft] = Low[freq<=ft]
        cont_PSD[freq > ft] = High[freq>ft]
        
        # CONTINUUM RBW = 1%.fc
        cont_RBW = 1./100. * freq * 1e6
        
        # SPECTRAL LINE PSD [dBm/Hz]
        sp_PSD = cont_PSD + 15.
        # SPECTRAL LINE RBW = 0.001%.fc
        sp_RBW = 0.001/100. * freq * 1E6
        
        cont_pow = cont_PSD + 10*np.log10(cont_RBW) 
        sp_pow  = sp_PSD + 10*np.log10(sp_RBW)  
            
        if unit == 'psd':
            out1, out2 = cont_PSD, sp_PSD
     
            
        if unit == 'power':
            out1, out2 = cont_pow, sp_pow
            
        if plot:
            import matplotlib.pyplot as plt
            plt.figure()
            plt.semilogx(freq,cont_PSD)
            plt.plot(freq,sp_PSD)
            plt.title('SKA thresholds in power flux density')
            plt.xlabel('frequency [MHz]')
            plt.ylabel('PSD [dBm/Hz]')
            plt.grid()
    
            plt.figure()
            plt.semilogx(freq,cont_pow)
            plt.plot(freq,sp_pow)
            plt.title('SKA thresholds in power')
            plt.xlabel('frequency [MHz]')
            plt.ylabel('PSD [dBm]')
            plt.grid()
    
        return out1, out2    
   
    def ITU_RA1631_gain(self,
                        phi_gain: u.deg,
                        D: u.m,
                        freq: u.Hz,
                        eta_a: u.percent,
                        high_SLL = False,
                        SLL = 4,
                        do_bessel = False,
                        unit = 'lin',
                        ):
        """
        *************
        ITU-R RA.1631
        *************
        Mathematical Model of Avg Radiation Pattern
        non-GSO systems and RAS stations for frequencies
        above 150 MHz
        By : Braam Otto - 2020

        Parameters
        ----------
        phi_gain: u.deg
            DESCRIPTION
            
        D : u.m
            Antenna diametre
            
        freq : u.Hz
            DESCRIPTION.
            
        eta_a : u.percent
            percentage of antenna efficiency
        
        high_SLL: bool , optional
            Flag to set the far sidelobes to SLL value
            
        SLL: float, optional
            Value of the far sidelobes above 10 degree boresight when
            high_SLL is set, default value is 4 dBi
            
        
        do_bessel: bool, optional
            Flag to calculate gain with Bessel coeficients
            
        unit: 
            options are 'lin' and 'dB'
        Returns
        -------
        G

        """
        ind_nan = np.isnan(phi_gain) # find nan values
        phi_gain = phi_gain.to_value(u.deg)

        D = D.to_value(u.m)
        freq = freq.to_value(u.Hz)
        eta_a = eta_a.to_value(u.percent)/100
        
        lambda0 = 3e8/freq
        
        # Effective Apperture
        Aeff = np.pi*(D/2)**2.
        
        # Ratio Diameter to Wavelength        
        d_wlen = D/lambda0

        # Antenna Efficiency
        # eta_a = 1.0

        # Maximum Antenna Gain:
        # Linear
        Gmax_lin =\
            eta_a * (np.pi * D/lambda0)**2.
        # Gmax2_lin =\
        #     (4*np.pi*Aeff)/(lambda0**2.)
        # Logarithmic
        # Gmax = 10.*np.log10(Gmax_lin)
        # Gmax2 = 10.*np.log10((4*np.pi*Aeff)/(lambda0**2.))

            
        if ~do_bessel: #no bessel
    
            
            G1 = -1. + 15.*np.log10(d_wlen)
            G1_lin = 10**(G1/10)
            # Off-boresight Angle Phi
            phi_m = (20./d_wlen)*np.sqrt(10*np.log10(Gmax_lin/G1_lin))
            phi_r = 15.85*((d_wlen)**(-0.6))
            
            # phi_gain = np.linspace(0., 180., 1000)
            # Broadcast Numpy Arrays
            (phi_gain, d_wlen,  Gmax_lin,  G1_lin, phi_m, phi_r) =\
                np.broadcast_arrays(phi_gain,
                                    d_wlen,
                                    Gmax_lin,
                                    G1_lin,
                                    phi_m,
                                    phi_r)
            # Simple ITU Gain Model
            # itu_gain_simple = np.zeros(phi_gain.shape, np.float64)
            itu_gain_simple_lin = np.zeros(phi_gain.shape, np.float64)
            
            # Case 1:
            mask = (0 <= phi_gain) & (phi_gain < phi_m)
            # itu_gain_simple[mask] =\
            #     Gmax[mask] - 2.5e-3 * (d_wlen[mask] * phi_gain[mask]) ** 2
            itu_gain_simple_lin[mask] =\
                Gmax_lin[mask] / 10**(( 2.5e-3 * (d_wlen[mask] * phi_gain[mask]) ** 2)/10)
                
            # Case 2:
            mask = (phi_m <= phi_gain) & (phi_gain < phi_r)
            # itu_gain_simple[mask] = G1[mask]
            itu_gain_simple_lin[mask] = G1_lin[mask]
            
            # Case 3:
            mask = (phi_r <= phi_gain) & (phi_gain < 10.)
            # itu_gain_simple[mask] = 29 - 25. * np.log10(phi_gain[mask])
            itu_gain_simple_lin[mask] = 10**2.9 / ( phi_gain[mask]**2.5)
            
            # Case 4:
            mask = (10. <= phi_gain) & (phi_gain < 34.1)
            # itu_gain_simple[mask] = 34 - 30. * np.log10(phi_gain[mask])
            itu_gain_simple_lin[mask] = 10**3.4 / (phi_gain[mask]**3)
            
            # Case 5:
            mask = (34.1 <= phi_gain) & (phi_gain < 80.)
            # itu_gain_simple[mask] = -12.
            itu_gain_simple_lin[mask] = 10**-1.2
            
            # Case 6:
            mask = (80. <= phi_gain) & (phi_gain < 120.)
            # itu_gain_simple[mask] = -7.
            itu_gain_simple_lin[mask] = 10**-0.7
            # Case 7:
            mask = (120. <= phi_gain) & (phi_gain <= 180.)
            # itu_gain_simple[mask] = -12.
            itu_gain_simple_lin[mask] = 10**-1.2            
            
            G = itu_gain_simple_lin
            
        if do_bessel:
            """
            ****************
            Bessel Functions
            ****************
            More accurate mathematical model of the radiation
            pattern representation of the main-beam for
            frequencies above 150 MHz
            """
            # Off-boresight Angle Phi
            # phi_gain2 = np.linspace(0, 20, 1000)
            # Broadcast Numpy Arrays
            (phi_gain, d_wlen, Gmax_lin, G1_lin, phi_m, phi_r) =\
                np.broadcast_arrays(phi_gain,
                                    d_wlen,
                                    Gmax_lin,
                                    G1_lin,
                                    phi_m,
                                    phi_r)
            # Bessel Function ITU Gain Model
            itu_gain_bessel_lin = np.zeros(phi_gain.shape, np.float64)
            # First Null of Antenna Pattern
            phi_0 = 69.88 / d_wlen
            x = (np.pi*D*phi_gain)/(360*lambda0)
            # Case 1:
            mask = (0 <= phi_gain) & (phi_gain < phi_0)
            itu_gain_bessel_lin[mask] =\
                Gmax_lin[mask] *\
                (sp.j1(2 * np.pi * x[mask]) / (np.pi * x[mask]))**2
            # Case 2:
            mask = (phi_0 <= phi_gain) & (phi_gain < 20.)
            B_sqrt = (10.**(1.6)) * np.pi * d_wlen[mask] / 2.
            itu_gain_bessel_lin[mask] =\
                (B_sqrt * (
                    np.cos(2.*np.pi*x[mask] - 3.*np.pi/4. + 0.0953)/(np.pi*x[mask])
                    )**2.)

            # Case 4:
            mask = (20. <= phi_gain) & (phi_gain < 34.1)
            # itu_gain_simple[mask] = 34 - 30. * np.log10(phi_gain[mask])
            itu_gain_bessel_lin[mask] = 10**3.4 / (phi_gain[mask]**3)
            
            # Case 5:
            mask = (34.1 <= phi_gain) & (phi_gain < 80.)
            # itu_gain_bessel[mask] = -12.
            itu_gain_bessel_lin[mask] = 10**-1.2
            
            # Case 6:
            mask = (80. <= phi_gain) & (phi_gain < 120.)
            # itu_gain_bessel[mask] = -7.
            itu_gain_bessel_lin[mask] = 10**-0.7
            # Case 7:
            mask = (120. <= phi_gain) & (phi_gain <= 180.)
            # itu_gain_bessel[mask] = -12.
            itu_gain_bessel_lin[mask] = 10**-1.2            


            G = itu_gain_bessel_lin

            
        if high_SLL:
            br = 10
            G[abs(phi_gain)>br] = 10**(SLL/10)

        G[ind_nan] = np.nan

        if unit=='lin':
            Go = G
        else:
            Go = 10*np.log10(G)
        return Go
    
#%%
if __name__ == "__main__":
    import matplotlib.pyplot as plt
    
    Observer = Telescope('SKA00', 
                         np.array([21.4,-30.7,1]),
                         diam = 15*u.m)
    phi = np.logspace(-3,np.log10(180),1000)*u.deg
    # phi = phi[None,None,None]
    freq = 11 * u.GHz
    G_bessel = Observer.gain(phi, Observer.diam,
                             freq, 100*u.percent,
                             do_bessel=True)
    
    G = Observer.gain(phi, Observer.diam,
                      freq, 100*u.percent,
                      do_bessel=False)

    
    plt.figure('Gain')
    plt.semilogx(phi,10*np.log10(G))
    plt.semilogx(phi,10*np.log10(G_bessel))
    plt.grid()
    
####################################################


    # def adc(self):
    #     """
    #         :description
    #         ____________
    #             Information on the MeerKAT Analogue-to-Digital
    #             Converter (ADC) Teledyne e2v AT84AS008; 10-bit 2 GSps
    #         :returns
    #         ________
    #         none
    #     """

    #     # ADC CONSTANTS
    #     self.adc_bits = 10 * u.dimensionless_unscaled
    #     self.voltage_peak_to_peak = 500. * self.milli_volt
    #     self.voltage_peak = self.voltage_peak_to_peak / 2

    #     # Determine the number of ADC Quantization Levels:
    #     self.adc_counts = 2 ** self.adc_bits

    #     # Voltage Scaling Factor is the ratio of the Voltage (peak-to-peak)
    #     # that can be represented by the ADC Quantization Levels (10-bits)
    #     self.voltage_scaling_factor =\
    #         self.voltage_peak / (self.adc_counts / 2)

    #     # Full Scale Voltage is the Maximum Voltage that can be
    #     # digitized linearly [dBuV]
    #     self.voltage_full_scale = 20. * np.log10(
    #         (self.adc_counts.value / 2 * self.voltage_scaling_factor.value)
    #         / 1e-6) *\
    #         self.db_uv

    #     self.power_full_scale =\
    #         (self.voltage_full_scale.value - 106.9) * self.db_m

    #     # Full Scale Power Levels is the Maximum Power Level that
    #     # can be digitized linearly [dBm]

    #     # Defining start of SATURATION as -3 dBFS:
    #     self.sat_count_log =\
    #         20. * np.log10(self.adc_counts) - 3
    #     self.sat_count_lin =\
    #         10. ** (self.sat_count_log / 20.)

    #     if self.verbose:
    #         print("\n*****")
    #         print("Telescope ADC: Teledyne e2v AT84AS008 (10-bit; 2 GSps)")
    #         print("Number of Bits = ",
    #               self.adc_bits)
    #         print("ADC Quantization Levels = ",
    #               self.adc_counts.value)
    #         print("ADC Voltage = ",
    #               self.voltage_peak_to_peak, " (peak-to-peak) or ",
    #               self.voltage_peak, " (peak)")
    #         print("Voltage Scaling Factor = ",
    #               self.voltage_scaling_factor, " (= ",
    #               self.voltage_peak_to_peak / self.adc_counts, ")")
    #         print("Full Scale Voltage = ",
    #               round(self.voltage_full_scale.value, 2),
    #               "[", self.voltage_full_scale.unit, "]")
    #         print("Full Scale Power (50 ohm) = ",
    #               round(self.power_full_scale.value, 2),
    #               " [", self.power_full_scale.unit, "]")
    #         print("Possible Saturation Limits = ",
    #               -self.sat_count_lin / 2,
    #               self.sat_count_lin / 2)
    #         print("-3 dBFS Voltage = ",
    #               self.sat_count_lin * self.voltage_scaling_factor)
    #         print("\n-3 dBFS = ", -2, " dBm")
    #         print("\t= ", -2 + 106.9, "dBuV")
    #         print("\t= ", 10. ** ((-2 + 106.9) / 20.) * 1E-6, "V")
    #         print("\t= ", (10. ** ((-2 + 106.9) / 20.) * 1E-6) / 500E-3,
    #               " ratio")
    #         print("\t= ", (10. ** ((-2 + 106.9) / 20.) * 1E-6) / 500E-3 * 1024,
    #               " count")
    #         print("*****\n")
    #     return np.floor(self.sat_count_lin)

    # def telescope_gain_pattern(self,
    #                            verbose,
    #                            band,
    #                            lambda0):
    #     """
    #         :description
    #         ____________
    #             Radiation / Gain Pattern for MeerKAT Telescope
    #             as computed with CST (<10 GHz) / GRASP (>10 GHz)
    #         :params
    #         _______
    #         :return
    #         _______
    #     """
    #     # Read in CST Computed Gain Data
    #     # L-Band
    #     if band == "L-band":
    #         mat = scipy.io.loadmat(
    #             "../data/Telescope_Gain_Data/MK_CST_Lband_%d.mat" % 1100)
    #     # Radiation Pattern Theta (Elevation)
    #     rad_theta = mat["th"].squeeze()
    #     # Radiation Pattern Phi (Azimuth)
    #     rad_phi = mat["ph"].squeeze()

    #     # Jones Matrix
    #     # Cross-pol: H-pol feed & V-pol wave
    #     JHV = mat["Jqv"].squeeze()
    #     # Co-pol: H-pol feed & H-pol wave
    #     JHH = mat["Jqh"].squeeze()
    #     # Co-pol: V-pol feed & V-pol wave
    #     JVV = mat["Jpv"].squeeze()
    #     # Cross-pol: V-pol feed & H-pol wave
    #     JVH = mat["Jph"].squeeze()

    #     # Normalise patterns to bore-sight
    #     JHH /= abs(JHH).max()
    #     JVV /= abs(JVV).max()
    #     JHV /= abs(JHH).max()
    #     JVH /= abs(JVV).max()
    #     Jones = [JHH,
    #              JHV,
    #              JVH,
    #              JVV]
    #     # Maximum Antenna Gain (Theoretical)
    #     # Telescope Dish Diameter
    #     D = 13.5  # m (MeerKAT)
    #     # Effective Apperture
    #     Aeff = np.pi*(D/2)**2.
    #     # Ratio Diameter to Wavelength
    #     d_wlen = D/lambda0
    #     # Antenna Efficiency
    #     eta_a = 1.0
    #     # Maximum Antenna Gain:
    #     # Linear
    #     Gmax_lin =\
    #         eta_a * (np.pi * D/lambda0)**2.
    #     # Logarithmic
    #     Gmax = 10.*np.log10(Gmax_lin)
    #     Gmax2 = 10.*np.log10((4*np.pi*Aeff)/(lambda0**2.))

    #     ITU_RA1631_gain(
    #         )
    #     if verbose:
    #         print("\n*****")
    #         print("Telescope Radiation/Gain Pattern:")
    #         print("Elevation :: Theta = ",
    #               np.min(rad_theta),
    #               np.max(rad_theta),
    #               np.shape(rad_theta),
    #               "delta_theta = ",
    #               round(np.max(rad_theta)/np.shape(rad_theta)[0], 2),
    #               "deg")
    #         print("Azimuth :: Phi = ",
    #               np.min(rad_phi),
    #               np.max(rad_phi),
    #               np.shape(rad_phi),
    #               "delta_phi = ",
    #               round(np.max(rad_phi)/np.shape(rad_phi)[0]),
    #               "deg\n")
    #         print("Telescope Dish Diameter = ",
    #               D,
    #               "m")
    #         print("Apperture Area = ",
    #               round(Aeff, 2),
    #               "m**2.")
    #         print("Maximum Gain = ",
    #               round(Gmax2, 2),
    #               " dBi\n")
    #         print("ITU-R RA.1631")
    #         print("G1 = %.1f dBi" % G1[0])
    #         print("phi_m = %.1f deg" % phi_m[0])
    #         print("phi_r = %.1f deg" % phi_r[0])
    #         print("*****\n")
    #     return rad_theta, rad_phi, Jones, Gmax2,\
    #         phi_gain, itu_gain_simple,\
    #         phi_gain2, itu_gain_bessel
            
            

