# -*- coding: utf-8 -*-
import astropy.units as u
import numpy as np

# def equatorial_to_topo_approx(lst_greenwich_deg, obs_lon, obs_lat, ra, dec):
def equatorial_to_topo_approx(mjds,Nsats, vis_mask,obs_lon, obs_lat, ra, dec):
    """
    by B. Winkel 2020

    Parameters
    ----------
    mjds : TYPE
        DESCRIPTION.
    Nsats : TYPE
        DESCRIPTION.
    vis_mask : TYPE
        DESCRIPTION.
    obs_lon : TYPE
        DESCRIPTION.
    obs_lat : TYPE
        DESCRIPTION.
    ra : TYPE
        DESCRIPTION.
    dec : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    """

    from astropy.time import Time

    obstime = Time(mjds, format='mjd')
    _lst_greenwich_deg = obstime.sidereal_time(
        kind='mean', longitude='greenwich'
        ).deg

    lst_greenwich_deg = (np.repeat(
        _lst_greenwich_deg, Nsats, axis=0
        ).T)[np.newaxis][vis_mask]

    hour_angle_deg = (lst_greenwich_deg[np.newaxis]*u.deg - obs_lon) - ra

    azim = 180.*u.deg - np.degrees(np.arctan2(
        np.sin(np.radians(-hour_angle_deg)),
        np.sin(np.radians(obs_lat)) * np.cos(np.radians(hour_angle_deg)) -
        np.cos(np.radians(obs_lat)) * np.tan(np.radians(dec))
        ))

    elev = np.degrees(np.arcsin(
        np.cos(np.radians(obs_lat)) * np.cos(np.radians(hour_angle_deg)) *
        np.cos(np.radians(dec)) +
        np.sin(np.radians(obs_lat)) * np.sin(np.radians(dec))
        ))

    return azim, elev

    #     return azim, elev

def eq_to_azel(mjds, obs_lon, obs_lat, ra, dec):
    # mjd of J2000 = 51544.5
    from astropy.time import Time
    obstime = Time(mjds, format='mjd')
    lst_greenwich_deg = obstime.sidereal_time(
        kind='mean', longitude= 'greenwich' #obs_lon*u.deg
        ).deg
      
    # az, el = equatorial_to_topo_approx(
    #     _lst_greenwich_deg,
    #     obs_lon, obs_lat,
    #     ra,
    #     dec
    #     )        
    
    _lst_greenwich = np.radians(lst_greenwich_deg)
    _obs_lon = np.radians(obs_lon)
    _obs_lat = np.radians(obs_lat)
    _ra = np.radians(ra)
    _dec = np.radians(dec)
    
    hour_angle_r = _lst_greenwich + _obs_lon - _ra
    # hour_angle_r = _lst_greenwich - _ra

    az = 180. - np.degrees(np.arctan2(
        np.sin((-hour_angle_r)),
        np.sin((_obs_lat)) * np.cos((hour_angle_r)) -
        np.cos((_obs_lat)) * np.tan((_dec))
        ))
    el = np.degrees(np.arcsin(
        np.cos((_obs_lat)) * np.cos((hour_angle_r)) *
        np.cos((_dec)) +
        np.sin((_obs_lat)) * np.sin((_dec))
        ))
    
    
    return az,el
    
        
    
def sky_cells_m1583(niters,
                    step_size=3 * u.deg,
                    lat_range=(0 * u.deg, 90 * u.deg),
                    test =0):
    """
    :description
    ____________
        function that tesellates a sphere and defines random pointing
        directions within the cells of the tesselation, as described
        in Recommendation ITU-R M.1583

    :params
    _______
         niters:
             defines the number of iterations in random pointings
             within the cells of the tesselation
         step_size:
         lat_range:
         test:

    :returns
    ________
        tel_az:
            random pointings (az)
        tel_el:
            random pointings (el)
        grid_info:
            dictionary with the information of the cells (not random)
            keys:
                cell_lon: centre longitud of cells
                cell_lat: centre latitude of cells
                cell_lon_low:
                cell_lon_high:
                cell_lat_low:
                cell_lat_high:
                solid_angle:
    """



    def sample(niters,
               low_lon,
               high_lon,
               low_lat,
               high_lat):
        """
        :description
        ____________
            This function takes a cell and defines a random pointing
            direction within it.

        :params
        _______
            niters:
            low_lon:
            high_lon:
            low_lat:
            high_lat:
        :returns
        ________
        """
        z_low, z_high = np.cos(np.radians(90 - low_lat)), np.cos(np.radians(90 - high_lat))
        az =\
            np.random.uniform(low_lon,
                              high_lon,
                              size=niters)
        el = 90 - np.degrees(np.arccos(
            np.random.uniform(z_low,
                              z_high,
                              size=niters)))
        return az, el


    cell_edges, cell_mids, solid_angles, tel_az, tel_el = [], [], [], [], []

    lat_range = (lat_range[0].to_value(u.deg), lat_range[1].to_value(u.deg))
    ncells_lat = int(
        (lat_range[1] - lat_range[0]) / step_size.to_value(u.deg) + 0.5
        )
    edge_lats = np.linspace(
        lat_range[0], lat_range[1], ncells_lat + 1, endpoint=True
        )
    mid_lats = 0.5 * (edge_lats[1:] + edge_lats[:-1])

    for low_lat, mid_lat, high_lat in zip(edge_lats[:-1], mid_lats, edge_lats[1:]):

        ncells_lon = int(360 * np.cos(np.radians(mid_lat)) / step_size.to_value(u.deg) + 0.5)
        edge_lons = np.linspace(0, 360, ncells_lon + 1, endpoint=True)
        mid_lons = 0.5 * (edge_lons[1:] + edge_lons[:-1])

        solid_angle = (edge_lons[1] - edge_lons[0]) * np.degrees(
            np.sin(np.radians(high_lat)) - np.sin(np.radians(low_lat))
            )
        for low_lon, mid_lon, high_lon in zip(edge_lons[:-1], mid_lons, edge_lons[1:]):
            cell_edges.append((low_lon, high_lon, low_lat, high_lat))
            cell_mids.append((mid_lon, mid_lat))
            solid_angles.append(solid_angle)
            cell_tel_az, cell_tel_el = sample(niters, low_lon, high_lon, low_lat, high_lat)
            tel_az.append(cell_tel_az)
            tel_el.append(cell_tel_el)

    tel_az = np.array(tel_az).T  # TODO, return u.deg
    tel_el = np.array(tel_el).T

    grid_info = np.column_stack([cell_mids, cell_edges, solid_angles])
    grid_info.dtype = np.dtype([  # TODO, return a QTable
        ('cell_lon', np.float), ('cell_lat', np.float),
        ('cell_lon_low', np.float), ('cell_lon_high', np.float),
        ('cell_lat_low', np.float), ('cell_lat_high', np.float),
        ('solid_angle', np.float),
        ])

    if test:
        import matplotlib.pyplot as plt
        # tel_az1, tel_el1, grid_info1 = sky_cells_m1583(
        #                                             niters,
        #                                             step_size=3 * u.deg,
        #                                             lat_range=(0 * u.deg, 90 * u.deg),
        #                                             test =0)
        npix = len(tel_az[0])

        fig = plt.figure(figsize=[12,10])
        plt.plot(tel_az[0], tel_el[0], 'bx')

        plt.plot(tel_az[1], tel_el[1], 'rx')
        plt.grid()
        plt.xlabel('az')
        plt.ylabel('el')
        plt.show()

        fig = plt.figure()
        plt.hist(tel_el.flatten(), bins=90, range=(0, 90))
        plt.xlabel('el')
        plt.show()

    return tel_az, tel_el, grid_info


def SKA_mid_site(area = 1,
                 give_core = False):
    """
    Parameters
    ----------
    area : int, optional
        options:
            1- wider area 100 MHz-2000 MHz

            2- second area 2000 MHz - 6000 MHz

            3- smaller area 6000 MHz - 25000 MHz

    give_core: boolean, optional
        returns the coordinates of the core, default is False

    Returns
    -------
    lons : TYPE
        DESCRIPTION.
    lats : TYPE
        DESCRIPTION.

    core_lon: float
        longitude of the core of the SKA in deg

    core_lat: float
        latitude of the core of SKA in deg

    core_alt:
        altitude above sea level in km
    """

    if area == 1:
        lons = [18.8,   20.1,   21.6,    22.05,   23.43,   22.22,   20.98,  19.0, 18.8]
        lats = [-29.35, -28.78, -28.91,  -29.52,  -30.59,  -31.96,  -32.37, -30.41, -29.35]
    elif area == 2:
        lons = [19.66, 21.66, 22.77, 22.63, 20.65, 19.57,19.66]
        lats = [-29.51, -29.22, -30.08, -31.51, -31.82, -30.81,-29.51]
    elif area == 3:
        lons = [20.49, 21.81, 22.76, 22.54, 21.42, 20.10, 20.49]
        lats = [-29.79, -29.34, -30.08, -31.06, -31.38, -30.68, -29.79]

    if give_core:
        core_lon = 21.442958
        core_lat = -30.712980
        core_alt = 1.053

        return lons,lats, core_lon, core_lat, core_alt
    else:
        return lons,lats


def plot_geodetic_view(geo_pos=[],
                       topo_pos=[]):
    """
        Plots the satesllites in geodetic view for one snapshot in time
        overlays all the position ofthe satellites in the constellation
        in white with the satellites above 0 deg elevation in red.

    params:
    -------

    returns:
    -------
        fig:
            figure handle
    """

    import cartopy.crs as ccrs
    import matplotlib.pyplot as plt
    # import matplotlib.ticker as mticker
    from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER

    def my_stock_img(ax):
        # import shutil
        # import requests
        # url = 'https://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74192/world.200411.3x5400x2700.jpg' #2 Mb
        # response = requests.get(url, stream=True)
        # with open('Earth_img.jpg', 'wb') as out_file:
        #     shutil.copyfileobj(response.raw, out_file)
        # del response
        source_proj = ccrs.PlateCarree()
        try:
            ax.imshow(plt.imread(r'figs\Earth_img.jpg'), origin='upper',
                               transform=source_proj,
                               extent=[-180, 180, -90, 90],
                               alpha=0.8)
        except:
            import shutil
            import requests
            import os
            url = 'https://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74192/world.200411.3x5400x2700.jpg' #2 Mb
            response = requests.get(url, stream=True)
            if not os.path.exists('figs'):
                os.makedirs('figs')

            with open(r'figs\Earth_img.jpg', 'wb') as out_file:
                shutil.copyfileobj(response.raw, out_file)
            del response
            ax.imshow(plt.imread(r'figs\Earth_img.jpg'), origin='upper',
                               transform=source_proj,
                               extent=[-180, 180, -90, 90],
                               alpha=0.8)

    fig = plt.figure('Sat_Tracks')
    ax = plt.axes(projection=ccrs.PlateCarree())
    # ax.stock_img()
    my_stock_img(ax)

    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
                      linewidth=2, color='gray', alpha=0.5, linestyle='--')
    gl.top_labels = False
    gl.right_labels= False
    # gl.xlocator = mticker.FixedLocator([-150,-100,-50,0,50,100,150])
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    # gl.xlabel_style = {'size': 15, 'color': 'gray'}
    # gl.xlabel_style = {'color': 'red', 'weight': 'bold'}

    if len(geo_pos) > 0:
        # visible satellites
        niter = 0
        ntime = 0 #slice(0,geo_pos.shape[1],1)

        f = ax.scatter(geo_pos[niter,ntime,:,0].flatten(),\
                       geo_pos[niter,ntime,:,1].flatten(),\
                       color='white',s=1)

        vis_mask = topo_pos[niter,ntime,:,1]>0 #elevation > 0
        # f = ax.scatter(geo_pos[niter,ntime,vis_mask,0],\
        #                geo_pos[niter,ntime,vis_mask,1],\
        #                c = topo_pos[niter,ntime,vis_mask,2],\
        #                s=30,\
        #                cmap='jet_r') # plot distance 
        f = ax.scatter(geo_pos[niter,ntime,vis_mask,0],\
                       geo_pos[niter,ntime,vis_mask,1],\
                       c = 'red',\
                       s=10)
        plt.plot([],alpha=0,
                 label='Number of visible satellites: '+\
                     str(np.sum(np.array(vis_mask))))
        # bar = fig.colorbar(f,fraction=0.1,pad=0.01,shrink = 0.9,label='km')

        plt.show()
    return fig


def angle(ska_az,ska_el,sat_az,sat_el):
    '''
        This function uses cupy to accelerate the calculation.
        angle between pointing vector and position of the satellite in AltAz frame
        angles need to be in degrees
        To simplify the calculation the expresion
        cos(alpha) = dot(A.B)/mod(A)/mod(B) is expanded in
        the spherical coordinates of A and B = (r.cos(az).cos(el), r.sin(az).cos(el), r.sin(el))
        input in degrees
        output in degrees
    '''
    import cupy as cp
    max_memory = cp.cuda.Device(0).mem_info[0]/1e9
    mem_required = (ska_az.nbytes)*10/1e9

    if mem_required > max_memory:
        print('ERROR')
        print('Total GPU memory: %.2f Gb'%(max_memory))
        print('Required memory: %.2f  Gb'%(mem_required))
        return None

    _el = cp.asarray(ska_el*np.pi/180)
    _az = cp.asarray(ska_az*np.pi/180)
    _sat_az = cp.asarray(sat_az*np.pi/180)
    _sat_el = cp.asarray(sat_el*np.pi/180)

    angle = cp.arccos(cp.cos(_el )*cp.cos(_az)*cp.cos(_sat_el )*cp.cos(_sat_az )+
                      cp.cos(_el )*cp.sin(_az )*cp.cos(_sat_el )* cp.sin(_sat_az )+
                      cp.sin(_el )*cp.sin(_sat_el ))*180/cp.pi

    out = cp.asnumpy(angle)
    del _el,_az,_sat_az,_sat_el
    mempool = cp.get_default_memory_pool()
    mempool.free_all_blocks()

    return out


def angle_np(ska_az,ska_el,sat_az,sat_el):
    '''
        To simplify the calculation the expresion
        cos(alpha) = dot(A.B)/mod(A)/mod(B) is expanded in
        the spherical coordinates of A and B = (r.cos(az).cos(el), r.sin(az).cos(el), r.sin(el))
        input in radians or astropy units
        output in radians or astropy units
    '''

    # _el = np.asarray(ska_el*np.pi/180)
    # _az = np.asarray(ska_az*np.pi/180)
    # _sat_az = np.asarray(sat_az*np.pi/180)
    # _sat_el = np.asarray(sat_el*np.pi/180)
    # angle = np.arccos(np.cos(_el )*np.cos(_az)*np.cos(_sat_el )*np.cos(_sat_az )+
    #                   np.cos(_el )*np.sin(_az )*np.cos(_sat_el )* np.sin(_sat_az )+
    #                   np.sin(_el )*np.sin(_sat_el ))*180/np.pi

    # out = angle
    # del _el,_az,_sat_az,_sat_el    
    
    Ce1 = np.cos(ska_el)
    Se1 = np.sin(ska_el)
    Ca1 = np.cos(ska_az)
    Sa1 = np.sin(ska_az)
    Ce2 = np.cos(ska_el)
    Se2 = np.sin(ska_el)
    Ca2 = np.cos(ska_az)
    Sa2 = np.sin(ska_az)
    
    Ce1Ce2 = Ce1*Ce2
        
    out = np.arccos(Ce1Ce2*Ca1*Ca2+
                      Ce1Ce2*Sa1*Sa2+
                      Se1*Se2)
    
    return out

#%%
# if __name__ == '__main__':

    #do tests
    
