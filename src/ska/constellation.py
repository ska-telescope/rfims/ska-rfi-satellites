# -*- coding: utf-8 -*-
"""
Created on Fri Aug 28 16:11:03 2020

@author: f.divruno
@author: Braam Otto

constellation.py

Description and propagation of a mega-constellation with fixed or steerable
beams, this is a class that uses the functionalities of
cysgp4 by Benjamin Winkel

(bwinkel@mpifr.de).

Copyright (C) 2020+  Federico Di Vruno (F.DiVruno@skatelescope.org)
"""


import numpy as np
import pycraf
import astropy.units as u
import astropy.constants as const
from pycraf import geometry as geometry
import matplotlib.pyplot as plt
import sys

sys.path += ['../../src']

# import ska

from ska.ground_terminals import Ground_terminals


class Constellation:
    """
    Class that defines a satellite constellation


    Parameters
    ----------
    const_name : string
        The name of the constellation only defines the orbital parameters 
        to be used for the simulation. These are obtained from public
        sources and can include errors.
        
        If any of altitudes, inclinations, nplanes or sats_per_plane is 
        empty, the module will try to match the constellation name with
        the set of pre-defined parameters based on public information.
        
        options:
                Starlink ph1,
                Starlink ph2,
                TLE_based,
                OneWeb ph1,
                OneWeb ph2,
                GW,
                GEO,
                Kuiper,
                Kepler,
                GNSS,
                Total
                

    observer_name : string
        Name of the observer station

    observer_geo : array
        Geodetic position of the observer (lon [deg],lat [deg],alt over
        sea level[km])
       
    altitudes: array, optional
        Array with the altitudes in km of the constellation being defined,
        one altitude per orbit shell
        default value is []
        
    inclinations: array, optional
        Array with the inclinations in deg of the constellation being defined,
        one inclination per orbit shell
        default value is []
        
    nplanes: array, optional
        Array with the number of planes in each orbit shell of the 
        constellation being defined
        default value is []
            
    sats_per_plane: array, optional
        Array with the number of satellites per plane in each orbit shell
        of the constellation being defined.
        default value is []

    beam_type: string, default value = 'isotropic'
        Options are:
                'isotropic',
                'steerable',
                'statistical',
                'fixed'
        default value is 'isotropic'
    
    TLE_file: string, optional
        filename for a TLE file to use, if this is provided is used to propagate.
        
    Nchannels: int, optional
        number of frequency channels modelled in the beam pattern
        8 for OneWeb, 1 for others

    """
    def __init__(self,
                 const_name,
                 observer_name,
                 observer_geo,
                 altitudes = [],
                 inclinations = [],
                 nplanes = [],
                 sats_per_plane = [],
                 beam_type: str = 'isotropic',
                 TLE_file='',
                 nchannels= 1):

        
        self.name = const_name
        self.satname = []
        self.observer_name = observer_name
        self.observer_geo = observer_geo

        self.observer_ecef = np.array([geometry.sphere_to_cart(
            observer_geo[2]*u.km+const.R_earth,
            observer_geo[0]*u.deg,
            observer_geo[1]*u.deg)[i].value for i in range(3)])


        self.altitudes = altitudes
        self.inclinations = inclinations
        self.nplanes = nplanes
        self.sats_per_plane = sats_per_plane
        
        self.beam_type = beam_type
        
        self.constellation_params()
        
        self.TLE_file = TLE_file

        pass
    
    #*******************************
    #Constellation parameters method
    #*******************************
    def constellation_params(self):
        """
        :description
        ____________
           gets the parameters of the known constellations

        :params
        _______
            none, uses the internal 
            
        :returns
        ________
            none
        """
        ph = []
        
        if (self.altitudes==[]) or\
            (self.inclinations==[]) or\
            (self.nplanes==[]) or\
            (self.sats_per_plane==[]):


            if 'SRS AI1.13' == self.name:

                altitudes = np.array([800,800,800,800,800])
                inclinations = np.array([51,51.6,97.3,28.5,98.2])
                nplanes = np.array([6, 6, 6, 6,6])
                sats_per_plane = np.array([1, 1, 1, 1, 1])   

            if 'SRS AI1.13 10 times' == self.name:

               altitudes = np.array([800,800,800,800,800])
               inclinations = np.array([51,51.6,97.3,28.5,98.2])
               nplanes = np.array([6, 6, 6, 6,6])
               sats_per_plane = np.array([10, 10, 10, 10, 10])   

            if 'SRS AI1.13 USA' == self.name:

                altitudes = np.array([260, 450, 525, 565, 697])
                inclinations = np.array([51,51.6,97.3,28.5,98.2])
                nplanes = np.array([6, 6, 6, 6,6])
                sats_per_plane = np.array([1, 1, 1, 1, 1])   
                


            if 'Starlink ph1' in self.name:
                # constellation_self.name = 'Starlink ph1'
                # https://fcc.report/IBFS/SAT-MOD-20200417-00037 Ku-Ka band

                altitudes = np.array([550, 540, 570, 560, 560])
                inclinations = np.array([53, 53.2, 70, 97.6, 97.6])
                nplanes = np.array([72, 72, 36, 6, 4])
                sats_per_plane = np.array([22, 22, 20, 58, 43])   
                
            if 'Starlink V' in self.name:
                # V band of starlink 
                # 

                altitudes = np.array([550, 540, 570, 560, 560])
                inclinations = np.array([53, 53.2, 70, 97.6, 97.6])
                nplanes = np.array([72, 72, 36, 6, 4])
                sats_per_plane = np.array([22, 22, 20, 58, 43])   
            

                
            if 'TLE' in self.name:
                # TLE based constellation, will have to look for the TLEs from CELESTRAK
                altitudes = np.array([0])
                inclinations = np.array([0])
                nplanes = np.array([0])
                sats_per_plane = np.array([0])
                
    
            if 'Starlink ph2' in self.name:
                # constellation_name = 'Starlink ph2'
                # original filig:
                #altitudes = np.array([328, 334, 345, 360, 373, 499, 604, 614])  # in km\n,
                #inclinations = np.array([30, 40, 53, 96.9, 75, 53, 148, 115.7])  # in deg\n,
                #nplanes = np.array([7178, 7178, 7178, 40, 1998, 4000, 12, 18])  #
                #sats_per_plane = np.array([1, 1, 1, 50, 1, 1, 12, 18])  #
                
                # new filing https://fcc.report/IBFS/SAT-AMD-20210818-00105
                altitudes = np.array([340, 345, 350, 360, 525, 530, 535, 604, 614])  # in km\n,
                inclinations = np.array([53, 46, 38, 96.9, 53, 43, 33, 148, 115.7])  # in deg\n,
                nplanes = np.array([48, 48, 48, 30, 28, 28, 28, 12, 18])  #
                sats_per_plane = np.array([110, 110, 110, 120, 120, 120, 120, 12, 18])  #

    
            if 'Test' in self.name:
                
                altitudes = np.array([328])  # in km\n,
                inclinations = np.array([30])  # in deg\n,
                nplanes = np.array([7178])
                sats_per_plane = np.array([1])

    
        
            if 'OneWeb' in self.name:
                # OneWeb constellation updated parameters with the latest
                # update from ECC report 271 07/12/2020
                altitudes = np.array([1200])
                inclinations = np.array([87.9])
                nplanes = np.array([18])
                sats_per_plane = np.array([40])

    
            if 'OneWeb ph2' in self.name:
                # OneWeb constellation updated parameters with the latest
                # https://fcc.report/IBFS/SAT-MPL-20211104-00144/13337531
                altitudes = np.array([1200, 1200, 1200])
                inclinations = np.array([87.9, 40, 55])
                nplanes = np.array([36, 32, 32])
                sats_per_plane = np.array([49, 72, 72])

    
            if 'Geo' in self.name:
                # Geostationary orbit
                altitudes = np.array([35700])
                inclinations = np.array([0])
                nplanes = np.array([1])
                sats_per_plane = np.array([180])

    
            if 'GNSS' in self.name:
                # Gps satellites
                altitudes = np.array([29599])
                inclinations = np.array([56])
                nplanes = np.array([12])
                sats_per_plane = np.array([2])

                
            if 'GW' in self.name:
                # Geostationary orbit
                altitudes = np.array([590, 600, 508,1145, 1145, 1145, 1145])
                inclinations = np.array([85, 50, 55,30, 40, 50, 60])
                nplanes = np.array([16, 40, 60,48, 48, 48, 48])
                sats_per_plane = np.array([30, 50, 60,36, 36, 36, 36])
                
            

                    
            if 'Total' in self.name:
                # SL 1, SL2, OW1, OW2, GW
                altitudes = np.concatenate((
                                           np.array([550, 540, 570, 560, 560]),
                                           np.array([328, 334, 345, 360, 373, 499, 604, 614]),
                                           np.array([1200]),#
                                           np.array([1200, 1200, 1200]),
                                           np.array([590, 600, 508]),
                                           np.array([1145, 1145, 1145, 1145])
                                                    ))
                inclinations = np.concatenate((
                                              np.array([53, 53.2, 70, 97.6, 97.6]),
                                              np.array([30, 40, 53, 96.9, 75, 53, 148, 115.7]),
                                              np.array([87.9]),
                                              np.array([87.9, 40, 55]),
                                              np.array([85, 50, 55]),
                                              np.array([30, 40, 50, 60])
    
                                              ))
                nplanes = np.concatenate((
                                         np.array([72, 72, 36, 6, 4]),
                                         np.array([7178, 7178, 7178, 40, 1998, 4000, 12, 18]),
                                         np.array([18]),
                                         np.array([36, 32, 32]),
                                         np.array([16, 40, 60]),
                                         np.array([48, 48, 48, 48])
                                        ))
                sats_per_plane = np.concatenate((
                                                np.array([22, 22, 20, 58, 43]),
                                                np.array([1, 1, 1, 50, 1, 1, 12, 18]),
                                                np.array([40]),
                                                np.array([49, 720, 720]), 
                                                np.array([30, 50, 60]),                                                
                                                np.array([36, 36, 36, 36])
                                               ))
    
            
            ph = (360 / (nplanes + 1) / sats_per_plane)
            
            if ((len(altitudes) == 0)\
                or (len(inclinations) == 0)\
                or (len(sats_per_plane) == 0)\
                or (len(nplanes) == 0)):
                print('error in setting classical orbital parameters, check name or \
                      parameters')
                return
    
            self.altitudes = altitudes
            self.inclinations = inclinations
            self.nplanes = nplanes
            self.sats_per_plane = sats_per_plane
            self.ph = ph    

    #***************************************************************
    # Initialization of the spfd function depending on the beam type
    #***************************************************************
    def spfd_init(self,
                  mesh: Ground_terminals = [],
                  avd_angle: u.deg = 0*u.deg,
                  filepath: str = '',
                  verbose=False
                  ):
        """
        Initializes the spectral power flux density (spfd) function depending
        on the selected beam type.
        
        Parameters
        ----------
        mesh : Ground_terminals, optional, default is []
            mesh object created with the ground_terminals class.
            

        avd_anlge: u.deg, optional default is []
            avoidance boresight angle towards the observer, this param is only
            used for the steerable beams or the statistical beam. for
            fixed beams it is ignored.
            The default value is 0*u.deg

        filepath: str, optional
            the filepath to look for the file with the fit of the statistical
            spfd as a function of elevation

        verbose: boolean, default is False
        """
        
        self.avd_angle = avd_angle

                
        # for isotropic transmitter
        if 'isotropic' in self.beam_type: 
            def spfd_func(el_angle = [],
                    pfd_constant = -146*u.dB(u.W/u.m**2),
                    min_el = 30*u.deg,
                    all_flag = False):
                """
                Isotropic radiator with fixed SPFD
                considers the maximum sat_angle to create power
                
                Parameters
                ----------
                el_angle = 
                
                spfd_constant: type float, units dBW/m2 in 4kHz
                    constant pfd on the ground to simulate
                    from ECC report 271 the typical pfd value is -146 dBW/m2
                    in a 4kHz bandwidth
                    
                
                min_el:  type float, unit deg
                    minimum user terminal angular elevation for a satellite to point its beam
                    to the observer location, below this angle output is NaN
                
                Returns
                -------
                spfd : TYPE
                    spectral power flux density in units of W/m2/Hz
                    -146 dBW/m2 is the nominal PFD of these Ku constellations
                    in 4kHz BW
                   
                    
                """
                if el_angle ==[]:
                    try:
                        if(all_flag==False):
                            el_angle = self.topo_pos[self.vis_mask,1]
                        else:
                            el_angle = self.topo_pos[...,1]
                    except:
                        el_angle = np.array([90])

               
                #-146 dBW/m2 is the nominal PFD of these Ku constellations
                # in 4kHz BW
                nom_spfd = (pfd_constant - 10*np.log10(4e3)*u.dB(u.Hz)).to(u.W/u.m**2/u.Hz)
                spfd_o = np.ones(el_angle.shape)* nom_spfd
                
                spfd_o[el_angle < min_el.to_value(u.deg)] = np.nan
                
                return spfd_o.to(u.W/u.m**2/u.Hz)[...,None] #add a dimension for the channels
            

        # for ConstX constellation default value
        if 'steerable' in self.beam_type:

            def spfd_func(
                     mesh: Ground_terminals,
                     avd_angle=avd_angle,
                     aggregate=False,
                     Ntrys=1,
                     Nbeams=1,
                     ):
                '''
                steerable spfd given a position of the ground terminals
                and the observer location

                Returns
                -------
                None.

                '''
                avd_angle = self.avd_angle
                # visibility mask (above local horizon)
                mask = self.vis_mask
                print("All Satellites...", self.ecef_pos.shape)
                print("Masked Satellites...", self.ecef_pos[mask].shape)
                print("***")
                #calculate steering and boresight angles
                st_ph, b_ph = calc_pointing(obs_pos=self.observer_ecef,
                                            mesh_pos=mesh.mesh_cart,
                                            sat_pos=self.ecef_pos[mask],
                                            plot=False)

                #calculate gain towards the observer
                # eirp_4kHz = single_beam_eirp(st_ph,
                #                         b_ph,
                #                         avd_angle = avd_angle,
                #                         plot=False,
                #                         ) # in u.dimmensionless_scalar

                eirp_4kHz = single_beam_csv(st_ph,
                                        b_ph,
                                        avd_angle = avd_angle,
                                        plot=False,
                                        ) # in u.dimmensionless_scalar



                # spectral EIRP (Power Spectral Density or PSD)
                # seirp = eirp_4kHz / (4e3 * u.Hz) # spectral eirp in [W/Hz]
                PSD = eirp_4kHz / (4e3 * u.Hz) # Power Spectral Density [W/Hz]

                # calculate spreading loss to obtain the PFD [dBW/m2]
                # dist = np.linalg.norm((self.observer_ecef - self.ecef_pos[mask]),axis=-1)
                dist = self.topo_pos[mask, 2]*1e3  # [m]

                spread_loss = 1/(4*np.pi*dist**2) * 1/u.m**2 # in linear units

                # spfd = seirp * spread_loss[None]
                spfd = PSD * spread_loss[None]

                if aggregate:
                    spfd = group_beams(spfd, Ntrys, Nbeams)

                return spfd

        if 'statistical' in self.beam_type:
            if filepath != '':
                aux = np.load(filepath)
            else:
                # aux = np.load(r'../src/ska/steerable_spfd_avd'+str(avd_angle)+'.npz')

                aux = np.load('../src/ska/starlink_spfd_vs_el_16beams_0degavoidance.npz')
            aux.allow_pickle=True
            f = np.atleast_1d(aux['f'])[0] #f needs angles in radians

            # spfd_vs_el = aux['spfd']
            # el = aux['el']
            # z = np.polyfit(el,spfd_vs_el,3)
            # self.spfd_fit = np.poly1d(z)

            self.spfd_fit = f


            def spfd_func(all_flag=False):
                '''
                Statistical spfd on the obsrver site as calculated with the
                script Steerable_beam_statistical_pfd.py

                Parameters
                ----------
                    all_flag: flag to calculate the spfd of all satellites
                            irrespective if they are visible or not.
                Returns
                -------
                None.

                '''
                if(all_flag==False):
                    el_angle = self.topo_pos[self.vis_mask,1]
                else:
                    el_angle = self.topo_pos[...,1]
                
                spfd =  self.spfd_fit(el_angle)*u.W/u.m**2/u.Hz

                # # due to the interpolation there where some negative values, 
                # # replace with the min value of the positive values.
                # spfd[spfd <= 0] = np.min(spfd[spfd>0])
                #spfd[el_angle<36] = np.nan
                
                return spfd[...,None] #adds another dimmension at the end for the frequency channel

            if verbose:
                el1 = np.arange(0,90,1)
                plt.figure('spfd_func')
                plt.plot(el1,10*np.log10(spfd_func(el1).value),
                         linewidth=3, label = str(avd_angle))
                plt.axhline(y=-146-10*np.log10(4e3),color = 'b',linewidth=3)
                plt.legend()

        if 'constant' in self.beam_type:
            'uses fixed values for different avoidance angles in bands'
            print('Using ')
            def spfd_func():
                '''
                Statistical spfd on the obsrver site as calculated fixed, is
                the mean value.

                Parameters
                ----------
                el_angle : TYPE
                    DESCRIPTION.

                Returns
                -------
                None.

                '''
                el_angle = self.topo_pos[self.vis_mask,1]
                avd_angle = self.avd_angle
                
                if (avd_angle >= 0 *u.deg) & (avd_angle < 10 *u.deg):
                    pfd_fix = -152 #dBW/m2 in 4 kHz

                if (avd_angle >= 10 *u.deg) & (avd_angle < 15 *u.deg):
                    pfd_fix = -165 #dBW/m2 in 4 kHz

                if (avd_angle >= 15 *u.deg) & (avd_angle < 20 *u.deg):
                    pfd_fix = -169.72 #dBW/m2 in 4 kHz

                if (avd_angle >= 20 *u.deg):
                    pfd_fix = -170 #dBW/m2 in 4 kHz

                spfd_fix = pfd_fix - 10*np.log10(4e3)

                spfd = np.ones(el_angle.shape)*10**(spfd_fix/10)

                return spfd * u.W/u.m**2/u.Hz

        if 'fixed' in self.beam_type:
            def spfd_func(
                     el = np.array([]),
                     az = np.array([]),
                     dist = np.array([]),
                     all_flag: bool = False,
                     unit: str = 'lin',
                     channel: int = -1):
                """
                ConstY satellites are characterized for a fixed beam antenna,
                the spfd is a function of the position and velocity of the satellites
                and the location of the observer.

                Parameters
                ----------
                all_flag: str. default is True
                    If False is selected, then only returns the visible satellite masks.
                
                unit: str. default is 'lin'
                    options 'lin' or 'log'

                channel: int. selects the channel to calculate, can be from
                        1 to 8. if the selction is -1 it calculates the 8
                        channels and returns all of them in separate rows.

                Returns
                -------
                spfd : TYPE
                    spectral power flux density in units of W/m2.Hz
                    if channel is -1 it returns the spdf for the 8 channels of
                    the oneWeb satellites (dimmnesion is vis_mask,8).
                """
                if len(el)*len(az)*len(dist) == 0 :
                    print('in1',len(el)*len(az)*len(dist))
                    if(all_flag==False):
                        mask = self.vis_mask
                        az = self.site_az_el[mask,0]
                        el = self.site_az_el[mask,1]
                        dist = self.site_az_el[mask,2]*1e3 # distance in metres
                    else:
                        print('in1',len(el)*len(az)*len(dist))    
                        az = self.site_az_el[...,0]
                        el = self.site_az_el[...,1]
                        dist = self.site_az_el[...,2]*1e3 # distance in metres
                    

                seirp = g_constY(el, az,
                                 unit= 'lin',
                                 channel = -1) * u.W/u.Hz

                #calculate spreading loss to obtain the PFD [dBW/m2]

                spread_loss = 1/4/np.pi/dist**2 * 1/u.m**2 # [dB]

                spfd_o = seirp * spread_loss[...,None]

                return spfd_o

        if 'SA.509' in self.beam_type:
            def spfd_func(phi, Ptx, BW, diam, freq):
                 # from rec SA.509
                # Using parameters from Rec 2141
                Ptx = Ptx.to(u.W)
                
                dist = self.topo_pos[...,2]*1e3 *u.m
                spread_loss = 1/4/np.pi/dist**2 
                
                phi = np.abs(phi.to(u.deg))
                D = diam.to(u.m)
                n = 0.55 
                lda = 3e8*u.m/u.s/freq.to(u.Hz)
                   
                
                Gm = 10*np.log10((np.pi*D/lda)**2*n) #in dBi
                
                phi_0 = (20*np.sqrt(3)/(D/lda)).to(u.dimensionless_unscaled) * u.deg
                phi_1 = phi_0 * np.sqrt(20/3)
                phi_2 = 10**((50 - Gm)/25) * u.deg
                
                
                  
                G1 = Gm - 3*(phi/phi_0)**2 #in dBi
                G2 = Gm - 20
                G3 = 29 - 25*np.log10(phi.to_value(u.deg))
                G4 = -13
                G5 = -8
                
                mask1 = (phi<phi_1)
                mask2 = (phi>=phi_1) & (phi < phi_2)
                mask3 = (phi>=phi_2) & (phi < 48*u.deg)
                mask4 = (phi>=48 *u.deg) & (phi < 120*u.deg)
                mask5 = (phi>=120 *u.deg) & (phi <= 180*u.deg)
                
                G = 10**((G1*mask1 + G2*mask2 + G3*mask3 + G4*mask4 + G5*mask5)/10)
                
                seirp = Ptx/BW*G
                
                spfd = (seirp * spread_loss).to(u.W/u.m**2/u.Hz) # W/m2/Hz
                
                return spfd

        # assign appropriate spfd_func according to beam_type
        self.spfd = spfd_func





    def propagate(self,
                  epoch=[],
                  iters=1,
                  time_step=1,
                  time_steps=1,
                  verbose=False,
                  rand_seed=-1
                  ):
        """
        propagates the orbital position of the satellites in the
        constellation.

        Parameters
        ----------
        epoch : TYPE, optional
            Mean Julian date epoch to start the propagation. The default is [].
            if no epoch is given it calculates the mjd value at the moment of
            execution of the script.

        iters : TYPE, optional
            Number of iterations to propagate. The default is 1.

        time_step : TYPE, optional
            time steps duration. The default is 1 second.

        time_steps : TYPE, optional
            Number of time steps to propaagte in one iteration. The default is 1.

        rand_seed : int, optional
            integer used as a seed for the random numbers generation in the propagation
            core, this allows repetition of the constellation
            default values is -1
        verbose : TYPE, optional
            Plos the snapshot of the constellation on the map. The default is False.
        
        TLE_file: string, optional
            file location to load TLEs that were saved previously

        Returns
        -------
           Generates the atributes:
                eci_pos
                eci_vel
                topo_pos
                geo_pos
                ecef_pos
                ecef_vel
                site_azel
                site_eci
        """

        self.verbose = verbose

        import cysgp4
        from astropy.time import Time, TimeDelta

        if epoch == []: #if no epoch is provided use J2000 as the date
            # J2000 = 51544.5
            self.mjd_epoch = Time.now().mjd
        else:
            self.mjd_epoch = epoch

        self.iters = iters
        self.time_steps = time_steps
        self.time_step = time_step
        self.rand_seed = rand_seed

        def load_tles(name):
            import requests
            try:

                if 'file' in name: #get the tles from space-track historic...
                    
                    # F = open(self.TLE_file)
                    # T = F.readlines()
                    # T1 = []
                    # for i in range(int(len(T)/2)):
                    #     T1.append('IRIDIUM 7 ---           \n')
                    #     T1.append(T[i*2])
                    #     T1.append(T[i*2+1])
                        
                        
                    # T2 = str(T1)
                    # T2 = str.replace(T2,"'","")
                    # T2 = str.replace(T2,"[","")
                    # T2 = str.replace(T2,"]",'\r\n')
                    # T2 = str.replace(T2,",","")
                    # T3 = str.replace(T2,'\\n ','\r\n')
                    # sat_tles = np.array(cysgp4.tles_from_text(T3))
                    
                    aux = np.load(self.TLE_file)
                    aux.allow_pickle = True
                    celestrak_geo = np.atleast_1d(aux['geo'])[0]
                    celestrak_oneweb = np.atleast_1d(aux['oneweb'])[0]
                    celestrak_starlink = np.atleast_1d(aux['starlink'])[0]
                    celestrak_active = np.atleast_1d(aux['active'])[0]

                    if ('oneweb' in name) or ('OneWeb' in name):
                        celestrak_latest = celestrak_oneweb

                    if ('starlink' in name) or ('Starlink' in name):
                        celestrak_latest = celestrak_starlink

                    if ('geo' in name) or ('Geo' in name):
                        celestrak_latest = celestrak_geo
                        
                    if ('active' in name) or ('Active' in name):
                        celestrak_latest = celestrak_active
                        
                else:
                    
                    if 'Starlink' in name:
                        celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/starlink.txt')
        
                    
                    if 'OneWeb' in name:
                        celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/oneweb.txt')
                        # #all the active satellites in the sky
                        # celestrak_active = requests.get('https://celestrak.com/NORAD/elements/active.txt')
                    
                    
                    if 'Geo' in name:
                    #Geostationary satellites from celestrack
                        celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/geo.txt')
                        # celestrak_latest = requests.get('https://celestrak.com/satcat/gpz.php')
                        # print(celestrak_latest)

                    if 'Active' in name:
                        # #all the active satellites in the sky
                        celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/active.txt')
    
                        
                    if 'Beidou' in name:
                    #Geostationary satellites from celestrack
                        celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/beidou.txt')
    
                    if 'Orbcomm' in name:
                    #Geostationary satellites from celestrack
                        celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/orbcomm.txt')                    
                        
                    if 'Iridium' in name:
                    #Geostationary satellites from celestrack
                        celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/iridium.txt')                    
                    
                    
            except:
                print('Error in getting TLEs from Celestrak')


            sat_tles = np.array(cysgp4.tles_from_text(celestrak_latest.text))
            
            if '2018' in name:
                ##remove starlink and oneweb from the list of active satellites
                satname = 'STARLINK'
                aux = []
                for k in range(len(sat_tles)):
                    if satname in sat_tles[k].__repr__():
                        aux.append(k)
                sat_tles = np.delete(sat_tles, aux)

                # remove OneWeb
                satname = 'ONEWEB'
                aux = []
                for k in range(len(sat_tles)):
                    if satname in sat_tles[k].__repr__():
                        aux.append(k)
                sat_tles = np.delete(sat_tles, aux)

                       



            if 'nogeo' in name:
                # remove GEOS
                celestrak_latest = requests.get('https://celestrak.com/NORAD/elements/geo.txt')
                geo_tles = np.array(cysgp4.tles_from_text(celestrak_latest.text))

                for j in range(len(geo_tles)):
                    satname = geo_tles[j].__repr__()
                    # print(satname)
                    aux = []
                    for k in range(len(sat_tles)):
                        if satname in sat_tles[k].__repr__():
                            aux.append(k)
                    sat_tles = np.delete(sat_tles, aux)

            return sat_tles


        def create_constellation(mjd_epoch,
                                 altitudes,
                                 inclinations,
                                 nplanes,
                                 sats_per_plane,
                                 phase):
            my_sat_tles = []
            sat_nr = 1
            seed_cont = 1
            for alt, inc, n0, s0, p0 in zip(
                altitudes, inclinations, nplanes, sats_per_plane, phase):

                # Satellite Mean Anomoly
                mas0 = np.linspace(0.0, 360.0, s0, endpoint=False)  # distribution of mean anomalies around the plane
                #         mas0 += np.random.uniform(0, 360, 1) # check this, replaced by (1)
                # Satellite Right Ascension
                raans0 = np.linspace(0.0, 360.0, n0, endpoint=False)  # right ascensions distribution
                mas, raans = np.meshgrid(mas0, raans0)

                if self.rand_seed >= 0 :
                    np.random.seed()
                    np.random.seed(self.rand_seed*seed_cont)
                    seed_cont += 1
                else:
                    np.random.seed()
                # Satellite Mean Anomoly Randomness
                mas += np.random.uniform(0.0, 360.0, n0)[:, np.newaxis]  # (1) # random phase of the satellites
                # mas += np.arange(0,n0*p0,p0)[:,np.newaxis] #(2) # phase of the satellites in the planes
                mas, raans = mas.flatten(), raans.flatten()

                # Satellite Mean Motion
                mm = cysgp4.satellite_mean_motion(alt)
                for ma, raan in zip(mas, raans):
                    my_sat_tles.append(
                        cysgp4.tle_linestrings_from_orbital_parameters(
                            'TEST {:d}'.format(sat_nr), sat_nr, np.atleast_1d(mjd_epoch)[0],
                            inc, raan, 0.001, 0., ma, mm
                        ))

                    sat_nr += 1

                with open('TEST_TLEs.txt', 'w') as myfile:
                    for line in my_sat_tles:
                        myfile.write(str(line[0]) + "\n" +
                                     str(line[1]) + "\n" +
                                     str(line[2]) + "\n")
                myfile.close()
                try:
                    sat_tles = np.array([
                        cysgp4.PyTle(*tle)
                        for tle in my_sat_tles
                    ])
                except:
                    print('error in ')
                    print(my_sat_tles)
            return sat_tles


        def greenwichsrt(Jdate):
            """
            Convert Julian time to sidereal time
            D. Vallado Ed. 4

            Parameters
            ----------

            Jdate: float
                Julian date (since Jan 1, 4713 BCE)

            Results
            -------

            tsr : float
                Sidereal time in radians
            """

            # Vallado Eq. 3-42 p. 184, Seidelmann 3.311-1
            tUT1 = (Jdate - 2451545.0) / 36525.0

            # Eqn. 3-47 p. 188
            gmst_sec = 67310.54841 + (876600 * 3600 + 8640184.812866)\
                * tUT1 + 0.093104 * tUT1 ** 2 - 6.2e-6 * tUT1 ** 3

            # 1/86400 and %(2*pi) implied by units of radians
            return gmst_sec * (2 * np.pi) / 86400.0 % (2 * np.pi)


        def eci_to_ecef2(eci_pos, eci_vel, mjd):
            """
            Receives the position and velocity vectors in the ECI ref frame and the
            geodetic position in spherical form
            Calculates the  rotation angles in Z and Y between the eci and geodetic frames.
            returns ecef position and velocity in meters and meters/s

            Parameters
            ----------
                eci_pos:

                eci_vel:

                mjd:

            Results
            -------

            """

            phi = greenwichsrt(mjd)
            ph0 = phi[...,np.newaxis]*np.ones(eci_pos[...,0].shape)

            dims = [eci_pos[...,0].shape[i] for i in range(eci_pos[...,0].ndim)]
            T_z = np.zeros(np.append(np.array(dims), [3, 3]))
            T_z[..., 2, 2] = np.ones(ph0.shape)
            T_z[..., 0, 0] = np.cos(ph0)
            T_z[..., 0, 1] = -np.sin(ph0)
            T_z[..., 1, 0] = np.sin(ph0)
            T_z[..., 1, 1] = np.cos(ph0)

            ecef_pos = np.einsum('...i,...ji', eci_pos, T_z)
            ecef_vel = np.einsum('...i,...ji', eci_vel, T_z)

            return ecef_pos, ecef_vel


        def geo_to_eci(geo_pos,mjds):
            """
            Converts a geodetic position in an ECI position in cartesian form
            dimensions of geo_pos and mjds should match up to the last one.

            Parameters
            ----------
            geo_pos : array(...,3)
                last dimension should be long[deg], lat[deg], alt over sea[km]

            mjds : array
                array of time steps in mean julian day format.

            Returns
            -------
            eci : array (...,3)
                The dimensions are kept from geo_pos and mjds

            """

            geo = cysgp4.PyCoordGeodetic(geo_pos[0],
                                         self.observer_geo[1],
                                         self.observer_geo[2])
            obs_eci_list = []
            for i in range(len(self.mjds.flatten())):
                pydt = cysgp4.PyDateTime.from_mjd(self.mjds.flatten()[i])
                obs_eci_list.append(cysgp4.PyEci(pydt,geo).loc)
            self.obs_eci =\
                np.reshape(np.array(obs_eci_list),
                           [self.iters,self.time_steps,3])


        def eci_to_ecef(eci_pos, geo_pos, eci_vel):
            """
            Receives the position and velocity vectors in the ECI ref frame and the
            geodetic position in spherical form
            Calculates the  rotation angles in Z and Y between the eci and geodetic frames.
            returns ecef position and velocity in meters and meters/s

            Parameters
            ----------

            Results
            -------

            """
            import astropy.constants as const

            def angle2d(va, vb):
                '''' angle between two vectors
                '''
                #        phi = np.arccos(np.einsum('...i,...i',va,vb)/np.linalg.norm(va,axis=-1)/np.linalg.norm(vb,axis=-1))
                phi_A = np.arctan2(va[..., 1], va[..., 0])
                phi_B = np.arctan2(vb[..., 1], vb[..., 0])
                phi = phi_B - phi_A

                return phi

            if self.verbose:
                print("ECI Pos = ", eci_pos[0, 0, 0, :], " = x, y, z [km]")
                print("Geodetic Pos = ", geo_pos[0, 0, 0, :], " = lon, lat, alt [deg], [deg], [km]")
                print("ECI Vel = ", eci_vel[0, 0, 0, :], " = vx, vy, vz [km/s]")
                print("Earth Radius = ", const.R_earth.value, "[", const.R_earth.unit, "]")

            geo_pos_cart = np.array(
                pycraf.geometry.sphere_to_cart(
                    geo_pos[..., 2] * u.km + const.R_earth,
                    geo_pos[..., 0] * u.deg,
                    geo_pos[..., 1] * u.deg)
            ).transpose(1, 2, 3, 0)
            Mgeo = np.linalg.norm(geo_pos_cart, axis=-1)
            Meci = np.linalg.norm(eci_pos, axis=-1)

            # angle in xy plane
            ph0 = angle2d(eci_pos[..., 0:2], geo_pos_cart[..., 0:2])

            # rotate in Y
            theci = np.arccos(eci_pos[..., 2] / Meci)
            thgeo = np.arccos(geo_pos_cart[..., 2] / Mgeo)
            th0 = thgeo - theci

            dims = [th0.shape[i] for i in range(th0.ndim)]
            T_y = np.zeros(np.append(np.array(dims), [3, 3]))
            T_y[..., 1, 1] = np.ones(th0.shape)
            T_y[..., 0, 0] = np.cos(th0)
            T_y[..., 0, 2] = np.sin(th0)
            T_y[..., 2, 0] = -np.sin(th0)
            T_y[..., 2, 2] = np.cos(th0)

            # rotate in Z
            dims = [ph0.shape[i] for i in range(ph0.ndim)]
            T_z = np.zeros(np.append(np.array(dims), [3, 3]))
            T_z[..., 2, 2] = np.ones(ph0.shape)
            T_z[..., 0, 0] = np.cos(ph0)
            T_z[..., 0, 1] = -np.sin(ph0)
            T_z[..., 1, 0] = np.sin(ph0)
            T_z[..., 1, 1] = np.cos(ph0)

            T = np.matmul(T_y, T_z)
            ecef_pos = np.einsum('...i,...ji', eci_pos, T)
            ecef_vel = np.einsum('...i,...ji', eci_vel, T)

            # check ecef_pos:
            #        ecef_sph = np.array(pycraf.geometry.cart_to_sphere(ecef_pos[...,0]*u.m,ecef_pos[...,1]*u.m,ecef_pos[...,2]*u.m))
            #        diff_lon = geo_pos[...,0]-ecef_sph[...,1]
            #        diff_lat = geo_pos[...,1]-ecef_sph[...,2]
            #        if (diff_lon.max() > 0.1) or (diff_lat>0.1):
            #            print(diff_lon.max(), diff_lat.max())
            #            return None,None
            return ecef_pos, ecef_vel


        #create ecef observer:
        ras_observer = cysgp4.PyObserver(
            self.observer_geo[0],
            self.observer_geo[1],
            self.observer_geo[2])


        # Synthetizing the TLEs from classical orbital elements
        if 'TLE' in self.name:
            self.tles = load_tles(self.name)
            
        else:
            self.tles = create_constellation(
                    self.mjd_epoch, 
                    self.altitudes, 
                    self.inclinations, 
                    self.nplanes, 
                    self.sats_per_plane, 
                    self.ph)

        for k in range(len(self.tles)):
            self.satname.append(self.tles[k].__repr__()[8:-2])

        self.satname = np.array(self.satname)

        # generate array of iterations and time steps
        # iterations
        mjds = np.atleast_1d(self.mjd_epoch)
        if len(mjds) == 1: #is a single value was provided, generate the time vector
            if self.iters > 1:
                if self.rand_seed >= 0 :
                    np.random.seed(self.rand_seed*100)
                else:
                    np.random.seed()
    
                mjds = mjds + np.random.random(self.iters)*10 #each iteration within 10 days

            # time steps, mjds is a [iters x time steps] matrix
            # self.mjds = mjds[:,np.newaxis] + \
            #             np.arange(0, self.time_steps*self.time_step, self.time_step)[np.newaxis] / \
            #                 (self.time_step * 60 * 60 * 24)
    
            self.mjds = mjds[:,np.newaxis] + \
                        np.arange(0, self.time_steps*self.time_step, self.time_step)[np.newaxis] / \
                            (60 * 60 * 24)
        else: #if the vector with specific mjds is provided, use it as is
            self.mjds = mjds
            
        # Create the constellations for each iteration:
        result = cysgp4.propagate_many(
            self.mjds[..., np.newaxis],
            self.tles[np.newaxis, np.newaxis],
            ras_observer,
            do_obs_pos=True,
            do_sat_azel=True,
            on_error = 'coerce_to_nan') #dismiss error due to too old TLEs)

        self.eci_pos = result['eci_pos'] # X,Y,Z in km
        self.eci_vel = result['eci_vel'] # Vx,Vy,Vz in km/s
        self.geo_pos = result['geo']     # long, lat, alt (in deg, deg, km)
        self.topo_pos = result['topo']   # az, el, distance, distance rate\
                                         # see cysgp4 documentation (in deg, deg, km, km/s) 
        self.site_az_el = result['sat_azel'] # phi_along, phi_across, dist

        if verbose:
        # Plot spherical view of satellites (indexed)
            fig = self.plot_spherical_view()

        # Plot geodetic view of the satellites
            # fig = self.plot_geodetic_view() #need Cartopy

        #TODO: CHECK WHY FDV CHANGED FROM km TO m
        # change the unit to meters
        self.eci_pos = (self.eci_pos) * 1e3 # changes [km] to [m]

        # change the reference frame of the satellite from zxy (z=vel,x=nadir,y) to xzy(x=vel,z=nadir,y)
        # eci_vel = (np.array((eci_vel[...,2],-eci_vel[...,1],eci_vel[...,0]))).transpose(1,2,0)

        # Transform the ECI coordinates (pos and vel) to ECEF
        ecef_pos, ecef_vel = eci_to_ecef(self.eci_pos, self.geo_pos, self.eci_vel)

        # ecef_pos, ecef_vel = eci_to_ecef2(self.eci_pos, self.eci_vel, self.mjds)

        # # can be done using pymap3d, slower but surely more accurate
        # # 1st create a datetime variable with the time of the position.
        # t = Time(self.mjds,format='mjd') #load the time in mjd format
        # t.format = 'datetime' # change the format to datetime
        # # match the dimension of the position array [iters,time,nsats]        
        # A = TimeDelta(np.zeros(self.eci_pos[...,0].shape)) 
        # A.format = 'datetime'
        # time = (t.value)[...,np.newaxis] + A.value

        # ecef_pos = np.array(
        #                 pm.eci2ecef(self.eci_pos[...,0],
        #                             self.eci_pos[...,1],
        #                             self.eci_pos[...,2],
        #                             time.tolist(),
        #                             use_astropy= False)).transpose(1,2,3,0)
        # ecef_vel = np.array(
        #                 pm.eci2ecef(self.eci_vel[...,0],
        #                             self.eci_vel[...,1],
        #                             self.eci_vel[...,2],
        #                             time.tolist(),
        #                             use_astropy= False)).transpose(1,2,3,0)
        # # self.ecef_pos = ecef_pos2
        # # self.ecef_vel = ecef_vel2

        ''' All Satellites '''
        self.ecef_pos = ecef_pos
        print("All Satellites ECEF...", self.ecef_pos.shape)
        print("***")

        ''' Indexed Satellites '''
        # self.ecef_pos = ecef_pos[0, 0, self.ind0:self.ind1:self.n, :]
        # print("ECI-to-ECEF")
        # print(self.ecef_pos.shape)
        # for a in range(0, len(self.ecef_pos)):
        #     print(self.ecef_pos[a, :])

        self.ecef_vel = ecef_vel
        self.vis_mask = self.topo_pos[..., 1] > 0

        # # check going from ECEF to geodetic
        # ell_wgs84 = pm.Ellipsoid('wgs84')
        # ecef_to_geo = np.array(pm.ecef2geodetic(ecef_pos[...,0],\
        #                             ecef_pos[...,1],\
        #                             ecef_pos[...,2],\
        #                             ell_wgs84)).transpose(1,2,3,0)
        # aux = np.copy(ecef_to_geo)
        # ecef_to_geo[...,0] = aux[...,1]
        # ecef_to_geo[...,1] = aux[...,0]
        # ecef_to_geo[...,2] /= 1e3

        # import matplotlib.pyplot as plt
        # dif = (ecef_to_geo - self.geo_pos)
        # mean = (ecef_to_geo + self.geo_pos)/2
        # dif_n = dif/mean*100
        # plt.figure()
        # plt.plot(dif_n[...,0].flatten())
        # plt.plot(dif_n[...,1].flatten())
        # plt.plot(dif_n[...,2].flatten())

        # check comparing both ECI to ECEF methods        
        # import matplotlib.pyplot as plt
        # dif = (ecef_pos2 - ecef_pos)
        # mean = (ecef_pos2 + ecef_pos)/2
        # dif_n = dif/mean*100
        # plt.figure()
        # plt.plot(dif_n[...,0].flatten())
        # plt.plot(dif_n[...,1].flatten())
        # plt.plot(dif_n[...,2].flatten())


    def geo_to_ecef(self, lat, lon, h, verbose):
        """ Geodetic to ECEF Conversion

            params:
            -------
                lat:
                lon:
                h:
                verbose:

            returns:
            --------
                [X, Y, Z]:
        """
        lat = np.deg2rad(lat)
        lon = np.deg2rad(lon)
        if verbose:
            print("***")
            print("Lat=", lat, " deg, Lon=", lon, " deg, h=", h, " m")
        a = 6378137.0          # semi-major equatorial axis [m]
        b = 6356752.31414      # semi-minor equatorial axis [m]
        e_sqrd = 1 - (b**2.)/(a**2.)
        if verbose:
            print("Semi-Major = ", a/1e3, " km")
            print("Semi-Minor = ", b/1e3, " km")
            print("e_sqrd = ", e_sqrd*1e3, "*1E-3")
        N = a/(1 - e_sqrd*(np.sin(lat.value)**2.))  # prime vertical radius of curvature
        if verbose:
            print("Prime Vertical Radius = ", N/1e3, " km")
        X = (N + h.value)*np.cos(lat.value)*np.cos(lon.value)
        Y = (N + h.value)*np.cos(lat.value)*np.sin(lon.value)
        Z = ((b**2.)/(a**2.)*N + h.value)*np.sin(lat.value)
        if verbose:
            print("(X, Y, Z) = ", X, Y, Z)
        ''' Meridian Arc - Distance between 2 points on same long. '''
        M = (a*(1-e_sqrd))/((1-e_sqrd*(np.sin(lat.value))**2.)**(3./2.))
        if verbose:
            print("Meridian Arc = ", M/1e3, " km")
        return [X, Y, Z]


    def plot_spherical_view(self, 
                            plot_all=True,
                            time_idx = 0):
        """
            Plots the satesllites in 3D spherical view for one snapshot in time
            overlays all the position of the satellites in the constellation
            in blue with the satellites above 0 deg elevation in red.

        params:
        -------
            plot_all = if set plots all the satellites generated for a defined
            time sample. if False plots only a subset.
            
            time_idx = time index to plot
            
        returns:
        -------
            fig:
                figure handle
        """

        ''' SKA-MID Virtual Core '''
        ''' Geodetic Coordinates '''
        # LAT & LON POINT ON EARTH's SURFACE
        # Lat = -30.712919 * u.deg    # [deg]
        # Lon = 21.443800 * u.deg     # [deg]
        # h = 1052 * u.m              # [m] elevation above MS
        
        Lon = self.observer_geo[0]
        Lat = self.observer_geo[1]
        h = self.observer_geo[2]*1e3
        
        midX0, midY0, midZ0 =\
            self.geo_to_ecef(lat=Lat*u.deg,
                             lon=Lon*u.deg,
                             h=h*u.m,
                             verbose=False)

        ''' ECEF AXES '''
        # X_ECEF-axis
        aX0, aY0, aZ0 =\
            self.geo_to_ecef(lat=0*u.deg,
                             lon=0*u.deg,
                             h=h*u.m,
                             verbose=False)
        # Y_ECEF-axis
        aX1, aY1, aZ1 =\
            self.geo_to_ecef(lat=0*u.deg,
                             lon=90*u.deg,
                             h=h*u.m,
                             verbose=False)
        # Z_ECEF-axis
        aX2, aY2, aZ2 =\
            self.geo_to_ecef(lat=90*u.deg,
                             lon=0*u.deg,
                             h=h*u.m,
                             verbose=False)

        ''' Earth Radius '''
        ''' SPHERICAL TO CARTESIAN '''
        # Spherical
        # Re = 6371E3
        Re = const.R_earth.value
        phi = np.linspace(0*u.deg, 360*u.deg, 100)
        theta = np.linspace(0*u.deg, 180*u.deg, 100)
        THETA, PHI = np.meshgrid(np.deg2rad(theta),
                                 np.deg2rad(phi))
        # Cartesian sphere 
        Xe = Re * np.sin(THETA) * np.cos(PHI)
        Ye = Re * np.sin(THETA) * np.sin(PHI)
        Ze = Re * np.cos(THETA)

        N = len(self.geo_pos[0, 0, :, :])

        ''' Satellite Indexing '''
        if plot_all:
            # To display all satellites in the constellation
            # ----------------------------------------------
            self.ind0 = 0
            self.ind1 = N
            self.n = 1
        else:
            # To display a certain index range in the constellation
            # -----------------------------------------------------
            self.ind0 = 200
            self.ind1 = 250
            self.n = 1

            # To display every n'th index in the constellation
            # ------------------------------------------------
            # self.ind0 = 0
            # self.ind1 = N
            # self.n = 20

        # Spherical Coordinates:
        # Azimuth or Longitude = PHI
        phi =\
            np.deg2rad(self.geo_pos[0,
                                    time_idx,
                                    self.ind0:self.ind1:self.n,
                                    0]*u.deg)

        # Polar/Elevation or Latitude = THETA
        theta =\
            np.deg2rad(90*u.deg - self.geo_pos[0,
                                               time_idx,
                                               self.ind0:self.ind1:self.n,
                                               1]*u.deg)

        # THETA, PHI = np.meshgrid(theta, phi)
        THETA = theta
        PHI = phi
        # Altitude
        R = Re*u.m + self.geo_pos[0,
                                  time_idx,
                                  self.ind0:self.ind1:self.n,
                                  2] * u.km

        # Cartesian Coordinates [ECEF]
        X = R * np.sin(THETA) * np.cos(PHI)
        Y = R * np.sin(THETA) * np.sin(PHI)
        Z = R * np.cos(THETA)

        # print("Spherical-to-Cartesian")
        # for a in range(0, len(X)):
        #     print(X[a], Y[a], Z[a])

        vis_mask2 = self.topo_pos[0,
                                  time_idx,
                                  self.ind0:self.ind1:self.n,
                                  1] > 0  # elevation > 0

        ind2 = np.array([id for id, val in enumerate(vis_mask2) if val == True])

        X0 = np.ma.array(X.value, copy=True, mask=False)
        Y0 = np.ma.array(Y.value, copy=True, mask=False)
        Z0 = np.ma.array(Z.value, copy=True, mask=False)
        if len(ind2) > 0:
            X0[ind2] = True
            Y0[ind2] = True
            Z0[ind2] = True

        fig = plt.figure(num='Spherical_Sat_Tracks', figsize=(10, 10))
        ax = fig.add_subplot(111, projection='3d')
        ax.plot_wireframe(Xe, Ye, Ze, linewidth=0.5, alpha=0.5)
        # SKA-MID
        ax.plot(midX0, midY0, midZ0, 'md')
        # X-Axis
        ax.plot([0, aX0], [0, aY0], [0, aZ0], '-r')
        ax.text(aX0, aY0, aZ0, "X", color='r')
        # Y-Axis
        ax.plot([0, aX1], [0, aY1], [0, aZ1], '-g')
        ax.text(aX1, aY1, aZ1, "Y", color='g')
        # Z-Axis
        ax.plot([0, aX2], [0, aY2], [0, aZ2], '-b')
        ax.text(aX2, aY2, aZ2, "Z", color='b')

        ax.scatter(X0, Y0, Z0, color='b', s=5, alpha=0.5)
        if len(ind2) > 0:
            ax.scatter(X[ind2], Y[ind2], Z[ind2],
                       color='r', s=10, alpha=0.75)
        ax.grid(False)
        ax.axis('off')
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_zticks([])
        ax.view_init(azim = 45, elev = 0)
        plt.tight_layout()
        return fig


    # def plot_geodetic_view(self,
    #                        snapshot=True,
    #                        sat_index=[],
    #                        figsize=[12,7]):
    #     """
    #         Plots the satesllites in geodetic view for one snapshot in time
    #         overlays all the position of the satellites in the constellation
    #         in white with the satellites above 0 deg elevation in red.

    #     params:
    #     -------
    #         snapshot: boolean, Deafult: True
    #             If set to True only plots one timestep of the array, if
    #             false plot all simulated timesteps. Be conscious that this
    #             can be quite heavy.
                
    #         sat_index: tuple, index of satellites to plot
    #             if left void plots all satellites available.
                
    #     returns:
    #     -------
    #         fig:
    #             figure handle
    #     """
    #     ''' SKA-MID Virtual Core '''
    #     ''' Geodetic Coordinates '''
    #     # LAT & LON POINT ON EARTH's SURFACE
    #     midLat = -30.712919 * u.deg    # [deg]
    #     midLon = 21.443800 * u.deg     # [deg]

    #     import cartopy.crs as ccrs
    #     import matplotlib.pyplot as plt
    #     # import matplotlib.ticker as mticker
    #     from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER


    #     def my_stock_img(ax):
    #         source_proj = ccrs.PlateCarree()
    #         try:
    #             ax.imshow(plt.imread(r'figs/Earth_img.jpg'), origin='upper',
    #                                transform=source_proj,
    #                                extent=[-180, 180, -90, 90],
    #                                alpha=0.8)
    #         except:
    #             import shutil
    #             import requests
    #             import os
    #             url = 'https://eoimages.gsfc.nasa.gov/images/imagerecords/74000/74192/world.200411.3x5400x2700.jpg' #2 Mb
    #             response = requests.get(url, stream=True)
    #             if not os.path.exists('figs'):
    #                 os.makedirs('figs')

    #             with open(r'figs/Earth_img.jpg', 'wb') as out_file:
    #                 shutil.copyfileobj(response.raw, out_file)
    #             del response
    #             ax.imshow(plt.imread(r'figs/Earth_img.jpg'), origin='upper',
    #                                transform=source_proj,
    #                                extent=[-180, 180, -90, 90],
    #                                alpha=0.8)

    #     fig = plt.figure(num='Sat_Tracks', figsize=figsize)
    #     ax = plt.axes(projection=ccrs.PlateCarree())
    #     my_stock_img(ax)

    #     gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True,
    #                       linewidth=2, color='gray', alpha=0.5, linestyle='--')
    #     gl.xlabels_top = False
    #     gl.ylabels_right= False
    #     # gl.xlocator = mticker.FixedLocator([-150,-100,-50,0,50,100,150])
    #     gl.xformatter = LONGITUDE_FORMATTER
    #     gl.yformatter = LATITUDE_FORMATTER
    #     # gl.xlabel_style = {'size': 15, 'color': 'gray'}
    #     # gl.xlabel_style = {'color': 'red', 'weight': 'bold'}

        
        
    #     #        ''' Satellite Indexing '''
    #     # To display all satellites in the constellation
    #     # ----------------------------------------------
    #     self.ind0 = 0
    #     self.ind1 = self.geo_pos.shape[2]
    #     self.n = 1

    #     # To display every n'th index in the constellation
    #     # ------------------------------------------------
    #     # self.ind0 = 0
    #     # self.ind1 = N
    #     # self.n = 20
            
    #     if len(self.geo_pos) > 0:
    #         # visible satellites
    #         niter = 0
    #         ntime = 0 #slice(0,geo_pos.shape[1],1)

    #         f = ax.scatter(self.geo_pos[niter,
    #                                     ntime,
    #                                     self.ind0:self.ind1:self.n,
    #                                     0].flatten(),\
    #                        self.geo_pos[niter,
    #                                     ntime,
    #                                     self.ind0:self.ind1:self.n,
    #                                     1].flatten(),\
    #                        color='white',s=1)
    #         # SKA-MID location
    #         f = ax.plot(self.observer_geo[0], self.observer_geo[1], 'ms')

    #         # Visibility Mask - Satellites visible to observer above horizon
    #         # Above horizon is where el > 0
    #         vis_mask = self.topo_pos[niter,
    #                                  ntime,
    #                                  :,
    #                                  1] > 0  # elevation > 0

    #         vis_mask2 = self.topo_pos[niter,
    #                                   ntime,
    #                                   self.ind0:self.ind1:self.n,
    #                                   1] > 0  # elevation > 0

    #         ind2 = np.array([id for id, val in enumerate(vis_mask2) if val == True])
    #         # f = ax.scatter(self.geo_pos[niter,ntime,vis_mask,0],\
    #         #                self.geo_pos[niter,ntime,vis_mask,1],\
    #         #                c = self.topo_pos[niter,ntime,vis_mask,2],\
    #         #                s=30,\
    #         #                cmap='jet_r') # plot distance 
    #         if len(ind2) > 0:
    #             f = ax.scatter(self.geo_pos[niter,
    #                                         ntime,
    #                                         self.ind0:self.ind1:self.n,
    #                                         0][ind2],\
    #                            self.geo_pos[niter,
    #                                         ntime,
    #                                         self.ind0:self.ind1:self.n,
    #                                         1][ind2],\
    #                            c = 'red',\
    #                            s=5)
    #         plt.plot([],alpha=0,
    #                  label='Number of visible satellites: '+\
    #                      str(np.sum(np.array(vis_mask))))
    #         # bar = fig.colorbar(f,fraction=0.1,pad=0.01,shrink = 0.9,label='km')
    #     return fig


    def plot_topo_view(self,
                       figname = '',
                       title = '',
                       num_timesteps = [],
                       color=''
                       ):
        """
        Plots the constellation from the topocentric reference frame , ie 
        from the observer point of view with persistance.

        Parameters
        ----------
        figname : TYPE, optional
            DESCRIPTION. The default is ''.

        Returns
        -------
        None.

        """       
        
        import matplotlib.pyplot as plt
    
        if figname != '':
            fig = plt.figure(figname)
        else:
            fig = plt.figure(figsize=(14,10))
        
        if num_timesteps == []:
            steps = np.arange(self.topo_pos.shape[1])
        else:
            steps = np.linspace(0,self.topo_pos.shape[1]-1,num_timesteps)
        
        if color=='':
            c = 'k'
        else:
            c = color
            
        for a in steps:
            i  = int(a)
            plt.plot(self.topo_pos[0,i, self.topo_pos[0,i, :, 1] > 0, 0],\
                     self.topo_pos[0,i, self.topo_pos[0,i, :, 1] > 0, 1],\
                     '.', color=c ,markersize=1)
        plt.xlim([0, 360])
        plt.ylim([0, 90])
        plt.xlabel('azimuth [deg]')
        plt.ylabel('elevation [deg]')
        plt.grid()
        if title!='':
            plt.title(title)
        else:
            plt.title('%s- %s - %.2f seconds' % (self.observer_name,self.name,\
                      self.time_step * self.time_steps))
        return fig

#***********************
# Steerable beam eirp functions 
#***********************

#****************************
# pointing angles calculation
#****************************
def calc_pointing(obs_pos: np.array,
                  mesh_pos: np.array,
                  sat_pos: np.array,
                  max_steering = 50*u.deg,
                  plot=False):
    """
    Calculates the steering angle from each satellite to each
    mesh point on earth, as well as the differential angle to
    an observer. All the coordinates are rectangular in ECEF
    reference frame.

    Parameters
    ----------
    obs_pos : array
        ECEF position of the observer

    mesh_pos : array
        ECEF position of the mesh on the ground

    sat_pos : array
        ECEF position of the satellites

    max_steering : float degrees
        Maximum permitted steering angle
        default is 50 degrees
    plot : Boolean

    Returns
    -------
    steering_ph : array(), in units.degrees
        Steering angle from the nadir direction of the satellite

    diff_ph : array(), in units.degrees
        boresight angle from the pointing direction to the site direction
    """

    def angle_nD(A,B,):
        """
        Angle between 3D vectors in two matrices of N dimensions
        as long as the angle is smaller than 180 deg, this function works
        well.
        
        This function works for arrays of any number of dimensions, the last 
        dimension has to be 3 (X,Y,Z)
        
        Parameters
        ----------
        A : array of dimmensions [...,3]
            First vector
        B : array of dimmensions [...,3]
            second vector

        Returns
        -------
        phi : array of dimmensions [...,1]
            smaller angle between the two vectors in radians

        """

        # inner or dot product
        phi = np.arccos(np.einsum('...i,...i',A,B)\
                        /np.linalg.norm(A,axis=-1)\
                        /np.linalg.norm(B,axis=-1))*u.rad
        return phi


    print("Number of Satellites...", sat_pos.shape[0])
    print("Number of Mesh Cells...", mesh_pos.shape[0])
    print("***")
    # calc angle between the satellites and the mesh 
    # vector from the satellites to the mesh points
    sat_to_mesh = mesh_pos[:,None,:] - sat_pos[None]
    print("Sat-to-mesh Vectors...", sat_to_mesh.shape)
    sat_theta = angle_nD(sat_to_mesh, -sat_pos[None])
    print("Sat_Theta Min, Max...", np.min(np.rad2deg(sat_theta.value)),
                                   np.max(np.rad2deg(sat_theta.value)))
    print("Sat Max Steering Deg...", max_steering)

    # change to NaN the angle for pointings greater than max steering angle
    sat_theta2 = np.copy(sat_theta)
    print("Sat_Theta Shape...", sat_theta2.shape, sat_theta2.shape[0]*sat_theta.shape[1])
    ind = sat_theta > max_steering
    print("Sat_Theta > Max Steering",
          np.sum(ind),
          np.sum(ind)/(sat_theta2.shape[0]*sat_theta.shape[1])*100, "%")

    if np.sum(ind)>0:
        sat_theta2[ind] = np.NaN
    print("Non-NaN indexed shape...",
          sat_theta2[~ind].shape,
          sat_theta2[~ind].shape[0]/(sat_theta2.shape[0]*sat_theta.shape[1])*100, "%")

    #-----------------
    # calc angle between the pointing vector and the sat_obs vector 
    sat_obs = obs_pos[None,:] - sat_pos #vector from the satellites to the observer
    dif_theta = angle_nD(sat_obs[None],sat_to_mesh)
    print("Dif_Theta Min, Max, Mean...", np.min(np.rad2deg(dif_theta)),
                                         np.max(np.rad2deg(dif_theta)),
                                         np.mean(np.rad2deg(dif_theta)))
    dif_theta2 = np.copy(dif_theta)
    dif_theta2[np.isnan(sat_theta2)]= np.nan
    print("Non-NaN'ed Min, Max, Mean...",
          np.min(np.rad2deg(dif_theta2[~np.isnan(dif_theta2)])),
          np.max(np.rad2deg(dif_theta2[~np.isnan(dif_theta2)])),
          np.mean(np.rad2deg(dif_theta2[~np.isnan(dif_theta2)])))

    if plot:
        import matplotlib.pyplot as plt
        plt.figure('sat_theta', figsize=(9, 6))
        plt.hist((sat_theta.flatten()).to_value(u.deg),
                 100,
                 histtype='step',
                 label='Steering Angle')
        # plt.hist((sat_theta2.flatten()).to_value(u.deg),100,histtype='step', label = 'steering angle with 56 deglimit')
        plt.title('Steering / Pointing Angle from Satellites to Mesh')
        tmp_min = np.min(np.rad2deg(sat_theta2[~np.isnan(sat_theta2)]))
        tmp_max = np.max(np.rad2deg(sat_theta2[~np.isnan(sat_theta2)]))
        plt.axvline(tmp_min.value, color='red')
        plt.axvline(tmp_max.value, color='red')
        plt.grid(True, 'both')
        plt.xlabel('Degrees [$^o$]')
        plt.ylabel('Number of Vectors')
        plt.legend()

        from scipy.stats import gaussian_kde
        fig = plt.figure('dif_theta', figsize=(9, 6))
        ax1 = fig.add_subplot(111)
        x = (dif_theta.flatten()).to_value(u.deg)

        quantiles, counts = np.unique(x, return_counts=True)
        cumprob = np.cumsum(counts).astype(np.double) / x.size
        n_bins = 100

        density = gaussian_kde(x)
        xs = np.linspace(np.min(x), np.max(x), 200)
        # density.covariance_factor = lambda : .25
        # density._compute_covariance()
        ax1.plot(xs, density(xs), '--r', linewidth=1, label="Histogram Density")

        # plot the histogram
        n, bins, pathces =\
            ax1.hist(x,
                     n_bins,
                     density=True,
                     histtype='step',
                     linewidth=1.5,
                     label='PDF')

        ax2 = ax1.twinx()
        # plot the cumulative histogram
        n, bins, patches = ax2.hist(x, n_bins, density=True, histtype='step',
                                    cumulative=True, color='orange', label='CDF',
                                    linewidth=1.5)

        # ind = np.argmin(abs(cumprob-0.1))
        ind = np.argmin(abs(quantiles-5))
        ax2.plot(quantiles, cumprob,
                 '--k', linewidth=1,
                 label='Theoretical CDF')
        ax2.plot(quantiles[ind], cumprob[ind], 'd', color='grey')
        ax2.axhline(cumprob[ind], color='grey', linestyle='--', linewidth=1)
        ax2.axvline(quantiles[ind], color='grey', linestyle='--', linewidth=1)

        lines_1, labels_1 = ax1.get_legend_handles_labels()
        lines_2, labels_2 = ax2.get_legend_handles_labels()
        lines = lines_1 + lines_2
        labels = labels_1 + labels_2
        ax2.legend(lines, labels, loc=4).set_zorder(2)

        # plt.hist((dif_theta2.flatten()).to_value(u.deg),100,histtype='step', label='boresight angle for steering < 56 deg')
        plt.title('Differential Angle between Pointing Vector and Satellite-to-Observer Vector')
        ax1.grid(True, 'both')
        ax1.set_xlabel('Degrees [$^o$]')
        ax1.set_ylabel('PDF')
        ax2.set_ylabel('CDF')

    steering_ph = sat_theta2
    diff_ph = dif_theta2
    print("Steering Phase Min, Max...",
          np.min(np.rad2deg(steering_ph[~np.isnan(steering_ph)])),
          np.max(np.rad2deg(steering_ph[~np.isnan(steering_ph)])))
    print("Diff Phase Min, Max...",
          np.min(np.rad2deg(diff_ph[~np.isnan(diff_ph)])),
          np.max(np.rad2deg(diff_ph[~np.isnan(diff_ph)])))
    return steering_ph, diff_ph


#****************************************************
# EIRP correction with the steering angle 
#****************************************************
def eirp_correct(st_ph=0,
                 unit='lin',
                 plot=False):
    """
    Satellite constellations with steerable beams use EIRP correction as a
    function of the elevation angle as described in ECC report 271

    Parameters
    ----------
    st_ph : array, optional
        Steering angle in radians. The default is 0.

    unit: string
        options are lin or log. Default value is lin

    plot: boolean
        generates a demonstration

    Returns
    -------
    eirp_4kHz :  W in 4kHz

    """

    # taken from ECC Report 271, Fig. 61, p.81 (EIRP values)
    # TODO: Braam changed st_ph from rad to deg
    theta = np.atleast_1d(np.rad2deg(st_ph))
    offsets = np.array([
        0.,  5., 10., 15., 20., 25., 30.,
        35., 40., 45., 50., 53., 55., 57., 60., 62.,
        ]) * u.deg
    gains = np.array([
        -20.3, -20.2, -20.1, -20.0, -19.7, -19.4, -19.0,
        -18.4, -17.7, -17.0, -16.0, -15.1, -15.3, -16., -21.0, -25.,
        ])

    if unit == 'lin':
        gains = 10**(gains/10)

    eirp_4kHz = np.interp(theta, offsets, gains, right=1e-30)

    # eirp_4kHz[np.isnan(theta)] = 10**-1.53 #if the angle is more than 56 deg, 
    # consider the maximum EIRP

    if plot:
        import matplotlib.pyplot as plt
        plt.figure('sat_Ptx', figsize=(9, 6))
        plt.plot(offsets, 10.*np.log10(gains))
        plt.grid(True, 'both')
        plt.title('Satellite EIRP vs. Steering Angle')
        plt.xlabel('Steering Angle [$^o$]')
        plt.ylabel('EIRP [dBW@4 kHz]')

        plt.figure(figsize=(9, 6))
        plt.plot(np.rad2deg(theta), 10.*np.log10(eirp_4kHz))
        plt.grid()
        plt.ylabel('EIRP [dBW@4 kHz]')
        plt.xlabel('Steering Angle [$^o$]')
        plt.title('EIRP')
    return eirp_4kHz * u.W



#*************************************
# single beam Ptx of steerable beam satellite
#*************************************
def single_beam_csv(st_ph: np.array,
                    d_ph : np.array,
                    avd_angle=0*u.deg,
                    plot=0):
    '''
    
    Gets the beam pattern from a simulated beam in CSV format.
    
    
    Parameters
    ----------
    st_ph : np.array
        DESCRIPTION.
    d_ph : np.array
        DESCRIPTION.
    avd_angle : TYPE, optional
        DESCRIPTION. The default is 0*u.deg.
    plot : TYPE, optional
        DESCRIPTION. The default is 0.

    Returns
    -------
    eirp_4khz : TYPE
        DESCRIPTION.

    '''
    
    folder = r'C:/Users/f.divruno/Dropbox (SKA)/Python_codes/Starlink-beam-pattern'
    aux = np.load(folder+'/starlink_beam_interpolated.npz')
    aux.allow_pickle=True
    
    f = np.atleast_1d(aux['f'])[0] #f needs angles in radians
    
    # Avoidance angle
    # Check where differential angle is less than avoidance angle
    d_ph[abs(d_ph) < avd_angle] = np.NaN
    # TODO: Differential angle is already in radians
    dph = np.array(d_ph.to_value(u.rad))
    
    ptx = eirp_correct(st_ph, plot=False) # in linear power units of u.W
    
    #clip dph for values larger than 1.48 radians (interpolation range)
    dph[abs(dph)>=1.48] = 1.48
    
    eirp_4khz = 10**(f(dph)/10) * ptx
        
    return eirp_4khz


#*************************************
# single beam Ptx of steerable beam satellite
#*************************************
def single_beam_eirp(st_ph : np.array,
                    d_ph : np.array,
                    avd_angle=0*u.deg,
                    isotropic=False,
                    plot=0):
    """
    Calculates the gain of a satellite considering the steering angle and
    differential angle (ie the difference between the pointing
    direction of the satellite and the observer on the ground)

    Parameters
    ----------
    st_ph : np.array, optional
        Steering angle in u.deg. 

    d_ph : np.array, optional
        Angular separation of the target from boresight direction, this is \
        used if the target is not in the main pointing direction of the beam.
        The default is np.array((0,0)).

    avd_angle : float, optional
        avoidance angle to passivate the beam, this is to implement protections
        to the SKA.

    isotropic : Boolean, optional
        Flag to set the radiation pattern as an isotropical antenna (sphere).
        The default is False

    test : TYPE, optional
        flag to plot the result. The default is 0.

    Returns
    -------
    eirp_4kHz : array
        EIRP in 4 kHz in W
    """

    # calculate the transmitted power as fucntion of steering angle
    # taken from ECC Report 271, Fig. 61 p. 81
    # This function can be changed for a constellation that has only one
    # fix power output.
    ptx = eirp_correct(st_ph, plot=False) # in linear power units of u.W

    # Avoidance angle
    # Check where differential angle is less than avoidance angle
    d_ph[d_ph < avd_angle] = np.NaN
    # TODO: Differential angle is already in radians
    dth = np.array(d_ph.to(u.rad))

    #calculate the gain relative to isotropic radiator    
    # This function is an assumed value based on publicly available information
    # of some of the current satellite constellations with steerable beams,
    # it is not representative of the real beam of any constellation.
    G = ((np.sinc(abs(dth)*11)*(10**-(abs(dth)*2.2)**2) + 10**-1.4)**2)

    # calcualte EIRP
    eirp_4kHz = G * ptx
    eirp_max = np.nanmax(eirp_4kHz)

    if plot:
        # Braam Otto 
        import matplotlib.pyplot as plt
        N = int(np.random.rand()*dth.shape[1])

        fig = plt.figure('Single beam antenna gain', figsize=(9, 6))
        ax1 = fig.add_subplot(111)
        ax1.plot(np.degrees(dth[:, :]), 10.*np.log10(G[:, :]))
        ax1.grid(True, 'both')
        ax1.set_xlabel('Differential Angle [$^o$]')
        ax1.set_ylabel('Normalised Gain [dB]')
        ax1.set_title('Satellite Gain')

        plt.figure('Single beam_gain_N',figsize=(9, 6))
        try:
            plt.plot(np.rad2deg(dth[:, N][~np.isnan(dth[:, N])]),
                     eirp_4kHz[:, N][~np.isnan(eirp_4kHz[:, N])].to(u.dB(u.W)) -
                     eirp_max.to(u.dB(u.W)))
        except ValueError:
            print("Empty...")

        # FdV
        # plt.plot(np.degrees(dth),
        #          eirp_4kHz[:,0].to(u.dB(u.W)) -
        #          np.nanmax(eirp_4kHz[:,0]).to(u.dB(u.W)))
        plt.axvline(x=10)
        plt.axvline(x=15)
        plt.axvline(x=20)
        plt.grid()
        plt.ylabel('EIRP [dBW@4 kHz]')
        plt.xlabel('Boresight Angle [$^o$]')
        plt.title('EIRP in Steering Angle N=%i' %N)

        plt.figure('single beam_gain_all',figsize=(9, 6))
        plt.plot(np.degrees(dth),
                 eirp_4kHz.to(u.dB(u.W)) - eirp_max.to(u.dB(u.W)))
        plt.grid()
        plt.ylabel('EIRP [dBW@4 kHz]')
        plt.xlabel('Boresight Angle [$^o$]')
        plt.title('Normalised EIRP vs. Boresight Angle')

        plt.figure('single beam_gain_polar',figsize=(9, 6))
        plt.polar(dth,
                  eirp_4kHz[:,0].to(u.dB(u.W)) -
                  np.nanmax(eirp_4kHz[:,0]).to(u.dB(u.W)))
        plt.ylim([-60,0])
        plt.grid('both')

    if isotropic:
        # assumes a radiated power of -20 dB of the maximum
        G = np.ones(dth.shape)*0.01
        eirp_4kHz = G * ptx

    return eirp_4kHz


#*******************************************************
# Function to group the single beams into one satellite
#*******************************************************
def group_beams(spfd,
                Ntrys,
                Nbeams,
                random=True,
                point_matrix=np.array([]),
                plot=False):
    """
    Description:
        To simulate a steerable beam satellite a defined number of beams has to
        be aggregated into one satellite to obtain only one power value
        or an array of values.
        
    Parameters
    ----------
    spfd : numpy.array with dimmensions of MxN where M is the number of ground
        mesh elements (ground terminals) and N is the number of satellites.
        astropy unit: W/m2/Hz

    Ntrys : TYPE
        number of iterations that the beams are randomly assigned to ground
        terminals.

    Nbeams : TYPE
       Number of beams per satellite

    random : TYPE, optional
        DESCRIPTION. The default is True.

    point_matrix : boolean array [Nground terminals,  niters, ntime_steps, nsats]
        [NOT IMPLEMENTED]
        Matrix intended for assignment of satelilte-ground terminal.
        The default is np.array([]).

    Returns
    -------
    None.
    """

    N = Ntrys
    Nmesh = spfd.shape[0]
    Nsats = spfd.shape[1]
    print("Group_Beam...Nmesh, Nsats =", Nmesh, Nsats)
    spfd_agg = np.zeros([N,spfd.shape[1]])*spfd.unit
    print("spfd_agg shape (Ntrys, Nsats) = ", spfd_agg.shape)
    print("***")

    for k in range(N): # generate this combination of beams Ntrys times, this is an arbitrary number 
        print("Try number...", k)
        spfd_try = np.copy(spfd)
        for l in range(Nsats):
            #find the possible ground terminals for the satellite k
            ind = np.where(~np.isnan(spfd_try[:, l]))
            # print("Index of possible ground terminals for satellite k...", ind[0].shape[0])
            # input("///")
            if (ind[0].shape[0])>0:
                #generate Nbeams random indexes
                # FdV
                ind_rand =\
                    np.random.randint(0,
                                      ind[0].shape[0],
                                      Nbeams)

                # Braam Otto
                # ind_rand = rnd.sample(range(ind[0].shape[0]), Nbeams)
                # print("Random Index = ", ind_rand)
                # input("///")

                #aggregate the pfd of the Nbeams
                spfd_agg[k, l] = (np.sum(spfd_try[ind[0][ind_rand], l]))

                # for z in ind_rand:
                #     spfd_try[ind[0][z],:] = np.nan # make nan the ground terminal used

    spfd_agg[spfd_agg==0] = np.NaN

    if plot:
        import matplotlib.pyplot as plt
        plt.figure('Aggregated SPFD')
        plt.hist((spfd_agg.flatten()).value,100,histtype='step')
        plt.title('SPFD of one satellite wiht %d beams  as seen\
                  form observer %s '%(Nbeams,spfd_agg.unit))
        plt.axvline(x = np.nanmean(spfd_agg.flatten().value),color='r')
        plt.axvline(x = -146 - 10*np.log10(4e3) ,color='b')
    return spfd_agg


#**********************
# ConstY eirp function
#**********************

def g_constY(el,
             az,
             unit = 'lin',
             phi_xzo = np.radians(np.linspace(-24,24,16)),
             fix_SLL=1,
             SLL=-23,
             beam_off=[],
             channels = True,
             channel = -1):
    """
    Fixed beams satellite constellation, this is implemented based on the 
    description of the OneWeb beams in ECC report 271
    
    ConstY EIRP in W/Hz or dBW/Hz.
    ZXY: cysgp4 default!
    Satellite motion direction is Z axis, nadir direction is X axis
    along track = xz angle
    across track = xy angle

    XYZ:
    Satellite motion direction is X axis, nadir direction is Z axis
    along track = zx angle
    across track = zy angle

    Parameters
    ----------
    el : TYPE
        elevation angle towards the observer in the along direction

    az : TYPE
        azimuth angle towards the observer in the across direction

    unit: string, optional
        Defines the output unit, the options are 'lin' and 'log'

    phi_zxo : TYPE, optional
        angular position of the beams.
        The default is np.radians(np.linspace(-24,24,16)) 16 beams separated
        by 3 degrees
        for one channel for example:
            CH1: np.radians(np.array((-24,0)))

            CH2: np.radians(np.array((-21,3)))

            CH3: np.radians(np.array((-18,6)))

            CH4: np.radians(np.array((-15,9)))

            CH5: np.radians(np.array((-12,12)))

            CH6: np.radians(np.array((-9,15)))

            CH7: np.radians(np.array((-6,18)))

            CH8: np.radians(np.array((-3,21)))

    fix_SLL : TYPE, optional
        flag to set the Side Lobe Level to a fixed value. The default is 1.

    SLL : TYPE, optional
        Value to set the side lobe level. The default is -23.

    beam_off : list, optional
        list of beams to passivate, this is useful to simulate only one ch.
        The default is [], the fist beam starts at 0 to 15.

    channel: int, optional
        number of channel to generate, the OW beam pattern is defined by
        one channel per 2 beams, separated 8 beams away.
        The default value is -1, this param overrides beam_off
    Returns
    -------
    EIRP in  from the satellite in W/Hz or dBW/Hz
    """

    el = np.radians(el)
    az = np.radians(az)
    Um = np.cos(el)*np.cos(az)
    Vm = np.cos(el)*np.sin(az)
    Km = np.sin(el)
#    # ang_along = np.degrees(np.arctan2(Um,Km))
#    # ang_across = np.degrees(np.arctan2(Vm,Km))
#    
#    # az = ang_across
#    # el = ang_along
    phi_xz = np.arctan2(Km,Um) #np.radians(el) # angle in along track
    phi_xy = np.arctan2(Vm,Um) #az # angle in across track
    # phi_zx = np.radians(phi_zx)
    # phi_zy = np.radians(phi_zy)

    # get the dimensions of the input value
    dim_in = len(np.shape(phi_xz))

    # vector in the across direction with the number of beams as the along
    # direction beams. Defaul is 16 beams
    phi_xyo = np.zeros(len(phi_xzo))

    # offset the angles to the beam angles
    phi_xz1 = phi_xz[...,np.newaxis] - phi_xzo[tuple([np.newaxis]*dim_in)]
    phi_xy1 = phi_xy[...,np.newaxis] - phi_xyo[tuple([np.newaxis]*dim_in)]

    phi_xz1[abs(phi_xz1)>=np.pi/2] = np.pi/2*np.sign(phi_xz1[abs(phi_xz1)>=np.pi/2])


    # use a sinc function with the rms of the two angles:
    a = 0.75
    b = 17
    r = ((phi_xy1*a)**2 + (phi_xz1*b)**2)**0.5
    G = (np.sinc(r))**2

    # do fixed SLL
    if fix_SLL:
        mask0 = (abs(phi_xz1)>np.radians(3.3)) + (abs(phi_xy1)>np.radians(74))
        G[mask0] = 10**(SLL/10)

    #if the number of channel is specified
    if channel  > -1:
        if channel>=8:
            print('Error channel has to be 0 to 7')
            return -1
        beam_off = np.arange(0,16)
        mask = [True]*16
        mask[int(channel)] = False
        mask[int(channel+8)] = False
        beam_off = beam_off[mask]

    # turn off beams
    if len(beam_off)>0:
        G[...,beam_off] = np.zeros([G.shape[0],len(beam_off)])

    if channels:
        # sum the beams with the same channel
        G1 = np.zeros(np.append(G.shape[:-1],8))
        G1[...,0] = G[...,0] + G[...,8]
        G1[...,1] = G[...,1] + G[...,9]
        G1[...,2] = G[...,2] + G[...,10]
        G1[...,3] = G[...,3] + G[...,11]
        G1[...,4] = G[...,4] + G[...,12]
        G1[...,5] = G[...,5] + G[...,13]
        G1[...,6] = G[...,6] + G[...,14]
        G1[...,7] = G[...,7] + G[...,15]
        G = G1
    else:
        # sum all the beams to generate the composite pattern
        G = np.sum(G,axis=-1)

    if unit == 'log':
        return 10*np.log10(G)*u.dB(u.W/u.Hz) - 49*u.dB(u.dimmensionless_scalar)
    else:
        return G * 10**(-4.9)

#%%
if __name__ == '__main__':
    
    font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 22}
    import matplotlib
    matplotlib.rc('font', **font)
    
    #sim the full constellation
    # obs_geo = np.array((21.7,-30,1)) # SKA_mid
    # obs_name = 'SKA-Mid South Africa'
    obs_geo = np.array((-2.306790, 53.235035 , 0.1)) #JBO
    obs_name = 'Jodrell Bank Observatory'

    # beam_type = 'statistical'
    # beam_type = 'statistical_constant'
    # beam_type = 'isotropic'
    beam_type = 'fixed'

    #create constellation
    const_name = 'OneWeb ph1'
    # const_name = 'Starlink ph1'
    # const_name = 'TLE-Geo'
    Const = Constellation(
        const_name,
        obs_name,
        obs_geo,
        beam_type = beam_type)


    # #propagate for a certain epoch
    Const.propagate(iters=1,
                     time_steps = 400,
                     time_step = 5,
                     verbose=True,
                     rand_seed=10)

    

    # #initialize spfd function
    avd_angle = 0 * u.deg
    Const.spfd_init(v)

    #calculate spfd
    Const_spfd = Const.spfd()

    ind = ~(np.isnan(Const_spfd))
    plt.figure('hist pfd')
    plt.hist(10*np.log10(Const_spfd[ind].value)+10*np.log10(4e3),100)

    # plt.figure('spfd vs el')
    # plt.plot(ConstX.topo_pos[ConstX.vis_mask,1],
    #           10*np.log10(ConstX_spfd[ind].value)+10*np.log10(4e3),'.')
    
#%%  Polar plot

    az = Const.topo_pos[Const.vis_mask,0]*np.pi/180
    el = 1 - Const.topo_pos[Const.vis_mask,1]/90
    az0 = Const.topo_pos[0,0,Const.vis_mask[0,0,:],0]*np.pi/180
    el0 = 1 - Const.topo_pos[0,0,Const.vis_mask[0,0,:],1]/90
    
    plt.figure('polar_sats',figsize=[15,15])
    axs = plt.subplot(projection='polar')
    axs.set_theta_zero_location("N")
    axs.set_yticks( ticks = np.linspace(0,1,10),labels=[str(int(i)) for i in np.linspace(90,0,10)])
    # axs.plot(90*np.pi/180,0.8,'.')
    axs.plot(az,el,'.', alpha=0.1)
    axs.plot(az0,el0,'o',color='black')
    axs.set_title('Starlink + OneWeb satellites seen from '+obs_name)
    

#%% Satellite altitude (h)
    h = (np.linalg.norm(Const.ecef_pos,axis=-1)*u.m - const.R_earth).to(u.km)

    az_rate = np.diff(Const.topo_pos[...,0], axis = 1)
    az_rate[abs(az_rate)>180]= np.nan
    el_rate = np.diff(Const.topo_pos[...,1], axis = 1)
    el_rate[abs(el_rate)>180]= np.nan
    phi_rate = np.sqrt(az_rate**2 + el_rate**2)
    
    plt.figure()
    plt.plot(phi_rate[0,:,:])
    plt.plot(az_rate[0,:,1])
    plt.plot(el_rate[0,:,1])
    
#%% plot GEO visibility
    # phi =     np.linspace(0,2*np.pi,1000) * u.rad
    # theta = 81 * u.deg
    # lon_0 = 53 * u.deg
    # lat_0 = 0 * u.deg
    # lon =  np.cos(np.pi*2*phi) * theta 
    # lat =  np.sin(np.pi*2*phi) * theta 
    # plt.figure('Sat_Tracks')
    # plt.plot(lon+lon_0,lat+lat_0,'r')
#%% 
    steering_ph = np.linspace(0,60,10)*u.deg
    boresight_ph = np.linspace(0,90,1000)*u.deg
    s_ph, b_ph = np.meshgrid(steering_ph,boresight_ph)

    g = single_beam_eirp(s_ph, b_ph,avd_angle=0*u.deg,plot=0)

#%%
    plt.figure('plot_sats', figsize=[8,10])
    Const.plot_topo_view(figname='plot_sats')
    plt.grid()
    plt.savefig('../../bin/figs/'+Const.name+'/Topo_view')
    
    
    
#%%
    # ptx = sat1.eirp_func(s_ph)
    # eirp1 = g[np.newaxis] * ptx[:,np.newaxis]

    # plt.figure(figsize=[12,6])
    # plt.plot(steering_ph,10*np.log10(ptx.value))
    # plt.grid()
    # plt.ylabel('EIRP in dBW per 4 kHz')
    # plt.xlabel('steering angle [deg]')
    # plt.title('EIRP')

    # plt.figure(figsize=[12,6])
    # plt.plot(boresight_ph, 10*np.log10(g.value))
    # plt.grid()
    # plt.ylabel('Gain in dB')
    # plt.xlabel('Boresight angle [deg]')
    # plt.title('Gain ')

    # plt.figure(figsize=[12,6])
    # plt.plot(boresight_ph, 10*np.log10((eirp1.T).value))
    # plt.legend(steering_ph)
    # plt.grid()
    # plt.ylabel('total EIRP')
    # plt.xlabel('Boresight angle [deg]')
    # plt.title('total EIRP')

    # dist = 1100e3
    # spread_loss = (1/4/np.pi/dist**2)
    # pfd = eirp1 * spread_loss

    # plt.figure(figsize=[12,6])
    # plt.plot(boresight_ph, 10*np.log10((pfd.T).value))
    # plt.legend(steering_ph)
    # plt.grid()
    # plt.ylabel('PFD in dBW/m2/4kHz')
    # plt.xlabel('Boresight angle [deg]')
    # plt.title('PFD check ')

    # spfd = pfd / 4e3/u.Hz

    # plt.figure(figsize=[12,6])
    # plt.plot(boresight_ph, 10*np.log10((spfd.T).value))
    # plt.legend(steering_ph)
    # plt.grid()
    # plt.ylabel('SPFD in dBW/m2/Hz')
    # plt.xlabel('Boresight angle [deg]')
    # plt.title('SPFD check ')
